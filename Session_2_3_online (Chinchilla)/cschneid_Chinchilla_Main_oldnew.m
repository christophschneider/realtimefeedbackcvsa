% author: Christoph Schneider, CNBI, 2017

% Online post-hoc analysis of CVSA data for experiment CVSA 2.3 Chinchilla
% Start this file to start processing - all the steps are implemented in a
% modular way and can therefore be reordered and/or commented out

% Can be called as function with the argument fileargs
% fileargs is a struct with the fields
% fileargs.pathname: path to folder containing the files
% fileargs.filename: cell array of file names

function cschneid_Chinchilla_Main_oldnew(fileargs)

clearvars -except fileargs;
if ~exist('fileargs','var')
    clear;
    clc;
    close all force;
else
    clearvars -except fileargs;
end

% Load defaults
% -------------------------------------------------------------------------
cschneid_EEG_Chinchilla_Default()
set(0,'DefaultFigureWindowStyle','docked');

% Get file parameters
% -------------------------------------------------------------------------
if ~exist('fileargs','var')
    cd(default.datapath);
    [filename,pathname,~] = uigetfile( ...
        { '*.mat','Clean data files (*.mat)';}, ...
        'MultiSelect','on');
    if iscell(filename)==0  % if only one file
        filename = {filename};
    end
else
    pathname = fileargs.pathname;
    filename = fileargs.filenames;
end

% delete feedbackcolor mat files from the processing stream
x = strfind(filename,'feedbackcolor');
filename(~cellfun(@isempty,x)) = [];
y = strfind(filename,'2online');
filename(~cellfun(@isempty,y)) = [];

%% Process files
% -------------------------------------------------------------------------
for f = 1:length(filename)
    
    load(fullfile(pathname,filename{f}))
    
    % --- load montage file
    load(default.montage);
    
    %--- load artifact- and bad channel markers
    args = struct('mode','read','filename', fullfile(pathname,filename{f}(1:end-4)),'channelnames',{eeg.channelLegend});
    [bad_channels, bad_trials] = cschneid_accessbadfile(args);
    
    % --- load feedback color file (if exists)
    feedbackcolorfilename = [filename{f}(1:end-3),'feedbackcolor.mat'];
    try
        % color = [trialID, color left, color right, lateralization index, good/bad index, elapsed time]
        color = load(fullfile(pathname,feedbackcolorfilename));
        color = color.savedata;
        color = [ones(size(color,1),1)*f,color]; % add run
        color(:,2) = color(:,2)+1; % add 1 to trial number (starts at 0)
        color = [color,zeros(size(color,1),1)]; % add bad trial mask
        color(ismember(color(:,2),bad_trials),8) = 1;
    catch
        color = [];
    end
    
    if eeg.samples(1)==0
        error('First sample is zero. Redo RAW2Clean!')
    end
    
    %--- check for NaNs in the EEG data
    warning backtrace off
    for k = 1:size(eeg.data,1)
        if sum(isnan(eeg.data(k,:))) > 1
            eeg.data(k,isnan(eeg.data(k,:))) = max(max(eeg.data));
            msg = ['channel ',num2str(k),' contains NaNs!'];
            warning(msg)
        end
    end
    warning backtrace on
    
    % --- detect early reaction
    starttriggers = eeg.triggerLegend{3}(~cellfun(@isempty,regexp(eeg.triggerLegend{1},'trial_start')));
    cuetriggers = eeg.triggerLegend{3}(~cellfun(@isempty,regexp(eeg.triggerLegend{1},'cue')));
    targettriggers = eeg.triggerLegend{3}(~cellfun(@isempty,regexp(eeg.triggerLegend{1},'target')));
    
    wrong_reaction = [];
    catch_trials = [];
    x = 0;
    for i = 1:size(eeg.triggers,2)-1
        if ismember(eeg.triggers(1,i),cuetriggers)
            x = x + 1;
            if eeg.triggers(1,i+1) == eeg.triggerLegend{3}(~cellfun(@isempty,regexp(eeg.triggerLegend{1},'reaction_wrong')))
                wrong_reaction = [wrong_reaction,x];
            end
            if ismember(eeg.triggers(1,i+1),targettriggers) && (mod(eeg.triggers(1,i+1),10) ~= eeg.triggers(1,i))
                catch_trials = [catch_trials,x];
            end
        end
        if ismember(eeg.triggers(1,i),targettriggers)
            if eeg.triggers(1,i+1) ~= eeg.triggerLegend{3}(~cellfun(@isempty,regexp(eeg.triggerLegend{1},'reaction_correct')))
                wrong_reaction = [wrong_reaction,x];
            end
        end
    end
    bad_trials = union(bad_trials,unique(wrong_reaction)); % consider such trials as bad
    
    % --- Build artifact structure
    trialnr = eeg.triggers(1,:).*0;
    x = 0;
    for i = 1:size(eeg.triggers,2)-1
        if ismember(eeg.triggers(1,i),starttriggers)
            x = x + 1;
        end
        trialnr(i) = x;
        if ismember(trialnr(i),bad_trials)
            eeg.triggers(3,i) = 1;
        else
            eeg.triggers(3,i) = 0;
        end
        if ismember(trialnr(i),catch_trials)
            eeg.triggers(4,i) = 1;
        else
            eeg.triggers(4,i) = 0;
        end
    end
    
    % --- filter EEG data temporally
    args = struct('struct',eeg,'type','butterworth 4','causal','no','band',[1,40]);
    [eeg_tfiltered] = cschneid_tempfilter(args);
    
    % --- plot channel correlations (to find bad channels)
    tr = eeg.triggers; % pool all trials
    tr(1,tr(1,:)<10) = 61; tr(1,tr(1,:)<40 & tr(1,:)>30) = 62;
    
    args = struct('data',eeg_tfiltered.data,'time',eeg_tfiltered.time,'triggers',tr,...
        'epoch',{{[61,62],[-default.baselinetime,0]}},'samplingRate',...
        eeg_tfiltered.samplingRate,'excludeartifacts','yes');
    [~, span] = cschneid_epoching(args);
    
    corrchan = corr(span');
    figure;
    subplot(1,9, 1:8)
    clims = [0,1];
    imagesc(corrchan,clims);
    daspect([1,1,1])
    title(['Channel cross correlation - run ',num2str(f)])
    colorbar;
    
    subplot(1,9,9)
    corrchanmedian = nanmedian(corrchan)';
    imagesc(corrchanmedian,clims);
    yticks(1:length(corrchanmedian));
    yticklabels(eeg.channelLegend(1:length(corrchanmedian)));
    title('Median')
    
    % --- plot raw data
    x = eeg_tfiltered.data;
    x(isnan(x)) = 0;
    args = struct('data',x,'time',eeg.time,'detrend','yes','labels',{eeg.channelLegend});
    cschneid_plotwaterfall(args);
    
    %     % --- plot epochs (artifact check)
    %     if ~exist('fileargs','var')
    %         args = struct('data',eeg_tfiltered.data,'time',eeg.time,'triggers',eeg.triggers,...
    %             'detrend','yes','starttriggers', [11,12], 'endtriggers', 21, ...
    %             'buffer', [-0.1,0.1],'badchannels',bad_channels,'badtrials',bad_trials,...
    %             'figurenumber',102,'samplingRate',eeg.samplingRate,'labels',{eeg.channelLegend});
    %         bad_trials = cschneid_plotepochs(args);
    %     end
    
    % --- save bad trials in artifacts file
    if ~strcmp(default.interpolate,'yes')
        args = struct('mode','write','filename', fullfile(pathname,filename{f}(1:end-4)),...
            'badchannels',bad_channels,'badtrials',bad_trials,'channelnames',{eeg.channelLegend});
        cschneid_accessbadfile(args);
    end
    
    %--- interpolate bad channels by default
    args = struct('struct',eeg,'badchannels',bad_channels,...
        'maxdistance',default.interpolationdistance,'layoutfile',fullfile(pathname,eeg.layout3D));
    eeg = cschneid_interpolate(args);
    
    %% frequency-domain data
    
    % --- compute PSDs
    eeg.artifact.samples = [];
    args = struct('struct',eeg,'default',default,'type','eeg','path',pathname,'filename',filename{f});
    [PSD] = cschneid_computePSD(args);
    PSD.triggers(3,:) = eeg.triggers(3,:);
    
    % --- plot mean PSD curves (to find bad channels)
    tr = PSD.triggers; % pool all trials
    tr(1,tr(1,:)<10) = 61; tr(1,tr(1,:)<40 & tr(1,:)>30) = 62;
    
    args = struct('data',PSD.data,'time',PSD.time,'triggers',tr,...
        'epoch',{{[61,62],[-default.baselinetime,0]}},'samplingRate',...
        PSD.samplingRate,'excludeartifacts','yes');
    [~, span] = cschneid_epoching(args);
    
    plotx = squeeze(mean(span,2));
    figure; semilogy(plotx');
    legend(PSD.channelLegend);
%     legend(PSD.channels);
    
    % --- overwrite interpolated channels with NaNs (for grand averages)
    if ~strcmp(default.interpolate,'yes')
        for k = 1:size(eeg.data,1)
            if ismember(k, bad_channels)
                eeg.data(k,:) = NaN(1,size(eeg.data(k,:),2));
                PSD.data(k,:) = NaN(1,size(PSD.data(k,:),2));
            end
        end
    end
    
    %% merge runs
    
    if exist('eeg','var')
        if f == 1
            concat_eeg = eeg;
            concat_color = color;
        else
            if ~strcmp(eeg.group,concat_eeg.group)
                error('Cannot merge runs of different experimental groups!')
            end
            newstart = concat_eeg.samples(end);
            concat_eeg.samples = cat(2,concat_eeg.samples,eeg.samples + newstart);
            concat_eeg.time = cat(2,concat_eeg.time,eeg.time + concat_eeg.time(end));
            concat_eeg.data = cat(2,concat_eeg.data,eeg.data);
            t_shift = eeg.triggers;
            t_shift(2,:) = t_shift(2,:) + newstart;
            concat_eeg.triggers = cat(2,concat_eeg.triggers,t_shift);
            concat_color = cat(1,concat_color,color);
        end
    end
    
    if exist('PSD','var')
        if f == 1
            concat_PSD = PSD;
        else
            newstart = length(concat_PSD.time);
            concat_PSD.time = cat(2,concat_PSD.time,PSD.time + concat_PSD.time(end));
            concat_PSD.data = cat(2,concat_PSD.data,PSD.data);
            t_shift = PSD.triggers;
            t_shift(2,:) = t_shift(2,:) + newstart;
            concat_PSD.triggers = cat(2,concat_PSD.triggers,t_shift);
        end
    end
    
end


%% REACTION TIMES

% RT matrix:
%  1  |   2   |     3      |  4  |   5    |       6       |        7
% run | index | background | cue | target | reaction time | trial duration

tID = 0;
run = 0;
RT = [];
Subj = {};
for k = 1:size(concat_eeg.triggers,2)
    if concat_eeg.triggers(1,k) == concat_eeg.triggerLegend{3}...
            (ismember(concat_eeg.triggerLegend{1},'run_start'))
        run = run + 1;
    end
    if (concat_eeg.triggers(1,k) > 10 && concat_eeg.triggers(1,k) < 20) && concat_eeg.triggers(3,k) == 0
        tID = tID + 1;
        RT(tID,1) = run;
        RT(tID,2) = tID;
        RT(tID,4) = concat_eeg.triggers(1,k);
        Subj{tID,1} = concat_eeg.subject;
    elseif (concat_eeg.triggers(1,k) > 10 && concat_eeg.triggers(1,k) < 20) && concat_eeg.triggers(3,k) == 1
        tID = tID + 1;
    end
    if concat_eeg.triggers(1,k) < 4 && concat_eeg.triggers(3,k) == 0
        RT(tID,3) = concat_eeg.triggers(1,k);
    end
    if (concat_eeg.triggers(1,k) > 30 && concat_eeg.triggers(1,k) < 40) && concat_eeg.triggers(3,k) == 0
        RT(tID,5) = concat_eeg.triggers(1,k);
        RT(tID,6) = NaN;
        RT(tID,7) = round((concat_eeg.triggers(2,k) - concat_eeg.triggers(2,k-1)) / concat_eeg.samplingRate);
        RT(tID,8) = round((concat_eeg.triggers(2,k) - concat_eeg.triggers(2,k-1)) / concat_eeg.samplingRate, 2);
    end
    if (concat_eeg.triggers(1,k) == concat_eeg.triggerLegend{3}...
            (ismember(concat_eeg.triggerLegend{1},'reaction_correct')))...
            && (concat_eeg.triggers(1,k-1) > 30 && concat_eeg.triggers(1,k-1) < 40)...
            && concat_eeg.triggers(3,k) == 0
        reactiontime = (concat_eeg.triggers(2,k) - concat_eeg.triggers(2,k-1)) / concat_eeg.samplingRate;
        RT(tID,6) = reactiontime;
    end
    
end
RT(sum(RT,2)==0,:) = [];

figure;
subplot(1,5,1:4); boxplot(RT(:,6),RT(:,7));
ylim([0 1]); grid on;
title('Reaction time for trials')
subplot(1,5,5); boxplot(RT(:,6));
ylim([0 1]); grid on;
title('Mean reaction time')

% ANOVA:
anov_rt = RT(:,6); % reaction time, dependent variable
anov_f1 = RT(:,3); % side | left/right/center
anov_f2 = (RT(:,4) == 11) | (RT(:,4) == 13) ; % background | bgw/wgb
varnames = {'Side';'Background'};
anovan(anov_rt,{anov_f1, anov_f2},3,3,varnames);

%% EXTRACT ALPHA PEAK
electrodes = default.electrodes;

% ONLY during task
tr = PSD.triggers; % pool all trials
tr(1,tr(1,:)<10) = 61; tr(1,tr(1,:)<40 & tr(1,:)>30) = 62;

args = struct('data',PSD.data,'time',PSD.time,'triggers',tr,...
    'epoch',{{[61,62],[-default.baselinetime,0]}},'samplingRate',...
    PSD.samplingRate,'excludeartifacts','yes');
[~, concatSpan] = cschneid_epoching(args);

% find peak frequency
mx = log(squeeze(mean(concatSpan(electrodes,:,2:end),2)));

figure; surf(mx,'FaceColor','interp');
title(['PSD over eyes-closed period, channels ',num2str(electrodes(1)),...
    ' to ',num2str(electrodes(end))]);
xlabel('Frequency (Hz)'); ylabel('channels');

[pks,locs] = findpeaks(nanmean(mx,1));
maxalphafreq = 0;
limit = -inf;
for k = 1:length(locs)
    if locs(k) > 8 && locs(k) < 14
        if pks(k) > limit
            maxalphafreq = locs(k);
            limit = pks(k);
        end
    end
end

% find maximally varying alpha frequency
mxvar = nanvar(squeeze(mean(concatSpan(electrodes,:,2:end),2)));
[pks,locs] = findpeaks(mxvar);
maxvaralphafreq = 0;
limit = -inf;
for k = 1:length(locs)
    if locs(k) > 8 && locs(k) < 14
        if pks(k) > limit
            maxvaralphafreq = locs(k);
            limit = pks(k);
        end
    end
end

disp(['Eyes closed period: alpha peak = ',num2str(maxalphafreq),...
    ' Hz, max variation alpha = ',num2str(maxvaralphafreq),' Hz.'])

%% ROIs

roiL = {'P7','P5','PO7'};
roiR = {'P8','P6','PO8'};
% roiL = {'P7','P5','PO7','P3','PO3','O1'};
% roiR = {'P8','P6','PO8','P4','PO4','O2'};
chRoiL = find(ismember(concat_PSD.channelLegend,roiL));
chRoiR = find(ismember(concat_PSD.channelLegend,roiR));

ALI = []; % Alpha Lateralization Index (alpha-LI)
for f = default.alphaband
    AroiR(f,:) = squeeze(nanmean(concat_PSD.data(chRoiR,:,f+1),1));
    AroiL(f,:) = squeeze(nanmean(concat_PSD.data(chRoiL,:,f+1),1));
    PO7(f,:) = squeeze(nanmean(concat_PSD.data(chRoiR(end),:,f+1),1));
    PO8(f,:) = squeeze(nanmean(concat_PSD.data(chRoiL(end),:,f+1),1));
%     AroiLR = (AroiR + AroiL)./2;
    ALI(f,:) = (AroiR(f,:) - AroiL(f,:)); % ./ AroiLR; CAVE: if division, baselining is more complicated
    PLI(f,:) = PO7(f,:) - PO8(f,:);
end
ALIx = mean(ALI(default.alphaband,:),1);
PLIx = mean(PLI(default.alphaband,:),1);

% --- extract epochs of alpha-LI
args = struct('data',ALIx,'time',concat_PSD.time,'triggers',concat_PSD.triggers,...
    'epoch',{{[1,31],[-default.baselinetime,0]}},'samplingRate',concat_PSD.samplingRate,'excludeartifacts','yes');
[epochsALIxL, ~] = cschneid_epoching(args);

args = struct('data',ALIx,'time',concat_PSD.time,'triggers',concat_PSD.triggers,...
    'epoch',{{[2,32],[-default.baselinetime,0]}},'samplingRate',concat_PSD.samplingRate,'excludeartifacts','yes');
[epochsALIxR, ~] = cschneid_epoching(args);

args = struct('data',PLIx,'time',concat_PSD.time,'triggers',concat_PSD.triggers,...
    'epoch',{{[1,31],[-default.baselinetime,0]}},'samplingRate',concat_PSD.samplingRate,'excludeartifacts','yes');
[epochsPLIxL, ~] = cschneid_epoching(args);

args = struct('data',PLIx,'time',concat_PSD.time,'triggers',concat_PSD.triggers,...
    'epoch',{{[2,32],[-default.baselinetime,0]}},'samplingRate',concat_PSD.samplingRate,'excludeartifacts','yes');
[epochsPLIxR, ~] = cschneid_epoching(args);

maxepochlength = max([max(cellfun(@(C) size(C,2), epochsALIxR)),...
    max(cellfun(@(C) size(C,2), epochsALIxL))]);

clear epochsALIxbslR epochsALIxbslL
paddingvec = NaN(1,maxepochlength);

epochsALIxbslR_padded = [];
for k = 1:size(epochsALIxR,2)
    if strcmp(default.baseline,'yes')
        bsl = mean(epochsALIxR{k}(1:round((concat_PSD.samplingRate)*default.baselinetime)));
        bsl(isnan(bsl)) = 0;
    else
        bsl = 0;
    end
    epochsALIxbslR{k} = epochsALIxR{k}-bsl;
    pdv = paddingvec;
    pdv(1:length(epochsALIxbslR{k})) = epochsALIxbslR{k};
    epochsALIxbslR_padded(k,:) = pdv;
end

epochsALIxbslL_padded = [];
for k = 1:size(epochsALIxL,2)
    if strcmp(default.baseline,'yes')
        bsl = mean(epochsALIxL{k}(1:round((concat_PSD.samplingRate)*default.baselinetime)));
        bsl(isnan(bsl)) = 0;
    else
        bsl = 0;
    end
    epochsALIxbslL{k} = epochsALIxL{k}-bsl;
    pdv = paddingvec;
    pdv(1:length(epochsALIxbslL{k})) = epochsALIxbslL{k};
    epochsALIxbslL_padded(k,:) = pdv;
end

maxtrialsamples = round((default.maxtriallength + default.baselinetime) * concat_PSD.samplingRate);
ALIxRmeans = nanmean(epochsALIxbslR_padded(:,1:maxtrialsamples),1);
ALIxLmedians = nanmean(epochsALIxbslL_padded(:,1:maxtrialsamples),1);
ALIxRmedians = nanmean(epochsALIxbslR_padded(:,1:maxtrialsamples),1);
ALIxLmeans = nanmean(epochsALIxbslL_padded(:,1:maxtrialsamples),1);
ALIxRstd = nanstd(epochsALIxbslR_padded(:,1:maxtrialsamples),1);
ALIxLstd = nanstd(epochsALIxbslL_padded(:,1:maxtrialsamples),1);
LItime = linspace(-default.baselinetime,default.maxtriallength,maxtrialsamples);

figure(333); clf;
boundedline(LItime, ALIxRmeans, ALIxRstd, '-b', LItime, ALIxLmeans, ALIxLstd, '-r', 'alpha');
cschneid_vline(0, 'k', '-', 'cue')
grid on;
title('Lateralization: Grand averages and standard error of the means');
legend({'right trials', 'left trials'})


ceilfix = @(x)ceil(abs(x)).*sign(x);
mean(nanmean(ceilfix(epochsALIxbslR_padded),2))
mean(nanmean(ceilfix(epochsALIxbslL_padded),2))
ALIclsf = cat(1,nanmean(sign(epochsALIxbslR_padded)./2+0.5,2),1-nanmean(sign(epochsALIxbslL_padded)./2+0.5,2));

FBright = epochsALIxbslR_padded;
FBright(FBright>1) = 1;
FBright(FBright<-1) = -1;
FBleft = -epochsALIxbslL_padded;
FBleft(FBleft>1) = 1;
FBleft(FBright<-1) = -1;
FBclsf = cat(1,nanmean(FBright./2+0.5,2),nanmean(FBleft./2+0.5,2));

%%
% % --- plot time average (ERPs)
% for c = 1:size(concat_eeg.data,1)
%     args = struct('data',concat_eeg.data,'channelLabels',{concat_eeg.channelLegend},'triggers',concat_eeg.triggers,...
%         'zero',[1,2,3],'limits',[-1, 1],'type','mean+std','samplingrate',concat_eeg.samplingRate,...
%         'channels',c,'figurenumber',101, 'baseline',[0,0],'excludeartifacts','yes');
%     cschneid_average(args);
% %     waitforbuttonpress
% end
%
% % --- plot frequency average
% for c = 1:size(concat_eeg.data,1)
%     args = struct('data',squeeze(abs(mean(concat_PSD.data(:,:,8:12),3))),...
%         'channelLabels',{concat_PSD.channels},'triggers',concat_PSD.triggers,'zero',[1],...
%         'limits',[-2, 5],'type','mean+std','samplingrate',concat_PSD.samplingRate,'channels',c,...
%         'figurenumber',101, 'baseline',[-1,0],'excludeartifacts','yes');
%     cschneid_average(args);
% %     waitforbuttonpress
% end

% % --- plot Hilbert transformed average
% for i = skipChanIdx
%     args = struct('data',concat_HT.data,...
%         'channelLabels',{concat_HT.channels},'triggers',concat_HT.triggers,'zero',11,...
%         'limits',[-2, 10],'type','mean+std','samplingrate',concat_HT.samplingRate,'channels',i,...
%         'figurenumber',101,'excludeartifacts','yes'); %'baseline',[-2,0] [-2, 6]
%     cschneid_average(args);
%     waitforbuttonpress
% end

%% Extract and baseline epochs

% --- epoch data
args = struct('data',concat_PSD.data,'time',concat_PSD.time,'triggers',concat_PSD.triggers,...
    'epoch',{{[2,32],[-default.baselinetime,0]}},'samplingRate',concat_PSD.samplingRate,'excludeartifacts','yes');
[epochsPSDR, ~] = cschneid_epoching(args);
args = struct('data',concat_PSD.data,'time',concat_PSD.time,'triggers',concat_PSD.triggers,...
    'epoch',{{[1,31],[-default.baselinetime,0]}},'samplingRate',concat_PSD.samplingRate,'excludeartifacts','yes');
[epochsPSDL, ~] = cschneid_epoching(args);

sR = max(cellfun(@(C) size(C,2), epochsPSDR));
sL = max(cellfun(@(C) size(C,2), epochsPSDL));
maxlength = max(sR,sL);

% --- baseline and average
for k = 1:size(epochsPSDR,2)
    if strcmp(default.baseline,'yes')
        bsl = nanmean(epochsPSDR{k}(:,1:round((concat_PSD.samplingRate)*default.baselinetime),:),2);
        bsl(isnan(bsl)) = 0; % should only affect deleted channels
    else
        bsl = 0;
    end
    l = size(epochsPSDR{k},2);
    epochsPSDR_bsl{k} = epochsPSDR{k} - repmat(bsl,[1,l,1]);
    mat = NaN(size(epochsPSDR{k},1),maxlength,size(epochsPSDR{k},3));
    mat(:,1:size(epochsPSDR{k},2),:) = epochsPSDR_bsl{k};
    epochsPSDR_padded(k,:,:,:) = mat;
end
for k = 1:size(epochsPSDL,2)
    if strcmp(default.baseline,'yes')
        bsl = nanmean(epochsPSDL{k}(:,1:round((concat_PSD.samplingRate)*default.baselinetime),:),2);
        bsl(isnan(bsl)) = 0; % should only affect deleted channels
    else
        bsl = 0;
    end
    l = size(epochsPSDL{k},2);
    epochsPSDL_bsl{k} = epochsPSDL{k} - repmat(bsl,[1,l,1]);
    mat = NaN(size(epochsPSDL{k},1),maxlength,size(epochsPSDL{k},3));
    mat(:,1:size(epochsPSDL{k},2),:) = epochsPSDL_bsl{k};
    epochsPSDL_padded(k,:,:,:) = mat;
end

%% baselined epochs
% --- mean over trials
PSDR_collapsed_trials = squeeze(nanmean(epochsPSDR_padded,1));
PSDL_collapsed_trials = squeeze(nanmean(epochsPSDL_padded,1));

% --- mean over time (exclude baseline)
PSDR_collapsed_time = squeeze(nanmean(epochsPSDR_padded(:,:,round((concat_PSD.samplingRate)*default.baselinetime)+1:end,:),3));
PSDL_collapsed_time = squeeze(nanmean(epochsPSDL_padded(:,:,round((concat_PSD.samplingRate)*default.baselinetime)+1:end,:),3));

% --- mean over alpha band
PSDR_collapsed_band = squeeze(nanmean(epochsPSDR_padded(:,:,:,default.alphaband+1),4));
PSDL_collapsed_band = squeeze(nanmean(epochsPSDL_padded(:,:,:,default.alphaband+1),4));

% --- mean over alpha band + trials
PSDR_collapsed_bandtrials = squeeze(nanmean(PSDR_collapsed_band,1));
PSDL_collapsed_bandtrials = squeeze(nanmean(PSDL_collapsed_band,1));

% --- mean over time + alpha band
PSDR_collapsed_timeband = squeeze(nanmean(PSDR_collapsed_time(:,:,default.alphaband+1),3));
PSDL_collapsed_timeband = squeeze(nanmean(PSDL_collapsed_time(:,:,default.alphaband+1),3));

% --- mean over time + alpha band + trials
PSDR_collapsed_timebandtrials = squeeze(nanmean(PSDR_collapsed_timeband,1));
PSDL_collapsed_timebandtrials = squeeze(nanmean(PSDL_collapsed_timeband,1));

% % --- mean over alpha band + time + trials
% PSDR_collapsed_bandtimetrials = squeeze(nanmean(PSDR_collapsed_bandtime,1));
% PSDL_collapsed_bandtimetrials = squeeze(nanmean(PSDL_collapsed_bandtime,1));

% --- mean over individual seconds
sec2sampl = round(concat_PSD.samplingRate * 4);
windowlabels = {[num2str(-default.baselinetime),' - 0'],'0 - 4','4 - 8','8 - 12','12 - 16','16 - 20'};
% baseline

bslperiod = [];
if round(default.baselinetime * concat_PSD.samplingRate) > 0
    bslperiod = 1:round(default.baselinetime * concat_PSD.samplingRate);
    PSDR_collapsed_bandtrialssecond(:,1) = squeeze(nanmean(PSDR_collapsed_bandtrials(:,bslperiod,:),2));
    PSDL_collapsed_bandtrialssecond(:,1) = squeeze(nanmean(PSDL_collapsed_bandtrials(:,bslperiod,:),2));
end

c = 0;
while ((c+1) * sec2sampl) < (size(PSDR_collapsed_bandtrials,2)-length(bslperiod))
    offset = c * sec2sampl;
    vec = 1:sec2sampl;
    period = offset + vec;
    PSDR_collapsed_bandtrialssecond = cat(2,PSDR_collapsed_bandtrialssecond,squeeze(nanmean(PSDR_collapsed_bandtrials(:,period,:),2)));
    PSDL_collapsed_bandtrialssecond = cat(2,PSDL_collapsed_bandtrialssecond,squeeze(nanmean(PSDL_collapsed_bandtrials(:,period,:),2)));
    c = c + 1;
end


% --- create left and right trial means
diff_alphaband_full = (PSDR_collapsed_timebandtrials - PSDL_collapsed_timebandtrials)';
diff_alphaband_win = PSDR_collapsed_bandtrialssecond - PSDL_collapsed_bandtrialssecond;

%% TOPOPLOTS

if strcmp(default.interpolate,'no')
    
    args = struct('data',diff_alphaband_full,'channels',{concat_PSD.channels},...
        'layoutfile',fullfile(pathname,'1005_2D.lay'),'title','Alpha band difference right-left baselined',...
        'timepointlabels',{{'attention period'}},'markers','names');
    cschneid_topoplot(args)
    
    %     args = struct('data',diff_alphaband_sec,'channels',{concat_PSD.channels},...
    %         'layoutfile',fullfile(pathname,'1005_2D.lay'),'title','Alpha band difference right-left',...
    %         'timepointlabels',{{'-1 - 0','0 - 1','1 - 2','2 - 3','3 - 4','4 - 5','5 - 6','6 - 7','7 - 8','8 - 9','9 - 10',...
    %         '10 - 11','11 - 12','12 - 13','13 - 14','14 - 15','15 - 16','16 - 17','17 - 18','18 - 19','19 - 20'}});
    %     cschneid_topoplot(args)
    
    empty = isnan(nanmean(diff_alphaband_win));
    plotmat = diff_alphaband_win(:,~empty);
    args = struct('data',plotmat,'channels',{concat_PSD.channels},...
        'layoutfile',fullfile(pathname,'1005_2D.lay'),'title','Alpha band difference right-left',...
        'timepointlabels',{windowlabels(~empty)});
    cschneid_topoplot(args)
    
end

%% nonbaselined epochs

% --- mean over time (exclude baseline)
% PSDR_collapsed_time = squeeze(nanmean(epochsPSDR,2));
% PSDL_collapsed_time = squeeze(nanmean(epochsPSDL,2));
PSDR_collapsed_time = squeeze(nanmean(epochsPSDR_padded,3));
PSDL_collapsed_time = squeeze(nanmean(epochsPSDL_padded,3));

% --- mean over time + alpha band
PSDR_collapsed_timeband = squeeze(nanmean(PSDR_collapsed_time(:,:,default.alphaband+1),3));
PSDL_collapsed_timeband = squeeze(nanmean(PSDL_collapsed_time(:,:,default.alphaband+1),3));

% --- mean over time + alpha band + trials
PSDR_collapsed_timebandtrials = squeeze(nanmean(PSDR_collapsed_timeband,1));
PSDL_collapsed_timebandtrials = squeeze(nanmean(PSDL_collapsed_timeband,1));

% --- create left and right trial means
diff_alphaband_concat = (PSDR_collapsed_timebandtrials - PSDL_collapsed_timebandtrials)';

%% TOPOPLOTS

if strcmp(default.interpolate,'no')
    
    args = struct('data',diff_alphaband_concat,'channels',{concat_PSD.channels},...
        'layoutfile',fullfile(pathname,'1005_2D.lay'),'title','Alpha band difference right-left',...
        'timepointlabels',{{'attention period'}},'markers','names');
    cschneid_topoplot(args)
    
end

%% Automatic feature selection
% featsR = PSDR_collapsed_time(:,:,1:20);
% featsL = PSDL_collapsed_time(:,:,1:20);
%
% % featsIDX = reshape(1:(size(featsR,1)*size(featsR,2)*size(featsR,3)), [size(featsR,1), size(featsR,2), size(featsR,3)]);
%
% featsR_concat = reshape(featsR,size(featsR,1),[]);
% featsL_concat = reshape(featsL,size(featsL,1),[]);
% featsIDX_concat = repmat(1:size(featsR_concat,2),size(featsR_concat,1),1);
%
% X = [featsR_concat;featsL_concat]';
% group = [featsR_concat(:,1)*0;featsL_concat(:,1)*0+1]';
% [IDX, Z] = rankfeatures(X,group,'Criterion','bhattacharyya');
%
% featscrit_concat = repmat(Z,size(featsR_concat,1),1);
%
% featsR2 = reshape(featsR_concat,[size(featsR,1), size(featsR,2), size(featsR,3)]);
% featsIDX = reshape(featsIDX_concat,[size(featsR,1), size(featsR,2), size(featsR,3)]);
% featscrit = reshape(featscrit_concat,[size(featsR,1), size(featsR,2), size(featsR,3)]);
%
% F = squeeze(featscrit(1,:,:));
% figure; surf(F); axis([1 20 0 size(featsR,2) 0 max(Z)])

% %% Classification
% 
% if strcmp(default.interpolate,'yes')
%     
%     freqz = default.alphaband;
%     classifiertype = 'linear';
%     
%     featuresR.all = [];
%     featuresL.all = [];
%     
%     for k = 1:size(epochsALIxbslR,2)
%         x = epochsALIxbslR{k}(round(concat_PSD.samplingRate*default.baselinetime)+1:end);
%         featuresR.mean(k) = mean(x);
%         featuresR.all = cat(2,featuresR.all,x);
%         featuresR.labels(k) = 1;
%     end
%     for k = 1:size(epochsALIxbslL,2)
%         x = epochsALIxbslL{k}(round(concat_PSD.samplingRate*default.baselinetime)+1:end);
%         featuresL.mean(k) = mean(x);
%         featuresL.all = cat(2,featuresL.all,x);
%         featuresL.labels(k) = 2;
%     end
%     
%     figure;
%     HR = histogram(featuresR.mean,'Normalization','pdf');
%     hold on;
%     HL = histogram(featuresL.mean,'Normalization','pdf');
%     x = min(union(HR.BinEdges,HL.BinEdges)):0.01:max(union(HR.BinEdges,HL.BinEdges));
% %     L = [mean(featuresL.mean),std(featuresL.mean)];
%     pdR = makedist('Normal',mean(featuresR.mean),std(featuresR.mean));
%     pdflineR = pdf(pdR,x);
%     plot(x,pdflineR,'b','LineWidth',4)
%     pdL = makedist('Normal',mean(featuresL.mean),std(featuresL.mean));
%     pdflineL = pdf(pdL,x);
%     plot(x,pdflineL,'r','LineWidth',4)
%     title('Histogram of the means of the LI of each trial')
%     legend({'right','left'})
%     
%     figure;
%     HR = histogram(featuresR.all,'Normalization','pdf');
%     hold on;
%     HL = histogram(featuresL.all,'Normalization','pdf');
%     x = min(union(HR.BinEdges,HL.BinEdges)):0.01:max(union(HR.BinEdges,HL.BinEdges));
% %     L = [mean(featuresL.mean),std(featuresL.mean)];
%     pdR = makedist('Normal',mean(featuresR.all),std(featuresR.all));
%     pdflineR = pdf(pdR,x);
%     plot(x,pdflineR,'b','LineWidth',4)
%     pdL = makedist('Normal',mean(featuresL.all),std(featuresL.all));
%     pdflineL = pdf(pdL,x);
%     plot(x,pdflineL,'r','LineWidth',4)
%     title('Histogram of the samples of the LI of each trial')
%     legend({'right','left'})
%     
%     % -------------------------------------------- mean over trials
%     features = cat(2,featuresR.mean,featuresL.mean);
%     labels = cat(2,featuresR.labels,featuresL.labels);
%     cvp = cvpartition(labels,'Kfold',10);
%     
%     for k = 1:cvp.NumTestSets
%         
%         Mdl = fitcdiscr(features(training(cvp,k))',labels(training(cvp,k)),'DiscrimType','linear','Gamma',0.5);
%         pred_label = predict(Mdl,features(test(cvp,k))');
%         clsf_error.ALI.mean(k) = mean(pred_label ~= labels(test(cvp,k))');
%         
%     end
%     
%     disp(['Mean accuracy [mean over trials] = ',num2str(1-mean(clsf_error.ALI.mean))]);
%     
%     % -------------------------------------------- single samples
%     features = cat(2,featuresR.all,featuresL.all);
%     labels = cat(2,featuresR.all*0+1,featuresL.all*0+2);
%     cvp = cvpartition(labels,'Kfold',10);
%     
%     for k = 1:cvp.NumTestSets
%         
%         Mdl = fitcdiscr(features(training(cvp,k))',labels(training(cvp,k)),'DiscrimType','linear','Gamma',0.5);
%         pred_label = predict(Mdl,features(test(cvp,k))');
%         clsf_error.ALI.sample(k) = mean(pred_label ~= labels(test(cvp,k))');
%         
%     end
%     
%     disp(['Mean accuracy [single sample] = ',num2str(1-mean(clsf_error.ALI.sample))]);
%     
%     % --------------------------------------------
%     featuresR.all = [];
%     featuresL.all = [];
%     
%     for k = 1:size(epochsPLIxR,2)
%         x = epochsPLIxR{k}(round(concat_PSD.samplingRate*default.baselinetime)+1:end);
%         featuresR.mean(k) = mean(x);
%         featuresR.all = cat(2,featuresR.all,x);
%         featuresR.labels(k) = 1;
%     end
%     for k = 1:size(epochsPLIxL,2)
%         x = epochsPLIxL{k}(round(concat_PSD.samplingRate*default.baselinetime)+1:end);
%         featuresL.mean(k) = mean(x);
%         featuresL.all = cat(2,featuresL.all,x);
%         featuresL.labels(k) = 2;
%     end
%     
%     % -------------------------------------------- mean over trials
%     features = cat(2,featuresR.mean,featuresL.mean);
%     labels = cat(2,featuresR.labels,featuresL.labels);
%     cvp = cvpartition(labels,'Kfold',10);
%     
%     for k = 1:cvp.NumTestSets
%         
%         Mdl = fitcdiscr(features(training(cvp,k))',labels(training(cvp,k)),'DiscrimType','linear','Gamma',0.5);
%         pred_label = predict(Mdl,features(test(cvp,k))');
%         clsf_error.PLI.mean(k) = mean(pred_label ~= labels(test(cvp,k))');
%         
%     end
%     
%     disp(['Mean accuracy [mean over trials] = ',num2str(1-mean(clsf_error.PLI.mean))]);
%     
%     % -------------------------------------------- single samples
%     features = cat(2,featuresR.all,featuresL.all);
%     labels = cat(2,featuresR.all*0+1,featuresL.all*0+2);
%     cvp = cvpartition(labels,'Kfold',10);
%     
%     for k = 1:cvp.NumTestSets
%         
%         Mdl = fitcdiscr(features(training(cvp,k))',labels(training(cvp,k)),'DiscrimType','linear','Gamma',0.5);
%         pred_label = predict(Mdl,features(test(cvp,k))');
%         clsf_error.PLI.sample(k) = mean(pred_label ~= labels(test(cvp,k))');
%         
%     end
%     
%     disp(['Mean accuracy [single sample] = ',num2str(1-mean(clsf_error.PLI.sample))]);
%     
% %     % --------------------------------------------
% %     
% %     epochs = cat(2,epochsPSDL_bsl,epochsPSDR_bsl);
% %     epochs = cellfun(@(x) mean(x,2),epochs,'UniformOutput',false);
% %     labels = cat(2,zeros(1,size(epochsPSDL_bsl,2)),ones(1,size(epochsPSDR_bsl,2)));
% %     cvp = cvpartition(labels,'Kfold',10);
% %     
% %     for k = 1:cvp.NumTestSets
% %         % --- training
% %         tr_epochs = epochs(training(cvp,k));
% %         tr_labels = labels(training(cvp,k));
% %         tr_samples = [];
% %         tr_samplelabels = [];
% %         
% %         for c = 1:size(tr_epochs,2)
% %             tr_samples = cat(2,tr_samples,tr_epochs{c});
% %             tr_samplelabels = cat(2,tr_samplelabels,...
% %                 ones(1,size(tr_epochs{c},2)).*tr_labels(c));
% %         end
% %         tr_samples = tr_samples(:,:,ismember(concat_PSD.frequency,freqz));
% % %         % --- detect outlier trials and don't use for model building 
% % %         NOT WORKING!!!!! (decreased test accuracy)
% % %         vv = [];
% % %         for ch = 1:size(tr_samples,1)
% % %             m = squeeze(tr_samples(ch,:,:));
% % %             for f = 1:size(tr_samples,3)
% % %                 v = find(abs(zscore(m(tr_samplelabels==1,f))>3));
% % %                 vv = [vv,v'];
% % %             end
% % %         end
% % %         [a,b]=hist(vv,unique(vv));
% % %         trialDelete = b((a/size(tr_samples,3))>1);
% % %         tr_samples(:,trialDelete,:) = [];
% % %         tr_samplelabels(trialDelete) = [];
% % %         % ---
% %         tr_features = permute(tr_samples,[1,3,2]);
% %         tr_features = reshape(tr_features,[],size(tr_features,3));
% %         % format: ch1-f1, ch2-f1, ch3-f1 ...
% %         fun = @(xT,yT,xt,yt) length(yt)*(cschneid_classerror(yt,...
% %             predict(fitcdiscr(xT,yT,'discrimtype',classifiertype,'Prior','uniform'),xt)));
% %         opt = statset('Display','iter','MaxIter',100,'TolFun',1e-02,'UseParallel',true);
% %         [sel,~] = sequentialfs(fun,tr_features',tr_samplelabels','cv',10,'options',opt);
% %         Mdl = fitcdiscr(tr_features(sel,:)',tr_samplelabels,'Prior','uniform',...
% %             'DiscrimType','linear','Gamma',0);
% %     
% %         % --- testing
% %         te_epochs = epochs(test(cvp,k));
% %         te_labels = labels(test(cvp,k));
% %         te_samples = [];
% %         te_samplelabels = [];
% %         for c = 1:size(te_epochs,2)
% %             te_samples = cat(2,te_samples,te_epochs{c});
% %             te_samplelabels = cat(2,te_samplelabels,...
% %                 ones(1,size(te_epochs{c},2)).*te_labels(c));
% %         end
% %         te_samples = te_samples(:,:,ismember(concat_PSD.frequency,freqz));
% %         te_features = permute(te_samples,[1,3,2]);
% %         te_features = reshape(te_features,[],size(te_features,3));
% %     
% %         te_labels_pred = predict(Mdl, te_features(sel,:)')';
% %         acc(k) = 1 - cschneid_classerror(te_samplelabels,te_labels_pred)
% %     end
% %     
% end

%% Feedback on screen (or not, if sham)

config.updaterate = 16;
config.framesize = eeg.samplingRate/config.updaterate;
config.framespan = 1:config.framesize;
% config taken with no changes from the online experiment
config.eegbufferlength = 1; % in seconds
config.baselinebufferlength = 1; % in seconds

config.numchannels = 64;
config.eegchannels = 1:62;
config.refchannels = 63:64;

% config.clusterL = [44,45,53]; % P7, P5, PO7
% config.clusterR = [51,52,57]; % P8, P6, PO8

config.clusterL = 53; % PO7
config.clusterR = 57; % PO8

config.PSDwindow = 0.5; % in seconds
config.PSDoverlap = 0.9; % has no effect (just one window)
config.PSDbins = 8:14;

config.smoothalpha = 0.1;	% parameter for exponential smoothing, weight of new sample

config.baselinetrigger = [11,12,13,14]; %[6,7,8,9];

config.side.left = -1;
config.side.right = 1;

% Load mapping parameters and replay LI from file
load(fullfile(pathname,[concat_eeg.subject,'.2online.mat']));

from_offline.map_mean = 0;
from_offline.map_halfspan = 2;

% Initialize the final output
LI_smoothed = 0;
% LI_replay = 0;
% t_start = now * 100000;
event = 0;
bsl = 0;
frame = 0;
buffer.eeg = NaN(config.eegchannels(end),config.eegbufferlength*concat_eeg.samplingRate);
buffer.bsl = NaN(config.updaterate * 2 * config.baselinebufferlength,1);
b = [];

% Calculate fixed parameters
psd_window = concat_eeg.samplingRate * config.PSDwindow;
psd_overlap = floor(psd_window * config.PSDoverlap);

while((frame + 1)*config.framesize < size(concat_eeg.data,2))
    
    frame = frame + 1;
    currentframesamples = config.framespan + ((frame-1)*config.framesize);
%     if sum(ismember(concat_eeg.triggers(2,:),currentframesamples))>0
%         concat_eeg.triggers(1,ismember(concat_eeg.triggers(2,:),currentframesamples)>0)
%     end
%     if length(concat_eeg.triggers(1,ismember(concat_eeg.triggers(2,:),currentframesamples)>0))>1
%         disp('wait...')
%     end
    if sum(ismember(concat_eeg.triggers(1,ismember(concat_eeg.triggers(2,:),currentframesamples)>0),91))>0
        buffer.eeg = NaN(config.eegchannels(end),config.eegbufferlength*concat_eeg.samplingRate);
        buffer.bsl = NaN(config.updaterate * 2 * config.baselinebufferlength,1);
    end
    ndf.frame = concat_eeg.data(:,currentframesamples);
    
    % Handle TOBI iD communication/triggers
    a = find(ismember(concat_eeg.triggers(2,:),currentframesamples));
    if ~isempty(a)
        b = [b,a];
        event = concat_eeg.triggers(1,a);
        trig(frame) = event(1);
    else
        trig(frame) = 0;
    end
    
    % Add newest samples to ringbuffer
    buffer.eeg = [buffer.eeg(:,config.framespan(end)+1:end),ndf.frame];
    savebuffereeg(:,:,frame) = buffer.eeg;
    
    % (1) Referencing -> already done above
    %     ref = mean(buffer.eeg(:,config.refchannels),2)';
    %     eeg = buffer.eeg(:,config.eegchannels)';
    %     eeg_ref = eeg - repmat(ref,size(eeg,1),1);
    eeg_ref = buffer.eeg;
    
    % (2) PSDs for clusters
    % --- right
    counter = 1;
    for c = config.clusterR
        [~,~,~,psdRvec(counter,:)]= spectrogram(eeg_ref(c,end-psd_window+1:end),psd_window,...
            psd_overlap,config.PSDbins,concat_eeg.samplingRate);
%         saveRpsd(c,frame,:) = psdRvec(counter,:);
        counter = counter + 1;
    end
    psdR = nanmean(psdRvec,1);
    % --- left
    counter = 1;
    for c = config.clusterL
        [~,~,~,psdLvec(counter,:)]= spectrogram(eeg_ref(c,end-psd_window+1:end),psd_window,...
            psd_overlap,config.PSDbins,concat_eeg.samplingRate);
        counter = counter + 1;
    end
    psdL = nanmean(psdLvec,1);
    
    % (3) Average over full log-alpha band
    avg_psdL = mean(psdL);
    avg_psdR = mean(psdR);
    
    % (4) Compute lateralization index LI
%     LI_now = avg_psdR - avg_psdL;
    LI_now = randn;
    
    % (5) Smooth LI
%     LI_smoothed = LI_now;
    if ~isnan(LI_now)
        if isnan(LI_smoothed)
            LI_smoothed = LI_now;
        else
            LI_smoothed = LI_smoothed * (1-config.smoothalpha) + LI_now * config.smoothalpha;
        end
    else
        LI_smoothed = LI_now;
    end
    
    % (6) Update baseline
    if ismember(event,config.baselinetrigger)
        buffer.bsl = [buffer.bsl(2:end);LI_smoothed];
        bsl = nanmean(buffer.bsl);
    end
    
    % (7) baseline LI
    LI_baselined = LI_smoothed - bsl;
%     LI_baselined = LI_smoothed;
    
    %     % (8) create replay LI for unattended side
    %     t_now = now * 100000;
    %     timepoint_feedback = mod(t_now - t_start,from_offline.LI_time(end));
    %     [~, index] = min(abs(from_offline.LI_time - timepoint_feedback));
    %     LI_replay_new = from_offline.LI(index);
    
    %     % (9) Smooth LI
    %     if ~isnan(LI_now)
    %         LI_replay = LI_replay * (1-config.smoothalpha) + LI_replay_new * config.smoothalpha;
    %     end
    
    % (10) map LI on scale from -1 to 1
    LI_mapped_attended = (LI_baselined - from_offline.map_mean)/from_offline.map_halfspan;
    LI_mapped_whatshould = (LI_smoothed - from_offline.map_mean)/from_offline.map_halfspan;
%     LI_mapped_attended = LI_baselined;
    LI_mapped_attended(LI_mapped_attended>1) = 1;
    LI_mapped_attended(LI_mapped_attended<-1) = -1;
    LI_mapped_whatshould(LI_mapped_whatshould>1) = 1;
    LI_mapped_whatshould(LI_mapped_whatshould<-1) = -1;
    %     LI_mapped_unattended = (LI_replay - from_offline.map_mean)/from_offline.map_halfspan;
    %     LI_mapped_unattended(LI_mapped_unattended>1) = 1;
    %     LI_mapped_unattended(LI_mapped_unattended<-1) = -1;
    
    %     % Handle sync TOBI iC communication
    %     if(tic_isattached(IC) == true)
    %         icmessage_setvalue(icm, 'CVSA', '1', LI_mapped_attended);
    %         icmessage_setvalue(icm, 'CVSA', '2', LI_mapped_unattended);
    %         icmessage_setvalue(icm, 'CVSA', '3', from_offline.meanreactiontime);
    %         icmessage_setvalue(icm, 'CVSA', '4', config.side.left);
    %         icmessage_setvalue(icm, 'CVSA', '5', config.side.right);
    %         tic_setmessage(IC, ics, ndf.frame.index);
    %     else
    %         tic_attach(IC, ica);
    %     end
    
    LI4onscreen(frame) = LI_baselined;
    LI4onscreenMapped(frame) = LI_mapped_attended;
    LIbetter(frame) = LI_mapped_whatshould;
    
end

%% extract epochs and plot
PSD_LIa = concat_PSD; PSD_LIb = concat_PSD; PSD_LIc = concat_PSD;
PSD_LIa.data = LI4onscreen;
PSD_LIb.data = LI4onscreenMapped;
PSD_LIc.data = LIbetter;

[epochs.LIa] = extractEpochs(PSD_LIa,default.baselinetime);
[epochs.LIb] = extractEpochs(PSD_LIb,default.baselinetime);
[epochs.LIc] = extractEpochs(PSD_LIc,default.baselinetime);


data1 = squeeze(nanmean(nanmean(epochs.LIa.right.matrix,1),2));
data2 = squeeze(nanmean(nanmean(epochs.LIa.left.matrix,1),2));
timevec = ((1:length(data1)) /16 ) - default.baselinetime;
figure;
hold on;
grid on;
plot(timevec,data1,'b')
plot(timevec,data2,'r')
vline(0);

randepochmeans.LImapped.right = squeeze(nanmean(nanmean(epochs.LIb.right.matrix,1),2));
randepochmeans.LImapped.left = squeeze(nanmean(nanmean(epochs.LIb.left.matrix,1),2));
timevec = ((1:length(data1)) /16 ) - default.baselinetime;
figure;
hold on;
grid on;
plot(timevec,randepochmeans.LImapped.right,'b')
plot(timevec,randepochmeans.LImapped.left,'r')
vline(0);

randepochmeans.LIshouldmapped.right = squeeze(nanmean(nanmean(epochs.LIc.right.matrix,1),2));
randepochmeans.LIshouldmapped.left = squeeze(nanmean(nanmean(epochs.LIc.left.matrix,1),2));
timevec = ((1:length(data1)) /16 ) - default.baselinetime;
figure;
hold on;
grid on;
plot(timevec,randepochmeans.LIshouldmapped.right,'b')
plot(timevec,randepochmeans.LIshouldmapped.left,'r')
vline(0);

randepochmeans.time = timevec;
save(['\\sti1files.epfl.ch\cnbi\cschneid\_Experiment_CVSA2\CVSA2.3(Chinchilla)\data_CLEAN\_Group\'...
    ,concat_eeg.sessionID,'.',default.GAsuffix,'.mat'],'randepochmeans','-append')


% %% 
% 
% LI4onscreen(isnan(LI4onscreen)) = [];
% LI4onscreenMapped(isnan(LI4onscreenMapped)) = [];
% 
% figure;
% grid on;
% plot(LI4onscreenMapped);
% hold on;
% for ct = 1:length(trig)
%     if trig(ct) > 0
%         cschneid_vline(ct,'r','-',num2str(trig(ct)))
%     end
% end
% 
% figure;
% tmpA = find(trig==1 | trig ==2); tmpB = find(trig==31 | trig ==32);
% if length(tmpA) == length(tmpB)
%     for k = 1:length(tmpA)
%         marker1 = tmpA(k);
%         marker2 = tmpB(k)-2;
%         lineA = LI4onscreenMapped(marker1:marker2);
%         tmp1 = concat_color(concat_color(:,1)==ceil(k/concat_color(end,2)) &...
%             concat_color(:,2)==k-(ceil(k/concat_color(end,2))-1)*concat_color(end,2),:);
%         lineB = tmp1(:,5)';
%         
%         rsA = interp1(linspace(0,1,length(lineA))*tmp1(end,7),lineA,linspace(0,1,512)*tmp1(end,7),'spline');
%         rsB = interp1(tmp1(:,7),lineB,linspace(0,1,512)*tmp1(end,7),'spline');
%         
%         plot(linspace(0,1,length(lineA))*tmp1(end,7),lineA); hold on;
%         plot(tmp1(:,7),lineB)
%         disp(['correlation: ',num2str(corr(rsA',rsB'))]);
%         corr2lines(k) = corr(rsA',rsB');
%         %         waitforbuttonpress;
%         hold off
%     end
%     corr2lines(logical(tr(3,ismember(tr(1,:),[1,2])))) = []; % delete artifact trials
% else
%     error('Start and end trigger numbers different')
% end
% 
% conversion = concat_eeg.triggers(2,end)/length(trig);
% FBtrig = concat_eeg.triggers;
% FBtrig(2,:) = ceil(concat_eeg.triggers(2,:)/conversion);
% 
% % --- LI in time before trial end
% tr = FBtrig;
% tr(1,ismember(tr(1,:),[31,32])) = 99;
% side = tr(1,ismember(tr(1,:),[1,2])); % 1 = left, 2 = right
% side(logical(tr(3,ismember(tr(1,:),[1,2])))) = []; % delete artifact trials
% sidemultiplier = -sign(side-1.5);
% 
% args = struct('data',LI4onscreenMapped,'time',linspace(1,concat_eeg.time(end),length(trig)),...
%     'triggers',tr,'epoch',{{99,[-0.5,0]}},'samplingRate',...
%     round(eeg_tfiltered.samplingRate/conversion),'excludeartifacts','yes');
% [epochs_LI4onscreenMapped, ~] = cschneid_epoching(args);
% 
% for ep = 1:size(epochs_LI4onscreenMapped,2)
%     brob = robustfit(1:length(epochs_LI4onscreenMapped{ep}),epochs_LI4onscreenMapped{ep}*(-sidemultiplier(ep)));
%     linFit.reconstructed(ep) = brob(2);
%     epmeanLI4onscreenMapped(ep) = mean(epochs_LI4onscreenMapped{ep});
%     ependLI4onscreenMapped(ep) = epochs_LI4onscreenMapped{ep}(end);
% end
% 
% 
% % 89890808
% runningLI = nanmean(nanmean(concat_PSD.data(config.clusterR,:,default.alphaband+1),1),3)...
%     - nanmean(nanmean(concat_PSD.data(config.clusterL,:,default.alphaband+1),1),3);
% figure;
% grid on;
% plot(runningLI);
% hold on;
% for ct = 1:length(concat_PSD.triggers)
%     cschneid_vline(concat_PSD.triggers(2,ct),'r','-',num2str(concat_PSD.triggers(1,ct)))
% end
% 
% runningLI(runningLI>1) = 1; runningLI(runningLI<-1) = -1;
% x = interp1(linspace(0,1,length(runningLI))*tmp1(end,7),runningLI,linspace(0,1,30000)*tmp1(end,7),'spline');
% y = interp1(linspace(0,1,length(LI4onscreenMapped))*tmp1(end,7),LI4onscreenMapped,linspace(0,1,30000)*tmp1(end,7),'spline');
% % corr(x(1:end-7)',y(8:end)')
% % figure; plot(x); hold on; plot(y)
% figure; plot(runningLI); hold on; plot(LI4onscreenMapped);
% % corr(runningLI',LI4onscreenMapped')
% [acf,lags,bounds] = autocorr(runningLI,1)
% [acf,lags,bounds] = autocorr(LI4onscreenMapped,1)
% 
% %% Relation reaction time <-> lateralization index / feedback
% 
% if ~isempty(concat_color)
%     % --- recorded color
%     colsummary = [];
%     for r = unique(concat_color(:,1))'
%         colrun = concat_color(concat_color(:,1)==r,2:end);
%         feedback.meanGBindex(r) = mean(colrun(:,4));
%         for t = unique(colrun(:,1))'
%             coltrial = colrun(colrun(:,1)==t,2:end);
%             feedback.meanTrialGBindex(r,t) = mean(colrun(colrun(:,1)==t,4));
%             colsummary = cat(1,colsummary,[r,t+(40*(r-1)),coltrial(end,4:6)]);
%         end
%     end
%     
%     usecol = colsummary(colsummary(:,5)~=1,1:end-1);
%     if sum(usecol(:,2)~=RT(:,2))==0 % check data integrity for trial numbers
%         RTGB = [RT(:,6),usecol(:,3)]; % reaction time / good bad index
%     end
%     a = zscore(RTGB(:,2));
%     b = zscore(RTGB(:,1));
%     figure; scatter(a,b,'filled')
%     hold on;
%     brob = robustfit(a,b);
%     plot(a,brob(1)+brob(2)*a,'g','LineWidth',2)
%     
%     % --- LI in time before trial end
%     tr = concat_eeg.triggers;
%     tr(1,ismember(tr(1,:),[31,32])) = 99;
%     side = tr(1,ismember(tr(1,:),[1,2])); % 1 = left, 2 = right
%     side(logical(tr(3,ismember(tr(1,:),[1,2])))) = []; % delete artifact trials
%     sidemultiplier = -sign(side-1.5);
%     
%     args = struct('data',concat_eeg.data,'time',concat_eeg.time,'triggers',tr,...
%         'epoch',{{99,[-0.5,0]}},'samplingRate',...
%         concat_eeg.samplingRate,'excludeartifacts','yes');
%     [epochs_concat_eeg, ~] = cschneid_epoching(args);
%     
%     for ep = 1:length(epochs_concat_eeg)
%         for ch = 1:size(epochs_concat_eeg{ep},1)
%             if sum(isnan(epochs_concat_eeg{ep}(ch,:))) == 0
%                 pxx = pwelch(epochs_concat_eeg{ep}(ch,:));
%                 pow(ch) = mean(pxx(default.alphaband+1));
%             else
%                 pow(ch) = NaN;
%             end
%         end
%         LI(ep) = nanmean(pow(chRoiR)) - nanmean(pow(chRoiL));
%     end
%     
%     a = zscore(LI.*sidemultiplier);
%     b = zscore(RT(:,6));
%     scatter(a,b,'filled')
%     brobLI = robustfit(a,b);
%     plot(a,brobLI(1)+brobLI(2)*a,'r','LineWidth',2)
%     
%     feedback.triallength = colsummary(:,4);
%     
%     % --- reconstructed color
%     a = zscore(ependLI4onscreenMapped.*(sidemultiplier*-1));
%     b = zscore(RT(:,6));
%     scatter(a,b,'filled')
%     brobreconstructed = robustfit(a,b);
%     plot(a,brobLI(1)+brobLI(2)*a,'c','LineWidth',2)
%     hold off;
%     
%     % --- linear fit GBindex at trial end
%     a = zscore(linFit.reconstructed);
%     b = zscore(RT(:,6));
%     scatter(a,b,'filled')
%     broblinfit = robustfit(a,b);
%     plot(a,brobLI(1)+brobLI(2)*a,'c','LineWidth',2)
%     hold off;
%     
%     feedback.triallength = colsummary(:,4);
%     
% else
%     brob = NaN(1,2);
%     brobLI = NaN(1,2);
%     feedback = [];
% end
% 
% figure;
% title('mean of last half second over reconstructed vs lateralization index')
% stem(zscore(epmeanLI4onscreenMapped));
% hold on; grid on;
% stem(zscore(LI));
% corr(epmeanLI4onscreenMapped',LI')
% 
% figure;
% title('mean of last half second over reconstructed vs recorded last sample')
% stem(epmeanLI4onscreenMapped.*(sidemultiplier*-1));
% hold on; grid on;
% stem(RTGB(:,2));
% corr((epmeanLI4onscreenMapped.*(sidemultiplier*-1))',RTGB(:,2))
% 
% figure;
% title('last sample of reconstructed vs recorded')
% stem(ependLI4onscreenMapped.*(sidemultiplier*-1));
% hold on; grid on;
% stem(RTGB(:,2));
% corr((ependLI4onscreenMapped.*(sidemultiplier*-1))',RTGB(:,2))
% 
% %% Save to mat file for group analysis
% 
% filename = fullfile(default.grouppath,[eeg.sessionID,'.',default.GAsuffix,'.mat']);
% if exist(filename,'file')
%     load(filename)
% else
%     savestruct = [];
% end
% 
% if strcmp(default.interpolate,'yes')
%     savestruct.clsf_err = clsf_error;
% %     savestruct.full_acc = acc;
% else
%     savestruct.subjectID = concat_eeg.subject;
%     savestruct.sessionID = concat_eeg.sessionID;
%     savestruct.layoutfile = fullfile(pathname,eeg.layout2D);
%     savestruct.electrodes = concat_PSD.channels;
%     
%     savestruct.LI.time = LItime;
%     savestruct.LI.meanR = ALIxRmeans;
%     savestruct.LI.meanL = ALIxLmeans;
%     
%     savestruct.topo.span = diff_alphaband_concat;
%     savestruct.topo.span_bsl = diff_alphaband_full;
%     savestruct.topo.windows = diff_alphaband_win;
%     savestruct.topo.t_windows = windowlabels;
%     
%     savestruct.RT = RT;
%     savestruct.group = concat_eeg.group;
%     savestruct.RTGB_slope = brob(2);
%     savestruct.RTLI_slope = brobLI(2);
%     savestruct.RTreconstructed_slope = brobreconstructed(2);
%     savestruct.RTlinfit = broblinfit(2);
%     savestruct.feedback = feedback;
%     
%     savestruct.LIcorr = nanmean(corr2lines);
%     
% end

% save(filename, 'savestruct');

%%
disp('--> end of main script <--')

end

function [epochs] = extractEpochs(datastruct,baselinetime)
% --- extract left, right, and center trials
args = struct('data',datastruct.data,'time',datastruct.time,'triggers',datastruct.triggers,...
    'epoch',{{{1,[31,32]},[-baselinetime,0]}},'samplingRate',datastruct.samplingRate,'excludeartifacts','yes');
[left.cell,~,left.trialnumbers] = cschneid_epoching(args);

args = struct('data',datastruct.data,'time',datastruct.time,'triggers',datastruct.triggers,...
    'epoch',{{{2,[31,32]},[-baselinetime,0]}},'samplingRate',datastruct.samplingRate,'excludeartifacts','yes');
[right.cell,~,right.trialnumbers] = cschneid_epoching(args);

args = struct('data',datastruct.data,'time',datastruct.time,'triggers',datastruct.triggers,...
    'epoch',{{[3,33],[-baselinetime,0]}},'samplingRate',datastruct.samplingRate,'excludeartifacts','yes');
[center.cell,~,center.trialnumbers] = cschneid_epoching(args);

% --- put into common-length matrices
sL = max(cellfun(@(C) size(C,2), left.cell));
sR = max(cellfun(@(C) size(C,2), right.cell));
sC = max(cellfun(@(C) size(C,2), center.cell));
epochs.maxlength = max([sL,sR,sC]);

left.matrix = NaN(size(left.cell,2),size(left.cell{1},1),epochs.maxlength,size(left.cell{1},3));
right.matrix = NaN(size(right.cell,2),size(right.cell{1},1),epochs.maxlength,size(right.cell{1},3));

for ep = 1:size(left.cell,2)
    left.matrix(ep,:,1:size(left.cell{ep},2),:) = left.cell{ep};
end
for ep = 1:size(right.cell,2)
    right.matrix(ep,:,1:size(right.cell{ep},2),:) = right.cell{ep};
end

epochs.left = left;
epochs.right = right;

end
