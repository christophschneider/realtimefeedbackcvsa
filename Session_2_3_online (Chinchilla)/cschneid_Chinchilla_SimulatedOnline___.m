% author: Christoph Schneider, CNBI, 2018

% Simulated online results of CVSA data for experiment CVSA 2.3 Chinchilla
% Start this file to start processing - all the steps are implemented in a
% modular way and can therefore be reordered and/or commented out

% Can be called as function with the argument fileargs
% fileargs is a struct with the fields
% fileargs.pathname: path to folder containing the files
% fileargs.filename: cell array of file names
% fileargs.simdefaults: full path filename to simdefaults file

% Outputs the simulated online classification results

function  [] = cschneid_Chinchilla_SimulatedOnline(fileargs)

close all force;
clearvars -except fileargs;
disp('-------------------------------------------------------------------')
cd('\\sti1files.epfl.ch\cnbi\cschneid\_Experiment_CVSA2\CVSA2.3(Chinchilla)\data_CLEAN');

% Initialize global structs
% -------------------------------------------------------------------------
allruns = struct;
newsavestruct = struct;

% Get file parameters
% -------------------------------------------------------------------------
if ~exist('fileargs','var')
    [filename,pathname,~] = uigetfile( ...
        { '*.mat','Clean data files (*.mat)';}, ...
        'MultiSelect','on');
    if iscell(filename)==0  % if only one file
        filename = {filename};
    end
else
    pathname = fileargs.pathname;
    filename = fileargs.filenames;
end


% delete feedbackcolor mat files from the processing stream
filename(contains(filename,'feedbackcolor')) = [];
filename(contains(filename,'2online')) = [];

% Load defaults
% -------------------------------------------------------------------------
if exist('fileargs','var')
    run(fileargs.simdefaults);
else
%     [filename,pathname,~] = uigetfile( ...
%             { '*.m','simdefault file (*.m)';}, ...
%             'MultiSelect','off');
    fn = filename(contains(filename,'_GA'));
    run(fullfile(pathname,fn{1}));
end
filename(contains(filename,'_GA')) = [];

disp(filename{1})

%% Process files
% -------------------------------------------------------------------------
for f = 1:length(filename)
    
    filecontent = load(fullfile(pathname,filename{f}));
    
    %% Eye tracker preprocessing
    
    if isfield(filecontent,'et')
        args = struct('struct',filecontent.et,'default',default,'plot','no');
        thisrun.et = cschneid_Chinchilla_ET_preprocessing(args);
    end
    
    %% EEG preprocessing
    
    if isfield(filecontent,'eeg')
        args = struct('struct',filecontent.eeg,'default',default,'filename',...
            fullfile(pathname,filename{f}),'plot','no');
        thisrun.eeg = cschneid_Chinchilla_EEG_preprocessing(args);
    end
    
    %% synchronize corrupted trials (blinks/saccades) between ET and EEG
    
    badTrials = union(thisrun.eeg.trialsBad(:),thisrun.et.trialsBad(:));
    thisrun.et.trialsBad = badTrials';
    thisrun.eeg.trialsBad = badTrials';
    
    %% Compute PSDs
    
    if isfield(filecontent,'eeg')
        [pn,fn1,fn2] = fileparts(fullfile(pathname,filename{f}));
        args = struct('struct',thisrun.eeg,'default',args.default,'type','eeg','path',pn,...
            'filename',[fn1,fn2],'save','yes');
        [thisrun.PSD] = cschneid_computePSD(args);
        % --- plot for identifying bad channels/trials
        args = struct('struct',thisrun.PSD,'default',default,'plot','no');
        cschneid_PSD_rawplots(args);
    end
    
    
    %% Merge runs
    
    if isstruct(thisrun)
        args = struct('allruns',allruns,'newrun',thisrun);
        allruns = cschneid_mergeruns(args);
    end
    
end

clearvars -except default allruns newsavestruct;
cprintf([0,0.5,0.2],'Preprocessing finished!\n');

%% ADD GENERAL FIELDS

if isfield(allruns,'et')
    newsavestruct.general.subject = allruns.et.subject;
    newsavestruct.general.date = allruns.et.date;
    newsavestruct.general.sessionID = allruns.et.sessionID;
    newsavestruct.general.ratiotrialsbad = length(allruns.et.trialsBad)/allruns.et.totaltrials;
elseif isfield(allruns,'eeg')
    newsavestruct.general.subject = allruns.eeg.subject;
    newsavestruct.general.date = allruns.eeg.date;
    newsavestruct.general.sessionID = allruns.eeg.sessionID;
    newsavestruct.general.ratiotrialsbad = length(allruns.eet.trialsBad)/allruns.eet.totaltrials;
end

%% EYE TRACKER ANALYSIS

if isfield(allruns,'et')
    if length(allruns.et.trialsBad)/allruns.et.totaltrials < default.toocorrupted
        args = struct('struct',allruns.et,'default',default,'savestruct',newsavestruct,'plot','yes');
        newsavestruct = cschneid_Chinchilla_ET_analysis(args);
    else
        disp('Too many et trials corrupted!')
    end
end

%% PSD ANALYSIS

if isfield(allruns,'PSD')
    if length(allruns.eeg.trialsBad)/allruns.eeg.totaltrials < default.toocorrupted
        args = struct('struct',allruns.PSD,'default',default,'savestruct',newsavestruct,'plot','yes');
        newsavestruct = cschneid_Chinchilla_EEG_analysis(args);
    else
        disp('Too many eeg trials corrupted!')
    end
end

%% Save in file

filename = fullfile(default.grouppath,[newsavestruct.general.sessionID,'.',default.GAsuffix,'.mat']);
if exist(filename,'file')
    load(filename,'savestruct')
    
    f = fieldnames(newsavestruct);
    for i = 1:length(f)
        savestruct.(f{i}) = newsavestruct.(f{i}); %#ok<STRNU>
    end
else
    savestruct = newsavestruct; %#ok<NASGU>
end

save(filename, 'savestruct');

end
