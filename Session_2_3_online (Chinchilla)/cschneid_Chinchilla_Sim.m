% author: Christoph Schneider, CNBI, 2018

% Simulated online results of CVSA data for experiment CVSA 2.3 Chinchilla
% Start this file to start processing - all the steps are implemented in a
% modular way and can therefore be reordered and/or commented out

% Can be called as function with the argument fileargs
% fileargs is a struct with the fields
% fileargs.pathname: path to folder containing the files
% fileargs.filename: cell array of file names

% Outputs a struct with all the concatenated runs of each modality

function  [] = cschneid_Chinchilla_Sim(fileargs)

close all force;
clearvars -except fileargs;
disp('-------------------------------------------------------------------')

% Load defaults
% -------------------------------------------------------------------------
% run(which(fileargs.defaultfile));
cschneid_Chinchilla_Default();
cschneid_Chinchilla_SimDefaults();

% Initialize global structs
% -------------------------------------------------------------------------
allruns = struct;
savestruct = struct;

% Get file parameters
% -------------------------------------------------------------------------
if ~exist('fileargs','var')
    cd(default.datapath);
    [filename,pathname,~] = uigetfile( ...
        { '*.mat','Clean data files (*.mat)';}, ...
        'MultiSelect','on');
    if iscell(filename)==0  % if only one file
        filename = {filename};
    end
else
    pathname = fileargs.pathname;
    filename = fileargs.filenames;
end

% delete feedbackcolor mat files from the processing stream
filename(contains(filename,'feedbackcolor')) = [];
filename(contains(filename,'2online')) = [];

disp(filename{1})

%% Process files
% -------------------------------------------------------------------------
for f = 1:length(filename)
    
    filecontent = load(fullfile(pathname,filename{f}));
    
    %% Eye tracker preprocessing
    
    if isfield(filecontent,'et')
        args = struct('struct',filecontent.et,'default',default,'plot','no');
        thisrun.et = cschneid_ET_preprocessing(args);
    end
    
    %% EEG preprocessing
    
    if isfield(filecontent,'eeg')
        args = struct('struct',filecontent.eeg,'default',default,'filename',...
            fullfile(pathname,filename{f}),'plot','no');
        thisrun.eeg = cschneid_EEG_preprocessing(args);
    end
    
    %% synchronize corrupted trials (blinks/saccades) between ET and EEG
    
    args = struct('run',thisrun,'useclass',default.artifact.consider4rejection);
    [thisrun] = cschneid_ETEEG_marryartifactstructures(args);
%     
%     badTrials = union(thisrun.eeg.trialsBad(:),thisrun.et.trialsBad(:));
%     thisrun.et.trialsBad = badTrials';
%     thisrun.eeg.trialsBad = badTrials';
    
    %% Merge runs
    
    if isstruct(thisrun)
        args = struct('allruns',allruns,'newrun',thisrun);
        allruns = cschneid_mergeruns(args);
    end
    
end

clearvars -except simdefault default allruns savestruct;
cprintf([0,0.5,0.2],'Preprocessing finished!\n');

%% ADD GENERAL FIELDS

if isfield(allruns,'et')
    newsavestruct.general.subject = allruns.et.subject;
    newsavestruct.general.date = allruns.et.date;
    newsavestruct.general.sessionID = allruns.et.sessionID;
%     newsavestruct.general.ratiotrialsbad = length(allruns.et.trialsBad)/allruns.et.totaltrials;
elseif isfield(allruns,'eeg')
    newsavestruct.general.subject = allruns.eeg.subject;
    newsavestruct.general.date = allruns.eeg.date;
    newsavestruct.general.sessionID = allruns.eeg.sessionID;
%     newsavestruct.general.ratiotrialsbad = length(allruns.eet.trialsBad)/allruns.eet.totaltrials;
end

%% ADD SPECIFIC FIELDS

if isfield(allruns,'et')
    newsavestruct.et.samplingrate = allruns.et.samplingRate;
    newsavestruct.et.time = allruns.et.time;
end
if isfield(allruns,'eeg')
    newsavestruct.eeg.samplingrate = allruns.eeg.samplingRate;
end


%% LOAD SAVED CLASSIFIERS

% fn = dir(fullfile(simdefault.fromOfflinePath,['*',newsavestruct.general.subject,'.*']));
% load(fullfile(simdefault.fromOfflinePath,fn.name),'savestruct');
% 
% et.classifier.usefeats = 1;
% et.classifier.model = savestruct.et.pupil.classifier.(simdefault.et.classifier_type).LDA.model;
% 
% eeg.classifier.usefeats = savestruct.PSD.classifier.(simdefault.psd.classifier_type).LDA.usefeats;
% eeg.classifier.model = savestruct.PSD.classifier.(simdefault.psd.classifier_type).LDA.model;

%X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X
% X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X

%% SIMULATE ONLINE EXPERIMENT

is_et = isfield(allruns,'et');
is_eeg = isfield(allruns,'eeg');
if strcmp(simdefault.eeg.signal_type,'psd')
    is_psd = 1;
else
    is_psd = 0;
end

trig_newrun = double(allruns.eeg.triggerLegend{3}(contains(allruns.eeg.triggerLegend{1},'run_start')));
trig_cue = double(allruns.eeg.triggerLegend{3}(contains(allruns.eeg.triggerLegend{1},'cue')));
trig_target = double(allruns.eeg.triggerLegend{3}(contains(allruns.eeg.triggerLegend{1},'target')));

if is_et
    et.buffer = NaN(1,round(simdefault.et.buffer * allruns.et.samplingRate));
    if simdefault.et.baseline > simdefault.et.buffer
        error('ET buffer is too short for requested baseline time');
    end
end
if is_eeg
    eeg.buffer = NaN(size(allruns.eeg.data,1),round(simdefault.eeg.buffer * allruns.eeg.samplingRate));
    if simdefault.eeg.baseline > simdefault.eeg.buffer
        error('EEG buffer is too short for requested baseline time');
    end
end
if is_psd
    PSD.buffer = NaN(length(simdefault.psd.usedchannels),round(simdefault.psd.buffer * simdefault.framerate),length(default.PSDbins));
    if simdefault.psd.baseline > simdefault.psd.buffer
        error('EEG buffer is too short for requested baseline time');
    end
end

frame = 0;
et.in_trial = 0;
eeg.in_trial = 0;

et.trial.counter = 0;
eeg.trial.counter = 0;

%% COMPUTE FIXED PARAMETERS

frameduration = 1 ./ simdefault.framerate;

psd_windowlength = allruns.eeg.samplingRate * default.PSDwindowlengthinseconds;
psd_freqbins = default.PSDbins;
psd_overlap = default.PSDoverlap;
if psd_windowlength > length(eeg.buffer)
    error('Window for PSD computation exceeds the EEG buffer length');
end

if ~isfield(allruns,'et')
    sessionduration  = allruns.et.time(end);
elseif ~isfield(allruns,'eeg')
    sessionduration  =allruns.eeg.time(end);
else
    sessionduration  = min(allruns.et.time(end),allruns.eeg.time(end));
end

et.smoothstretch = round(default.et.pupilsmoothwindow * allruns.et.samplingRate);
numframes = floor(sessionduration / frameduration);

PSD.data = NaN(length(simdefault.psd.usedchannels),numframes,length(psd_freqbins));
et.events = NaN(1,numframes);
eeg.events = NaN(1,numframes);

%% ALLOCATE VARIABLES


%% RUN SIMULATION LOOP

while (frame + 1) * frameduration < sessionduration

    frame = frame + 1;
    current.frametimespan = [(frame - 1) * frameduration, frame * frameduration];
    
    %% update buffer and baselines
    
    % EYE TRACKER
    if is_et
        et.current.framesamples = find(allruns.et.time >= current.frametimespan(1) & ...
            allruns.et.time < current.frametimespan(2));
        et.current_samples = allruns.et.pupilArea(et.current.framesamples);
        
        % restart buffer when new run starts
        if sum(ismember(allruns.et.triggers(1,ismember(allruns.et.triggers(2,:),et.current.framesamples)>0),trig_newrun))>0
            et.buffer = NaN(1,round(simdefault.et.buffer * allruns.et.samplingRate));
        end
        
        % update baseline after cue
        if sum(ismember(allruns.et.triggers(1,ismember(allruns.et.triggers(2,:),et.current.framesamples)>0),trig_cue))>0
            bsl_length = round(simdefault.et.baseline * allruns.et.samplingRate);
            et.baseline = et.buffer((end-bsl_length+1):end);
            et.baseline_smoothed = et.buffer_smoothed((end-bsl_length+1):end);
        end
        
        len = length(et.current_samples);
        et.buffer = [et.buffer((len+1):end),et.current_samples];
        et.buffer_smoothed = movmedian(et.buffer,et.smoothstretch);
    end
    
    % EEG
    if is_eeg
        eeg.current.framesamples = find(allruns.eeg.time >= current.frametimespan(1) & ...
            allruns.eeg.time < current.frametimespan(2));
        eeg.current_samples = allruns.eeg.data(:,eeg.current.framesamples);
        
        % restart buffer when new run starts
        if sum(ismember(allruns.eeg.triggers(1,ismember(allruns.eeg.triggers(2,:),eeg.current.framesamples)>0),trig_newrun))>0
            eeg.buffer = NaN(size(allruns.eeg.data,1),round(simdefault.eeg.buffer * allruns.eeg.samplingRate));
        end
        
        % update baseline after cue
        if sum(ismember(allruns.eeg.triggers(1,ismember(allruns.eeg.triggers(2,:),eeg.current.framesamples)>0),trig_cue))>0
            bsl_length = round(simdefault.eeg.baseline * allruns.eeg.samplingRate);
            eeg.baseline = eeg.buffer(:,(end-bsl_length+1):end);
        end
        
        len = size(eeg.current_samples,2);
        eeg.buffer = [eeg.buffer(:,(len+1):end),eeg.current_samples];
    end
    
    % PSD
    if is_psd
        
        % restart buffer when new run starts
        if sum(ismember(allruns.eeg.triggers(1,ismember(allruns.eeg.triggers(2,:),eeg.current.framesamples)>0),trig_newrun))>0
            PSD.buffer = NaN(length(simdefault.psd.usedchannels),round(simdefault.psd.buffer * simdefault.framerate),length(default.PSDbins));
        end
        
        % update baseline after cue
        if sum(ismember(allruns.eeg.triggers(1,ismember(allruns.eeg.triggers(2,:),eeg.current.framesamples)>0),trig_cue))>0
            bsl_length = round(simdefault.psd.baseline * simdefault.framerate);
            PSD.baseline = PSD.buffer(:,(end-bsl_length+1):end);
        end
        
        for chan = 1:length(simdefault.psd.usedchannels)
            [~,~,~,PSD.data(chan,frame,:)] = spectrogram(eeg.buffer(...
                simdefault.psd.usedchannels(chan),end-psd_windowlength+1:end),...
                psd_windowlength,psd_overlap,psd_freqbins,allruns.eeg.samplingRate);
        end
        PSD.current_samples = PSD.data(:,frame,:);

        len = 1;
        PSD.buffer = [PSD.buffer(:,(len+1):end,:),PSD.current_samples];
    end
    
    
    %% Handle TOBI iD communication/triggers
    if is_et
        et.current_triggers = find(ismember(allruns.et.triggers(2,:),et.current.framesamples));
        if ~isempty(et.current_triggers)
            events = allruns.et.triggers(1,et.current_triggers);
%             et.current.event(frame) = events(1);
            et.events(frame) = events(1);
        else
%             et.current.event(frame) = 0;
            et.events(frame) = 0;
        end
    end
    if is_eeg
        eeg.current_triggers = find(ismember(allruns.eeg.triggers(2,:),eeg.current.framesamples));
        if ~isempty(eeg.current_triggers)
            events = allruns.eeg.triggers(1,eeg.current_triggers);
            eeg.events(frame) = events(1);
        else
            eeg.events(frame) = 0;
        end
    end
    
    %% FEATURE EXTRACTION
    

    % EYE TRACKER
    if is_et
        
        % --- trial start
        if ismember(et.events(frame),trig_cue)
            et.trial.counter = et.trial.counter + 1;
            et.in_trial = 1;
            et.trial.frame = 0;
            et.classifier.postprob = 0.5;
            et.classifier.buffer{et.trial.counter} = [];
            et.trial.buffer{et.trial.counter} = [];
            et.trial.postprob{et.trial.counter} = [];
            et.trial.postprob_smoothed{et.trial.counter}(1) = 0.5;
        % --- trial end
        elseif ismember(et.events(frame),trig_target)
            et.in_trial = 0;
        end
        % --- during trial
        if et.in_trial
            et.trial.frame = et.trial.frame + 1;
            % data preprocessing
            if strcmp(simdefault.et.classifier_type(1),'s')
                data = et.buffer_smoothed;
            elseif strcmp(simdefault.et.classifier_type(1),'X')
                data = et.buffer;
            end
            % baselining
            if strcmp(simdefault.et.classifier_type(2),'r')
                data = et.buffer ./ mean(et.baseline);
            elseif strcmp(simdefault.et.classifier_type(2),'X')
                data = et.buffer;
            end
            
            % fill trial buffer
            et.trial.buffer{et.trial.counter} = [et.trial.buffer{et.trial.counter},data((end-length(et.current.framesamples)+1):end)];
           
            if strcmp(simdefault.et.classifier_type(4),'w')
                timeintrial = double(size(et.trial.buffer{et.trial.counter},2) ./ allruns.et.samplingRate);
                if (timeintrial > simdefault.et.classifier_window(1)) && ...
                        (timeintrial < simdefault.et.classifier_window(2))
                    et.classifier.buffer{et.trial.counter} = [et.classifier.buffer{et.trial.counter},data((end-length(et.current.framesamples)+1):end)];
                end
            else
                et.classifier.buffer{et.trial.counter} = [et.classifier.buffer{et.trial.counter},data((end-length(et.current.framesamples)+1):end)];
            end
            
            % classifiy
            if ~isempty(et.classifier.buffer{et.trial.counter})
                data = nanmean(et.classifier.buffer{et.trial.counter});
                [~,pred_score,~] = predict(et.classifier.model,data(et.classifier.usefeats,:)');
                et.classifier.postprob = pred_score(:,2);
            end
            
            % set posterior probability of classes
            et.trial.postprob{et.trial.counter}(et.trial.frame) = et.classifier.postprob;
            if et.trial.frame > 1
                et.trial.postprob_smoothed{et.trial.counter}(et.trial.frame) = ...
                    mean([et.classifier.postprob,et.trial.postprob_smoothed{et.trial.counter}(et.trial.frame-1)]);
            end
        end
    end
    
    % PSD data
    if is_eeg
        
        % --- trial start
        if ismember(eeg.events(frame),trig_cue)
            eeg.trial.counter = eeg.trial.counter + 1;
            eeg.in_trial = 1;
            PSD.trial.frame = 0;
            PSD.classifier.postprob = 0.5;
            PSD.classifier.buffer{eeg.trial.counter} = [];
            PSD.trial.buffer{eeg.trial.counter} = [];
            PSD.trial.postprob{eeg.trial.counter} = [];
            PSD.trial.postprob_smoothed{eeg.trial.counter}(1) = 0.5;
        % --- trial end
        elseif ismember(eeg.events(frame),trig_target)
            eeg.in_trial = 0;
        end
        % --- during trial
        if eeg.in_trial
            PSD.trial.frame = PSD.trial.frame + 1;
            data = PSD.current_samples;
            % which frequency bands
            switch simdefault.psd.classifier_type(2)
                case 'a'    % alpha band
                    data = data(:,:,default.alphaband+1);
                case 'b'    % beta band
                    data = data(:,:,default.betaband+1);
                case 't'    % theta band
                    data = data(:,:,default.thetaband+1);
            end
            % band averages
            switch simdefault.psd.classifier_type(3)
                case 'b'    % band average
                    data = nanmean(data,3);
                case 'X'    % no average     
            end
            
            % reshape to feature space
            data = reshape(data,numel(data),[]);
            
            % fill trial buffer
            PSD.trial.buffer{eeg.trial.counter} = [PSD.trial.buffer{eeg.trial.counter},data(:)];
            
            % time averages
            switch simdefault.psd.classifier_type(4)
                case 't'    % time average over trial
                    data = mean(PSD.trial.buffer{eeg.trial.counter},2);  
                case 'X'    % no average
                    data = PSD.trial.buffer;
            end
           
            % classifiy
            if ~isempty(PSD.trial.buffer{eeg.trial.counter})
                [~,pred_score,~] = predict(eeg.classifier.model,data(eeg.classifier.usefeats)');
                PSD.classifier.postprob = pred_score(:,2);
            end
            
            % set posterior probability of classes
            PSD.trial.postprob{eeg.trial.counter}(PSD.trial.frame) = PSD.classifier.postprob;
            if eeg.trial.frame > 1
                PSD.trial.postprob_smoothed{eeg.trial.counter}(PSD.trial.frame) = ...
                    mean([PSD.classifier.postprob,PSD.trial.postprob_smoothed{eeg.trial.counter}(PSD.trial.frame-1)]);
            end
        end
    end
    
end

%% Save in file

filename = fullfile(default.grouppath,[newsavestruct.general.sessionID,'.',default.GAsuffix,'.mat']);
if exist(filename,'file')
    load(filename,'savestruct')
    
    f = fieldnames(newsavestruct);
    for i = 1:length(f)
        savestruct.(f{i}) = newsavestruct.(f{i}); %#ok<STRNU>
    end
else
    savestruct = newsavestruct; %#ok<NASGU>
end

save(filename, 'savestruct');

end
