% online simulation default file

simdefault.dataPath = '\\sti1files.epfl.ch\cnbi\cschneid\_Experiment_CVSA2\CVSA2.3(Chinchilla)\data_CLEAN';

simdefault.fromOfflinePath = '\\sti1files.epfl.ch\cnbi\cschneid\_Experiment_CVSA2\CVSA2.1(Adler)\data_CLEAN\_Group';

simdefault.savePath = '\\sti1files.epfl.ch\cnbi\cschneid\_Experiment_CVSA2\CVSA2.3(Chinchilla)\data_CLEAN\_SimOnline';

simdefault.framerate = 16; % in Hz (= update frequency of paradigm)

%% EYE TRACKER

simdefault.et.buffer = 1;  % in seconds
simdefault.et.baseline = 0.1; % in seconds
% simdefault.et.baseline_type = 'ratio';
simdefault.et.signal_tyep = 'pupil';
simdefault.et.classifier_window = [0.9,1.1];
simdefault.et.classifier_type = 'srtw'; % see description of classifier names in EEG/ET analyis scripts

%% ELECTROENCEPHALOGRAM

simdefault.eeg.buffer = 1;  % in seconds
simdefault.eeg.PSD.window = 1; % in seconds
simdefault.eeg.baseline = 1; % in seconds
% simdefault.eeg.baseline_type = 'none';
simdefault.eeg.signal_type = 'psd';

%% POWER SPECTRAL DENSITIES
simdefault.psd.usedchannels = 44:60;
simdefault.psd.buffer = 1; % in seconds
simdefault.psd.baseline = 0.5;
simdefault.psd.classifier_type = 'saXt'; % see description of classifier names in EEG/ET analyis scripts
