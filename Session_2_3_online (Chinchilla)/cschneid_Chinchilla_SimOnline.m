% author: Christoph Schneider, CNBI, 2018

% Computes simulated online results of CVSA data for experiment CVSA 2.3 Chinchilla
% Start this file to start processing - all the steps are implemented in a
% modular way and can therefore be reordered and/or commented out

clear;
close all force;
clc;

% Load defaults
% -------------------------------------------------------------------------
defaultscript = 'cschneid_Chinchilla_SimDefaults.m';
run(which(defaultscript));

% Select folders
% -------------------------------------------------------------------------
cd(simdefault.dataPath);
folders = uipickfiles('Prompt','Select Folders');


for f = 1:size(folders,2)
    disp(['++++ Processing folder ',num2str(f),' of ',num2str(size(folders,2)),' ++++']);
    % find files in folder
    args.pathname = folders{f};
    listing = dir([folders{f},'/*.mat']);
    args.filenames = {listing.name}'; 
    args.defaultfile = defaultscript;
    
    % call analysis function
    cschneid_Chinchilla_Sim(args);
end

disp('---> Batch file processing end <---')