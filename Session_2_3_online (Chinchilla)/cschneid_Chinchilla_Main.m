% author: Christoph Schneider, CNBI, 2017

% Offline analysis of CVSA data for experiment CVSA 2.3 Chinchilla
% Start this file to start processing - all the steps are implemented in a
% modular way and can therefore be reordered and/or commented out

% Can be called as function with the argument fileargs
% fileargs is a struct with the fields
% fileargs.pathname: path to folder containing the files
% fileargs.filename: cell array of file names

% Outputs a struct with all the concatenated runs of each modality

function  [] = cschneid_Chinchilla_Main(fileargs)

close all force;
clearvars -except fileargs;
disp('-------------------------------------------------------------------')

% Load defaults
% -------------------------------------------------------------------------
cschneid_Chinchilla_Default()

% Get file parameters
% -------------------------------------------------------------------------
if ~exist('fileargs','var')
    cd(default.datapath);
    [filename,pathname,~] = uigetfile( ...
        { '*.mat','Clean data files (*.mat)';}, ...
        'MultiSelect','on');
    if iscell(filename)==0  % if only one file
        filename = {filename};
    end
else
    pathname = fileargs.pathname;
    filename = fileargs.filenames;
end

% delete feedbackcolor mat files from the processing stream
filename(contains(filename,'feedbackcolor')) = [];
filename(contains(filename,'2online')) = [];

disp(filename{1})
allruns = struct;

%% Process files
% -------------------------------------------------------------------------
for f = 1:length(filename)
    
    clearvars -except f filename pathname default allruns
    
    
    filecontent = load(fullfile(pathname,filename{f}));
    
    %% Eye tracker preprocessing
    
    if isfield(filecontent,'et')
        filecontent.et.run = str2double(filename{f}(strfind(filename{f},'run')+3));
        args = struct('struct',filecontent.et,'default',default,'plot','no');
        run.et = cschneid_ET_preprocessing(args);
    end
    
    %% EEG preprocessing
    
    if isfield(filecontent,'eeg')
        filecontent.eeg.run = str2double(filename{f}(strfind(filename{f},'run')+3));
        args = struct('struct',filecontent.eeg,'default',default,'filename',...
            fullfile(pathname,filename{f}),'plot','no');
        run.eeg = cschneid_EEG_preprocessing(args);
    end
    
    %% synchronize corrupted trials (blinks/saccades) between ET and EEG
    
    args = struct('run',run,'useclass',default.artifact.consider4rejection);
    [run] = cschneid_ETEEG_marryartifactstructures(args);

    %% Compute PSDs
    
    if isfield(filecontent,'eeg')
        [pn,fn1,fn2] = fileparts(fullfile(pathname,filename{f}));
        args = struct('struct',run.eeg,'default',default,'type','eeg','path',pn,...
            'filename',[fn1,fn2],'save','yes');
        [run.PSD] = cschneid_computePSD(args);
        
        % --- plot for identifying bad channels/trials
        args = struct('struct',run.PSD,'default',default,'plot','no');
        cschneid_PSD_rawplots(args);
    end
    
    
    %% Merge runs
    
    if isstruct(run)
        args = struct('allruns',allruns,'newrun',run);
        allruns = cschneid_mergeruns(args);
    end
    
end

newsavestruct = struct;
clearvars -except default allruns newsavestruct;
cprintf([0,0.5,0.2],'Preprocessing finished!\n');

%% ADD GENERAL FIELDS

if isfield(allruns,'et')
    newsavestruct.general.subject = allruns.et.subject;
    newsavestruct.general.date = allruns.et.date;
    newsavestruct.general.sessionID = allruns.et.sessionID;
elseif isfield(allruns,'eeg')
    newsavestruct.general.subject = allruns.eeg.subject;
    newsavestruct.general.date = allruns.eeg.date;
    newsavestruct.general.sessionID = allruns.eeg.sessionID;
end

%% EYE TRACKER ANALYSIS

% if isfield(allruns,'et')
%     if length(allruns.et.trialsBad)/allruns.et.totaltrials < default.toocorrupted
%         args = struct('struct',allruns.et,'default',default,'savestruct',newsavestruct,'plot','yes');
%         newsavestruct = cschneid_Chinchilla_ET_analysis(args);
%     else
%         disp('Too many et trials corrupted!')
%     end
% end

%% PSD ANALYSIS

if isfield(allruns,'PSD')
%     if length(allruns.eeg.trialsBad)/allruns.eeg.totaltrials < default.toocorrupted
        args = struct('struct',allruns.PSD,'default',default,'savestruct',newsavestruct,'plot','yes');
        newsavestruct = cschneid_PSD_analysis_CVSA2(args);
%     else
%         disp('Too many eeg trials corrupted!')
%     end
end

%% Save in file

filename = fullfile(default.grouppath,[newsavestruct.general.sessionID,'.',default.GAsuffix,'.mat']);
if exist(filename,'file')
    load(filename,'savestruct')
    
    f = fieldnames(newsavestruct);
    for i = 1:length(f)
        savestruct.(f{i}) = newsavestruct.(f{i}); %#ok<STRNU>
    end
else
    savestruct = newsavestruct; %#ok<NASGU>
end

save(filename, 'savestruct');

end
