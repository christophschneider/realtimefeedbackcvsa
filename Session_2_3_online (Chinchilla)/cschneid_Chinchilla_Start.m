% author: Christoph Schneider, CNBI, 2017

% Offline analysis of CVSA data for experiment CVSA 2.3 Chinchilla
% Start this file to start processing - all the steps are implemented in a
% modular way and can therefore be reordered and/or commented out

clear;
close all force;
clc;

% Load defaults
% -------------------------------------------------------------------------
cschneid_Chinchilla_Default()

% Select folders
% -------------------------------------------------------------------------
cd(default.datapath);
folders = uipickfiles('Prompt','Select Folders');

for f = 1:size(folders,2)
    disp(['++++ Processing folder ',num2str(f),' of ',num2str(size(folders,2)),' ++++']);
    % find files in folder
    args.pathname = folders{f};
    listing = dir([folders{f},'/*.mat']);
    args.filenames = {listing.name}'; 
    
    % call analysis function
    cschneid_Chinchilla_Main(args);
%     cschneid_Chinchilla_Main_oldnew(args);
end

disp('---> Batch file processing end <---')