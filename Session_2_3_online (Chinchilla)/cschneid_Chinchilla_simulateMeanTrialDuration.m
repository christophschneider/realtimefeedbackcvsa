p1 = 0;
p2 = 0.006;
fs = 32;
reps = 100000;
mintime = 2;
maxtime = 20;
savet = NaN(1,reps);
sevep = NaN(1,reps);
smoothalpha = 0.1;
map_mean = 0;
map_halfspan = 2;

tic
p = 0.5;
for r = 1:reps
    LI_smoothed = 0.5;
    bsl = mean(randn(1,16));
    for t = 1:(maxtime*fs)
        LI_now = randn;
        
        LI_smoothed = LI_smoothed * (1-smoothalpha) + LI_now * smoothalpha;
        
        LI_baselined = LI_smoothed - bsl;
        
        LI_mapped_attended = (LI_baselined - map_mean)/map_halfspan;
        LI_mapped_attended(LI_mapped_attended>1) = 1;
        LI_mapped_attended(LI_mapped_attended<-1) = -1;

        savet(r) = t;
        
        p = (LI_mapped_attended + 1)/2 * p2;
        
        if (rand < p) && (t > 2*fs)
            break
        end
    end
end
toc

endtimes = savet/32;
figure;
histogram(endtimes,'normalization','pdf');

disp(['mean of endtimes: ' num2str(mean(endtimes))]);
disp(['mean of endtimes without maxtime: ' num2str(mean(endtimes(endtimes~=maxtime)))]);
disp(['ratio of endtimes == maxtime: ' num2str(length(endtimes(endtimes==maxtime))/length(endtimes))]);



% tic
% p = 0.5;
% for r = 1:reps
%     for t = 1:(maxtime*fs)
%         savet(r) = t;
%         p_new = betarnd(2,2) * p2;
%         p = mean([p,p_new]);
%         if (rand < p) && (t > 2*fs)
%             break
%         end
%     end
% end
% toc
% 
% endtimes = savet/32;
% figure;
% histogram(endtimes,'normalization','pdf');
% 
% disp(['mean of endtimes: ' num2str(mean(endtimes))]);
% disp(['mean of endtimes without maxtime: ' num2str(mean(endtimes(endtimes~=maxtime)))]);
% disp(['ratio of endtimes == maxtime: ' num2str(length(endtimes(endtimes==maxtime))/length(endtimes))]);
