% author: Christoph Schneider, CNBI, 2017

% Defines the default values for the EEG processing CVSA2.2(Biber).
% +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

%% GENERAL

% Path to the data files (CLEAN data required) | PC / Mac
if ispc
    default.datapath = '\\sti1files.epfl.ch\cnbi\cschneid\_Experiment_CVSA2\CVSA2.2(Biber)\data_CLEAN';
%     default.datapath = '\\sti1files.epfl.ch\cnbi\cschneid\_Experiment_CVSA1\data_CLEAN';
elseif isunix
    default.datapath = '/Volumes/cnbi/cschneid/_Experiment_CVSA2/CVSA2.2(Biber)/data_CLEAN';
end

% maximum valid reaction time (in seconds)
default.maxreactime = 1;

% minimum valid reaction time (in seconds)
default.minreactime = 0.2;

% classifier type used ('linear' or ...)
default.classifier = 'linear';

% baseline features for classification? yes/no
default.baselinefeatures = 'no';

% specify if bad channels should be interpolated; 
% 'yes' for classification, 'no' for average
default.interpolate = 'yes';

% path for group level analysis
default.grouppath = '_Group';
default.GAsuffix = '_GA';

% path for storing plots
default.plotpath = '_Plots';

% ratio of bad trials to exclude subject
default.artifact.toocorrupted = 0.5; % Finally needs to be defined by the threshold

% artifact classes to consider in the trial/sample rejection
default.artifact.consider4rejection.trials = {'eeg','button'};
default.artifact.consider4rejection.samples = {'blinks','saccades'};

%% EEG

% channel layout montage for the g.tec HIamp 64 channel system
default.montage = 'HIamp62.mat';

% locations file for topoplots
default.locsfile = 'HIamp-10-20-Cap62.locs';

% Target sampling rate (downsample to ___ Hz) | no downsampling = 0
default.samplingRateTarget = 512;

% Window length for power spectral density in seconds
default.PSDwindowlengthinseconds = 1.0;

% default baseline in seconds:
default.eeg.baselinetime = 1.0;
default.PSD.baselinetime = 0.5;

% Sliding window shift for power spectral density in seconds
default.PSDoverlapinseconds = 1/16; % CNBI loop at 16Hz
default.PSDoverlap = 1 - default.PSDoverlapinseconds/default.PSDwindowlengthinseconds;

% Frequency steps at which to evaluate the power spectral density
default.PSDbins = linspace(0,55,56);

% define range of studied frequency bands in Hz
default.thetaband = 4:7;
default.alphaband = 8:14; 
default.betaband = 16:28;

% define time ranges for sequential topoplots for each band
% baseline is there by default (if more than 0 seconds)
default.topotimes.theta = 0:0.5:5;
default.topotimes.alpha = 0:0.5:5;
default.topotimes.beta = 0:0.5:5;

% define regions of interest per hemisphere for the lateralization index
default.LI.roiL = {'P7','P5','PO7'};
default.LI.roiR = {'P8','P6','PO8'};

% maximum distance of neighboring electrodes for interpolation in mm
default.interpolationdistance = 50;

% channel numbers included in the classification
default.PSD.classification.channels = 44:60;

% Number of folds for the cross validation
default.PSD.classification.numfolds = 10;

% Number of repetitions of the classification
default.PSD.classification.numrep = 100;

% Amount of Normalization of the LDA
default.PSD.classification.gamma = 0.5;

% Maximum amount of features to classify
default.PSD.classification.maxfeatures = 10;

% which classifier for the detailed result plots
default.PSD.bestclsf = 'saXt';

%% EYE TRACKER

% diagonal of used screen in inch
default.et.screensize = 24;

% width to height ratio of used screen
default.et.screenratio = [16,9];

% distance of eyes to screen center in cm
default.et.distance2screen = 50;

% Additional time around detected blinks in seconds
default.et.blinkPadding = 0.05;

% sliding time window for drift correction in seconds
default.et.smoothingwindowtime = 60;

% default baseline in seconds:
default.et.baselinetime = 0.1;

% fixation circle around screen center in degrees of visual angle 
% (for detecting gaze towards targets)
default.et.fixationThreshold = 2;
    % 2 degrees in: Roijendijk, L., Farquhar, J., Van Gerven, M., Jensen, O., & Gielen, S. (2013). Exploring the impact of target eccentricity and task difficulty on covert visual spatial attention and its implications for brain computer interfacing. PLoS ONE, 8(12). https://doi.org/10.1371/journal.pone.0080489
    % 3 degrees in: Horschig, J. M., Oosterheert, W., Oostenveld, R., & Jensen, O. (2014). Modulation of Posterior Alpha Activity by Spatial Attention Allows for Controlling A Continuous Brain?Computer Interface. Brain Topography, 852?864. https://doi.org/10.1007/s10548-014-0401-7

% minimum duration outside of center circle for saccades (in seconds)
default.et.fixationDuration = 0.050;  % 20 ms is lower end of saccade duration
    % 0.05 seconds in: Roijendijk, L., Farquhar, J., Van Gerven, M., Jensen, O., & Gielen, S. (2013). Exploring the impact of target eccentricity and task difficulty on covert visual spatial attention and its implications for brain computer interfacing. PLoS ONE, 8(12). https://doi.org/10.1371/journal.pone.0080489

% Window for moving median smoothing for pupil diameter in seconds (set to
% zero for no filtering)
default.et.pupilsmoothwindow = 0.050;

% Number of folds for the cross validation
default.et.classification.numfolds = 100;

% Number of repetitions of the classification
default.et.classification.numrep = 50;

% Number of repetitions of the classification in seconds after cue onset
default.et.classification.window = [0.9,1.1];

% which classifier for the detailed result plots
default.et.bestclsf = 'Xrtw';
