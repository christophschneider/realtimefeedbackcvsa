% CVSA 2.2 Biber group level analysis

clc;
clear;
close all;

% Load defaults
% -------------------------------------------------------------------------
cschneid_Biber_Default()
plotsfolder = 'Groupplots';

cd(fullfile(default.datapath,'\_Group'));
[filename,pathname,~] = uigetfile( ...
    { '*.mat','Clean data files (*.mat)';}, ...
    'MultiSelect','on');
if iscell(filename)==0          % if only one file
    filename = {filename};
end

usegroup = questdlg('Which experimental group do you want to include?','Groups','Real','Sham','Both','Both');
prepost = questdlg('Which time point do you want to include?','Groups','Pre','Post','Both','Both');
ppv = {'Pre','Post'};

LI = {};
RT = [];
plotpath = fullfile(pathname,plotsfolder,[prepost,'_',usegroup]);
f = 0;

for fnx = 1:size(filename,2)
    
    load(filename{fnx})
    
    if (strcmpi(usegroup,'Both') || strcmpi(savestruct.PSD.group,usegroup)) && ...
            (strcmpi(prepost,'Both') || strcmpi(ppv{savestruct.PSD.run},prepost))
        
        disp(['Loading file ',filename{fnx}]);
        f = f+1;
        
        % --- general information
        subjectID{f} = savestruct.general.subject;
        sessionID{f} = savestruct.general.sessionID;
        
        % --- eye tracker
        if isfield(savestruct,'et')
            % --- rejected trials
            biberstruct.RRT(f) = length(savestruct.et.artifact.trials.total)/savestruct.et.totaltrials;
            
            fn = fieldnames(savestruct.et.pupil.classification);
            for c = 1:numel(fn)
                et.clsfacc(f,c) = 1 - savestruct.et.pupil.classification.(fn{c}).LDA.testerror;
                et.clsfstd(f,c) = 1 - savestruct.et.pupil.classification.(fn{c}).LDA.testerrorstd;
                et.clsftype{f,c} = fn{c};
            end
            clsfname = [default.et.bestclsf(1:2),'XX'];
            et.acccurve{f} = nanmean(savestruct.et.pupil.classification.(clsfname).LDA.sampleaccuracy(1,:,:),3);
            
            % add mean epochs for the grand average
            et.epochs_sr.white{f} = nanmean(savestruct.et.pupil.epochs_smoothed.white.matrix_ratio,1);
            et.epochs_sr.black{f} = nanmean(savestruct.et.pupil.epochs_smoothed.black.matrix_ratio,1);
            if isfield(savestruct.et.pupil.epochs_smoothed,'center')
                et.epochs_sr.center{f} = nanmean(savestruct.et.pupil.epochs_smoothed.center.matrix_ratio,1);
            end
        end
        
        % --- eeg
        if isfield(savestruct,'PSD')
            % --- rejected trials
            % take out center trials for rejection rate for classification
            tpos_center = find(cellfun(@(s) contains('cue_center', s), savestruct.PSD.triggerLegend{1}));
            centertrials = savestruct.PSD.triggers(3,savestruct.PSD.triggers(1,:) == savestruct.PSD.triggerLegend{3}(tpos_center));
            biberstruct.RRT(f) = length(savestruct.PSD.artifact.trials.total)/savestruct.PSD.totaltrials;
            biberstruct.RRT_nocenter(f) = length(setdiff(savestruct.PSD.artifact.trials.total,centertrials))/...
                savestruct.PSD.totaltrials;
            biberstruct.RRT_clsftrialnum(f) = savestruct.PSD.totaltrials - length(setdiff(savestruct.PSD.artifact.trials.total,centertrials));
            
            % --- chosen features
            biberstruct.features.full(f,:,:) = savestruct.PSD.classifier.saXt.LDA.featmat;
            biberstruct.features.channelLegend = savestruct.PSD.channelLegend;
            biberstruct.features.channelpos = default.PSD.classification.channels;
            %         biberstruct.features.channelpos = 1:62;
            biberstruct.features.alphaband = default.alphaband;
            
            % --- reaction times
            RT{f} = savestruct.PSD.RT;
            
            % --- Thut lateralization index
            eeg.LI.IAB(f,:) = savestruct.LI_PSD.iAB;
            eeg.LI.cmp(f,:) = [savestruct.LI_PSD.LIcmp.PLIvsILI.r,savestruct.LI_PSD.LIcmp.PLIvsILI.p];
            
            % --- pLI lateralization index
            eeg.pLI.IAB(f,:) = savestruct.pLI_PSD.iAB;
            eeg.pLI.cmp(f,:) = [savestruct.pLI_PSD.LIcmp.PLIvsILI.r,savestruct.pLI_PSD.LIcmp.PLIvsILI.p];
            
            % --- spectrum of LI electrodes
            eeg.spec.leftcue.leftcluster{f} = savestruct.PSD.spectrumLI.leftcue.leftcluster;
            eeg.spec.leftcue.rightcluster{f} = savestruct.PSD.spectrumLI.leftcue.rightcluster;
            eeg.spec.leftcue.LI{f} = savestruct.PSD.spectrumLI.leftcue.LI;
            
            eeg.spec.rightcue.leftcluster{f} = savestruct.PSD.spectrumLI.rightcue.leftcluster;
            eeg.spec.rightcue.rightcluster{f} = savestruct.PSD.spectrumLI.rightcue.rightcluster;
            eeg.spec.rightcue.LI{f} = savestruct.PSD.spectrumLI.rightcue.LI;
            
            % --- topoplot data
            eeg.topo.avg.alpha.left(f,:) = savestruct.PSD.band.left.alpha.average;
            eeg.topo.avg.beta.left(f,:) = savestruct.PSD.band.left.beta.average;
            eeg.topo.avg.theta.left(f,:) = savestruct.PSD.band.left.theta.average;
            
            eeg.topo.avg.alpha.right(f,:) = savestruct.PSD.band.right.alpha.average;
            eeg.topo.avg.beta.right(f,:) = savestruct.PSD.band.right.beta.average;
            eeg.topo.avg.theta.right(f,:) = savestruct.PSD.band.right.theta.average;
            
            if isfield(savestruct.PSD.band,'center')
                eeg.topo.avg.alpha.center(f,:) = savestruct.PSD.band.center.alpha.average;
                eeg.topo.avg.beta.center(f,:) = savestruct.PSD.band.center.beta.average;
                eeg.topo.avg.theta.center(f,:) = savestruct.PSD.band.center.theta.average;
            end
            
            % baselined topo
            eeg.topo.bsl.alpha.left(f,:) =  mean(savestruct.PSD.band.left.alpha.power(:,9:end),2) - mean(savestruct.PSD.band.left.alpha.power(:,1:8),2);
            eeg.topo.bsl.alpha.right(f,:) = mean(savestruct.PSD.band.right.alpha.power(:,9:end),2) - mean(savestruct.PSD.band.right.alpha.power(:,1:8),2);
            % --------------
            
            eeg.topo.time.alpha.left{f} = savestruct.PSD.band.left.alpha.power;
            eeg.topo.time.beta.left{f} = savestruct.PSD.band.left.beta.power;
            eeg.topo.time.theta.left{f} = savestruct.PSD.band.left.theta.power;
            
            eeg.topo.time.alpha.right{f} = savestruct.PSD.band.right.alpha.power;
            eeg.topo.time.beta.right{f} = savestruct.PSD.band.right.beta.power;
            eeg.topo.time.theta.right{f} = savestruct.PSD.band.right.theta.power;
            
            if isfield(savestruct.PSD.band,'center')
                eeg.topo.time.alpha.center{f} = savestruct.PSD.band.center.alpha.power;
                eeg.topo.time.beta.center{f} = savestruct.PSD.band.center.beta.power;
                eeg.topo.time.theta.center{f} = savestruct.PSD.band.center.theta.power;
            end
            
            % --- alpha lateralization index (Thut)
            if isfield(savestruct.LI_PSD,'epochs')
                eeg.LI.left{f} = squeeze(nanmean(savestruct.LI_PSD.epochs.left.matrix,1));
                eeg.LI.right{f} = squeeze(nanmean(savestruct.LI_PSD.epochs.right.matrix,1));
                if isfield(savestruct.LI_PSD.epochs,'center')
                    eeg.LI.center{f} = squeeze(nanmean(savestruct.LI_PSD.epochs.center.matrix,1));
                end
            end
            
            % --- alpha lateralization index pLI
            if isfield(savestruct.LI_PSD,'epochs')
                eeg.pLI.left{f} = squeeze(nanmean(savestruct.pLI_PSD.epochs.left.matrix,1));
                eeg.pLI.right{f} = squeeze(nanmean(savestruct.pLI_PSD.epochs.right.matrix,1));
                if isfield(savestruct.LI_PSD.epochs,'center')
                    eeg.pLI.center{f} = squeeze(nanmean(savestruct.pLI_PSD.epochs.center.matrix,1));
                end
            end
            
            % --- alpha lateralization index LI baselined
            if isfield(savestruct.LI_PSD,'epochs')
                eeg.bLI.left{f} = squeeze(nanmean(savestruct.pLI_PSD.epochs.left.matrix_bsl,1));
                eeg.bLI.right{f} = squeeze(nanmean(savestruct.pLI_PSD.epochs.right.matrix_bsl,1));
                if isfield(savestruct.LI_PSD.epochs,'center')
                    eeg.bLI.center{f} = squeeze(nanmean(savestruct.pLI_PSD.epochs.center.matrix_bsl,1));
                end
            end
            
            % --- alpha lateralization index Mahalanobis
            if isfield(savestruct.LI_PSD,'epochs')
                Lside.full = savestruct.pLI_PSD.epochs.left.matrix_bsl;
                Rside.full = savestruct.pLI_PSD.epochs.right.matrix_bsl;
                Lside.mean = nanmean(Lside.full,3);
                Rside.mean = nanmean(Rside.full,3);
                eeg.bLI.Mahatrials(f) = mean([mean(mahal(Lside.mean,Rside.mean)),mean(mahal(Rside.mean,Lside.mean))]);
                for t = 1:size(Lside.full,3)
                    a = Lside.full(~isnan(Lside.full(:,:,t)),:,t);
                    b = Rside.full(~isnan(Rside.full(:,:,t)),:,t);
                    if isempty(a) || isempty(b)
                        eeg.bLI.Mahasamples{f}(t) = NaN;
                    else
                        eeg.bLI.Mahasamples{f}(t) = mean([mean(mahal(a,b)),mean(mahal(b,a))]);
                    end
                end
            end
            
            LIpos = ismember(savestruct.PSD.channelLegend,[default.LI.roiL(end),default.LI.roiR(end)]);
            bslspan = 1:floor(default.PSD.baselinetime * savestruct.PSD.samplingRate);
            span = ceil(default.PSD.baselinetime * savestruct.PSD.samplingRate):size(savestruct.PSD.band.left.alpha.power,2);
            eeg.palphapower.bsl(f) =  mean([nanmean(nanmean(savestruct.PSD.band.left.alpha.power(LIpos,bslspan))),nanmean(nanmean(savestruct.PSD.band.right.alpha.power(LIpos,bslspan)))]);
            eeg.palphapower.trial(f) =  mean([nanmean(nanmean(savestruct.PSD.band.left.alpha.power(LIpos,span))),nanmean(nanmean(savestruct.PSD.band.right.alpha.power(LIpos,span)))]);
            
            % --- classification results
            fn = fieldnames(savestruct.PSD.classification);
            for c = 1:numel(fn)
                eeg.clsfacc(f,c) = 1 - savestruct.PSD.classification.(fn{c}).LDA.testerror;
                eeg.clsfaccstd(f,c) = savestruct.PSD.classification.(fn{c}).LDA.testerrorstd;
                eeg.clsftype{f,c} = fn{c};
                eeg.clfacctrial(f,c) = savestruct.PSD.classification.(fn{c}).LDA.trialaccuracy;
            end
            clsfname = [default.PSD.bestclsf(1:2),'XX'];
            eeg.acccurve{f} = nanmean(savestruct.PSD.classification.(clsfname).LDA.sampleaccuracy(1,:,:),3);
            
        end
        
        % --- hybrid BCI
        if isfield(savestruct,'hybrid')
            fn = fieldnames(savestruct.hybrid.classification);
            for c = 1:numel(fn)
                fnx = fieldnames(savestruct.hybrid.classification.(fn{c}).LDA.first);
                for d = 1:numel(fnx)
                    name = ['first_',fnx{d}];
                    hyb.(name).clsfacc(f,c) = 1 - savestruct.hybrid.classification.(fn{c}).LDA.first.(fnx{d}).testerror;
                    hyb.(name).clsfstd(f,c) = 1 - savestruct.hybrid.classification.(fn{c}).LDA.first.(fnx{d}).testerrorstd;
                    hyb.(name).clsftype{f,c} = fn{c};
                end
                hyb.second.clsfacc(f,c) = 1 - savestruct.hybrid.classification.(fn{c}).LDA.second.testerror;
                hyb.second.clsfstd(f,c) = 1 - savestruct.hybrid.classification.(fn{c}).LDA.second.testerrorstd;
                hyb.second.clsftype{f,c} = fn{c};
            end
            clsfname = [default.et.bestclsf(1:2),'XX'];
            et.acccurve{f} = nanmean(savestruct.et.pupil.classification.(clsfname).LDA.sampleaccuracy(1,:,:),3);
            
            % add mean epochs for the grand average
            et.epochs_sr.white{f} = nanmean(savestruct.et.pupil.epochs_smoothed.white.matrix_ratio,1);
            et.epochs_sr.black{f} = nanmean(savestruct.et.pupil.epochs_smoothed.black.matrix_ratio,1);
            if isfield(savestruct.et.pupil.epochs_smoothed,'center')
                et.epochs_sr.center{f} = nanmean(savestruct.et.pupil.epochs_smoothed.center.matrix_ratio,1);
            end
        end
        
        biberstruct.artifact(f) = sum(ismember(savestruct.PSD.artifact.trials.total,RT{f}.trial(ismember(RT{f}.cue,[1,2]))));
        
        usefreq = ismember(savestruct.PSD.frequency,default.alphaband);
        usesamples = (9:size(savestruct.PSD.spectrumLI.leftcue.LI,1));
        biberstruct.alpha_bsl.leftcue.left(f) = nanmean(nanmean(savestruct.PSD.spectrumLI.leftcue.leftcluster(usesamples,usefreq),2),1);
        biberstruct.alpha_bsl.leftcue.right(f) = nanmean(nanmean(savestruct.PSD.spectrumLI.leftcue.rightcluster(usesamples,usefreq),2),1);
        biberstruct.alpha_bsl.rightcue.left(f) = nanmean(nanmean(savestruct.PSD.spectrumLI.rightcue.leftcluster(usesamples,usefreq),2),1);
        biberstruct.alpha_bsl.rightcue.right(f) = nanmean(nanmean(savestruct.PSD.spectrumLI.rightcue.rightcluster(usesamples,usefreq),2),1);
        
    else
        disp(['Skipping file ',filename{fnx}]);
        continue
    end
    
end

%%%%%%%%%%%%

%%%%%%%%%%%%

%%%%%%%%%%%%

%% treating different length epochs between subjects

if exist('et','var')
    % --- pupillometry accuracy over time
    minsize = min(cellfun('size', et.acccurve, 2));
    mat = NaN(numel(et.acccurve),minsize);
    for c = 1:numel(et.acccurve)
        mat(c,:) = et.acccurve{c}(1:minsize);
    end
    et.acccurve = mat;
    
    % --- pupillometry pupil size over time
    minsize_white = min(cellfun('size', et.epochs_sr.white, 3));
    minsize_black = min(cellfun('size', et.epochs_sr.black, 3));
    minsize = min([minsize_white, minsize_black]);
    if isfield(et.epochs_sr,'center')
        minsize_center = min(cellfun('size', et.epochs_sr.center, 3));
        minsize = min([minsize_white, minsize_black, minsize_center]);
    end
    
    mat_white = NaN(numel(et.epochs_sr.white),minsize);
    mat_black = NaN(numel(et.epochs_sr.black),minsize);
    if isfield(et.epochs_sr,'center')
        mat_center = NaN(numel(et.epochs_sr.center),minsize);
    end
    for c = 1:numel(et.epochs_sr.white)
        mat_white(c,:) = et.epochs_sr.white{c}(1:minsize);
        mat_black(c,:) = et.epochs_sr.black{c}(1:minsize);
        if isfield(et.epochs_sr,'center')
            mat_center(c,:) = et.epochs_sr.center{c}(1:minsize);
        end
    end
    et.epochs_sr.white = mat_white;
    et.epochs_sr.black = mat_black;
    if isfield(et.epochs_sr,'center')
        et.epochs_sr.center = mat_center;
    end
    
    clear minsize* mat*
end

if exist('eeg','var')
    % --- psd spectrum
    minsize = min(cellfun('size', eeg.spec.leftcue.leftcluster, 1));
    
    d1 = numel(eeg.spec.leftcue.leftcluster);
    d2 = minsize;
    d3 = size(eeg.spec.leftcue.leftcluster{1},2);
    
    mat_leftcue_left = NaN(d1,d2,d3);
    mat_leftcue_right = NaN(d1,d2,d3);
    mat_leftcue_LI = NaN(d1,d2,d3);
    mat_rightcue_left = NaN(d1,d2,d3);
    mat_rightcue_right = NaN(d1,d2,d3);
    mat_rightcue_LI = NaN(d1,d2,d3);
    
    for c = 1:d1
        mat_leftcue_left(c,:,:) = eeg.spec.leftcue.leftcluster{c}(1:minsize,:);
        mat_leftcue_right(c,:,:) = eeg.spec.leftcue.rightcluster{c}(1:minsize,:);
        mat_leftcue_LI(c,:,:) = eeg.spec.leftcue.LI{c}(1:minsize,:);
        mat_rightcue_left(c,:,:) = eeg.spec.rightcue.leftcluster{c}(1:minsize,:);
        mat_rightcue_right(c,:,:) = eeg.spec.rightcue.rightcluster{c}(1:minsize,:);
        mat_rightcue_LI(c,:,:) = eeg.spec.rightcue.LI{c}(1:minsize,:);
    end
    eeg.spec.leftcue.leftcluster = mat_leftcue_left;
    eeg.spec.leftcue.rightcluster = mat_leftcue_right;
    eeg.spec.leftcue.LI = mat_leftcue_LI;
    eeg.spec.rightcue.leftcluster = mat_rightcue_left;
    eeg.spec.rightcue.rightcluster = mat_rightcue_right;
    eeg.spec.rightcue.LI = mat_rightcue_LI;
    
    clear minsize* mat* d1 d2 d3
    
    % --- psd topoplots
    minsize = min(cellfun('size', eeg.topo.time.theta.left, 2));
    
    d1 = numel(eeg.topo.time.theta.left);
    d3 = minsize;
    
    if isfield(eeg.topo.time.alpha,'center')
        mat_alpha_center = NaN(d1,size(eeg.topo.time.alpha.center{1},1),minsize);
        mat_beta_center = NaN(d1,size(eeg.topo.time.alpha.center{1},1),minsize);
        mat_theta_center = NaN(d1,size(eeg.topo.time.alpha.center{1},1),minsize);
    end
    
    for c = 1:d1
        mat_alpha_left(c,:,:) = eeg.topo.time.alpha.left{c}(:,1:minsize);
        mat_beta_left(c,:,:) = eeg.topo.time.beta.left{c}(:,1:minsize);
        mat_theta_left(c,:,:) = eeg.topo.time.theta.left{c}(:,1:minsize);
        
        mat_alpha_right(c,:,:) = eeg.topo.time.alpha.right{c}(:,1:minsize);
        mat_beta_right(c,:,:) = eeg.topo.time.beta.right{c}(:,1:minsize);
        mat_theta_right(c,:,:) = eeg.topo.time.theta.right{c}(:,1:minsize);
        
        if isfield(eeg.topo.time.theta,'center')
            if ~isempty( eeg.topo.time.theta.center{c})
                mat_alpha_center(c,:,:) = eeg.topo.time.alpha.center{c}(:,1:minsize);
                mat_beta_center(c,:,:) = eeg.topo.time.beta.center{c}(:,1:minsize);
                mat_theta_center(c,:,:) = eeg.topo.time.theta.center{c}(:,1:minsize);
            end
        end
        
    end
    
    eeg.topo.time.alpha.left = mat_alpha_left;
    eeg.topo.time.beta.left = mat_beta_left;
    eeg.topo.time.theta.left = mat_theta_left;
    
    eeg.topo.time.alpha.right = mat_alpha_right;
    eeg.topo.time.beta.right = mat_beta_right;
    eeg.topo.time.theta.right = mat_theta_right;
    
    if isfield(eeg.topo.time.theta,'center')
        eeg.topo.time.alpha.center = mat_alpha_center;
        eeg.topo.time.beta.center = mat_beta_center;
        eeg.topo.time.theta.center = mat_theta_center;
    end
    
    eeg.topo.time.timeline = linspace(0, minsize / savestruct.PSD.samplingRate, minsize) - default.PSD.baselinetime;
    
    clear minsize* mat* d1 d2
    
    % --- LI over time
    minsize_L = min(cellfun('size', eeg.LI.left, 1));
    minsize_R = min(cellfun('size', eeg.LI.right, 1));
    minsize = min([minsize_L,minsize_R]);
    if isfield(eeg.LI,'center')
        minsize_C = min(cellfun('size', eeg.LI.center(~isempty(eeg.LI.center)), 1));
        minsize = min([minsize_L,minsize_R,minsize_C]);
    end
    
    
    d1 = numel(eeg.LI.left);
    
    for c = 1:d1
        mat_LI_left(c,:) = eeg.LI.left{c}(1:minsize);
        mat_LI_right(c,:) = eeg.LI.right{c}(1:minsize);
        mat_Maha(c,:) = eeg.bLI.Mahasamples{c}(1:minsize);
        if isfield(eeg.LI,'center')
            if ~isempty( eeg.LI.center{c})
                mat_LI_center(c,:) = eeg.LI.center{c}(1:minsize);
            else
                mat_LI_center(c,:) = NaN(1,minsize);
            end
        end
    end
    
    eeg.LI.left = mat_LI_left;
    eeg.LI.right = mat_LI_right;
    eeg.bLI.Mahasamples = mat_Maha;
    if isfield(eeg.LI,'center')
        eeg.LI.center = mat_LI_center;
    end
    
    clear minsize* mat* d1
    
    % --- pLI over time
    minsize_L = min(cellfun('size', eeg.pLI.left, 1));
    minsize_R = min(cellfun('size', eeg.pLI.right, 1));
    minsize = min([minsize_L,minsize_R]);
    if isfield(eeg.pLI,'center')
        minsize_C = min(cellfun('size', eeg.pLI.center(~isempty(eeg.pLI.center)), 1));
        minsize = min([minsize_L,minsize_R,minsize_C]);
    end
    
    d1 = numel(eeg.pLI.left);
    
    for c = 1:d1
        mat_LI_left(c,:) = eeg.pLI.left{c}(1:minsize);
        mat_LI_right(c,:) = eeg.pLI.right{c}(1:minsize);
        if isfield(eeg.pLI,'center')
            if ~isempty( eeg.pLI.center{c})
                mat_LI_center(c,:) = eeg.pLI.center{c}(1:minsize);
            else
                mat_LI_center(c,:) = NaN(1,minsize);
            end
        end
    end
    
    eeg.pLI.left = mat_LI_left;
    eeg.pLI.right = mat_LI_right;
    if isfield(eeg.pLI,'center')
        eeg.pLI.center = mat_LI_center;
    end
    
    clear minsize* mat* d1
    
    % --- pLI baselined over time
    minsize_L = min(cellfun('size', eeg.bLI.left, 1));
    minsize_R = min(cellfun('size', eeg.bLI.right, 1));
    minsize = min([minsize_L,minsize_R]);
    if isfield(eeg.bLI,'center')
        minsize_C = min(cellfun('size', eeg.bLI.center(~isempty(eeg.bLI.center)), 1));
        minsize = min([minsize_L,minsize_R,minsize_C]);
    end
    
    d1 = numel(eeg.bLI.left);
    
    for c = 1:d1
        mat_LI_left(c,:) = eeg.bLI.left{c}(1:minsize);
        mat_LI_right(c,:) = eeg.bLI.right{c}(1:minsize);
        if isfield(eeg.bLI,'center')
            if ~isempty( eeg.bLI.center{c})
                mat_LI_center(c,:) = eeg.bLI.center{c}(1:minsize);
            else
                mat_LI_center(c,:) = NaN(1,minsize);
            end
        end
    end
    
    eeg.bLI.left = mat_LI_left;
    eeg.bLI.right = mat_LI_right;
    if isfield(eeg.bLI,'center')
        eeg.bLI.center = mat_LI_center;
    end
    
    clear minsize* mat* d1
    
    % --- PSD classificatio accuracies over time
    minsize = min(cellfun('size', eeg.acccurve, 2));
    mat = NaN(numel(eeg.acccurve),minsize);
    for c = 1:numel(eeg.acccurve)
        mat(c,:) = eeg.acccurve{c}(1:minsize);
    end
    eeg.acccurve = mat;
    
    clear minsize* mat*
end

biberstruct.subject = subjectID;
biberstruct.pLI = eeg.pLI;
biberstruct.bLI = eeg.bLI;
biberstruct.iLI = eeg.LI;
biberstruct.palphapower = eeg.palphapower;

%% pupillometry data

if exist('et','var')
    
    % --- plot overall classification accuracies
    mdata = mean(et.clsfacc,1);
    sdata = std(et.clsfacc,0,1);
    
    fig = figure('Position', get(0, 'Screensize'));
    hold on;
    grid on;
    barwitherr(sdata,mdata);
    set(gca,'XTick',1:length(mdata));
    set(gca,'XTickLabel',et.clsftype(1,:));
    title('Classification accuracies of different methods for et data');
    xlabel('classifier model')
    ylabel('classification accuracy')
    ylim([0,1]);
    
    filename = 'pupil.clsfmodels_accuracy.png';
    F = getframe(fig);
    imwrite(F.cdata, fullfile(plotpath,filename), 'png');
    
    % --- plot classification accuracies w.r.t. time in trial
    clsfname = [default.et.bestclsf(1:3),'X'];
    fig = figure('Position', get(0, 'Screensize'));
    hold on;
    ylim([0,1]);
    xdata = (1:size(et.acccurve,2))./savestruct.et.samplingRate - default.et.baselinetime;
    for subj = 1:size(et.acccurve,1)
        plot(xdata, et.acccurve(subj,:),'color',[0.8,0.8,0.8]);
    end
    plot(xdata,mean(et.acccurve,1),'r','linewidth',2)
    hline(0.5,'k')
    cschneid_vline(0,'k','-','cue')
    grid on;
    title(['Classification accuracies over time for ',clsfname,' trained on trial means']);
    xlabel('time [s]')
    ylabel('classification accuracy')
    ylim([0,1]);
    
    filename = ['pupil.clsf_accuracy_over_time_',clsfname,'.png'];
    F = getframe(fig);
    imwrite(F.cdata, fullfile(plotpath,filename), 'png');
    
    % --- plot best classification model results for all subjects
    clsfname = default.et.bestclsf;
    data = et.clsfacc(:,contains(et.clsftype(1,:),clsfname));
    fig = figure('Position', get(0, 'Screensize'));
    hold on;
    grid on;
    bar(1:length(data), data)
    bar(length(data)+1, mean(data))
    set(gca,'XTick',1:length(data)+1);
    set(gca,'XTickLabel', [subjectID,{'all'}]);
    title(['Classification accuracies for the model "',clsfname,'"']);
    xlabel('subject')
    ylabel('classification accuracy')
    hline(0.5,'k:');
    ylim([0,1]);
    
    filename = ['pupil.clsf_accuracy_all_subjects_',clsfname,'.png'];
    F = getframe(fig);
    imwrite(F.cdata, fullfile(plotpath,filename), 'png');
    
    % --- plot pupil size over time (grand average)
    fig = figure('Position', get(0, 'Screensize'));
    colorsX = cat(1,[1,0.6,0],[0,0.5,0.75],[0.5,0.5,0.5]);
    if isfield(et.epochs_sr,'center')
        means = squeeze([nanmean(et.epochs_sr.white,1);nanmean(et.epochs_sr.black,1);nanmean(et.epochs_sr.center,1)]);
        bounds(:,1,:) = squeeze([nanstd(et.epochs_sr.white,0,1);nanstd(et.epochs_sr.black,0,1);nanstd(et.epochs_sr.center,0,1)])';
        legendstring = {'white','black','center'};
    else
        means = squeeze([nanmean(et.epochs_sr.white,1);nanmean(et.epochs_sr.black,1)]);
        bounds(:,1,:) = squeeze([nanstd(et.epochs_sr.white,0,1);nanstd(et.epochs_sr.black,0,1)])';
        legendstring = {'white','black'};
    end
    timeline = (1:length(means))./savestruct.et.samplingRate - default.et.baselinetime;
    boundedline(timeline,means,bounds,'alpha','cmap',colorsX)
    co = get(gca,'colororder');
    grid on;
    legend(legendstring);
    xlabel('time [s]')
    ylabel('pupil size (ratio to baseline)')
    cschneid_vline(0, 'k', '-', 'cue')
    title('Grand average of pupil size (smoothed + ratio to baseline)');
    
    filename = 'pupil.size_over_time.png';
    F = getframe(fig);
    imwrite(F.cdata, fullfile(plotpath,filename), 'png');
end

%% EEG data
if exist('eeg','var')
    %% EEG spectrum
    
    % take means over subjects
    eeg.spec.means.leftcue.leftcluster = permute(nanmean(eeg.spec.leftcue.leftcluster,1),[2,3,1]);
    eeg.spec.means.leftcue.rightcluster = permute(nanmean(eeg.spec.leftcue.rightcluster,1),[2,3,1]);
    eeg.spec.means.leftcue.LI = permute(nanmean(eeg.spec.leftcue.LI,1),[2,3,1]);
    
    eeg.spec.means.rightcue.leftcluster = permute(nanmean(eeg.spec.rightcue.leftcluster,1),[2,3,1]);
    eeg.spec.means.rightcue.rightcluster = permute(nanmean(eeg.spec.rightcue.rightcluster,1),[2,3,1]);
    eeg.spec.means.rightcue.LI = permute(nanmean(eeg.spec.rightcue.LI,1),[2,3,1]);
    
    % define maximum time and amplitude
    t_max = size(eeg.spec.means.leftcue.LI,1)/savestruct.PSD.samplingRate;
    zmax = max([
        max(max(abs(eeg.spec.means.leftcue.leftcluster))),...
        max(max(abs(eeg.spec.means.leftcue.rightcluster))),...
        max(max(abs(eeg.spec.means.rightcue.leftcluster))),...
        max(max(abs(eeg.spec.means.rightcue.rightcluster))),...
        max(max(abs(eeg.spec.means.leftcue.LI))),...
        max(max(abs(eeg.spec.means.rightcue.LI)))]);
    
    % compute x-axis values
    xt = linspace(0,t_max,size(eeg.spec.means.leftcue.LI,1))-default.PSD.baselinetime;
    ticks = [xt(1),unique(round(floor(xt(xt>0))))];
    tickind = ticks.*0;
    for t = 1:length(ticks)
        [~,tickind(t)] = min(abs(xt-ticks(t)));
    end
    
    % plot
    fig = figure('Position', get(0, 'Screensize'));
    suptitle('Time-frequency plots for left and right trials')
    
    subplot(2,3,1)
    pcolor(eeg.spec.means.leftcue.leftcluster')
    shading interp;
    caxis([-zmax,zmax]);
    ax1 = gca;
    set(ax1, 'XTick', tickind, 'XTickLabel', ticks);
    xlabel('seconds'); ylabel('frequency [Hz]');
    cschneid_vline(tickind(ticks==0),'k','-','cue');
    title('Left cue: left cluster (P7, P5, PO7)');
    colorbar;
    
    subplot(2,3,2)
    pcolor(eeg.spec.means.leftcue.rightcluster')
    shading interp;
    caxis([-zmax,zmax]);
    ax1 = gca;
    set(ax1, 'XTick', tickind, 'XTickLabel', ticks);
    xlabel('seconds'); ylabel('frequency [Hz]');
    cschneid_vline(tickind(ticks==0),'k','-','cue');
    title('Left cue: right cluster (P8, P6, PO8)');
    colorbar;
    
    subplot(2,3,3)
    pcolor(eeg.spec.means.leftcue.LI')
    shading interp;
    caxis([-zmax,zmax]);
    ax1 = gca;
    set(ax1, 'XTick', tickind, 'XTickLabel', ticks);
    xlabel('seconds'); ylabel('frequency [Hz]');
    cschneid_vline(tickind(ticks==0),'k','-','cue');
    title('Left cue: lateralization (right-left)');
    colorbar;
    
    subplot(2,3,4)
    pcolor(eeg.spec.means.rightcue.leftcluster')
    shading interp;
    caxis([-zmax,zmax]);
    ax1 = gca;
    set(ax1, 'XTick', tickind, 'XTickLabel', ticks);
    xlabel('seconds'); ylabel('frequency [Hz]');
    cschneid_vline(tickind(ticks==0),'k','-','cue');
    title('Right cue: left cluster (P7, P5, PO7)');
    colorbar;
    
    subplot(2,3,5)
    pcolor(eeg.spec.means.rightcue.rightcluster')
    shading interp;
    caxis([-zmax,zmax]);
    ax1 = gca;
    set(ax1, 'XTick', tickind, 'XTickLabel', ticks);
    xlabel('seconds'); ylabel('frequency [Hz]');
    cschneid_vline(tickind(ticks==0),'k','-','cue');
    title('Right cue: right cluster (P8, P6, PO8)');
    colorbar;
    
    subplot(2,3,6)
    pcolor(eeg.spec.means.rightcue.LI')
    shading interp;
    caxis([-zmax,zmax]);
    ax1 = gca;
    set(ax1, 'XTick', tickind, 'XTickLabel', ticks);
    xlabel('seconds'); ylabel('frequency [Hz]');
    cschneid_vline(tickind(ticks==0),'k','-','cue');
    title('Right cue: lateralization (right-left)');
    colorbar;
    
    filename = fullfile(plotpath,'psd.LIspectrum.png');
    F = getframe(fig);
    imwrite(F.cdata, filename, 'png')
    
    %% EEG topoplots
    
    % --- plot alpha band *****************************************************
    
    % --- global
    if isfield(eeg.topo.avg.alpha,'center')
        data = [mean(eeg.topo.avg.alpha.left,1)',mean(eeg.topo.avg.alpha.center,1)',...
            mean(eeg.topo.avg.alpha.right,1)'];
        plotname = 'psd.topo.alpha_lcr.png';
        args = struct('data',data,'channels',{savestruct.PSD.channelLegend},...
            'layoutfile',default.locsfile,'title','Alpha band power',...
            'timepointlabels',{{'Left trials', 'Center trials', 'Right trials'}},'markers','on',...
            'save','yes','savename',fullfile(plotpath,plotname));
        cschneid_topoplot_eeglab(args)
    else
        data = [mean(eeg.topo.avg.alpha.left,1)',mean(eeg.topo.avg.alpha.right,1)'];
        plotname = 'psd.topo.alpha_lr.png';
        args = struct('data',data,'channels',{savestruct.PSD.channelLegend},...
            'layoutfile',default.locsfile,'title','Alpha band power',...
            'timepointlabels',{{'Left trials', 'Right trials'}},'markers','on',...
            'save','yes','savename',fullfile(plotpath,plotname));
        cschneid_topoplot_eeglab(args)
    end
    
    % --- difference
    data = mean(eeg.topo.avg.alpha.right,1)' - mean(eeg.topo.avg.alpha.left,1)';
    
    % XXX changed here
    biberstruct.topo.alpha.data.nobsl.left = nanmean(eeg.topo.avg.alpha.left,1)';
    biberstruct.topo.alpha.data.nobsl.right = nanmean(eeg.topo.avg.alpha.right,1)';
    biberstruct.topo.alpha.data.nobsl.diff = nanmean(eeg.topo.avg.alpha.right,1)' - nanmean(eeg.topo.avg.alpha.left,1)';
    
    biberstruct.topo.alpha.data.bsl.left = nanmean(eeg.topo.bsl.alpha.left,1)';
    biberstruct.topo.alpha.data.bsl.right = nanmean(eeg.topo.bsl.alpha.right,1)';
    biberstruct.topo.alpha.data.bsl.diff = nanmean(eeg.topo.bsl.alpha.right,1)' - nanmean(eeg.topo.bsl.alpha.left,1)';
    % XXX
    
    biberstruct.topo.alpha.channels = savestruct.PSD.channelLegend;
    biberstruct.topo.alpha.layout = default.locsfile;
    plotname = 'psd.topo.alpha_lrdiff.png';
    args = struct('data',data,'channels',{savestruct.PSD.channelLegend},...
        'layoutfile',default.locsfile,'title','Alpha band power difference',...
        'timepointlabels',{{'Right minus left trials'}},'markers','on','save','yes',...
        'savename',fullfile(plotpath,plotname));
    cschneid_topoplot_eeglab(args)
    
    % --- time steps
    clear data ticks
    timeline = eeg.topo.time.timeline;
    data(:,1) = (mean(mean(eeg.topo.time.alpha.right(:,:,timeline < 0),3),1) ...
        + mean(mean(eeg.topo.time.alpha.left(:,:,timeline < 0),3),1))/2;
    ticks = {'baseline'};
    for win = 1:length(default.topotimes.alpha)-1
        t = timeline>default.topotimes.alpha(win) & timeline<default.topotimes.alpha(win+1);
        addit = (mean(mean(eeg.topo.time.alpha.right(:,:,t),3),1) ...
            + mean(mean(eeg.topo.time.alpha.left(:,:,t),3),1))/2;
        data = cat(2,data,addit(:));
        ticks = cat(2,ticks,[num2str(default.topotimes.alpha(win)),' - ',num2str(default.topotimes.alpha(win+1))]);
    end
    
    plotname = 'psd.topo.alpha_time.png';
    args = struct('data',data,'channels',{savestruct.PSD.channelLegend},...
        'layoutfile',default.locsfile,'title','Global alpha over time (average between left and right)',...
        'timepointlabels',{ticks},'markers','on','save','yes','savename',fullfile(plotpath,plotname));
    cschneid_topoplot_eeglab(args)
    
    % --- class contrast time steps
    clear data ticks
    data(:,1) = mean(mean(eeg.topo.time.alpha.right(:,:,timeline<0),3) - ...
        mean(eeg.topo.time.alpha.left(:,:,timeline<0),3),1);
    ticks = {'baseline'};
    for win = 1:length(default.topotimes.alpha)-1
        t = timeline>default.topotimes.alpha(win) & timeline<default.topotimes.alpha(win+1);
        addit =  mean(mean(eeg.topo.time.alpha.right(:,:,t),3) - ...
            mean(eeg.topo.time.alpha.left(:,:,t),3),1);
        data = cat(2,data,addit(:));
        ticks = cat(2,ticks,[num2str(default.topotimes.alpha(win)),' - ',num2str(default.topotimes.alpha(win+1))]);
    end
    
    plotname = 'psd.topo.alpha_timediff.png';
    args = struct('data',data,'channels',{savestruct.PSD.channelLegend},...
        'layoutfile',default.locsfile,'title','Alpha lateralization over time',...
        'timepointlabels',{ticks},'markers','on','save','yes','savename',fullfile(plotpath,plotname));
    cschneid_topoplot_eeglab(args)
    
    % --- plot beta band *****************************************************
    
    % --- global
    if isfield(eeg.topo.avg.beta,'center')
        data = [mean(eeg.topo.avg.beta.left,1)',mean(eeg.topo.avg.beta.center,1)',...
            mean(eeg.topo.avg.beta.right,1)'];
        plotname = 'psd.topo.beta_lcr.png';
        args = struct('data',data,'channels',{savestruct.PSD.channelLegend},...
            'layoutfile',default.locsfile,'title','Beta band power',...
            'timepointlabels',{{'Left trials', 'Center trials', 'Right trials'}},'markers','on',...
            'save','yes','savename',fullfile(plotpath,plotname));
        cschneid_topoplot_eeglab(args)
    else
        data = [mean(eeg.topo.avg.beta.left,1)',mean(eeg.topo.avg.beta.right,1)'];
        plotname = 'psd.topo.beta_lr.png';
        args = struct('data',data,'channels',{savestruct.PSD.channelLegend},...
            'layoutfile',default.locsfile,'title','Beta band power',...
            'timepointlabels',{{'Left trials', 'Right trials'}},'markers','on',...
            'save','yes','savename',fullfile(plotpath,plotname));
        cschneid_topoplot_eeglab(args)
    end
    
    % --- difference
    data = mean(eeg.topo.avg.beta.right,1)' - mean(eeg.topo.avg.beta.left,1)';
    plotname = 'psd.topo.beta_lrdiff.png';
    args = struct('data',data,'channels',{savestruct.PSD.channelLegend},...
        'layoutfile',default.locsfile,'title','Beta band power difference',...
        'timepointlabels',{{'Right minus left trials'}},'markers','on','save','yes',...
        'savename',fullfile(plotpath,plotname));
    cschneid_topoplot_eeglab(args)
    
    % --- time steps
    clear data ticks
    timeline = eeg.topo.time.timeline;
    data(:,1) = (mean(mean(eeg.topo.time.beta.right(:,:,timeline < 0),3),1) ...
        + mean(mean(eeg.topo.time.beta.left(:,:,timeline < 0),3),1))/2;
    ticks = {'baseline'};
    for win = 1:length(default.topotimes.beta)-1
        t = timeline>default.topotimes.beta(win) & timeline<default.topotimes.beta(win+1);
        addit = (mean(mean(eeg.topo.time.beta.right(:,:,t),3),1) ...
            + mean(mean(eeg.topo.time.beta.left(:,:,t),3),1))/2;
        data = cat(2,data,addit(:));
        ticks = cat(2,ticks,[num2str(default.topotimes.beta(win)),' - ',num2str(default.topotimes.beta(win+1))]);
    end
    
    plotname = 'psd.topo.beta_time.png';
    args = struct('data',data,'channels',{savestruct.PSD.channelLegend},...
        'layoutfile',default.locsfile,'title','Global beta over time (average between left and right)',...
        'timepointlabels',{ticks},'markers','on','save','yes','savename',fullfile(plotpath,plotname));
    cschneid_topoplot_eeglab(args)
    
    % --- class contrast time steps
    clear data ticks
    data(:,1) = mean(mean(eeg.topo.time.beta.right(:,:,timeline<0),3) - ...
        mean(eeg.topo.time.beta.left(:,:,timeline<0),3),1);
    ticks = {'baseline'};
    for win = 1:length(default.topotimes.beta)-1
        t = timeline>default.topotimes.beta(win) & timeline<default.topotimes.beta(win+1);
        addit =  mean(mean(eeg.topo.time.beta.right(:,:,t),3) - ...
            mean(eeg.topo.time.beta.left(:,:,t),3),1);
        data = cat(2,data,addit(:));
        ticks = cat(2,ticks,[num2str(default.topotimes.beta(win)),' - ',num2str(default.topotimes.beta(win+1))]);
    end
    
    plotname = 'psd.topo.beta_timediff.png';
    args = struct('data',data,'channels',{savestruct.PSD.channelLegend},...
        'layoutfile',default.locsfile,'title','Beta lateralization over time',...
        'timepointlabels',{ticks},'markers','on','save','yes','savename',fullfile(plotpath,plotname));
    cschneid_topoplot_eeglab(args)
    
    % --- plot theta band *****************************************************
    
    % --- global
    if isfield(eeg.topo.avg.theta,'center')
        data = [mean(eeg.topo.avg.theta.left,1)',mean(eeg.topo.avg.theta.center,1)',...
            mean(eeg.topo.avg.theta.right,1)'];
        plotname = 'psd.topo.theta_lcr.png';
        args = struct('data',data,'channels',{savestruct.PSD.channelLegend},...
            'layoutfile',default.locsfile,'title','Theta band power',...
            'timepointlabels',{{'Left trials', 'Center trials', 'Right trials'}},'markers','on',...
            'save','yes','savename',fullfile(plotpath,plotname));
        cschneid_topoplot_eeglab(args)
    else
        data = [mean(eeg.topo.avg.theta.left,1)',mean(eeg.topo.avg.theta.right,1)'];
        plotname = 'psd.topo.theta_lr.png';
        args = struct('data',data,'channels',{savestruct.PSD.channelLegend},...
            'layoutfile',default.locsfile,'title','Theta band power',...
            'timepointlabels',{{'Left trials', 'Right trials'}},'markers','on',...
            'save','yes','savename',fullfile(plotpath,plotname));
        cschneid_topoplot_eeglab(args)
    end
    
    % --- difference
    data = mean(eeg.topo.avg.theta.right,1)' - mean(eeg.topo.avg.theta.left,1)';
    plotname = 'psd.topo.theta_lrdiff.png';
    args = struct('data',data,'channels',{savestruct.PSD.channelLegend},...
        'layoutfile',default.locsfile,'title','Theta band power difference',...
        'timepointlabels',{{'Right minus left trials'}},'markers','on','save','yes',...
        'savename',fullfile(plotpath,plotname));
    cschneid_topoplot_eeglab(args)
    
    % --- time steps
    clear data ticks
    timeline = eeg.topo.time.timeline;
    data(:,1) = (mean(mean(eeg.topo.time.theta.right(:,:,timeline < 0),3),1) ...
        + mean(mean(eeg.topo.time.theta.left(:,:,timeline < 0),3),1))/2;
    ticks = {'baseline'};
    for win = 1:length(default.topotimes.theta)-1
        t = timeline>default.topotimes.theta(win) & timeline<default.topotimes.theta(win+1);
        addit = (mean(mean(eeg.topo.time.theta.right(:,:,t),3),1) ...
            + mean(mean(eeg.topo.time.theta.left(:,:,t),3),1))/2;
        data = cat(2,data,addit(:));
        ticks = cat(2,ticks,[num2str(default.topotimes.theta(win)),' - ',num2str(default.topotimes.theta(win+1))]);
    end
    
    plotname = 'psd.topo.theta_time.png';
    args = struct('data',data,'channels',{savestruct.PSD.channelLegend},...
        'layoutfile',default.locsfile,'title','Global theta over time (average between left and right)',...
        'timepointlabels',{ticks},'markers','on','save','yes','savename',fullfile(plotpath,plotname));
    cschneid_topoplot_eeglab(args)
    
    % --- class contrast time steps
    clear data ticks
    data(:,1) = mean(mean(eeg.topo.time.theta.right(:,:,timeline<0),3) - ...
        mean(eeg.topo.time.theta.left(:,:,timeline<0),3),1);
    ticks = {'baseline'};
    for win = 1:length(default.topotimes.theta)-1
        t = timeline>default.topotimes.theta(win) & timeline<default.topotimes.theta(win+1);
        addit =  mean(mean(eeg.topo.time.theta.right(:,:,t),3) - ...
            mean(eeg.topo.time.theta.left(:,:,t),3),1);
        data = cat(2,data,addit(:));
        ticks = cat(2,ticks,[num2str(default.topotimes.theta(win)),' - ',num2str(default.topotimes.theta(win+1))]);
    end
    
    plotname = 'psd.topo.theta_timediff.png';
    args = struct('data',data,'channels',{savestruct.PSD.channelLegend},...
        'layoutfile',default.locsfile,'title','Theta lateralization over time',...
        'timepointlabels',{ticks},'markers','on','save','yes','savename',fullfile(plotpath,plotname));
    cschneid_topoplot_eeglab(args)
    
    %% Lateralization index
    
    % --- Lateralization index over time Thut (grand average)
    clear means bounds legendstring timeline
    fig = figure('Position', get(0, 'Screensize'));
    colorsX = cat(1,[1,0,0],[0,0,1],[0.5,0.5,0.5]);
    if isfield(eeg.LI,'center')
        means = squeeze([nanmean(eeg.LI.left,1);nanmean(eeg.LI.right,1);nanmean(eeg.LI.center,1)]);
        bounds(:,1,:) = squeeze([nanstd(eeg.LI.left,0,1);nanstd(eeg.LI.right,0,1);nanstd(eeg.LI.center,0,1)])';
        legendstring = {'left','right','center'};
    else
        means = squeeze([nanmean(eeg.LI.left,1);nanmean(eeg.LI.right,1)]);
        bounds(:,1,:) = squeeze([nanstd(eeg.LI.left,0,1);nanstd(eeg.LI.right,0,1)])';
        legendstring = {'left','right'};
    end
    
    biberstruct.iLI.means = means;
    biberstruct.iLI.bounds = bounds;
    biberstruct.iLI.legend = legendstring;
    [biberstruct.iLI.signif005,~,~] = permstattest({eeg.LI.left,eeg.LI.right},1000,0.05,0.05,'ttest2',0,1);
    [biberstruct.iLI.signif01,~,~] = permstattest({eeg.LI.left,eeg.LI.right},1000,0.1,0.1,'ttest2',0,1);
    
    timeline = (1:length(means))./savestruct.PSD.samplingRate - default.PSD.baselinetime;
    boundedline(timeline,means,bounds,'alpha','cmap',colorsX)
    xlimits = find(biberstruct.iLI.signif005);
    ylimits = [-0.5,0.5];
    ylim(ylimits);
    co = get(gca,'colororder');
    grid on;
    legend(legendstring);
    xlabel('time [s]')
    ylabel('alpha lateralization index')
    cschneid_vline(0, 'k', '-', 'cue')
    title('Grand average of the alpha lateralization index (Thut)');
    if sum(xlimits) > 0
        boxxlimits = [xlimits(1),xlimits(end)];
        rectangle('Position',[timeline(boxxlimits(1)),ylimits(1),...
            timeline(boxxlimits(2))-timeline(boxxlimits(1)),ylimits(2)-ylimits(1)])
    end
    
    filename = 'LI.over_time.png';
    F = getframe(fig);
    imwrite(F.cdata, fullfile(plotpath,filename), 'png');
    
    % --- Lateralization index over time pLI (grand average)
    clear means bounds legendstring timeline
    fig = figure('Position', get(0, 'Screensize'));
    colorsX = cat(1,[1,0,0],[0,0,1],[0.5,0.5,0.5]);
    if isfield(eeg.pLI,'center')
        means = squeeze([nanmean(eeg.pLI.left,1);nanmean(eeg.pLI.right,1);nanmean(eeg.pLI.center,1)]);
        bounds(:,1,:) = squeeze([nanstd(eeg.pLI.left,0,1);nanstd(eeg.pLI.right,0,1);nanstd(eeg.pLI.center,0,1)])';
        legendstring = {'left','right','center'};
    else
        means = squeeze([nanmean(eeg.pLI.left,1);nanmean(eeg.pLI.right,1)]);
        bounds(:,1,:) = squeeze([nanstd(eeg.pLI.left,0,1);nanstd(eeg.pLI.right,0,1)])';
        legendstring = {'left','right'};
    end
    
    biberstruct.pLI.means = means;
    biberstruct.pLI.bounds = bounds;
    biberstruct.pLI.legend = legendstring;
    [biberstruct.pLI.signif005,allclusters,nsignif] = permstattest({eeg.pLI.left,eeg.pLI.right},1000,0.05,0.05,'ttest2',0,1);
    [biberstruct.pLI.signif01,allclusters01,nsignif01] = permstattest({eeg.pLI.left,eeg.pLI.right},1000,0.1,0.1,'ttest2',0,1);
    
    timeline = (1:length(means))./savestruct.PSD.samplingRate - default.PSD.baselinetime;
    boundedline(timeline,means,bounds,'alpha','cmap',colorsX)
    xlimits = find(biberstruct.pLI.signif005);
    ylimits = [-1,1];
    ylim(ylimits);
    co = get(gca,'colororder');
    grid on;
    legend(legendstring);
    xlabel('time [s]')
    ylabel('alpha lateralization index')
    cschneid_vline(0, 'k', '-', 'cue')
    title('Grand average of the alpha lateralization index (pLI)');
    if sum(xlimits) > 0
        boxxlimits = [xlimits(1),xlimits(end)];
        rectangle('Position',[timeline(boxxlimits(1)),ylimits(1),...
            timeline(boxxlimits(2))-timeline(boxxlimits(1)),ylimits(2)-ylimits(1)])
    end
    
    filename = 'pLI.over_time.png';
    F = getframe(fig);
    imwrite(F.cdata, fullfile(plotpath,filename), 'png');
    
    % --- Lateralization index over time pLI baselined (grand average)
    clear means bounds legendstring timeline
    fig = figure('Position', get(0, 'Screensize'));
    colorsX = cat(1,[1,0,0],[0,0,1],[0.5,0.5,0.5]);
    if isfield(eeg.pLI,'center')
        means = squeeze([nanmean(eeg.bLI.left,1);nanmean(eeg.bLI.right,1);nanmean(eeg.bLI.center,1)]);
        bounds(:,1,:) = squeeze([nanstd(eeg.bLI.left,0,1);nanstd(eeg.bLI.right,0,1);nanstd(eeg.bLI.center,0,1)])';
        legendstring = {'left','right','center'};
    else
        means = squeeze([nanmean(eeg.bLI.left,1);nanmean(eeg.bLI.right,1)]);
        bounds(:,1,:) = squeeze([nanstd(eeg.bLI.left,0,1);nanstd(eeg.bLI.right,0,1)])';
        legendstring = {'left','right'};
    end
    
    biberstruct.bLI.means = means;
    biberstruct.bLI.bounds = bounds;
    biberstruct.bLI.legend = legendstring;
    [biberstruct.bLI.signif005,allclusters,nsignif] = permstattest({eeg.bLI.left,eeg.bLI.right},1000,0.05,0.05,'ttest2',0,1);
    [biberstruct.bLI.signif01,allclusters01,nsignif01] = permstattest({eeg.bLI.left,eeg.bLI.right},1000,0.1,0.1,'ttest2',0,1);
    
    timeline = (1:length(means))./savestruct.PSD.samplingRate - default.PSD.baselinetime;
    boundedline(timeline,means,bounds,'alpha','cmap',colorsX)
    xlimits = find(biberstruct.bLI.signif005);
    ylimits = [-1,1];
    ylim(ylimits);
    co = get(gca,'colororder');
    grid on;
    legend(legendstring);
    xlabel('time [s]')
    ylabel('alpha lateralization index')
    cschneid_vline(0, 'k', '-', 'cue')
    title('Grand average of the baselined alpha lateralization index (bLI)');
    if sum(xlimits) > 0
        boxxlimits = [xlimits(1),xlimits(end)];
        rectangle('Position',[timeline(boxxlimits(1)),ylimits(1),...
            timeline(boxxlimits(2))-timeline(boxxlimits(1)),ylimits(2)-ylimits(1)])
    end
    
    filename = 'bLI.over_time.png';
    F = getframe(fig);
    imwrite(F.cdata, fullfile(plotpath,filename), 'png');
    
    
    %% EEG-PSD classification
    
    numtr = round(mean(biberstruct.RRT_clsftrialnum));
    [~,pci] = binofit(round(numtr/2),numtr,0.01);
    [~,pcX] = binofit(round(numtr/2),numtr,1 - 0.6827);
    randstd = (pcX(2) - pcX(1))/2;
    
    % --- save for overall
    biberstruct.results.clsfacc = eeg.clsfacc;
    biberstruct.results.trialacc = eeg.clfacctrial;
    biberstruct.results.classifier = eeg.clsftype;
    biberstruct.results.trialchancelevel.mean = pci(2);
    
    % --- plot overall classification accuracies
    mdata = mean(eeg.clsfacc,1);
    sdata = std(eeg.clsfacc,0,1);
    
    fig = figure('Position', get(0, 'Screensize'));
    hold on;
    grid on;
    barwitherr(sdata,mdata);
    set(gca,'XTick',1:length(mdata));
    set(gca,'XTickLabel',eeg.clsftype(1,:));
    title('Classification accuracies of different methods for PSD data');
    xlabel('classifier model')
    ylabel('classification accuracy')
    ylim([0,1]);
    hline(pci(2),'r:','mean binomial chance level')
    
    filename = 'psd.clsfmodels_accuracy.png';
    F = getframe(fig);
    imwrite(F.cdata, fullfile(plotpath,filename), 'png');
    
    % --- plot classification accuracies w.r.t. time in trial
    clsfname = [default.PSD.bestclsf(1:3),'X'];
    fig = figure('Position', get(0, 'Screensize'));
    hold on;
    ylim([0,1]);
    xdata = (1:size(eeg.acccurve,2))./savestruct.PSD.samplingRate - default.PSD.baselinetime;
    for subj = 1:size(eeg.acccurve,1)
        plot(xdata, eeg.acccurve(subj,:),'color',[0.8,0.8,0.8]);
    end
    plot(xdata,mean(eeg.acccurve,1),'r','linewidth',2)
    hline(0.5,'k')
    cschneid_vline(0,'k','-','cue')
    grid on;
    title(['Classification accuracies over time for ',clsfname,' trained on trial means']);
    xlabel('time [s]')
    ylabel('classification accuracy')
    ylim([0,1]);
    
    filename = ['psd.clsf_accuracy_over_time_',clsfname,'.png'];
    F = getframe(fig);
    imwrite(F.cdata, fullfile(plotpath,filename), 'png');
    
    % --- plot best classification model results for all subjects
    clsfname = default.PSD.bestclsf;
    data = eeg.clsfacc(:,contains(eeg.clsftype(1,:),clsfname));
    datastd = eeg.clsfaccstd(:,contains(eeg.clsftype(1,:),clsfname));
    fig = figure('Position', get(0, 'Screensize'));
    subplot(1,20,1:18)
    hold on;
    grid on;
    bar(1:length(data), data)
    set(gca,'XTick',1:length(data));
    set(gca,'XTickLabel', subjectID);
    title(['Classification accuracies for the model "',clsfname,'"']);
    xlabel('subject')
    ylabel('classification accuracy')
    hline(0.5,'k:');
    ylim([0,1]);
    hline(pci(2),'r:','mean binomial chance level')
    
    subplot(1,20,19:20)
    hold on;
    grid on;
    bar(mean(data),'w');
    errorbar(mean(data),std(data),'k')
    set(gca,'XTickLabel',{' '})
    ylim([0,1]);
    hline(pci(2),'r:')
    hline(0.5,'k:');
    ax = gca;
    ax.YAxisLocation = 'right';
    xlabel('average')
    
    filename = ['psd.clsf_accuracy_all_subjects_',clsfname,'.png'];
    F = getframe(fig);
    imwrite(F.cdata, fullfile(plotpath,filename), 'png');
    
    % --- plot classification model results for all subjects (samples)
    clsfname = {'pabX','saXX','XaXX','babX'};
    data = eeg.clsfacc(:,contains(eeg.clsftype(1,:),clsfname));
    fig = figure('Position', get(0, 'Screensize'));
    subplot(1,20,1:17)
    hold on;
    grid on;
    bar(1:size(data,1), data)
    set(gca,'XTick',1:length(data));
    set(gca,'XTickLabel', subjectID);
    title('Sample classification accuracies');
    legend(eeg.clsftype(1,contains(eeg.clsftype(1,:),clsfname)));
    xlabel('subject')
    ylabel('classification accuracy')
    hline(0.5,'k:');
    ylim([0,1]);
    
    subplot(1,20,18:20)
    hold on;
    grid on;
    bar(diag(mean(data)),'stacked')
    errorbar(mean(data),std(data),'k','LineStyle','none')
    set(gca,'XTickLabel',{' '})
    ylim([0,1]);
    hline(pci(2),'r:')
    hline(0.5,'k:');
    ax = gca;
    ax.YAxisLocation = 'right';
    xlabel('average')
    
    filename = 'psd.clsf_accuracy_all_subjects_samples_clsf.png';
    F = getframe(fig);
    imwrite(F.cdata, fullfile(plotpath,filename), 'png');
    
    % --- plot classification model results for all subjects (trials)
    clsfname = {'pabt','saXt','XaXt','babt'};
    data = eeg.clsfacc(:,contains(eeg.clsftype(1,:),clsfname));
    fig = figure('Position', get(0, 'Screensize'));
    subplot(1,20,1:17)
    hold on;
    grid on;
    bar(1:size(data,1), data)
    set(gca,'XTick',1:length(data));
    set(gca,'XTickLabel', subjectID);
    title('Trial classification accuracies');
    legend(eeg.clsftype(1,contains(eeg.clsftype(1,:),clsfname)));
    xlabel('subject')
    ylabel('classification accuracy')
    hline(0.5,'k:');
    ylim([0,1]);
    hline(pci(2),'r:','mean binomial chance level')
    
    subplot(1,20,18:20)
    hold on;
    grid on;
    bar(diag(mean(data)),'stacked')
    errorbar(mean(data),std(data),'k','LineStyle','none')
    set(gca,'XTickLabel',{' '})
    ylim([0,1]);
    hline(pci(2),'r:')
    hline(0.5,'k:');
    ax = gca;
    ax.YAxisLocation = 'right';
    xlabel('average')
    
    filename = 'psd.clsf_accuracy_all_subjects_trials_clsf.png';
    F = getframe(fig);
    imwrite(F.cdata, fullfile(plotpath,filename), 'png');
    
    % --- plot normalized classification accuracies w.r.t. the chance level (trials)
    clsfname = {'pabt','saXt','XaXt','babt'};
    mclsf = eeg.clsfacc(:,contains(eeg.clsftype(1,:),clsfname));
    data = (mclsf - 0.5) / randstd;
    fig = figure('Position', get(0, 'Screensize'));
    subplot(1,20,1:17)
    hold on;
    grid on;
    bar(1:size(data,1), data)
    set(gca,'XTick',1:length(data));
    set(gca,'XTickLabel', subjectID);
    title('Trial classification accuracies');
    legend(eeg.clsftype(1,contains(eeg.clsftype(1,:),clsfname)));
    xlabel('subject')
    ylabel('classification accuracy')
    ax = gca;
    
    subplot(1,20,18:20)
    hold on;
    grid on;
    bar(diag(mean(data)),'stacked')
    errorbar(mean(data),std(data),'k','LineStyle','none')
    set(gca,'XTickLabel',{' '})
    ylim(ax.YLim);
    ax = gca;
    ax.YAxisLocation = 'right';
    xlabel('average')
    
    filename = 'psd.clsf_accuracy_all_subjects_trials_normalized.png';
    F = getframe(fig);
    imwrite(F.cdata, fullfile(plotpath,filename), 'png');
    
    
    
    
    
    %% Channels and bands selected by the classifier
    
    % --- difference
    abs_freq = squeeze(sum(biberstruct.features.full,1));
    rel_freq = abs_freq./numel(abs_freq);
    
    data = zeros(length(biberstruct.features.channelLegend),1);
    data(biberstruct.features.channelpos) = sum(rel_freq,2);
    plotname = 'psd.topo.features.png';
    args = struct('data',data,'channels',{savestruct.PSD.channelLegend},...
        'layoutfile',default.locsfile,'title','Heatmap selected features',...
        'timepointlabels',{{'relative frequency of choice'}},'markers','names','save','yes',...
        'savename',fullfile(plotpath,plotname),'colormap',flipud(hot),'zlim',[0,0.045]);
    cschneid_topoplot_eeglab(args)
    
    
    data = sum(rel_freq,1);
    fig = figure('Position', get(0, 'Screensize'));
    hold on;
    grid on;
    bar(biberstruct.features.alphaband,data)
    xlabel('frequency band');
    ylabel('relative frequency of choice')
    filename = 'psd.clsf_feature_frequency.png';
    F = getframe(fig);
    imwrite(F.cdata, fullfile(plotpath,filename), 'png');
    
end

%% hybrid BCI (classifier fusion)

if exist('hyb','var')
    
    fn = fieldnames(hyb);
    
    for c = 1:numel(fn)
        
        % --- plot overall classification accuracies
        mdata = mean(hyb.(fn{c}).clsfacc,1);
        sdata = std(hyb.(fn{c}).clsfacc,0,1);
        
        fig = figure('Position', get(0, 'Screensize'));
        hold on;
        grid on;
        barwitherr(sdata,mdata);
        set(gca,'XTick',1:length(mdata));
        set(gca,'XTickLabel',hyb.(fn{c}).clsftype(1,:));
        title(['Classification accuracies of different methods for ',fn{c},' data']);
        xlabel('classifier model')
        ylabel('classification accuracy')
        ylim([0,1]);
        
        filename = ['hybrid.',fn{c},'.clsfmodels_accuracy.png'];
        F = getframe(fig);
        imwrite(F.cdata, fullfile(plotpath,filename), 'png');
        
        % --- plot best classification model results for all subjects
        clsfname = hyb.(fn{c}).clsftype{1};
        data = hyb.(fn{c}).clsfacc(:,contains(hyb.(fn{c}).clsftype(1,:),clsfname));
        fig = figure('Position', get(0, 'Screensize'));
        hold on;
        grid on;
        bar(1:length(data), data)
        bar(length(data)+1, mean(data))
        set(gca,'XTick',1:length(data)+1);
        set(gca,'XTickLabel', [subjectID,{'all'}]);
        title(['Classification accuracies for the model "',fn{c},'"']);
        xlabel('subject')
        ylabel('classification accuracy')
        hline(0.5,'k:');
        ylim([0,1]);
        
        filename = ['clsf_accuracy_all_subjects.hybrid.',fn{c},'.png'];
        F = getframe(fig);
        imwrite(F.cdata, fullfile(plotpath,filename), 'png');
        
    end
    
end

%% Reaction time saving

if exist('RT','var')
    biberstruct.RT = RT;
    save(['upstream_Biber.',prepost,'.',usegroup,'.mat'],'biberstruct')
end

    
%% Correlation resting state alpha (Dachs) vs. aLI span (Biber)

load(fullfile('\\sti1files.epfl.ch\cnbi\cschneid\_Experiment_CVSA2\CVSA2.4(Dachs)\data_CLEAN\_Group','__Dachs_Alpha_Power.mat'))
alispan = mean(eeg.LI.right - eeg.LI.left,2);

u = unique(dachsstruct.sessionID);
x_max = [];
x_mean = [];
y = [];
for ses = 1:length(u)
    whichdachs = ismember(dachsstruct.sessionID,u(ses));
    runvec = find(ismember(sessionID, u(ses)));
    for r = runvec
        x_max = [x_max, dachsstruct.maxApow(whichdachs)];
        x_mean = [x_mean, dachsstruct.meanApow(whichdachs)];
        y = [y, alispan(r)];
    end
end

figure;
subplot(2,1,1)
x = log(x_mean);
scatter(x,y,'.k')
hold on;
[brob,~] = robustfit(x,y);
plot(x,brob(1) + brob(2)*x,'k','LineWidth',1,'LineStyle','-')
xlabel('log alpha power')
ylabel('aLI span')
title('aLI span vs. mean resting state alpha')
subplot(2,1,2)
x = log(x_max);
scatter(x,y,'.k')
hold on;
[brob,~] = robustfit(x,y);
plot(x,brob(1) + brob(2)*x,'k','LineWidth',1,'LineStyle','-')
xlabel('log alpha power')
ylabel('aLI span')
title('aLI span vs. max resting state alpha')

%%
disp('--- end of batch ---')