% clear;
clearvars -except x* y*
close all;
clc;

goodsubj = {'ae4','ae5','ae6','ae8','af1','af3','af4'};
badsubj = {'ae1','ae2','ae7','ae9','ae10','af2'};
allsubj = {'ae1','ae2','ae3','ae4','ae5','ae6','ae7','ae8','ae9','ae10','af1','af2','af3','af4'};
oksubj = {'ae1','ae2','ae4','ae5','ae6','ae7','ae8','ae9','ae10','af1','af2','af3','af4'};
% oksubj = {'ae1','ae2','ae4','ae5','ae7','ae9','ae10','af1','af2','af4'};

load('\\sti1files.epfl.ch\cnbi\cschneid\_Experiment_CVSA2\Umbrella\reactiontimes.mat')
cd '\\sti1files.epfl.ch\cnbi\cschneid\_Experiment_CVSA2\Umbrella\';

A = [RT.A(1:2),RT.A(2),RT.A(3:end)];
RT.A = A

cues = [1,2];

if numel(cues) == 1
    switch cues
        case 1
            side = 'left';
        otherwise
            side = 'right';
    end
else
    side = 'left + right';
end

avgtype = 'mean'
usesubj = find(ismember(allsubj,oksubj));

s = 0;
for sub = usesubj
    s = s + 1;
    
    % --- Adler
    x = RT.A{sub};
    day1.left(s) = avg(x(~(x.exclude) & x.cue == 1,:).reactionTime,avgtype);
    day1.right(s) = avg(x(~(x.exclude) & x.cue == 2,:).reactionTime,avgtype);
    day1.center(s) = avg(x(~(x.exclude) & x.cue == 3,:).reactionTime,avgtype);
    
    % --- Biber
    
    x = RT.B.real.pre{sub};
    if isempty(setdiff(x.cue,cues))
        sdc = 0;
    else
        sdc = setdiff(x.cue,cues);
    end
    
    order(s) = x.day(1);
    
    Exp_pre_norm(s) = avg(x(~(x.exclude | x.catchTrial | x.cue == sdc),:).reactionTime,avgtype);
    Exp_pre_catch(s) = avg(x(~(x.exclude | ~x.catchTrial | x.cue == sdc),:).reactionTime,avgtype);
    exp.pre.norm.left(s) = avg(x(~(x.exclude | x.catchTrial) & x.cue == 1,:).reactionTime,avgtype);
    exp.pre.norm.right(s) = avg(x(~(x.exclude | x.catchTrial) & x.cue == 2,:).reactionTime,avgtype);
    exp.pre.catch.left(s) = avg(x(~x.exclude & x.catchTrial & x.cue == 1,:).reactionTime,avgtype);
    exp.pre.catch.right(s) = avg(x(~x.exclude & x.catchTrial & x.cue == 2,:).reactionTime,avgtype);
    
    % --- day2 only the pre runs / no catch trials
    if order(s) == 2
        day2.left(s) = avg(x(~(x.exclude | x.catchTrial) & x.cue == 1,:).reactionTime,avgtype);
        day2.right(s) = avg(x(~(x.exclude | x.catchTrial) & x.cue == 2,:).reactionTime,avgtype);
        day2.left_catch(s) = avg(x(~(x.exclude) & x.catchTrial & x.cue == 1,:).reactionTime,avgtype);
        day2.right_catch(s) = avg(x(~(x.exclude) & x.catchTrial & x.cue == 2,:).reactionTime,avgtype);
    else
        day3.left(s) = avg(x(~(x.exclude | x.catchTrial) & x.cue == 1,:).reactionTime,avgtype);
        day3.right(s) = avg(x(~(x.exclude | x.catchTrial) & x.cue == 2,:).reactionTime,avgtype);
        day3.left_catch(s) = avg(x(~(x.exclude) & x.catchTrial & x.cue == 1,:).reactionTime,avgtype);
        day3.right_catch(s) = avg(x(~(x.exclude) & x.catchTrial & x.cue == 2,:).reactionTime,avgtype);
    end
    
    x = RT.B.real.post{sub};
    if isempty(setdiff(x.cue,cues))
        sdc = 0;
    else
        sdc = setdiff(x.cue,cues);
    end
    Exp_post_norm(s) = avg(x(~(x.exclude | x.catchTrial | x.cue == sdc),:).reactionTime,avgtype);
    Exp_post_catch(s) = avg(x(~(x.exclude | ~x.catchTrial | x.cue == sdc),:).reactionTime,avgtype);
    exp.post.norm.left(s) = avg(x(~(x.exclude | x.catchTrial) & x.cue == 1,:).reactionTime,avgtype);
    exp.post.norm.right(s) = avg(x(~(x.exclude | x.catchTrial) & x.cue == 2,:).reactionTime,avgtype);
    exp.post.catch.left(s) = avg(x(~x.exclude & x.catchTrial & x.cue == 1,:).reactionTime,avgtype);
    exp.post.catch.right(s) = avg(x(~x.exclude & x.catchTrial & x.cue == 2,:).reactionTime,avgtype);
    
    % --- day2 only the post runs / no catch trials
    if order(s) == 2
        day2_post.left(s) = avg(x(~(x.exclude | x.catchTrial) & x.cue == 1,:).reactionTime,avgtype);
        day2_post.right(s) = avg(x(~(x.exclude | x.catchTrial) & x.cue == 2,:).reactionTime,avgtype);
        day2_post.left_catch(s) = avg(x(~(x.exclude) & x.catchTrial & x.cue == 1,:).reactionTime,avgtype);
        day2_post.right_catch(s) = avg(x(~(x.exclude) & x.catchTrial & x.cue == 2,:).reactionTime,avgtype);
    else
        day3_post.left(s) = avg(x(~(x.exclude | x.catchTrial) & x.cue == 1,:).reactionTime,avgtype);
        day3_post.right(s) = avg(x(~(x.exclude | x.catchTrial) & x.cue == 2,:).reactionTime,avgtype);
        day3_post.left_catch(s) = avg(x(~(x.exclude) & x.catchTrial & x.cue == 1,:).reactionTime,avgtype);
        day3_post.right_catch(s) = avg(x(~(x.exclude) & x.catchTrial & x.cue == 2,:).reactionTime,avgtype);
    end
    
    x = RT.B.sham.pre{sub};
    if isempty(setdiff(x.cue,cues))
        sdc = 0;
    else
        sdc = setdiff(x.cue,cues);
    end
    Sham_pre_norm(s) = avg(x(~(x.exclude | x.catchTrial | x.cue == sdc),:).reactionTime,avgtype);
    Sham_pre_catch(s) = avg(x(~(x.exclude | ~x.catchTrial | x.cue == sdc),:).reactionTime,avgtype);
    sham.pre.norm.left(s) = avg(x(~(x.exclude | x.catchTrial) & x.cue == 1,:).reactionTime,avgtype);
    sham.pre.norm.right(s) = avg(x(~(x.exclude | x.catchTrial) & x.cue == 2,:).reactionTime,avgtype);
    sham.pre.catch.left(s) = avg(x(~x.exclude & x.catchTrial & x.cue == 1,:).reactionTime,avgtype);
    sham.pre.catch.right(s) = avg(x(~x.exclude & x.catchTrial & x.cue == 2,:).reactionTime,avgtype);
    
    if order(s) == 3
        day2.left(s) = avg(x(~(x.exclude | x.catchTrial) & x.cue == 1,:).reactionTime,avgtype);
        day2.right(s) = avg(x(~(x.exclude | x.catchTrial) & x.cue == 2,:).reactionTime,avgtype);
        day2.left_catch(s) = avg(x(~(x.exclude) & x.catchTrial & x.cue == 1,:).reactionTime,avgtype);
        day2.right_catch(s) = avg(x(~(x.exclude) & x.catchTrial & x.cue == 2,:).reactionTime,avgtype);
    else
        day3.left(s) = avg(x(~(x.exclude | x.catchTrial) & x.cue == 1,:).reactionTime,avgtype);
        day3.right(s) = avg(x(~(x.exclude | x.catchTrial) & x.cue == 2,:).reactionTime,avgtype);
        day3.left_catch(s) = avg(x(~(x.exclude) & x.catchTrial & x.cue == 1,:).reactionTime,avgtype);
        day3.right_catch(s) = avg(x(~(x.exclude) & x.catchTrial & x.cue == 2,:).reactionTime,avgtype);
    end
    
    
    x = RT.B.sham.post{sub};
    if isempty(setdiff(x.cue,cues))
        sdc = 0;
    else
        sdc = setdiff(x.cue,cues);
    end
    Sham_post_norm(s) = avg(x(~(x.exclude | x.catchTrial | x.cue == sdc),:).reactionTime,avgtype);
    Sham_post_catch(s) = avg(x(~(x.exclude | ~x.catchTrial | x.cue == sdc),:).reactionTime,avgtype);
    sham.post.norm.left(s) = avg(x(~(x.exclude | x.catchTrial) & x.cue == 1,:).reactionTime,avgtype);
    sham.post.norm.right(s) = avg(x(~(x.exclude | x.catchTrial) & x.cue == 2,:).reactionTime,avgtype);
    sham.post.catch.left(s) = avg(x(~x.exclude & x.catchTrial & x.cue == 1,:).reactionTime,avgtype);
    sham.post.catch.right(s) = avg(x(~x.exclude & x.catchTrial & x.cue == 2,:).reactionTime,avgtype);
    
    if order(s) == 3
        day2_post.left(s) = avg(x(~(x.exclude | x.catchTrial) & x.cue == 1,:).reactionTime,avgtype);
        day2_post.right(s) = avg(x(~(x.exclude | x.catchTrial) & x.cue == 2,:).reactionTime,avgtype);
        day2_post.left_catch(s) = avg(x(~(x.exclude) & x.catchTrial & x.cue == 1,:).reactionTime,avgtype);
        day2_post.right_catch(s) = avg(x(~(x.exclude) & x.catchTrial & x.cue == 2,:).reactionTime,avgtype);
    else
        day3_post.left(s) = avg(x(~(x.exclude | x.catchTrial) & x.cue == 1,:).reactionTime,avgtype);
        day3_post.right(s) = avg(x(~(x.exclude | x.catchTrial) & x.cue == 2,:).reactionTime,avgtype);
        day3_post.left_catch(s) = avg(x(~(x.exclude) & x.catchTrial & x.cue == 1,:).reactionTime,avgtype);
        day3_post.right_catch(s) = avg(x(~(x.exclude) & x.catchTrial & x.cue == 2,:).reactionTime,avgtype);
    end
    
    % --- Chinchilla
    x = RT.C.real{sub};
    if isempty(setdiff(x.cue,cues))
        sdc = 0;
    else
        sdc = setdiff(x.cue,cues);
    end
    chincRT.real.all(s) = avg(x(~(x.exclude | x.catchTrial | x.cue == sdc),:).reactionTime,avgtype);
    chincRT.real.left(s) = avg(x(~(x.exclude | x.catchTrial) & x.cue == 1,:).reactionTime,avgtype);
    chincRT.real.right(s) = avg(x(~(x.exclude | x.catchTrial) & x.cue == 2,:).reactionTime,avgtype);
    
    x = RT.C.sham{sub};
    if isempty(setdiff(x.cue,cues))
        sdc = 0;
    else
        sdc = setdiff(x.cue,cues);
    end
    chincRT.sham.all(s) = avg(x(~(x.exclude | x.catchTrial | x.cue == sdc),:).reactionTime,avgtype);
    chincRT.sham.left(s) = avg(x(~(x.exclude | x.catchTrial) & x.cue == 1,:).reactionTime,avgtype);
    chincRT.sham.right(s) = avg(x(~(x.exclude | x.catchTrial) & x.cue == 2,:).reactionTime,avgtype);

end

day1.diff = day1.right - day1.left;
day2.diff = day2.right - day2.left;
day3.diff = day3.right - day3.left;

LRdiff = [day1.diff;day2.diff;day3.diff];
hist(LRdiff(:))

%% Write to file
left = day1.left';
center = day1.center';
right = day1.right';
varnames = {'left','center','right'};
T = table(left,center,right,'VariableNames',varnames);

writetable(T,'RTday14JASP')

%% Compute and write to file
Exp_pre_diff = Exp_pre_catch - Exp_pre_norm;
Exp_post_diff = Exp_post_catch - Exp_post_norm;
Sham_pre_diff = Sham_pre_catch - Sham_pre_norm;
Sham_post_diff = Sham_post_catch - Sham_post_norm;

% Exp_pre_diff = Exp_pre_norm;
% Exp_post_diff = Exp_post_norm;
% Sham_pre_diff = Sham_pre_norm;
% Sham_post_diff = Sham_post_norm;

% Exp_pre_diff = Exp_pre_catch;
% Exp_post_diff = Exp_post_catch;
% Sham_pre_diff = Sham_pre_catch;
% Sham_post_diff = Sham_post_catch;

varnames = {'exp_pre','exp_post','sham_pre','sham_post','day'};
T = table(Exp_pre_diff',Exp_post_diff',Sham_pre_diff',Sham_post_diff',order','VariableNames',varnames);
writetable(T,'RTdiff4JASP')

varnames = {'exp_pre','exp_post','sham_pre','sham_post','day'};
T = table(Exp_pre_norm',Exp_post_norm',Sham_pre_norm',Sham_post_norm',order','VariableNames',varnames);
writetable(T,'RTnorm4JASP')

varnames = {'pre','post','group','day'};
pre = [Exp_pre_norm';Sham_pre_norm'];
post = [Exp_post_norm';Sham_post_norm'];
group = [zeros(size(Exp_pre_norm'));ones(size(Sham_pre_norm'))];
orderx = [order';order'];
T = table(pre,post,group,orderx,'VariableNames',varnames);
writetable(T,'RTdiff4ANCOVA')

% varnames = {'pre','post','group','day'};
% pre = [Exp_pre_diff';Sham_pre_diff'];
% post = [Exp_post_diff';Sham_post_diff'];
% group = [zeros(size(Exp_pre_diff'));ones(size(Sham_pre_diff'))];
% orderx = [order';order'];
% T = table(pre,post,group,orderx,'VariableNames',varnames);
% writetable(T,'RTdiff4ANCOVA')

figure;
subplot(1,3,1)
plot([Exp_pre_diff;Exp_post_diff],'k');
hold on;
scatter([Exp_pre_diff.*0 + 1, Exp_post_diff*0 + 2], [Exp_pre_diff,Exp_post_diff],...
    'ok','filled','MarkerEdgeColor',[0 0 0],'MarkerFaceColor',[0.7 0.7 0.7]);
xlim([0.8,2.2])
xticks([1,2])
xticklabels({'pre','post'});
ylabel('validity effect (VE)')
ylim([0,0.4])
title('Experimental feedback')

subplot(1,3,2)
plot([Sham_pre_diff;Sham_post_diff],'k');
hold on;
scatter([Sham_pre_diff.*0 + 1, Sham_post_diff*0 + 2], [Sham_pre_diff,Sham_post_diff],...
    'ok','filled','MarkerEdgeColor',[0 0 0],'MarkerFaceColor',[0.7 0.7 0.7]);
xlim([0.8,2.2])
xticks([1,2])
xticklabels({'pre','post'});
ylabel('validity effect (VE)')
ylim([0,0.4])
title('Sham feedback')

subplot(1,3,3)
a = Exp_post_diff-Exp_pre_diff;
b = Sham_post_diff-Sham_pre_diff;
plot([a;b],'k');
hold on;
scatter([a.*0 + 1, b*0 + 2], [a,b],...
    'ok','filled','MarkerEdgeColor',[0 0 0],'MarkerFaceColor',[0.7 0.7 0.7]);
xlim([0.8,2.2])
xticks([1,2])
xticklabels({'experimental','sham'});
ylabel('validity effect (VE)')
% ylim([0,0.4])
title('Pre-Post distance')


%% Write to file
left_catch = cat(1,day2.left_catch',day3.left_catch');
left_norm = cat(1,day2.left',day3.left');
right_catch = cat(1,day2.right_catch',day3.right_catch');
right_norm = cat(1,day2.right',day3.right');
varnames = {'left_catch','left','right','right_catch'};
T = table(left_catch,left_norm,right_norm,right_catch,'VariableNames',varnames);

writetable(T,'RTday234JASP')

%% Compute and write to file

varnames = {'day1_left','day1_right','day2_left','day2_right','day3_left','day3_right',...
    'day2_left_catch','day2_right_catch','day3_left_catch','day3_right_catch'};
T = table(day1.left(:),day1.right(:),day2.left(:),day2.right(:),day3.left(:),day3.right(:),...
    day2.left_catch(:),day2.right_catch(:),day3.left_catch(:),day3.right_catch(:),'VariableNames',varnames);
writetable(T,'RTlr4JASP')

    % --- post-hoc tests
    disp('Bonferroni corrected p-value for significance = 0.0125');
    [~,p1,~,stat] = ttest(mean([day1.left(:),day1.right(:)],2),mean([day2.left(:),day2.right(:)],2));
    disp(['t-test Exp pre-post: p = ',num2str(round(p1,3)),', t = ',num2str(round(stat.tstat,3))]);
    [~,p2,~,stat] = ttest(mean([day1.left(:),day1.right(:)],2),mean([day3.left(:),day3.right(:)],2));
    disp(['t-test Sham pre-post: p = ',num2str(round(p2,3)),', t = ',num2str(round(stat.tstat,3))]);
    [~,p3,~,stat] = ttest(mean([day2.left(:),day2.right(:)],2),mean([day3.left(:),day3.right(:)],2));
    disp(['t-test pre Exp-Sham: p = ',num2str(round(p3,3)),', t = ',num2str(round(stat.tstat,3))]);
    
    disp('False discovery rate correction for multiple comparisons')
    [h, crit_p, adj_ci_cvrg, adj_p] = fdr_bh([p1,p2,p3],0.05)


dRl(order==2) = day2.left(order==2);
dRl(order==3) = day3.left(order==3);

dRr(order==2) = day2.right(order==2);
dRr(order==3) = day3.right(order==3);

dSl(order==3) = day2.left(order==3);
dSl(order==2) = day3.left(order==2);

dSr(order==3) = day2.right(order==3);
dSr(order==2) = day3.right(order==2);

varnames = {'day1_left','day1_right','exp_left','exp_right','sham_left','sham_right'};
T = table(day1.left(:),day1.right(:),dRl(:),dRr(:),dSl(:),dSr(:),'VariableNames',varnames);
writetable(T,'RTlrES4JASP')

% [~,p] = ttest(day1.left,day1.right)
% [~,p] = ttest(day2.left,day2.right)
% [~,p] = ttest(day3.left,day3.right)

%% Left-right differences
LRD.day1 = day1.left(:) - day1.right(:);
LRD.day2 = day2.left(:) - day2.right(:);
LRD.day3 = day3.left(:) - day3.right(:);
LRD.exp = dRl(:) - dRr(:);
LRD.sham = dSl(:) - dSr(:);
varnames = {'day1','day2','day3','exp','sham'};
T = table(LRD.day1,LRD.day2,LRD.day3,LRD.exp,LRD.sham,'VariableNames',varnames);
writetable(T,'RTlrdiffJASP')

mean(mean([LRD.day1,LRD.day2,LRD.day3]))
mean(std([LRD.day1,LRD.day2,LRD.day3]))

%% RT changes over time
LRD.day1 = mean([day1.left(:),day1.right(:)],2);
LRD.day2 = mean([day2.left(:),day2.right(:)],2);
LRD.day3 = mean([day3.left(:),day3.right(:)],2);
varnames = {'day1','day2','day3'};
T = table(LRD.day1,LRD.day2,LRD.day3,'VariableNames',varnames);
writetable(T,'RTdaysJASP')

%% validity effect
VE.day2.left = day2.left_catch(:) - day2.left(:);
VE.day2.right = day2.right_catch(:) - day2.right(:);
VE.day3.left = day3.left_catch(:) - day3.left(:);
VE.day3.right = day3.right_catch(:) - day3.right(:);
varnames = {'VEday2L','VEday2R','VEday3L','VEday3R'};
T = table(VE.day2.left,VE.day2.right,VE.day3.left,VE.day3.right,'VariableNames',varnames);
writetable(T,'RTVE23JASP')

mean(mean([LRD.day1,LRD.day2,LRD.day3]))
mean(std([LRD.day1,LRD.day2,LRD.day3]))

VE.day2_post.left = day2_post.left_catch(:) - day2_post.left(:);
VE.day2_post.right = day2_post.right_catch(:) - day2_post.right(:);
VE.day3_post.left = day3_post.left_catch(:) - day3_post.left(:);
VE.day3_post.right = day3_post.right_catch(:) - day3_post.right(:);


%% measures for paper

% centerVSlr = nanmean((day1.left+day1.right)/2 - day1.center);
% nanstd((day1.left+day1.right)/2 - day1.center)
% catchVSnot = mean(nanmean([left_catch-left_norm,right_catch-right_norm]));
% mean(nanstd([left_catch-left_norm,right_catch-right_norm]))
% 
% mean(Exp_post_norm - Exp_pre_norm)
% std(Exp_post_norm - Exp_pre_norm)
% 
% mean(Sham_post_norm - Sham_pre_norm)
% std(Sham_post_norm - Sham_pre_norm)
% 
% mean(Exp_post_catch - Exp_pre_catch)
% std(Exp_post_catch - Exp_pre_catch)
% 
% mean(Sham_post_catch - Sham_pre_catch)
% std(Sham_post_catch - Sham_pre_catch)


%% Plot

shift = 0.05;
gray = [0.4,0.4,0.4];

fig = figure('Position', get(0, 'Screensize'));
set(gcf,'color','w');

subplot(2,2,4)
hold on;
grid on;
x = 1:3;
y1 = [nanmean(day1.left),nanmean(day2.left),nanmean(day3.left)];
y2 = [nanmean(day1.right),nanmean(day2.right),nanmean(day3.right)];
s1 = [nanstd(day1.left),nanstd(day2.left),nanstd(day3.left)]/sqrt(length(day1.left));
s2 = [nanstd(day1.right),nanstd(day2.right),nanstd(day3.right)]/sqrt(length(day1.right));
errorbar(x-shift,y1,s1,'LineWidth',1,'Color',gray,'Marker','<','markers',12,'MarkerFaceColor',gray)
errorbar(x+shift,y2,s2,'LineWidth',1,'Color','k','Marker','>','markers',12,'MarkerFaceColor','k')
xlim([0.8,3.2])
xticks([1,2,3])
ylabel('reaction time [s]')
xticklabels({'day 1','day 2 pre','day 3 pre'})

x = 2:3;
y1 = [nanmean(day2.left_catch),nanmean(day3.left_catch)];
y2 = [nanmean(day2.right_catch),nanmean(day3.right_catch)];
s1 = [nanstd(day2.left_catch),nanstd(day3.left_catch)]/sqrt(length(day1.left));
s2 = [nanstd(day2.right_catch),nanstd(day3.right_catch)]/sqrt(length(day1.right));
errorbar(x-shift,y1,s1,'LineWidth',1,'Color',gray,'Marker','<','markers',12,'MarkerFaceColor',gray,'LineStyle','--')
errorbar(x+shift,y2,s2,'LineWidth',1,'Color','k','Marker','>','markers',12,'MarkerFaceColor','k','LineStyle','--')

legend({'left valid','right valid','left invalid','right invalid'},'Location','northwest')
title('D  Left and Right Side')
set(gca, 'FontSize', 16)
% axis('square')
ylim([0.33,0.56]);

% figure;
subplot(2,2,3)
hold on;
grid on;
x = 1:2;
y1 = [nanmean(Exp_pre_norm),nanmean(Exp_post_norm)];
y2 = [nanmean(Sham_pre_norm),nanmean(Sham_post_norm)];
s1 = [nanstd(Exp_pre_norm),nanstd(Exp_post_norm)]/sqrt(length(Exp_pre_norm));
s2 = [nanstd(Sham_pre_norm),nanstd(Sham_post_norm)]/sqrt(length(Exp_pre_norm));
% y1 = [median(Exp_pre_norm),median(Exp_post_norm)];
% y2 = [median(Sham_pre_norm),median(Sham_post_norm)];
% s1 = [mad(Exp_pre_norm),mad(Exp_post_norm)]/sqrt(length(Exp_pre_norm));
% s2 = [mad(Sham_pre_norm),mad(Sham_post_norm)]/sqrt(length(Exp_pre_norm));
errorbar(x-shift,y1,s1,'LineWidth',1,'Color','k','Marker','square','markers',12,'MarkerFaceColor','w')
errorbar(x+shift,y2,s2,'LineWidth',1,'Color','k','Marker','square','markers',12,'MarkerFaceColor','k')

y1 = [nanmean(Exp_pre_catch),nanmean(Exp_post_catch)];
y2 = [nanmean(Sham_pre_catch),nanmean(Sham_post_catch)];
s1 = [nanstd(Exp_pre_catch),nanstd(Exp_post_catch)]/sqrt(length(Exp_pre_norm));
s2 = [nanstd(Sham_pre_catch),nanstd(Sham_post_catch)]/sqrt(length(Exp_pre_norm));
% y1 = [median(Exp_pre_catch),median(Exp_post_catch)];
% y2 = [median(Sham_pre_catch),median(Sham_post_catch)];
% s1 = [mad(Exp_pre_catch),mad(Exp_post_catch)]/sqrt(length(Exp_pre_norm));
% s2 = [mad(Sham_pre_catch),mad(Sham_post_catch)]/sqrt(length(Exp_pre_norm));
errorbar(x-shift,y1,s1,'LineWidth',1,'Color','k','Marker','diamond','markers',12,'MarkerFaceColor','w')
errorbar(x+shift,y2,s2,'LineWidth',1,'Color','k','Marker','diamond','markers',12,'MarkerFaceColor','k')
xlim([0.8,2.2])
xticks([1,2])
ylim([0.33,0.56]);
ylabel('reaction time [s]')
xticklabels({'pre','post'})
legend({'Exp. valid','Sham valid','Exp. invalid','Sham invalid'},'Location','best')
title('C  Valid and Invalid Cue')
set(gca, 'FontSize', 16)
% axis('square')

subplot(2,2,1)
hold on;
grid on;
x = 1:2;
y1 = [nanmean(Exp_pre_diff),nanmean(Exp_post_diff)];
y2 = [nanmean(Sham_pre_diff),nanmean(Sham_post_diff)];
s1 = [nanstd(Exp_pre_diff),nanstd(Exp_post_diff)]/sqrt(length(Exp_pre_norm));
s2 = [nanstd(Sham_pre_diff),nanstd(Sham_post_diff)]/sqrt(length(Exp_pre_norm));
% y1 = [median(Exp_pre_diff),median(Exp_post_diff)];
% y2 = [median(Sham_pre_diff),median(Sham_post_diff)];
% s1 = [mad(Exp_pre_diff),mad(Exp_post_diff)]/sqrt(length(Exp_pre_norm));
% s2 = [mad(Sham_pre_diff),mad(Sham_post_diff)]/sqrt(length(Exp_pre_norm));
[~,p1] = ttest(Exp_pre_diff,Exp_post_diff);
[~,p2] = ttest(Sham_pre_diff,Sham_post_diff);
scatter(x-shift,y1,100,'o','filled','MarkerFaceColor','w','MarkerEdgeColor','k')
scatter(x+shift,y2,100,'o','filled','MarkerFaceColor','k','MarkerEdgeColor','k')
scatter(NaN,NaN,'*k')
line(nan, nan, 'Linestyle', 'none', 'Marker', 'none', 'Color', 'none');
% errorbar(x-shift,y1,s1,'LineWidth',1,'Color','k','Marker','o','markers',12,'MarkerFaceColor','w')
% errorbar(x+shift,y2,s2,'LineWidth',1,'Color','k','Marker','o','markers',12,'MarkerFaceColor','k')
errorbar(x-shift,y1,s1,'LineWidth',1,'Color','k','Marker','o')
errorbar(x+shift,y2,s2,'LineWidth',1,'Color','k','Marker','o')
% plot(x-shift,y1,'Marker','o','markers',12,'MarkerFaceColor','w')
scatter(x-shift,y1,100,'o','filled','MarkerFaceColor','w','MarkerEdgeColor','k')
scatter(x+shift,y2,100,'o','filled','MarkerFaceColor','k','MarkerEdgeColor','k')
H = sigstar({[1,2]-shift},0.0321,0,'down');
xlim([0.8,2.2])
xticks([1,2])
xticklabels({'pre','post'})
ylabel('RT invalid - RT valid [s]')
ylabel('Validity effect [s]')
legend({'Experimental','Sham','p < .05'},'Location','northwest')
title('A  Validity Effect')
set(gca, 'FontSize', 16)
% axis('square')

subplot(2,2,2)
b = (Exp_post_diff-Exp_pre_diff);
a = (Sham_post_diff-Sham_pre_diff);
plot([a;b],'Color',gray);
hold on;
scatter(a.*0 + 1, a,...
    'ok','filled','MarkerEdgeColor',[0 0 0],'MarkerFaceColor','k');
scatter(b*0 + 2, b,...
    'ok','filled','MarkerEdgeColor',[0 0 0],'MarkerFaceColor','w');
xlim([0.8,2.2])
xticks([1,2])
xticklabels({'sham','experimental'});
ylabel('post - pre validity effect [s]')
title('B  Subject changes in VE')
% axis('square')
ax = gca;
ax.YGrid = 'on';
set(gca, 'FontSize', 16)
box('off')

% cleanfigure;
% matlab2tikz('C:\Users\cschneid\Dropbox\PaperCVSA1\LaTeX\figs\testfile.tex')

%%
plotfontsize = 14;

figure;
% title('A  Validity Effect')
set(gcf,'color','w');
axis('square')
hold on;
% grid on;

x = 1:2;
y1 = [nanmean(Exp_pre_diff),nanmean(Exp_post_diff)];
y2 = [nanmean(Sham_pre_diff),nanmean(Sham_post_diff)];
s1 = [nanstd(Exp_pre_diff),nanstd(Exp_post_diff)]/sqrt(length(Exp_pre_norm));
s2 = [nanstd(Sham_pre_diff),nanstd(Sham_post_diff)]/sqrt(length(Exp_pre_norm));
[~,p1] = ttest(Exp_pre_diff,Exp_post_diff);
[~,p2] = ttest(Sham_pre_diff,Sham_post_diff);
scatter(x-shift,y1,100,'o','filled','MarkerFaceColor','w','MarkerEdgeColor','k')
scatter(x+shift,y2,100,'o','filled','MarkerFaceColor','k','MarkerEdgeColor','k')
scatter(x-shift,y1,'*k')
errorbar(x-shift,y1,s1,'LineWidth',1,'Color','k','Marker','o')
errorbar(x+shift,y2,s2,'LineWidth',1,'Color','k','Marker','o')
scatter(x-shift,y1,500,'o','filled','MarkerFaceColor','w','MarkerEdgeColor','k')
scatter(x+shift,y2,500,'o','filled','MarkerFaceColor','k','MarkerEdgeColor','k')
H = sigstar({[1,2]-shift},0.0321,0,'down');
axis('square')

xlim([0.6,2.4])
xticks([1,2])
xticklabels({'pre','post'})
set(gca,'Ytick',0:0.02:1)
ylabel('Validity effect [s]')
% legend({'Experimental','Sham','p < .05'},'Location','northwest')
legend({'Experimental','Sham'},'Location','northwest')
legend boxoff

set(gca, 'FontSize', plotfontsize)

cleanfigure;
strx = strcat('yticklabel style={/pgf/number format/fixed,/pgf/number format/precision=5},scaled y ticks=false', ...
    ',legend image post style={scale=1},legend style = {legend cell align=left, font=\Large}',...
    ',major grid style={gray!20}',...
    ',label style={font=\Large}',...
    ',tick label style={font=\Large}');
matlab2tikz('C:\Users\cschneid\Dropbox\PaperCVSA1\LaTeX\figs\interactionVE.tex',...
    'extraAxisOptions',strx);

% -------------------------------------------------------------------------
figure;
% title('B  Subject changes in VE')
set(gcf,'color','w');
axis('square')
hold on;
% ax = gca;
% ax.YGrid = 'on';

a = (Sham_post_diff-Sham_pre_diff);
b = (Exp_post_diff-Exp_pre_diff);
scatter(b*0 + 2, b, 100,'o','filled','MarkerEdgeColor','k','MarkerFaceColor','w');
scatter(a.*0 + 1, a, 100,'o','filled','MarkerEdgeColor','k','MarkerFaceColor','k');
plot([0.8,2.2],[0,0],'k')
plot([a;b],'Color',gray);
scatter(a.*0 + 1, a, 100,'o','filled','MarkerEdgeColor','k','MarkerFaceColor','k');
scatter(b*0 + 2, b, 100,'o','filled','MarkerEdgeColor','k','MarkerFaceColor','w');

xlim([0.6,2.4])
xticks([1,2])
xticklabels({'sham','experimental'});
set(gca, 'FontSize', plotfontsize)
ylabel('pre - post change in VE [s]')

legend({'Experimental','Sham'},'Location','northwest','Orientation','vertical')
legend boxoff

cleanfigure;
strx = strcat('yticklabel style={/pgf/number format/fixed,/pgf/number format/precision=5},scaled y ticks=false', ...
    ',legend image post style={scale=1},legend style = {legend cell align=left, font=\Large}',...
    ',major grid style={gray!20}',...
    ',label style={font=\Large}',...
    ',tick label style={font=\Large}');
matlab2tikz('C:\Users\cschneid\Dropbox\PaperCVSA1\LaTeX\figs\prepostindividual.tex',...
    'extraAxisOptions',strx);

% -------------------------------------------------------------------------
figure;
% title('C  Valid and Invalid Cue')
set(gcf,'color','w');
axis('square')
hold on;
% grid on;

x = 1:2;
y1 = [nanmean(Exp_pre_norm),nanmean(Exp_post_norm)];
y2 = [nanmean(Sham_pre_norm),nanmean(Sham_post_norm)];
y3 = [nanmean(Exp_pre_catch),nanmean(Exp_post_catch)];
y4 = [nanmean(Sham_pre_catch),nanmean(Sham_post_catch)];
s1 = [nanstd(Exp_pre_norm),nanstd(Exp_post_norm)]/sqrt(length(Exp_pre_norm));
s2 = [nanstd(Sham_pre_norm),nanstd(Sham_post_norm)]/sqrt(length(Exp_pre_norm));
s3 = [nanstd(Exp_pre_catch),nanstd(Exp_post_catch)]/sqrt(length(Exp_pre_norm));
s4 = [nanstd(Sham_pre_catch),nanstd(Sham_post_catch)]/sqrt(length(Exp_pre_norm));

% dummy data for legend
scatter(x-shift,y1, 100,'square','filled','MarkerEdgeColor','k','MarkerFaceColor','w');
scatter(x+shift,y2, 100,'square','filled','MarkerEdgeColor','k','MarkerFaceColor','k');
scatter(x-shift,y3, 70,'diamond','filled','MarkerEdgeColor','k','MarkerFaceColor','w');
scatter(x+shift,y4, 70,'diamond','filled','MarkerEdgeColor','k','MarkerFaceColor','k');

% plot real data with error bars
errorbar(x-shift,y1,s1,'LineWidth',1,'Color','k','Marker','square','markers',5,'MarkerFaceColor','w')
errorbar(x+shift,y2,s2,'LineWidth',1,'Color','k','Marker','square','markers',5,'MarkerFaceColor','k')
errorbar(x-shift,y3,s3,'LineWidth',1,'Color','k','Marker','diamond','markers',5,'MarkerFaceColor','w')
errorbar(x+shift,y4,s4,'LineWidth',1,'Color','k','Marker','diamond','markers',5,'MarkerFaceColor','k')

% plot markers again for tikz
scatter(x-shift,y1, 500,'square','filled','MarkerEdgeColor','k','MarkerFaceColor','w');
scatter(x+shift,y2, 500,'square','filled','MarkerEdgeColor','k','MarkerFaceColor','k');
scatter(x-shift,y3, 200,'diamond','filled','MarkerEdgeColor','k','MarkerFaceColor','w');
scatter(x+shift,y4, 200,'diamond','filled','MarkerEdgeColor','k','MarkerFaceColor','k');


xlim([0.6,2.4])
xticks([1,2])
xticklabels({'pre','post'})
ylim([0.33,0.6]);
ylabel('reaction time [s]')
set(gca,'Ytick',-1:0.05:2)

% lgd = legend({'Exp invalid ','Sham invalid ','Exp valid ','Sham valid '},'Location','southoutside','Orientation','horizontal');
% lgd.Position = lgd.Position + [-0.1,-0.3,0,0];
legend({'Exp valid','Sham valid','Exp invalid','Sham invalid'},'Location','northwest')
legend boxoff
set(gca, 'FontSize', plotfontsize)


cleanfigure;
strx = strcat('yticklabel style={/pgf/number format/fixed,/pgf/number format/precision=5},scaled y ticks=false', ...
    ',legend image post style={scale=1},legend style = {legend cell align=left, font=\Large}',...
    ',major grid style={gray!20}',...
    ',label style={font=\Large}',...
    ',tick label style={font=\Large}');
matlab2tikz('C:\Users\cschneid\Dropbox\PaperCVSA1\LaTeX\figs\RTs.tex',...
    'extraAxisOptions',strx);

% -------------------------------------------------------------------------
figure;
% title('D  Left and Right Side')
set(gcf,'color','w');
axis('square')
hold on;
% grid on;

x = 1:3;
y1 = [nanmean(day1.left),nanmean(day2.left),nanmean(day3.left)];
y2 = [nanmean(day1.right),nanmean(day2.right),nanmean(day3.right)];
y3 = [NaN,nanmean(day2.left_catch),nanmean(day3.left_catch)];
y4 = [NaN,nanmean(day2.right_catch),nanmean(day3.right_catch)];
s1 = [nanstd(day1.left),nanstd(day2.left),nanstd(day3.left)]/sqrt(length(day1.left));
s2 = [nanstd(day1.right),nanstd(day2.right),nanstd(day3.right)]/sqrt(length(day1.right));
s3 = [NaN,nanstd(day2.left_catch),nanstd(day3.left_catch)]/sqrt(length(day1.left));
s4 = [NaN,nanstd(day2.right_catch),nanstd(day3.right_catch)]/sqrt(length(day1.right));

% dummy data for legend
scatter(x-shift,y1, 70,'<','filled','MarkerEdgeColor',gray,'MarkerFaceColor',gray);
scatter(x+shift,y2, 70,'>','filled','MarkerEdgeColor','k','MarkerFaceColor','k');
scatter(x-shift,y3, 100,'<','filled','MarkerEdgeColor',gray,'MarkerFaceColor','w');
scatter(x+shift,y4, 100,'>','filled','MarkerEdgeColor','k','MarkerFaceColor','w');

% valid trials
errorbar(x-shift,y1,s1,'LineWidth',1,'Color',gray,'Marker','<','markers',12,'MarkerFaceColor',gray)
errorbar(x+shift,y2,s2,'LineWidth',1,'Color','k','Marker','>','markers',12,'MarkerFaceColor','k')
% invalid trials
errorbar(x-shift,y3,s3,'LineWidth',1,'Color',gray,'Marker','<','markers',12,'MarkerFaceColor',gray,'LineStyle','-')
errorbar(x+shift,y4,s4,'LineWidth',1,'Color','k','Marker','>','markers',12,'MarkerFaceColor','k','LineStyle','-')

% plot markers again for tikz
scatter(x-shift,y1, 1000,'<','filled','MarkerEdgeColor',gray,'MarkerFaceColor',gray,'LineWidth',1);
scatter(x+shift,y2, 1000,'>','filled','MarkerEdgeColor','k','MarkerFaceColor','k','LineWidth',1);
scatter(x-shift,y3, 1000,'<','filled','MarkerEdgeColor',gray,'MarkerFaceColor','w','LineWidth',2);
scatter(x+shift,y4, 1000,'>','filled','MarkerEdgeColor','k','MarkerFaceColor','w','LineWidth',2);

xlim([0.6,3.4])
xticks([1,2,3])
xticklabels({'day 1','day 2 pre','day 3 pre'})
ylim([0.33,0.6]);
ylabel('reaction time [s]')
set(gca,'Ytick',-1:0.05:2)

% lgd = legend({'left valid','right valid','left invalid','right invalid'},'Location','southoutside','Orientation','horizontal');
% lgd.Position = lgd.Position + [-0.1,-0.3,0,0];
% legendmarkeradjust(20)
legend({'left valid','right valid','left invalid','right invalid'},'Location','northwest')
legend boxoff
set(gca, 'FontSize', plotfontsize)

cleanfigure;
strx = strcat('yticklabel style={/pgf/number format/fixed,/pgf/number format/precision=5},scaled y ticks=false', ...
    ',legend image post style={scale=3},legend style = {legend cell align=left, font=\Large}',...
    ',major grid style={gray!20}',...
    ',label style={font=\Large}',...
    ',tick label style={font=\Large}');
matlab2tikz('C:\Users\cschneid\Dropbox\PaperCVSA1\LaTeX\figs\RT_LR.tex',...
    'extraAxisOptions',strx);

%% ji9a o9rjafo9j?j3r9?zarh90 an0jvr 'k a0wj '3
figure;
hold on;
grid on;
x = 1:5;
y1 = [nanmean(day1.left),nanmean(day2.left),nanmean(day2_post.left),nanmean(day3.left),nanmean(day3_post.left)];
y2 = [nanmean(day1.right),nanmean(day2.right),nanmean(day2_post.right),nanmean(day3.right),nanmean(day3_post.right)];
s1 = [nanstd(day1.left),nanstd(day2.left),nanstd(day2_post.left),nanstd(day3.left),nanstd(day3_post.left)]/sqrt(length(day1.left));
s2 = [nanstd(day1.right),nanstd(day2.right),nanstd(day2_post.right),nanstd(day3.right),nanstd(day3_post.right)]/sqrt(length(day1.right));
errorbar(x-shift,y1,s1,'LineWidth',1,'Color',gray,'Marker','<','markers',12,'MarkerFaceColor',gray)
errorbar(x+shift,y2,s2,'LineWidth',1,'Color','k','Marker','>','markers',12,'MarkerFaceColor','k')
% xlim([0.8,3.2])
xticks([1,2,3,4,5])
ylabel('reaction time (RT) [s]')
xticklabels({'day 1','day 2 pre','day 2 post','day 3 pre','day 3 post'})

x = 2:5;
y1 = [nanmean(day2.left_catch),nanmean(day2_post.left_catch),nanmean(day3.left_catch),nanmean(day3_post.left_catch)];
y2 = [nanmean(day2.right_catch),nanmean(day2_post.right_catch),nanmean(day3.right_catch),nanmean(day3_post.right_catch)];
s1 = [nanstd(day2.left_catch),nanstd(day2_post.left_catch),nanstd(day3.left_catch),nanstd(day3_post.left_catch)]/sqrt(length(day1.left));
s2 = [nanstd(day2.right_catch),nanstd(day2_post.right_catch),nanstd(day3.right_catch),nanstd(day3_post.right_catch)]/sqrt(length(day1.right));
errorbar(x-shift,y1,s1,'LineWidth',1,'Color',gray,'Marker','<','markers',12,'MarkerFaceColor',gray,'LineStyle','--')
errorbar(x+shift,y2,s2,'LineWidth',1,'Color','k','Marker','>','markers',12,'MarkerFaceColor','k','LineStyle','--')

legend({'left side valid','right side valid','left side invalid','right side invalid'},'Location','southwest')
title('A  Left and Right Side')
set(gca, 'FontSize', 16)


% Holladrio Hujtulij??????
figure;
hold on;
grid on;
x = 1:5;
y1 = [nanmean(day1.left),nanmean(exp.pre.norm.left),nanmean(exp.post.norm.left),nanmean(sham.pre.norm.left),nanmean(sham.post.norm.left)];
y2 = [nanmean(day1.right),nanmean(exp.pre.norm.right),nanmean(exp.post.norm.right),nanmean(sham.pre.norm.right),nanmean(sham.post.norm.right)];
s1 = [nanstd(day1.left),nanstd(exp.pre.norm.left),nanstd(exp.post.norm.left),nanstd(sham.pre.norm.left),nanstd(sham.post.norm.left)]/sqrt(length(day1.left));
s2 = [nanstd(day1.right),nanstd(exp.pre.norm.right),nanstd(exp.post.norm.right),nanstd(sham.pre.norm.right),nanstd(sham.post.norm.right)]/sqrt(length(day1.right));
errorbar(x-shift,y1,s1,'LineWidth',1,'Color',gray,'Marker','<','markers',12,'MarkerFaceColor',gray)
errorbar(x+shift,y2,s2,'LineWidth',1,'Color','k','Marker','>','markers',12,'MarkerFaceColor','k')
% xlim([0.8,3.2])
xticks([1,2,3,4,5])
ylabel('reaction time (RT) [s]')
xticklabels({'day 1','exp pre','exp post','sham pre','sham post'})

x = 2:5;
y1 = [nanmean(exp.pre.catch.left),nanmean(exp.post.catch.left),nanmean(sham.pre.catch.left),nanmean(sham.post.catch.left)];
y2 = [nanmean(exp.pre.catch.right),nanmean(exp.post.catch.right),nanmean(sham.pre.catch.right),nanmean(sham.post.catch.right)];
s1 = [nanstd(exp.pre.catch.left),nanstd(exp.post.catch.left),nanstd(sham.pre.catch.left),nanstd(sham.post.catch.left)]/sqrt(length(day1.left));
s2 = [nanstd(exp.pre.catch.right),nanstd(exp.post.catch.right),nanstd(sham.pre.catch.right),nanstd(sham.post.catch.right)]/sqrt(length(day1.right));
errorbar(x-shift,y1,s1,'LineWidth',1,'Color',gray,'Marker','<','markers',12,'MarkerFaceColor',gray,'LineStyle','--')
errorbar(x+shift,y2,s2,'LineWidth',1,'Color','k','Marker','>','markers',12,'MarkerFaceColor','k','LineStyle','--')

legend({'left side valid','right side valid','left side invalid','right side invalid'},'Location','southwest')
title('A  Left and Right Side')
set(gca, 'FontSize', 16)

% Validity effect left/right
VE.exp.pre.left = exp.pre.catch.left - exp.pre.norm.left;
VE.exp.pre.right = exp.pre.catch.right - exp.pre.norm.right;
VE.exp.post.left = exp.post.catch.left - exp.post.norm.left;
VE.exp.post.right = exp.post.catch.right - exp.post.norm.right;

VE.sham.pre.left = sham.pre.catch.left - sham.pre.norm.left;
VE.sham.pre.right = sham.pre.catch.right - sham.pre.norm.right;
VE.sham.post.left = sham.post.catch.left - sham.post.norm.left;
VE.sham.post.right = sham.post.catch.right - sham.post.norm.right;

figure;
subplot(1,2,1)
hold on;
grid on;
x = 1:2;
y1 = [nanmean(VE.exp.pre.left),nanmean(VE.exp.post.left)];
y2 = [nanmean(VE.sham.pre.left),nanmean(VE.sham.post.left)];
s1 = [nanstd(VE.exp.pre.left),nanstd(VE.exp.post.left)]/sqrt(length(day1.left));
s2 = [nanstd(VE.sham.pre.left),nanstd(VE.sham.post.left)]/sqrt(length(day1.left));
errorbar(x-shift,y1,s1,'LineWidth',1,'Color',gray,'Marker','<','markers',12,'MarkerFaceColor',gray)
errorbar(x+shift,y2,s2,'LineWidth',1,'Color',gray,'Marker','<','markers',12,'MarkerFaceColor',gray,'LineStyle','--')
ylim([0.1,0.21]);
xticks([1,2])
ylabel('Validity effect [s]')
xticklabels({'pre','post'})
legend({'Exp left side','Sham left side'},'Location','southeast')
title('A  VE Left Side')
set(gca, 'FontSize', 16)

subplot(1,2,2)
hold on;
grid on;
x = 1:2;
y1 = [nanmean(VE.exp.pre.right),nanmean(VE.exp.post.right)];
y2 = [nanmean(VE.sham.pre.right),nanmean(VE.sham.post.right)];
s1 = [nanstd(VE.exp.pre.right),nanstd(VE.exp.post.right)]/sqrt(length(day1.right));
s2 = [nanstd(VE.sham.pre.right),nanstd(VE.sham.post.right)]/sqrt(length(day1.right));
errorbar(x-shift,y1,s1,'LineWidth',1,'Color','k','Marker','>','markers',12,'MarkerFaceColor','k')
errorbar(x+shift,y2,s2,'LineWidth',1,'Color','k','Marker','>','markers',12,'MarkerFaceColor','k','LineStyle','--')
ylim([0.1,0.21]);
xticks([1,2])
ylabel('Validity effect [s]')
xticklabels({'pre','post'})
legend({'Exp right side','Sham right side'},'Location','southeast')
title('A  VE Right Side')
set(gca, 'FontSize', 16)

%% plot changes per group RT valid
figure;
subplot(2,2,2)
hold on;
grid on;
Y_1 = [nanmean(nanmean([day1.left(order==2);day1.right(order==2)],1)),...
    nanmean(nanmean([day2.left(order==2);day2.right(order==2)],1)),...
    nanmean(nanmean([day2_post.left(order==2);day2_post.right(order==2)],1)),...
    nanmean(nanmean([day3.left(order==2);day3.right(order==2)],1)),...
    nanmean(nanmean([day3_post.left(order==2);day3_post.right(order==2)],1))];
SEM_1 = [nanstd(nanmean([day1.left(order==2);day1.right(order==2)],1))./sqrt(length(day1.left(order==2))),...
    nanstd(nanmean([day2.left(order==2);day2.right(order==2)],1))./sqrt(length(day2.left(order==2))),...
    nanstd(nanmean([day2_post.left(order==2);day2_post.right(order==2)],1))./sqrt(length(day2_post.left(order==2))),...
    nanstd(nanmean([day3.left(order==2);day3.right(order==2)],1))./sqrt(length(day3.left(order==2))),...
    nanstd(nanmean([day3_post.left(order==2);day3_post.right(order==2)],1))./sqrt(length(day3_post.left(order==2)))];
Y_2 = [nanmean(nanmean([day1.left(order==3);day1.right(order==3)],1)),...
    nanmean(nanmean([day2.left(order==3);day2.right(order==3)],1)),...
    nanmean(nanmean([day2_post.left(order==3);day2_post.right(order==3)],1)),...
    nanmean(nanmean([day3.left(order==3);day3.right(order==3)],1)),...
    nanmean(nanmean([day3_post.left(order==3);day3_post.right(order==3)],1))];
SEM_2 = [nanstd(nanmean([day1.left(order==3);day1.right(order==3)],1))./sqrt(length(day1.left(order==3))),...
    nanstd(nanmean([day2.left(order==3);day2.right(order==3)],1))./sqrt(length(day2.left(order==3))),...
    nanstd(nanmean([day2_post.left(order==3);day2_post.right(order==3)],1))./sqrt(length(day2_post.left(order==3))),...
    nanstd(nanmean([day3.left(order==3);day3.right(order==3)],1))./sqrt(length(day3.left(order==3))),...
    nanstd(nanmean([day3_post.left(order==3);day3_post.right(order==3)],1))./sqrt(length(day3_post.left(order==3)))];

x = 1:5;
errorbar(x+shift,Y_2,SEM_2,'LineWidth',1,'Color','k','Marker','square','markers',12,'MarkerFaceColor','k')
errorbar(x-shift,Y_1,SEM_1,'LineWidth',1,'Color','k','Marker','diamond','markers',12,'MarkerFaceColor','w')
xlim([0.8,5.2])
xticks([1,2,3,4,5])
ylabel('reaction time (RT) valid [s]')
xticklabels({'day 1','day2 pre','day2 post','day3 pre','day3 post'})
legend({'sham day 2','exp. day 2'})
title('Evolution of the valid reaction times (separate groups)')
set(gca, 'FontSize', 14)

% -----------------------------------------------------------
subplot(2,2,1)
hold on;
grid on;

Y_0 = [nanmean(nanmean([day1.left;day1.right],1)),nanmean(nanmean([day2.left;day2.right],1))];
SEM_0 = [nanstd(nanmean([day1.left;day1.right],1))./sqrt(length(day1.left)),...
    nanstd(nanmean([day2.left;day2.right],1))./sqrt(length(day2.left))];      
Y_1 = [nanmean(nanmean([day2_post.left(order==2);day2_post.right(order==2)],1)),...
    nanmean(nanmean([day3.left(order==2);day3.right(order==2)],1)),...
    nanmean(nanmean([day3_post.left(order==2);day3_post.right(order==2)],1))];
SEM_1 = [nanstd(nanmean([day2_post.left(order==2);day2_post.right(order==2)],1))./sqrt(length(day2_post.left(order==2))),...
    nanstd(nanmean([day3.left(order==2);day3.right(order==2)],1))./sqrt(length(day3.left(order==2))),...
    nanstd(nanmean([day3_post.left(order==2);day3_post.right(order==2)],1))./sqrt(length(day3_post.left(order==2)))];
Y_2 = [nanmean(nanmean([day2_post.left(order==3);day2_post.right(order==3)],1)),...
    nanmean(nanmean([day3.left(order==3);day3.right(order==3)],1)),...
    nanmean(nanmean([day3_post.left(order==3);day3_post.right(order==3)],1))];
SEM_2 = [nanstd(nanmean([day2_post.left(order==3);day2_post.right(order==3)],1))./sqrt(length(day2_post.left(order==3))),...
    nanstd(nanmean([day3.left(order==3);day3.right(order==3)],1))./sqrt(length(day3.left(order==3))),...
    nanstd(nanmean([day3_post.left(order==3);day3_post.right(order==3)],1))./sqrt(length(day3_post.left(order==3)))];

x = 3:5;
errorbar(x+shift,Y_2,SEM_2,'LineWidth',1,'Color','k','Marker','square','markers',12,'MarkerFaceColor','k')
errorbar(x-shift,Y_1,SEM_1,'LineWidth',1,'Color','k','Marker','diamond','markers',12,'MarkerFaceColor','w')
plot([2,3],[Y_0(2),Y_1(1)],'LineWidth',1,'Color','k')
plot([2,3],[Y_0(2),Y_2(1)],'LineWidth',1,'Color','k')
x = 1:2;
errorbar(x,Y_0,SEM_0,'LineWidth',1,'Color','k','Marker','o','markers',12,'MarkerFaceColor',gray)
x = 3:5;
errorbar(x+shift,Y_2,SEM_2,'LineWidth',1,'Color','k','Marker','square','markers',12,'MarkerFaceColor','k')
errorbar(x-shift,Y_1,SEM_1,'LineWidth',1,'Color','k','Marker','diamond','markers',12,'MarkerFaceColor','w')
xlim([0.8,5.2])
xticks([1,2,3,4,5])
ylabel('reaction time (RT) valid [s]')
xticklabels({'day 1','day2 pre','day2 post','day3 pre','day3 post'})
legend({'sham day 2','exp. day 2'})
title('Evolution of the valid reaction times')
set(gca, 'FontSize', 14)

% ************************************************************
subplot(2,2,4)
hold on;
grid on;

Y_1 = [nanmean([VE.day2.left(order==2);VE.day2.right(order==2)]),...
    nanmean([VE.day2_post.left(order==2);VE.day2_post.right(order==2)]),...
    nanmean([VE.day3.left(order==2);VE.day3.right(order==2)]),...
    nanmean([VE.day3_post.left(order==2);VE.day3_post.right(order==2)])];
SEM_1 = [nanstd([VE.day2.left(order==2);VE.day2.right(order==2)])./sqrt(length(VE.day2.left(order==2))),...
    nanstd([VE.day2_post.left(order==2);VE.day2_post.right(order==2)])./sqrt(length(VE.day2_post.left(order==2))),...
    nanstd([VE.day3.left(order==2);VE.day3.right(order==2)])./sqrt(length(VE.day3.left(order==2))),...
    nanstd([VE.day3_post.left(order==2);VE.day3_post.right(order==2)])./sqrt(length(VE.day3_post.left(order==2)))];
Y_2 = [nanmean([VE.day2.left(order==3);VE.day2.right(order==3)]),...
    nanmean([VE.day2_post.left(order==3);VE.day2_post.right(order==3)]),...
    nanmean([VE.day3.left(order==3);VE.day3.right(order==3)]),...
    nanmean([VE.day3_post.left(order==3);VE.day3_post.right(order==3)])];
SEM_2 = [nanstd([VE.day2.left(order==3);VE.day2.right(order==3)])./sqrt(length(VE.day2.left(order==3))),...
    nanstd([VE.day2_post.left(order==3);VE.day2_post.right(order==3)])./sqrt(length(VE.day2_post.left(order==3))),...
    nanstd([VE.day3.left(order==3);VE.day3.right(order==3)])./sqrt(length(VE.day3.left(order==3))),...
    nanstd([VE.day3_post.left(order==3);VE.day3_post.right(order==3)])./sqrt(length(VE.day3_post.left(order==3)))];

x = 2:5;
errorbar(x+shift,Y_2,SEM_2,'LineWidth',1,'Color','k','Marker','square','markers',12,'MarkerFaceColor','k')
errorbar(x-shift,Y_1,SEM_1,'LineWidth',1,'Color','k','Marker','diamond','markers',12,'MarkerFaceColor','w')
xlim([1.8,5.2])
xticks([2,3,4,5])
ylabel('Validity effect (VE) [s]')
xticklabels({'day2 pre','day2 post','day3 pre','day3 post'})
legend({'sham day 2','exp. day 2'},'Location','northwest')
title('Evolution of the validity effect (separate groups)')
set(gca, 'FontSize', 14)

% -----------------------------------------------------------
subplot(2,2,3)
hold on;
grid on;

Y_0 = nanmean([VE.day2.left;VE.day2.right]);
SEM_0 = nanstd([VE.day2.left;VE.day2.right],1)./sqrt(length(VE.day2.left));  

Y_1 = [nanmean([VE.day2_post.left(order==2);VE.day2_post.right(order==2)]),...
    nanmean([VE.day3.left(order==2);VE.day3.right(order==2)]),...
    nanmean([VE.day3_post.left(order==2);VE.day3_post.right(order==2)])];
SEM_1 = [nanstd([VE.day2_post.left(order==2);VE.day2_post.right(order==2)])./sqrt(length(VE.day2_post.left(order==2))),...
    nanstd([VE.day3.left(order==2);VE.day3.right(order==2)])./sqrt(length(VE.day3.left(order==2))),...
    nanstd([VE.day3_post.left(order==2);VE.day3_post.right(order==2)])./sqrt(length(VE.day3_post.left(order==2)))];
Y_2 = [nanmean([VE.day2_post.left(order==3);VE.day2_post.right(order==3)]),...
    nanmean([VE.day3.left(order==3);VE.day3.right(order==3)]),...
    nanmean([VE.day3_post.left(order==3);VE.day3_post.right(order==3)])];
SEM_2 = [nanstd([VE.day2_post.left(order==3);VE.day2_post.right(order==3)])./sqrt(length(VE.day2_post.left(order==3))),...
    nanstd([VE.day3.left(order==3);VE.day3.right(order==3)])./sqrt(length(VE.day3.left(order==3))),...
    nanstd([VE.day3_post.left(order==3);VE.day3_post.right(order==3)])./sqrt(length(VE.day3_post.left(order==3)))];

x = 3:5;
errorbar(x+shift,Y_2,SEM_2,'LineWidth',1,'Color','k','Marker','square','markers',12,'MarkerFaceColor','k')
errorbar(x-shift,Y_1,SEM_1,'LineWidth',1,'Color','k','Marker','diamond','markers',12,'MarkerFaceColor','w')
plot([2,3],[Y_0(1),Y_1(1)],'LineWidth',1,'Color','k')
plot([2,3],[Y_0(1),Y_2(1)],'LineWidth',1,'Color','k')
x = 2;
errorbar(x,Y_0,SEM_0,'LineWidth',1,'Color','k','Marker','o','markers',12,'MarkerFaceColor',gray)
x = 3:5;
errorbar(x+shift,Y_2,SEM_2,'LineWidth',1,'Color','k','Marker','square','markers',12,'MarkerFaceColor','k')
errorbar(x-shift,Y_1,SEM_1,'LineWidth',1,'Color','k','Marker','diamond','markers',12,'MarkerFaceColor','w')
xlim([1.8,5.2])
xticks([2,3,4,5])
ylabel('Validity effect (VE) [s]')
xticklabels({'day2 pre','day2 post','day3 pre','day3 post'})
legend({'sham day 2','exp. day 2'},'Location','northwest')
title('Evolution of the validity effect')
set(gca, 'FontSize', 14)


%% --- anova

% --- define data
Y = cat(1,Exp_pre_diff(:),Exp_post_diff(:),Sham_pre_diff(:),Sham_post_diff(:));
S = cat(1,(1:length(usesubj))',(1:length(usesubj))',(1:length(usesubj))',(1:length(usesubj))');
F1 = cat(1,-ones(length(usesubj),1),-ones(length(usesubj),1),ones(length(usesubj),1),ones(length(usesubj),1)); % Feedback
F2 = cat(1,-ones(length(usesubj),1),ones(length(usesubj),1),-ones(length(usesubj),1),ones(length(usesubj),1)); % Time
F3 = repmat(order,1,4)'; % Order
FACTNAMES = {'feedback','time'};

% --- plot data
figure;
dat = [Exp_pre_diff(:),Exp_post_diff(:),Sham_pre_diff(:),Sham_post_diff(:)];
boxplot(dat);
jitter = repmat(1:size(dat,2),size(dat,1),1) + rand(size(dat))/2 - 0.25;
hold on;
scatter(jitter(:),dat(:));

% Assumption 1: dependent variable continuous <check> Yes

% Assumption 2: within subject factors are matched pairs <check> Yes

% Assumption 3: no significant outliers in any group
% <check> using 3 x standard deviation as cutoff

isoutlier(Exp_pre_diff,'mean')
isoutlier(Exp_post_diff,'mean')
isoutlier(Sham_pre_diff,'mean')
isoutlier(Sham_post_diff,'mean')

% Assumption 4: Residuals of the GLM are normally distributed
% <check> using a fitted GLM and Shapiro-Wilk test

tbl = table(Y,F1,F2,F3);
mdl = fitglm(tbl,'ResponseVar','Y','CategoricalVars',[2,3,4]);
figure; histogram(mdl.Residuals.Raw);
[~, pN, W] = swtest(mdl.Residuals.Raw,0.05);
disp(['Shapiro-Wilk test for GLM residuals: p = ',num2str(round(pN,3)),', W = ',num2str(round(W,3))]);

% Assumption 5: Sphericity <check> always met when having only 2 levels of
% observations for any given factor

if pN > 0.05
    % --- perform repeated measures ANOVA
    disp('Difference RT invalid-valid cue');
    stats = rm_anova2(Y,S,F1,F2,FACTNAMES)
    
    % --- post-hoc tests
    disp('Bonferroni corrected p-value for significance = 0.0125');
    [~,p1,~,stat] = ttest(Exp_pre_diff,Exp_post_diff);
    disp(['t-test Exp pre-post: p = ',num2str(round(p1,3)),', t = ',num2str(round(stat.tstat,3))]);
    [~,p2,~,stat] = ttest(Sham_pre_diff,Sham_post_diff);
    disp(['t-test Sham pre-post: p = ',num2str(round(p2,3)),', t = ',num2str(round(stat.tstat,3))]);
    [~,p3,~,stat] = ttest(Exp_pre_diff,Sham_pre_diff);
    disp(['t-test pre Exp-Sham: p = ',num2str(round(p3,3)),', t = ',num2str(round(stat.tstat,3))]);
    [~,p4,~,stat] = ttest(Exp_post_diff,Sham_post_diff);
    disp(['t-test post Exp-Sham: p = ',num2str(round(p4,3)),', t = ',num2str(round(stat.tstat,3))]);
    
    disp('False discovery rate correction for multiple comparisons')
    [h, crit_p, adj_ci_cvrg, adj_p] = fdr_bh([p1,p2,p3,p4],0.05)
    
    % --- effect size
    [stats,varargout] = mes2way(Y,[F1,F2],'partialeta2','doDataPlot',true,'nBoot',10000,'isDep',[1 1])
    
    [stats,varargout] = mes1way(Y(1:(2*length(usesubj))),'partialeta2','group',F2(1:(2*length(usesubj))),'nBoot',10000,'isDep',1)
    
else
    
    % --- Friedman test (if normality assumption for residual not met)
    v = [Exp_pre_diff(:),Exp_post_diff(:),Sham_pre_diff(:),Sham_post_diff(:)];
%     v = [Exp_pre_norm(:),Exp_post_norm(:),Sham_pre_norm(:),Sham_post_norm(:)];
    p = signrank(Exp_pre_norm(:) - Sham_pre_norm(:),Exp_post_norm(:) - Sham_post_norm(:))
    [p,tbl,stats] = friedman(v);
    
end

disp('End of script')


function [averageddata] = avg(data,type)
switch type
    case 'mean'
        averageddata = nanmean(data);
    case 'median'
        averageddata = nanmedian(data);
    case 'madmean'
        averageddata = cschneid_madmean(data);
    case 'pseudomedian'
        averageddata = pseudomedian(data);
end
end