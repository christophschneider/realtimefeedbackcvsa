close all;
clear;
clc;

RTall = [];
subj_all = [];

load('\\sti1files.epfl.ch\cnbi\cschneid\_Experiment_CVSA2\CVSA2.1(Adler)\data_CLEAN\_Group\Reactiontimes_Adler.mat');
RTall = cat(2,RTall,RTstruct.RT);
subj_all = cat(2,subj_all,RTstruct.subject);

load('\\sti1files.epfl.ch\cnbi\cschneid\_Experiment_CVSA2\CVSA2.2(Biber)\data_CLEAN\_Group\Reactiontimes_Biber.mat');
RTall = cat(2,RTall,RTstruct.RT);
subj_all = cat(2,subj_all,RTstruct.subject);

clearvars -except RTall subj_all

subjects = unique(subj_all);
avgRT = NaN(length(subjects),5);

for s = 1:length(subjects)
   
    idx = find(strcmp(subj_all,subjects(s)));
    RT_tmp = [];
    for run = 1:length(idx)
       
        RT_tmp = cat(1,RT_tmp,RTall{idx(run)});
    end
    % mean RT is a matrix with following rows
    % catch left | left | center | right | catch right
    RT_tmp.reactionTime(RT_tmp.reactionTime<0) = NaN;
    
    avgRT(s,1) = nanmedian(RT_tmp.reactionTime((RT_tmp.cue == 1) & RT_tmp.catchtrial));
    avgRT(s,2) = nanmedian(RT_tmp.reactionTime((RT_tmp.cue == 1) & ~RT_tmp.catchtrial));
    avgRT(s,3) = nanmedian(RT_tmp.reactionTime(RT_tmp.cue == 3));
    avgRT(s,4) = nanmedian(RT_tmp.reactionTime((RT_tmp.cue == 2) & ~RT_tmp.catchtrial));
    avgRT(s,5) = nanmedian(RT_tmp.reactionTime((RT_tmp.cue == 2) & RT_tmp.catchtrial));
end

h = boxplot(avgRT,'labels',{'catch left','left','center','right','catch right'},...
    'notch','on');
ylabel('seconds')
set(h,{'linew'},{1.2})

disp('Reaction time averaging complete.')