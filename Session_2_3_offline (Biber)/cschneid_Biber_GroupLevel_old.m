% CVSA 2.2 Biber group level analysis

clc;
clear;
close all;

% Load defaults
% -------------------------------------------------------------------------
cschneid_Biber_Default()
plotsfolder = 'Groupplots';

cd(fullfile(default.datapath,'\_Group'));
[filename,pathname,~] = uigetfile( ...
    { '*.mat','Clean data files (*.mat)';}, ...
    'MultiSelect','on');
if iscell(filename)==0          % if only one file
    filename = {filename};
end

LI = {};
RT = [];
subjectID = cell(1,size(filename,2));
plotpath = fullfile(pathname,plotsfolder);

for f = 1:size(filename,2)
    
    disp(['Loading subject ',num2str(f)]);
    
    load(filename{f})
    
    % --- general information
    subjectID{f} = savestruct.general.subject;
    if exist('ratiotrialsbad','var')
        ratiotrialsbad(f,:) = array2table([savestruct.general.ratiotrialsbad.parts',...
            savestruct.general.ratiotrialsbad.full]);
    else
        ratiotrialsbad = array2table([savestruct.general.ratiotrialsbad.parts',...
            savestruct.general.ratiotrialsbad.full],'VariableNames',...
            [savestruct.general.ratiotrialsbad.legend,'full']);
    end
    
%     % --- eye tracker
%     if isfield(savestruct,'et')
%         fn = fieldnames(savestruct.et.pupil.classification);
%         for c = 1:numel(fn)
%             et.clsfacc(f,c) = 1 - savestruct.et.pupil.classification.(fn{c}).LDA.testerror;
%             et.clsfstd(f,c) = 1 - savestruct.et.pupil.classification.(fn{c}).LDA.testerrorstd;
%             et.clsftype{f,c} = fn{c};
%         end
%         clsfname = [default.et.bestclsf(1:2),'XX'];
%         et.acccurve{f} = nanmean(savestruct.et.pupil.classification.(clsfname).LDA.sampleaccuracy(1,:,:),3);
%         
%         % add mean epochs for the grand average
%         et.epochs_sr.white{f} = nanmean(savestruct.et.pupil.epochs_smoothed.white.matrix_ratio,1);
%         et.epochs_sr.black{f} = nanmean(savestruct.et.pupil.epochs_smoothed.black.matrix_ratio,1);
%         if isfield(savestruct.et.pupil.epochs_smoothed,'center')
%             et.epochs_sr.center{f} = nanmean(savestruct.et.pupil.epochs_smoothed.center.matrix_ratio,1);
%         end
%         del.et(f) = 0;
%     else
%         del.et(f) = 1;
%     end
    
    % --- eeg
    if isfield(savestruct,'PSD')
        % --- reaction times
        t = savestruct.PSD.RT;
        t(ismember(savestruct.PSD.RT.index,savestruct.PSD.trialsBad),:) = [];
        RT{f} = t;
        
        % --- spectrum of LI electrodes
        eeg.spec.leftcue.leftcluster{f} = savestruct.PSD.spectrumLI.leftcue.leftcluster;
        eeg.spec.leftcue.rightcluster{f} = savestruct.PSD.spectrumLI.leftcue.rightcluster;
        eeg.spec.leftcue.LI{f} = savestruct.PSD.spectrumLI.leftcue.LI;
        
        eeg.spec.rightcue.leftcluster{f} = savestruct.PSD.spectrumLI.rightcue.leftcluster;
        eeg.spec.rightcue.rightcluster{f} = savestruct.PSD.spectrumLI.rightcue.rightcluster;
        eeg.spec.rightcue.LI{f} = savestruct.PSD.spectrumLI.rightcue.LI;
        
        % --- topoplot data
        eeg.topo.avg.alpha.left(f,:) = savestruct.PSD.band.left.alpha.average;
        eeg.topo.avg.beta.left(f,:) = savestruct.PSD.band.left.beta.average;
        eeg.topo.avg.theta.left(f,:) = savestruct.PSD.band.left.theta.average;
        
        eeg.topo.avg.alpha.right(f,:) = savestruct.PSD.band.right.alpha.average;
        eeg.topo.avg.beta.right(f,:) = savestruct.PSD.band.right.beta.average;
        eeg.topo.avg.theta.right(f,:) = savestruct.PSD.band.right.theta.average;
        
        if isfield(savestruct.PSD.band,'center')
            eeg.topo.avg.alpha.center(f,:) = savestruct.PSD.band.center.alpha.average;
            eeg.topo.avg.beta.center(f,:) = savestruct.PSD.band.center.beta.average;
            eeg.topo.avg.theta.center(f,:) = savestruct.PSD.band.center.theta.average;
        end
        
        eeg.topo.time.alpha.left{f} = savestruct.PSD.band.left.alpha.power;
        eeg.topo.time.beta.left{f} = savestruct.PSD.band.left.beta.power;
        eeg.topo.time.theta.left{f} = savestruct.PSD.band.left.theta.power;
        
        eeg.topo.time.alpha.right{f} = savestruct.PSD.band.right.alpha.power;
        eeg.topo.time.beta.right{f} = savestruct.PSD.band.right.beta.power;
        eeg.topo.time.theta.right{f} = savestruct.PSD.band.right.theta.power;
        
        if isfield(savestruct.PSD.band,'center')
            eeg.topo.time.alpha.center{f} = savestruct.PSD.band.center.alpha.power;
            eeg.topo.time.beta.center{f} = savestruct.PSD.band.center.beta.power;
            eeg.topo.time.theta.center{f} = savestruct.PSD.band.center.theta.power;
        end
        
        % --- alpha lateralization index
        if isfield(savestruct.LI_PSD,'epochs')
            eeg.LI.left{f} = squeeze(nanmean(savestruct.LI_PSD.epochs.left.matrix,1));
            eeg.LI.right{f} = squeeze(nanmean(savestruct.LI_PSD.epochs.right.matrix,1));
            if isfield(savestruct.LI_PSD.epochs,'center')
                eeg.LI.center{f} = squeeze(nanmean(savestruct.LI_PSD.epochs.center.matrix,1));
            end
        end
        
        % --- classification results
        fn = fieldnames(savestruct.PSD.classification);
        for c = 1:numel(fn)
            eeg.clsfacc(f,c) = 1 - savestruct.PSD.classification.(fn{c}).LDA.testerror;
            eeg.clsfaccstd(f,c) = 1 - savestruct.PSD.classification.(fn{c}).LDA.testerrorstd;
            eeg.clsftype{f,c} = fn{c};
        end
        clsfname = [default.PSD.bestclsf(1:2),'XX'];
        eeg.acccurve{f} = nanmean(savestruct.PSD.classification.(clsfname).LDA.sampleaccuracy(1,:,:),3);
        
        del.eeg(f) = 0;
    else
        del.eeg(f) = 1;
    end
    
end

%% delete bad runs

if exist('et','var')
    use = ~del.et;
    subjectID = subjectID(use);
    et.clsfacc = et.clsfacc(use,:);
    et.clsftype = et.clsftype(use,:);
    et.acccurve = et.acccurve(use);
    et.epochs_sr.white = et.epochs_sr.white(use);
    et.epochs_sr.black = et.epochs_sr.black(use);
end

if exist('eeg','var')
    use = ~del.eeg;
    subjectID = subjectID(use);
    eeg.spec.leftcue.leftcluster = eeg.spec.leftcue.leftcluster(use);
    eeg.spec.leftcue.rightcluster = eeg.spec.leftcue.rightcluster(use);
    eeg.spec.leftcue.LI = eeg.spec.leftcue.LI(use);
    
    eeg.spec.rightcue.leftcluster = eeg.spec.rightcue.leftcluster(use);
    eeg.spec.rightcue.rightcluster = eeg.spec.rightcue.rightcluster(use);
    eeg.spec.rightcue.LI = eeg.spec.rightcue.LI(use);
    
    eeg.topo.avg.alpha.left = eeg.topo.avg.alpha.left(use,:);
    eeg.topo.avg.alpha.right = eeg.topo.avg.alpha.right(use,:);
    eeg.topo.avg.beta.left = eeg.topo.avg.beta.left(use,:);
    eeg.topo.avg.beta.right = eeg.topo.avg.beta.right(use,:);
    eeg.topo.avg.theta.left = eeg.topo.avg.theta.left(use,:);
    eeg.topo.avg.theta.right = eeg.topo.avg.theta.right(use,:);
    
    eeg.topo.time.alpha.left = eeg.topo.time.alpha.left(use);
    eeg.topo.time.alpha.right = eeg.topo.time.alpha.right(use);
    eeg.topo.time.beta.left = eeg.topo.time.beta.left(use);
    eeg.topo.time.beta.right = eeg.topo.time.beta.right(use);
    eeg.topo.time.theta.left = eeg.topo.time.theta.left(use);
    eeg.topo.time.theta.right = eeg.topo.time.theta.right(use);
    
    eeg.LI.left = eeg.LI.left(use);
    eeg.LI.right = eeg.LI.right(use);
    
    eeg.clsfacc = eeg.clsfacc(use,:);
    eeg.clsftype = eeg.clsftype(use,:);
    eeg.acccurve = eeg.acccurve(use);
end


%% treating different length epochs between subjects

if exist('et','var')
    % --- pupillometry accuracy over time
    minsize = min(cellfun('size', et.acccurve, 2));
    mat = NaN(numel(et.acccurve),minsize);
    for c = 1:numel(et.acccurve)
        mat(c,:) = et.acccurve{c}(1:minsize);
    end
    et.acccurve = mat;
    
    % --- pupillometry pupil size over time
    minsize_white = min(cellfun('size', et.epochs_sr.white, 3));
    minsize_black = min(cellfun('size', et.epochs_sr.black, 3));
    minsize = min([minsize_white, minsize_black]);
    if isfield(et.epochs_sr,'center')
        minsize_center = min(cellfun('size', et.epochs_sr.center, 3));
        minsize = min([minsize_white, minsize_black, minsize_center]);
    end
    
    mat_white = NaN(numel(et.epochs_sr.white),minsize);
    mat_black = NaN(numel(et.epochs_sr.black),minsize);
    if isfield(et.epochs_sr,'center')
        mat_center = NaN(numel(et.epochs_sr.center),minsize);
    end
    for c = 1:numel(et.epochs_sr.white)
        mat_white(c,:) = et.epochs_sr.white{c}(1:minsize);
        mat_black(c,:) = et.epochs_sr.black{c}(1:minsize);
        if isfield(et.epochs_sr,'center')
            mat_center(c,:) = et.epochs_sr.center{c}(1:minsize);
        end
    end
    et.epochs_sr.white = mat_white;
    et.epochs_sr.black = mat_black;
    if isfield(et.epochs_sr,'center')
        et.epochs_sr.center = mat_center;
    end
    
    clear minsize* mat*
end

if exist('eeg','var')
    % --- psd spectrum
    minsize = min(cellfun('size', eeg.spec.leftcue.leftcluster, 1));
    
    d1 = numel(eeg.spec.leftcue.leftcluster);
    d2 = minsize;
    d3 = size(eeg.spec.leftcue.leftcluster{1},2);
    
    mat_leftcue_left = NaN(d1,d2,d3);
    mat_leftcue_right = NaN(d1,d2,d3);
    mat_leftcue_LI = NaN(d1,d2,d3);
    mat_rightcue_left = NaN(d1,d2,d3);
    mat_rightcue_right = NaN(d1,d2,d3);
    mat_rightcue_LI = NaN(d1,d2,d3);
    
    for c = 1:d1
        mat_leftcue_left(c,:,:) = eeg.spec.leftcue.leftcluster{c}(1:minsize,:);
        mat_leftcue_right(c,:,:) = eeg.spec.leftcue.rightcluster{c}(1:minsize,:);
        mat_leftcue_LI(c,:,:) = eeg.spec.leftcue.LI{c}(1:minsize,:);
        mat_rightcue_left(c,:,:) = eeg.spec.rightcue.leftcluster{c}(1:minsize,:);
        mat_rightcue_right(c,:,:) = eeg.spec.rightcue.rightcluster{c}(1:minsize,:);
        mat_rightcue_LI(c,:,:) = eeg.spec.rightcue.LI{c}(1:minsize,:);
    end
    eeg.spec.leftcue.leftcluster = mat_leftcue_left;
    eeg.spec.leftcue.rightcluster = mat_leftcue_right;
    eeg.spec.leftcue.LI = mat_leftcue_LI;
    eeg.spec.rightcue.leftcluster = mat_rightcue_left;
    eeg.spec.rightcue.rightcluster = mat_rightcue_right;
    eeg.spec.rightcue.LI = mat_rightcue_LI;
    
    clear minsize* mat* d1 d2 d3
    
    % --- psd topoplots
    minsize = min(cellfun('size', eeg.topo.time.theta.left, 2));
    
    d1 = numel(eeg.topo.time.theta.left);
    d3 = minsize;
    
    for c = 1:d1
        mat_alpha_left(c,:,:) = eeg.topo.time.alpha.left{c}(:,1:minsize);
        mat_beta_left(c,:,:) = eeg.topo.time.beta.left{c}(:,1:minsize);
        mat_theta_left(c,:,:) = eeg.topo.time.theta.left{c}(:,1:minsize);
        
        mat_alpha_right(c,:,:) = eeg.topo.time.alpha.right{c}(:,1:minsize);
        mat_beta_right(c,:,:) = eeg.topo.time.beta.right{c}(:,1:minsize);
        mat_theta_right(c,:,:) = eeg.topo.time.theta.right{c}(:,1:minsize);
        
        if isfield(eeg.topo.time.theta,'center')
            mat_alpha_center(c,:,:) = eeg.topo.time.alpha.center{c}(:,1:minsize);
            mat_beta_center(c,:,:) = eeg.topo.time.beta.center{c}(:,1:minsize);
            mat_theta_center(c,:,:) = eeg.topo.time.theta.center{c}(:,1:minsize);
        end
        
    end
    
    eeg.topo.time.alpha.left = mat_alpha_left;
    eeg.topo.time.beta.left = mat_beta_left;
    eeg.topo.time.theta.left = mat_theta_left;
    
    eeg.topo.time.alpha.right = mat_alpha_right;
    eeg.topo.time.beta.right = mat_beta_right;
    eeg.topo.time.theta.right = mat_theta_right;
    
    if isfield(eeg.topo.time.theta,'center')
        eeg.topo.time.alpha.center = mat_alpha_center;
        eeg.topo.time.beta.center = mat_beta_center;
        eeg.topo.time.theta.center = mat_theta_center;
    end
    
    eeg.topo.time.timeline = linspace(0, minsize / savestruct.PSD.samplingRate, minsize) - default.PSD.baselinetime;
    
    clear minsize* mat* d1 d2
    
    % --- LI over time
    minsize_L = min(cellfun('size', eeg.LI.left, 1));
    minsize_R = min(cellfun('size', eeg.LI.right, 1));
    minsize = min([minsize_L,minsize_R]);
    if isfield(eeg.LI,'center')
        minsize_C = min(cellfun('size', eeg.LI.center, 1));
        minsize = min([minsize_L,minsize_R,minsize_C]);
    end
    
    d1 = numel(eeg.LI.left);
    
    for c = 1:d1
        mat_LI_left(c,:) = eeg.LI.left{c}(1:minsize);
        mat_LI_right(c,:) = eeg.LI.right{c}(1:minsize);
        if isfield(eeg.LI,'center')
            mat_LI_center(c,:) = eeg.LI.center{c}(1:minsize);
        end
    end
    
    eeg.LI.left = mat_LI_left;
    eeg.LI.right = mat_LI_right;
    if isfield(eeg.LI,'center')
        eeg.LI.center = mat_LI_center;
    end
    
    clear minsize* mat* d1
    
    % --- PSD classificatio accuracies over time
    minsize = min(cellfun('size', eeg.acccurve, 2));
    mat = NaN(numel(eeg.acccurve),minsize);
    for c = 1:numel(eeg.acccurve)
        mat(c,:) = eeg.acccurve{c}(1:minsize);
    end
    eeg.acccurve = mat;
    
    clear minsize* mat*
end

%% pupillometry data

if exist('et','var')
    
    % --- plot overall classification accuracies
    mdata = mean(et.clsfacc,1);
    sdata = std(et.clsfacc,0,1);
    
    fig = figure('Position', get(0, 'Screensize'));
    hold on;
    grid on;
    barwitherr(sdata,mdata);
    set(gca,'XTick',1:length(mdata));
    set(gca,'XTickLabel',et.clsftype(1,:));
    title('Classification accuracies of different methods for et data');
    xlabel('classifier model')
    ylabel('classification accuracy')
    
    filename = 'pupil.clsfmodels_accuracy.png';
    F = getframe(fig);
    imwrite(F.cdata, fullfile(plotpath,filename), 'png');
    
    % --- plot classification accuracies w.r.t. time in trial
    clsfname = [default.et.bestclsf(1:3),'X'];
    fig = figure('Position', get(0, 'Screensize'));
    hold on;
    ylim([0,1]);
    xdata = (1:size(et.acccurve,2))./savestruct.et.samplingRate - default.et.baselinetime;
    for subj = 1:size(et.acccurve,1)
        plot(xdata, et.acccurve(subj,:),'color',[0.8,0.8,0.8]);
    end
    plot(xdata,mean(et.acccurve,1),'r','linewidth',2)
    hline(0.5,'k')
    cschneid_vline(0,'k','-','cue')
    grid on;
    title(['Classification accuracies over time for ',clsfname,' trained on trial means']);
    xlabel('time [s]')
    ylabel('classification accuracy')
    
    filename = ['pupil.clsf_accuracy_over_time_',clsfname,'.png'];
    F = getframe(fig);
    imwrite(F.cdata, fullfile(plotpath,filename), 'png');
    
    % --- plot best classification model results for all subjects
    clsfname = default.et.bestclsf;
    etdata = et.clsfacc(:,contains(et.clsftype(1,:),clsfname));
    fig = figure('Position', get(0, 'Screensize'));
    hold on;
    grid on;
    bar(1:length(etdata), etdata)
    bar(length(etdata)+1, mean(etdata))
    set(gca,'XTick',1:length(etdata)+1);
    set(gca,'XTickLabel', [subjectID,{'all'}]);
    title(['Classification accuracies for the model "',clsfname,'"']);
    xlabel('subject')
    ylabel('classification accuracy')
    hline(0.5,'k:');
    
    filename = ['pupil.clsf_accuracy_all_subjects_',clsfname,'.png'];
    F = getframe(fig);
    imwrite(F.cdata, fullfile(plotpath,filename), 'png');
    
    % --- plot pupil size over time (grand average)
    fig = figure('Position', get(0, 'Screensize'));
    colorsX = cat(1,[1,0.6,0],[0,0.5,0.75],[0.5,0.5,0.5]);
    if isfield(et.epochs_sr,'center')
        means = squeeze([nanmean(et.epochs_sr.white,1);nanmean(et.epochs_sr.black,1);nanmean(et.epochs_sr.center,1)]);
        bounds(:,1,:) = squeeze([nanstd(et.epochs_sr.white,0,1);nanstd(et.epochs_sr.black,0,1);nanstd(et.epochs_sr.center,0,1)])';
        legendstring = {'white','black','center'};
    else
        means = squeeze([nanmean(et.epochs_sr.white,1);nanmean(et.epochs_sr.black,1)]);
        bounds(:,1,:) = squeeze([nanstd(et.epochs_sr.white,0,1);nanstd(et.epochs_sr.black,0,1)])';
        legendstring = {'white','black'};
    end
    timeline = (1:length(means))./savestruct.et.samplingRate - default.et.baselinetime;
    boundedline(timeline,means,bounds,'alpha','cmap',colorsX)
    co = get(gca,'colororder');
    grid on;
    legend = legendstring;
    xlabel('time [s]')
    ylabel('pupil size (ratio to baseline)')
    cschneid_vline(0, 'k', '-', 'cue')
    title('Grand average of pupil size (smoothed + ratio to baseline)');
    
    filename = 'pupil.size_over_time.png';
    F = getframe(fig);
    imwrite(F.cdata, fullfile(plotpath,filename), 'png');
end

%% EEG data
if exist('eeg','var')
    %% EEG spectrum
    
    % take means over subjects
    eeg.spec.means.leftcue.leftcluster = permute(nanmean(eeg.spec.leftcue.leftcluster,1),[2,3,1]);
    eeg.spec.means.leftcue.rightcluster = permute(nanmean(eeg.spec.leftcue.rightcluster,1),[2,3,1]);
    eeg.spec.means.leftcue.LI = permute(nanmean(eeg.spec.leftcue.LI,1),[2,3,1]);
    
    eeg.spec.means.rightcue.leftcluster = permute(nanmean(eeg.spec.rightcue.leftcluster,1),[2,3,1]);
    eeg.spec.means.rightcue.rightcluster = permute(nanmean(eeg.spec.rightcue.rightcluster,1),[2,3,1]);
    eeg.spec.means.rightcue.LI = permute(nanmean(eeg.spec.rightcue.LI,1),[2,3,1]);
    
    % define maximum time and amplitude
    t_max = size(eeg.spec.means.leftcue.LI,1)/savestruct.PSD.samplingRate;
    zmax = max([
        max(max(abs(eeg.spec.means.leftcue.leftcluster))),...
        max(max(abs(eeg.spec.means.leftcue.rightcluster))),...
        max(max(abs(eeg.spec.means.rightcue.leftcluster))),...
        max(max(abs(eeg.spec.means.rightcue.rightcluster))),...
        max(max(abs(eeg.spec.means.leftcue.LI))),...
        max(max(abs(eeg.spec.means.rightcue.LI)))]);
    
    % compute x-axis values
    xt = linspace(0,t_max,size(eeg.spec.means.leftcue.LI,1))-default.PSD.baselinetime;
    ticks = [xt(1),unique(round(floor(xt(xt>0))))];
    tickind = ticks.*0;
    for t = 1:length(ticks)
        [~,tickind(t)] = min(abs(xt-ticks(t)));
    end
    
    % plot
    fig = figure('Position', get(0, 'Screensize'));
    suptitle('Time-frequency plots for left and right trials')
    
    subplot(2,3,1)
    pcolor(eeg.spec.means.leftcue.leftcluster')
    shading interp;
    caxis([-zmax,zmax]);
    ax1 = gca;
    set(ax1, 'XTick', tickind, 'XTickLabel', ticks);
    xlabel('seconds'); ylabel('frequency [Hz]');
    cschneid_vline(tickind(ticks==0),'k','-','cue');
    title('Left cue: left cluster (P7, P5, PO7)');
    colorbar;
    
    subplot(2,3,2)
    pcolor(eeg.spec.means.leftcue.rightcluster')
    shading interp;
    caxis([-zmax,zmax]);
    ax1 = gca;
    set(ax1, 'XTick', tickind, 'XTickLabel', ticks);
    xlabel('seconds'); ylabel('frequency [Hz]');
    cschneid_vline(tickind(ticks==0),'k','-','cue');
    title('Left cue: right cluster (P8, P6, PO8)');
    colorbar;
    
    subplot(2,3,3)
    pcolor(eeg.spec.means.leftcue.LI')
    shading interp;
    caxis([-zmax,zmax]);
    ax1 = gca;
    set(ax1, 'XTick', tickind, 'XTickLabel', ticks);
    xlabel('seconds'); ylabel('frequency [Hz]');
    cschneid_vline(tickind(ticks==0),'k','-','cue');
    title('Left cue: lateralization (right-left)');
    colorbar;
    
    subplot(2,3,4)
    pcolor(eeg.spec.means.rightcue.leftcluster')
    shading interp;
    caxis([-zmax,zmax]);
    ax1 = gca;
    set(ax1, 'XTick', tickind, 'XTickLabel', ticks);
    xlabel('seconds'); ylabel('frequency [Hz]');
    cschneid_vline(tickind(ticks==0),'k','-','cue');
    title('Right cue: left cluster (P7, P5, PO7)');
    colorbar;
    
    subplot(2,3,5)
    pcolor(eeg.spec.means.rightcue.rightcluster')
    shading interp;
    caxis([-zmax,zmax]);
    ax1 = gca;
    set(ax1, 'XTick', tickind, 'XTickLabel', ticks);
    xlabel('seconds'); ylabel('frequency [Hz]');
    cschneid_vline(tickind(ticks==0),'k','-','cue');
    title('Right cue: right cluster (P8, P6, PO8)');
    colorbar;
    
    subplot(2,3,6)
    pcolor(eeg.spec.means.rightcue.LI')
    shading interp;
    caxis([-zmax,zmax]);
    ax1 = gca;
    set(ax1, 'XTick', tickind, 'XTickLabel', ticks);
    xlabel('seconds'); ylabel('frequency [Hz]');
    cschneid_vline(tickind(ticks==0),'k','-','cue');
    title('Right cue: lateralization (right-left)');
    colorbar;
    
    filename = fullfile(plotpath,'psd.LIspectrum.png');
    F = getframe(fig);
    imwrite(F.cdata, filename, 'png')
    
    %% EEG topoplots
    
    % --- plot alpha band *****************************************************
    
    % --- global
    if isfield(eeg.topo.avg.alpha,'center')
        data = [mean(eeg.topo.avg.alpha.left,1)',mean(eeg.topo.avg.alpha.center,1)',...
            mean(eeg.topo.avg.alpha.right,1)'];
        plotname = 'psd.topo.alpha_lcr.png';
        args = struct('data',data,'channels',{savestruct.PSD.channelLegend},...
            'layoutfile',default.locsfile,'title','Alpha band power',...
            'timepointlabels',{{'Left trials', 'Center trials', 'Right trials'}},'markers','on',...
            'save','yes','savename',fullfile(plotpath,plotname));
        cschneid_topoplot_eeglab(args)
    else
        data = [mean(eeg.topo.avg.alpha.left,1)',mean(eeg.topo.avg.alpha.right,1)'];
        plotname = 'psd.topo.alpha_lr.png';
        args = struct('data',data,'channels',{savestruct.PSD.channelLegend},...
            'layoutfile',default.locsfile,'title','Alpha band power',...
            'timepointlabels',{{'Left trials', 'Right trials'}},'markers','on',...
            'save','yes','savename',fullfile(plotpath,plotname));
        cschneid_topoplot_eeglab(args)
    end
    
    % --- difference
    data = mean(eeg.topo.avg.alpha.right,1)' - mean(eeg.topo.avg.alpha.left,1)';
    plotname = 'psd.topo.alpha_lrdiff.png';
    args = struct('data',data,'channels',{savestruct.PSD.channelLegend},...
        'layoutfile',default.locsfile,'title','Alpha band power difference',...
        'timepointlabels',{{'Right minus left trials'}},'markers','on','save','yes',...
        'savename',fullfile(plotpath,plotname));
    cschneid_topoplot_eeglab(args)
    
    % --- time steps
    clear data ticks
    timeline = eeg.topo.time.timeline;
    data(:,1) = (mean(mean(eeg.topo.time.alpha.right(:,:,timeline < 0),3),1) ...
        + mean(mean(eeg.topo.time.alpha.left(:,:,timeline < 0),3),1))/2;
    ticks = {'baseline'};
    for win = 1:length(default.topotimes.alpha)-1
        t = timeline>default.topotimes.alpha(win) & timeline<default.topotimes.alpha(win+1);
        addit = (mean(mean(eeg.topo.time.alpha.right(:,:,t),3),1) ...
            + mean(mean(eeg.topo.time.alpha.left(:,:,t),3),1))/2;
        data = cat(2,data,addit(:));
        ticks = cat(2,ticks,[num2str(default.topotimes.alpha(win)),' - ',num2str(default.topotimes.alpha(win+1))]);
    end
    
    plotname = 'psd.topo.alpha_time.png';
    args = struct('data',data,'channels',{savestruct.PSD.channelLegend},...
        'layoutfile',default.locsfile,'title','Global alpha over time (average between left and right)',...
        'timepointlabels',{ticks},'markers','on','save','yes','savename',fullfile(plotpath,plotname));
    cschneid_topoplot_eeglab(args)
    
    % --- class contrast time steps
    clear data ticks
    data(:,1) = mean(mean(eeg.topo.time.alpha.right(:,:,timeline<0),3) - ...
        mean(eeg.topo.time.alpha.left(:,:,timeline<0),3),1);
    ticks = {'baseline'};
    for win = 1:length(default.topotimes.alpha)-1
        t = timeline>default.topotimes.alpha(win) & timeline<default.topotimes.alpha(win+1);
        addit =  mean(mean(eeg.topo.time.alpha.right(:,:,t),3) - ...
            mean(eeg.topo.time.alpha.left(:,:,t),3),1);
        data = cat(2,data,addit(:));
        ticks = cat(2,ticks,[num2str(default.topotimes.alpha(win)),' - ',num2str(default.topotimes.alpha(win+1))]);
    end
    
    plotname = 'psd.topo.alpha_timediff.png';
    args = struct('data',data,'channels',{savestruct.PSD.channelLegend},...
        'layoutfile',default.locsfile,'title','Alpha lateralization over time',...
        'timepointlabels',{ticks},'markers','on','save','yes','savename',fullfile(plotpath,plotname));
    cschneid_topoplot_eeglab(args)
    
    % --- plot beta band *****************************************************
    
    % --- global
    if isfield(eeg.topo.avg.beta,'center')
        data = [mean(eeg.topo.avg.beta.left,1)',mean(eeg.topo.avg.beta.center,1)',...
            mean(eeg.topo.avg.beta.right,1)'];
        plotname = 'psd.topo.beta_lcr.png';
        args = struct('data',data,'channels',{savestruct.PSD.channelLegend},...
            'layoutfile',default.locsfile,'title','Beta band power',...
            'timepointlabels',{{'Left trials', 'Center trials', 'Right trials'}},'markers','on',...
            'save','yes','savename',fullfile(plotpath,plotname));
        cschneid_topoplot_eeglab(args)
    else
        data = [mean(eeg.topo.avg.beta.left,1)',mean(eeg.topo.avg.beta.right,1)'];
        plotname = 'psd.topo.beta_lr.png';
        args = struct('data',data,'channels',{savestruct.PSD.channelLegend},...
            'layoutfile',default.locsfile,'title','Beta band power',...
            'timepointlabels',{{'Left trials', 'Right trials'}},'markers','on',...
            'save','yes','savename',fullfile(plotpath,plotname));
        cschneid_topoplot_eeglab(args)
    end
    
    % --- difference
    data = mean(eeg.topo.avg.beta.right,1)' - mean(eeg.topo.avg.beta.left,1)';
    plotname = 'psd.topo.beta_lrdiff.png';
    args = struct('data',data,'channels',{savestruct.PSD.channelLegend},...
        'layoutfile',default.locsfile,'title','Beta band power difference',...
        'timepointlabels',{{'Right minus left trials'}},'markers','on','save','yes',...
        'savename',fullfile(plotpath,plotname));
    cschneid_topoplot_eeglab(args)
    
    % --- time steps
    clear data ticks
    timeline = eeg.topo.time.timeline;
    data(:,1) = (mean(mean(eeg.topo.time.beta.right(:,:,timeline < 0),3),1) ...
        + mean(mean(eeg.topo.time.beta.left(:,:,timeline < 0),3),1))/2;
    ticks = {'baseline'};
    for win = 1:length(default.topotimes.beta)-1
        t = timeline>default.topotimes.beta(win) & timeline<default.topotimes.beta(win+1);
        addit = (mean(mean(eeg.topo.time.beta.right(:,:,t),3),1) ...
            + mean(mean(eeg.topo.time.beta.left(:,:,t),3),1))/2;
        data = cat(2,data,addit(:));
        ticks = cat(2,ticks,[num2str(default.topotimes.beta(win)),' - ',num2str(default.topotimes.beta(win+1))]);
    end
    
    plotname = 'psd.topo.beta_time.png';
    args = struct('data',data,'channels',{savestruct.PSD.channelLegend},...
        'layoutfile',default.locsfile,'title','Global beta over time (average between left and right)',...
        'timepointlabels',{ticks},'markers','on','save','yes','savename',fullfile(plotpath,plotname));
    cschneid_topoplot_eeglab(args)
    
    % --- class contrast time steps
    clear data ticks
    data(:,1) = mean(mean(eeg.topo.time.beta.right(:,:,timeline<0),3) - ...
        mean(eeg.topo.time.beta.left(:,:,timeline<0),3),1);
    ticks = {'baseline'};
    for win = 1:length(default.topotimes.beta)-1
        t = timeline>default.topotimes.beta(win) & timeline<default.topotimes.beta(win+1);
        addit =  mean(mean(eeg.topo.time.beta.right(:,:,t),3) - ...
            mean(eeg.topo.time.beta.left(:,:,t),3),1);
        data = cat(2,data,addit(:));
        ticks = cat(2,ticks,[num2str(default.topotimes.beta(win)),' - ',num2str(default.topotimes.beta(win+1))]);
    end
    
    plotname = 'psd.topo.beta_timediff.png';
    args = struct('data',data,'channels',{savestruct.PSD.channelLegend},...
        'layoutfile',default.locsfile,'title','Beta lateralization over time',...
        'timepointlabels',{ticks},'markers','on','save','yes','savename',fullfile(plotpath,plotname));
    cschneid_topoplot_eeglab(args)
    
    % --- plot theta band *****************************************************
    
    % --- global
    if isfield(eeg.topo.avg.theta,'center')
        data = [mean(eeg.topo.avg.theta.left,1)',mean(eeg.topo.avg.theta.center,1)',...
            mean(eeg.topo.avg.theta.right,1)'];
        plotname = 'psd.topo.theta_lcr.png';
        args = struct('data',data,'channels',{savestruct.PSD.channelLegend},...
            'layoutfile',default.locsfile,'title','Theta band power',...
            'timepointlabels',{{'Left trials', 'Center trials', 'Right trials'}},'markers','on',...
            'save','yes','savename',fullfile(plotpath,plotname));
        cschneid_topoplot_eeglab(args)
    else
        data = [mean(eeg.topo.avg.theta.left,1)',mean(eeg.topo.avg.theta.right,1)'];
        plotname = 'psd.topo.theta_lr.png';
        args = struct('data',data,'channels',{savestruct.PSD.channelLegend},...
            'layoutfile',default.locsfile,'title','Theta band power',...
            'timepointlabels',{{'Left trials', 'Right trials'}},'markers','on',...
            'save','yes','savename',fullfile(plotpath,plotname));
        cschneid_topoplot_eeglab(args)
    end
    
    % --- difference
    data = mean(eeg.topo.avg.theta.right,1)' - mean(eeg.topo.avg.theta.left,1)';
    plotname = 'psd.topo.theta_lrdiff.png';
    args = struct('data',data,'channels',{savestruct.PSD.channelLegend},...
        'layoutfile',default.locsfile,'title','Theta band power difference',...
        'timepointlabels',{{'Right minus left trials'}},'markers','on','save','yes',...
        'savename',fullfile(plotpath,plotname));
    cschneid_topoplot_eeglab(args)
    
    % --- time steps
    clear data ticks
    timeline = eeg.topo.time.timeline;
    data(:,1) = (mean(mean(eeg.topo.time.theta.right(:,:,timeline < 0),3),1) ...
        + mean(mean(eeg.topo.time.theta.left(:,:,timeline < 0),3),1))/2;
    ticks = {'baseline'};
    for win = 1:length(default.topotimes.theta)-1
        t = timeline>default.topotimes.theta(win) & timeline<default.topotimes.theta(win+1);
        addit = (mean(mean(eeg.topo.time.theta.right(:,:,t),3),1) ...
            + mean(mean(eeg.topo.time.theta.left(:,:,t),3),1))/2;
        data = cat(2,data,addit(:));
        ticks = cat(2,ticks,[num2str(default.topotimes.theta(win)),' - ',num2str(default.topotimes.theta(win+1))]);
    end
    
    plotname = 'psd.topo.theta_time.png';
    args = struct('data',data,'channels',{savestruct.PSD.channelLegend},...
        'layoutfile',default.locsfile,'title','Global theta over time (average between left and right)',...
        'timepointlabels',{ticks},'markers','on','save','yes','savename',fullfile(plotpath,plotname));
    cschneid_topoplot_eeglab(args)
    
    % --- class contrast time steps
    clear data ticks
    data(:,1) = mean(mean(eeg.topo.time.theta.right(:,:,timeline<0),3) - ...
        mean(eeg.topo.time.theta.left(:,:,timeline<0),3),1);
    ticks = {'baseline'};
    for win = 1:length(default.topotimes.theta)-1
        t = timeline>default.topotimes.theta(win) & timeline<default.topotimes.theta(win+1);
        addit =  mean(mean(eeg.topo.time.theta.right(:,:,t),3) - ...
            mean(eeg.topo.time.theta.left(:,:,t),3),1);
        data = cat(2,data,addit(:));
        ticks = cat(2,ticks,[num2str(default.topotimes.theta(win)),' - ',num2str(default.topotimes.theta(win+1))]);
    end
    
    plotname = 'psd.topo.theta_timediff.png';
    args = struct('data',data,'channels',{savestruct.PSD.channelLegend},...
        'layoutfile',default.locsfile,'title','Theta lateralization over time',...
        'timepointlabels',{ticks},'markers','on','save','yes','savename',fullfile(plotpath,plotname));
    cschneid_topoplot_eeglab(args)
    
    %% Lateralization index
    
    % --- Lateralization index over time (grand average)
    clear means bounds legendstring timeline
    fig = figure('Position', get(0, 'Screensize'));
    colorsX = cat(1,[1,0,0],[0,0,1],[0.5,0.5,0.5]);
    if isfield(eeg.LI,'center')
        means = squeeze([nanmean(eeg.LI.left,1);nanmean(eeg.LI.right,1);nanmean(eeg.LI.center,1)]);
        bounds(:,1,:) = squeeze([nanstd(eeg.LI.left,0,1);nanstd(eeg.LI.right,0,1);nanstd(eeg.LI.center,0,1)])';
        legendstring = {'left','right','center'};
    else
        means = squeeze([nanmean(eeg.LI.left,1);nanmean(eeg.LI.right,1)]);
        bounds(:,1,:) = squeeze([nanstd(eeg.LI.left,0,1);nanstd(eeg.LI.right,0,1)])';
        legendstring = {'left','right'};
    end
    timeline = (1:length(means))./savestruct.PSD.samplingRate - default.PSD.baselinetime;
    boundedline(timeline,means,bounds,'alpha','cmap',colorsX)
    co = get(gca,'colororder');
    grid on;
    legend(legendstring);
    xlabel('time [s]')
    ylabel('alpha lateralization index (ratio to baseline)')
    cschneid_vline(0, 'k', '-', 'cue')
    title('Grand average of the alpha lateralization index');
    
    filename = 'LI.over_time.png';
    F = getframe(fig);
    imwrite(F.cdata, fullfile(plotpath,filename), 'png');
    
    %% EEG-PSD classification
    
    % --- plot overall classification accuracies
    mdata = mean(eeg.clsfacc,1);
    sdata = std(eeg.clsfacc,0,1);
    
    fig = figure('Position', get(0, 'Screensize'));
    hold on;
    grid on;
    barwitherr(sdata,mdata);
    set(gca,'XTick',1:length(mdata));
    set(gca,'XTickLabel',eeg.clsftype(1,:));
    title('Classification accuracies of different methods for PSD data');
    xlabel('classifier model')
    ylabel('classification accuracy')
    
    filename = 'psd.clsfmodels_accuracy.png';
    F = getframe(fig);
    imwrite(F.cdata, fullfile(plotpath,filename), 'png');
    
    % --- plot classification accuracies w.r.t. time in trial
    clsfname = [default.PSD.bestclsf(1:3),'X'];
    fig = figure('Position', get(0, 'Screensize'));
    hold on;
    ylim([0,1]);
    xdata = (1:size(eeg.acccurve,2))./savestruct.PSD.samplingRate - default.PSD.baselinetime;
    for subj = 1:size(eeg.acccurve,1)
        plot(xdata, eeg.acccurve(subj,:),'color',[0.8,0.8,0.8]);
    end
    plot(xdata,mean(eeg.acccurve,1),'r','linewidth',2)
    hline(0.5,'k')
    cschneid_vline(0,'k','-','cue')
    grid on;
    title(['Classification accuracies over time for ',clsfname,' trained on trial means']);
    xlabel('time [s]')
    ylabel('classification accuracy')
    
    filename = ['psd.clsf_accuracy_over_time_',clsfname,'.png'];
    F = getframe(fig);
    imwrite(F.cdata, fullfile(plotpath,filename), 'png');
    
    % --- plot best classification model results for all subjects
    clsfname = default.PSD.bestclsf;
    eegdata = eeg.clsfacc(:,contains(eeg.clsftype(1,:),clsfname));
    fig = figure('Position', get(0, 'Screensize'));
    hold on;
    grid on;
    bar(1:length(eegdata), eegdata)
    bar(length(eegdata)+1, mean(eegdata))
    set(gca,'XTick',1:length(eegdata)+1);
    set(gca,'XTickLabel', [subjectID,{'all'}]);
    title(['Classification accuracies for the model "',clsfname,'"']);
    xlabel('subject')
    ylabel('classification accuracy')
    hline(0.5,'k:');
    
    filename = ['psd.clsf_accuracy_all_subjects_',clsfname,'.png'];
    F = getframe(fig);
    imwrite(F.cdata, fullfile(plotpath,filename), 'png');
    
    % --- plot classification model results for all subjects (samples)
    clsfname = {'pabX','aabX','saXX'};
    data = eeg.clsfacc(:,contains(eeg.clsftype(1,:),clsfname));
    data(end+1,:) = mean(data);
    fig = figure('Position', get(0, 'Screensize'));
    hold on;
    grid on;
    bar(1:length(data), data)
    set(gca,'XTick',1:length(data)+1);
    set(gca,'XTickLabel', [subjectID,{'all'}]);
    title('Sample classification accuracies');
    legend({'full (saXX)','aLI (aabX)','pLI (pabX)'});
    xlabel('subject')
    ylabel('classification accuracy')
    hline(0.5,'k:');
    ylim([0,1]);
    
    % --- plot classification model results for all subjects (trials)
    clsfname = {'pabt','aabt','saXt'};
    data = eeg.clsfacc(:,contains(eeg.clsftype(1,:),clsfname));
    data(end+1,:) = mean(data);
    fig = figure('Position', get(0, 'Screensize'));
    hold on;
    grid on;
    bar(1:length(data), data)
    set(gca,'XTick',1:length(data)+1);
    set(gca,'XTickLabel', [subjectID,{'all'}]);
    title('Trial classification accuracies');
    legend({'full (saXt)','aLI (aabt)','pLI (pabt)'});
    xlabel('subject')
    ylabel('classification accuracy')
    hline(0.5,'k:');
    ylim([0,1]);
end

if exist('eegdata','var') && exist('etdata','var')
    [c, p] = corr(eegdata,etdata);
    disp(['Correlation between classification results = ',num2str(c),' ; p-value', num2str(p)]);
end

%% Reaction time saving

if exist('RT','var')
   RTstruct = struct('RT',{RT},'subject',{subjectID});
   save('Reactiontimes_Biber.mat','RTstruct') 
end

%%
disp('--- end of batch ---')