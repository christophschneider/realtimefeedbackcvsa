function [savestruct] = cschneid_Biber_ET_analysis(args)
%| function [savestruct] = cschneid_ET_analysis(args)
%|
%| processes eye tracker data from the input struct 'psd'
%|
%| INPUT
%| args.struct: PREPROCESSED eye tracker structure (cschneid_ET_preprocessing)
%| args.default: struct with default values provided in the default file
%| args.savestruct: structure that holds computed results - if defined this
%|                  function will add further fields (savestruct.et.___)
%| args.plot[optional]: 'yes' for obtaining plots
%|
%| OUTPUT
%| savestruct: structure that holds all computed results for saving

cprintf([0,0,1],'Eye tracker analysis ...\n');

% --- free the args structure
if ~isfield(args,'savestruct')
    savestruct = struct;
else
    savestruct = args.savestruct;
end
if ~isfield(args,'plot')
    plotflag = 'no';
else
    plotflag = args.plot;
end
default = args.default;
et = args.struct;
clear args;

%% EYE TRACKER ANALYSIS: Pupil Size

% --- save general information about the eye tracker data
savestruct.et = et;

% --- save percentage of trials corrupted with saccades
trialsWithSaccades = setdiff(et.trialsWithSaccades,et.trialsWithBlinks);
savestruct.et.ratio_TrialsWithSaccades = round(length(trialsWithSaccades)/et.totaltrials,2);

% --- smooth pupil area data
win = round(et.samplingRate * default.et.pupilsmoothwindow);
if  win > 0
    et.pupilArea_smoothed =  medfilt1(et.pupilArea,win);
end

% --- replace left/right triggers with black/white triggers
% 4 = cue to white side | 5 = cue to black side
[et] = triggers_LR2BW(et);

% --- extract epochs
[savestruct.et.pupil.epochs] = extractEpochs(et.pupilArea,et,default.et.baselinetime);
[savestruct.et.pupil.epochs_smoothed] = extractEpochs(et.pupilArea_smoothed,et,default.et.baselinetime);
[savestruct.et.pupil.epochs_detrended] = extractEpochs(et.pupilArea_detrended,et,default.et.baselinetime);

% --- plot trial means and single trials
if strcmp(plotflag,'yes')
    plotname = fullfile(pwd,default.plotpath,[savestruct.et.sessionID,'.et.epochsaverages.png']);
    plotepochs_nbsl_bsl(savestruct.et.pupil.epochs_smoothed,et.samplingRate,default.et.baselinetime,plotname)
end

%% Classification

% Abbreviations for savestruct.et.pupil.classification.abcd
% a = X... raw; s... smoothed; d... detrended
% b = X... raw; b... baselined; r... ratio
% c = X... all samples; t... mean over time per trial
% d = X... all time; w... smaller time window

stretch = round(savestruct.et.samplingRate*default.et.baselinetime):...
    savestruct.et.pupil.epochs.maxlength;

args = struct('data',savestruct.et.pupil.epochs,'stretch',stretch,...
    'signaltype','raw','means',{{}},'default',default,'plot','no');
[savestruct.et.pupil.classification.XXXX] = classifyPupilSize(args);
disp(['Mean accuracy [LDA|samples|raw|alltime] = ',...
    num2str(1-mean(savestruct.et.pupil.classification.XXXX.LDA.meanerror))]);

args = struct('data',savestruct.et.pupil.epochs_smoothed,'stretch',stretch,...
    'signaltype','raw','means',{{}},'default',default,'plot','no');
[savestruct.et.pupil.classification.sXXX] = classifyPupilSize(args);
disp(['Mean accuracy [LDA|samples|smoothed|alltime] = ',...
    num2str(1-mean(savestruct.et.pupil.classification.sXXX.LDA.meanerror))]);

args = struct('data',savestruct.et.pupil.epochs_smoothed,'stretch',stretch,...
    'signaltype','baselined','means',{{}},'default',default,'plot','no');
[savestruct.et.pupil.classification.sbXX] = classifyPupilSize(args);
disp(['Mean accuracy [LDA|samples|smoothed-baselined|alltime] = ',...
    num2str(1-mean(savestruct.et.pupil.classification.sbXX.LDA.meanerror))]);

args = struct('data',savestruct.et.pupil.epochs_smoothed,'stretch',stretch,...
    'signaltype','ratio','means',{{}},'default',default,'plot','no');
[savestruct.et.pupil.classification.srXX] = classifyPupilSize(args);
disp(['Mean accuracy [LDA|samples|smoothed-ratio|alltime] = ',...
    num2str(1-mean(savestruct.et.pupil.classification.srXX.LDA.meanerror))]);

args = struct('data',savestruct.et.pupil.epochs,'stretch',stretch,...
    'signaltype','ratio','means',{{}},'default',default,'plot','no');
[savestruct.et.pupil.classification.XrXX] = classifyPupilSize(args);
disp(['Mean accuracy [LDA|samples|ratio|alltime] = ',...
    num2str(1-mean(savestruct.et.pupil.classification.XrXX.LDA.meanerror))]);

args = struct('data',savestruct.et.pupil.epochs_smoothed,'stretch',stretch,...
    'signaltype','ratio','means',{{'time'}},'default',default,'plot','no');
[savestruct.et.pupil.classification.srtX] = classifyPupilSize(args);
disp(['Mean accuracy [LDA|means|smoothed-ratio|alltime] = ',...
    num2str(1-mean(savestruct.et.pupil.classification.srtX.LDA.meanerror))]);

% best window after grand average study:
window = default.et.classification.window;
winstr = [num2str(window(1)),'-',num2str(window(2)),'s'];
stretch = round(savestruct.et.samplingRate*(default.et.baselinetime + window(1))):...
    round(savestruct.et.samplingRate*(default.et.baselinetime + window(2)));

plotname = fullfile(pwd,default.plotpath,[savestruct.et.sessionID,'.et.clsf_srtw.png']);
args = struct('data',savestruct.et.pupil.epochs_smoothed,'stretch',stretch,...
    'signaltype','ratio','means',{{}},'default',default,'plot','yes','name',plotname);
[savestruct.et.pupil.classification.srXw] = classifyPupilSize(args);
disp(['Mean accuracy [LDA|samples|smoothed-ratio|',winstr,'] = ',...
    num2str(1-mean(savestruct.et.pupil.classification.srXw.LDA.meanerror))]);

plotname = fullfile(pwd,default.plotpath,[savestruct.et.sessionID,'.et.clsf_srtw.png']);
args = struct('data',savestruct.et.pupil.epochs_smoothed,'stretch',stretch,...
    'signaltype','ratio','means',{{}},'default',default,'plot','yes','name',plotname);
[savestruct.et.pupil.classification.srXw_timefeats] = classifyPupilSize_timefeatures(args);
disp(['Mean accuracy [LDA|samples|smoothed-ratio|',winstr,'|timepoints] = ',...
    num2str(1-mean(savestruct.et.pupil.classification.srXw.LDA.meanerror))]);

args = struct('data',savestruct.et.pupil.epochs_smoothed,'stretch',stretch,...
    'signaltype','ratio','means',{{'time'}},'default',default,'plot','no');
[savestruct.et.pupil.classification.srtw] = classifyPupilSize(args);
disp(['Mean accuracy [LDA|means|smoothed-ratio|',winstr,'] = ',...
    num2str(1-mean(savestruct.et.pupil.classification.srtw.LDA.meanerror))]);

args = struct('data',savestruct.et.pupil.epochs,'stretch',stretch,...
    'signaltype','baselined','means',{{}},'default',default,'plot','no');
[savestruct.et.pupil.classification.XbXw] = classifyPupilSize(args);
disp(['Mean accuracy [LDA|samples|ratio|',winstr,'] = ',...
    num2str(1-mean(savestruct.et.pupil.classification.XbXw.LDA.meanerror))]);

args = struct('data',savestruct.et.pupil.epochs,'stretch',stretch,...
    'signaltype','ratio','means',{{'time'}},'default',default,'plot','no');
[savestruct.et.pupil.classification.Xrtw] = classifyPupilSize(args);
disp(['Mean accuracy [LDA|means|ratio|',winstr,'] = ',...
    num2str(1-mean(savestruct.et.pupil.classification.Xrtw.LDA.meanerror))]);

end


%% AUXILIARY FUNCTIONS
%%

function [et] = triggers_LR2BW(et)
% replaces left/right [1/2] triggers with white/black [4/5] triggers
% adds field triggersBW to et struct

BGW = [11,13]; % start trial triggers for black-gray-white background
WGB = [12,14]; % start trial triggers for white-gray-black background

et.triggersBW = et.triggers;

starttriggers = et.triggerLegend{3}(~cellfun(@isempty,regexp(et.triggerLegend{1},'trial_start')));
cuetriggers = et.triggerLegend{3}(~cellfun(@isempty,regexp(et.triggerLegend{1},'cue')));
targettriggers = et.triggerLegend{3}(~cellfun(@isempty,regexp(et.triggerLegend{1},'target')));

trialstarttriggers = find(ismember(et.triggers(1,:),starttriggers));
trialcuetriggers = find(ismember(et.triggers(1,:),cuetriggers));
trialendtriggers = find(ismember(et.triggers(1,:),targettriggers));

for tr = 1:length(trialstarttriggers)
    if ismember(et.triggersBW(1,trialstarttriggers(tr)),BGW)
        switch et.triggersBW(1,trialcuetriggers(tr))
            case 1
                et.triggersBW(1,trialcuetriggers(tr)) = 5;
                et.triggersBW(1,trialendtriggers(tr)) = 35;
            case 2
                et.triggersBW(1,trialcuetriggers(tr)) = 4;
                et.triggersBW(1,trialendtriggers(tr)) = 34;
        end
    elseif ismember(et.triggersBW(1,trialstarttriggers(tr)),WGB)
        switch et.triggersBW(1,trialcuetriggers(tr))
            case 1
                et.triggersBW(1,trialcuetriggers(tr)) = 4;
                et.triggersBW(1,trialendtriggers(tr)) = 34;
            case 2
                et.triggersBW(1,trialcuetriggers(tr)) = 5;
                et.triggersBW(1,trialendtriggers(tr)) = 35;
        end
    end
end
end

function [epochs] = extractEpochs(data,datastruct,baselinetime)

% --- extract black and white trials
args = struct('data',data,'time',datastruct.time,'triggers',datastruct.triggersBW,...
    'epoch',{{[4,34],[-baselinetime,0]}},'samplingRate',datastruct.samplingRate,'excludeartifacts','yes');
[white.cell,~,white.trialnumbers] = cschneid_epoching(args);

args = struct('data',data,'time',datastruct.time,'triggers',datastruct.triggersBW,...
    'epoch',{{[5,35],[-baselinetime,0]}},'samplingRate',datastruct.samplingRate,'excludeartifacts','yes');
[black.cell,~,black.trialnumbers] = cschneid_epoching(args);

% --- put into common-length matrices
sW = max(cellfun(@(C) size(C,2), white.cell));
sB = max(cellfun(@(C) size(C,2), black.cell));
epochs.maxlength = max([sW,sB]);

white.matrix = NaN(size(white.cell,2),size(white.cell{1},1),epochs.maxlength,size(white.cell{1},3));
black.matrix = NaN(size(black.cell,2),size(black.cell{1},1),epochs.maxlength,size(black.cell{1},3));

for ep = 1:size(white.cell,2)
    white.matrix(ep,:,1:size(white.cell{ep},2),:) = white.cell{ep};
end
for ep = 1:size(black.cell,2)
    black.matrix(ep,:,1:size(black.cell{ep},2),:) = black.cell{ep};
end

% --- create baselined data
white.bsl = nanmean(white.matrix(:,:,1:round((datastruct.samplingRate)*baselinetime),:),3);
black.bsl = nanmean(black.matrix(:,:,1:round((datastruct.samplingRate)*baselinetime),:),3);

white.matrix_bsl = white.matrix - repmat(white.bsl,1,1,epochs.maxlength,1);
black.matrix_bsl = black.matrix - repmat(black.bsl,1,1,epochs.maxlength,1);

white.matrix_ratio = white.matrix ./ repmat(white.bsl,1,1,epochs.maxlength);
black.matrix_ratio = black.matrix ./ repmat(black.bsl,1,1,epochs.maxlength);

epochs.white = white;
epochs.black = black;

end

function [] = plotepochs_nbsl_bsl(epochs,samplingRate,baselinetime,name)

fig = figure('Position', get(0, 'Screensize'));
co = get(gca,'colororder');
subplot(2,2,1);
means = squeeze([nanmean(epochs.white.matrix,1);nanmean(epochs.black.matrix,1)]);
bounds(:,1,:) = squeeze([nanstd(epochs.white.matrix,1);nanstd(epochs.black.matrix,1)])';
timeline = (1:epochs.maxlength)./samplingRate - baselinetime;
boundedline(timeline,means,bounds,'cmap',[co(3,:);co(1,:)],'alpha')
cschneid_vline(0, 'k', '-', 'cue')
legend({'white','black'})
title('Non-baselined pupil area: means+std over time')

subplot(2,2,2)
hold on;
hi = plot(timeline,squeeze(epochs.white.matrix),'Color',co(3,:));
h(1) = hi(1);
hi = plot(timeline,squeeze(epochs.black.matrix),'Color',co(1,:));
h(2) = hi(1);
cschneid_vline(0, 'k', '-', 'cue')
legend(h,{'white','black'})
title('Non-baselined pupil area: single trials over time')

subplot(2,2,3);
means = squeeze([nanmean(epochs.white.matrix_ratio,1);nanmean(epochs.black.matrix_ratio,1)]);
bounds(:,1,:) = squeeze([nanstd(epochs.white.matrix_ratio,1);nanstd(epochs.black.matrix_ratio,1)])';
boundedline(timeline,means,bounds,'cmap',[co(3,:);co(1,:)],'alpha')
cschneid_vline(0, 'k', '-', 'cue')
co = get(gca,'colororder');
legend({'white','black'})
title('Baselined pupil area: means+std over time')

subplot(2,2,4)
hold on;
hi = plot(timeline,squeeze(epochs.white.matrix_ratio),'Color',co(3,:));
h(1) = hi(1);
hi = plot(timeline,squeeze(epochs.black.matrix_ratio),'Color',co(1,:));
h(2) = hi(1);
cschneid_vline(0, 'k', '-', 'cue')
legend(h,{'white','black'})
title('Non-baselined pupil area: single trials over time')

% --- save plot
F = getframe(fig);
imwrite(F.cdata, name, 'png')

end

function [clsf_results] = classifyPupilSize(args)

% --- create features
if ischar(args.signaltype)
    switch args.signaltype
        case 'raw'
            features.W = args.data.white.matrix(:,:,args.stretch);
            features.B = args.data.black.matrix(:,:,args.stretch);
        case 'baselined'
            features.W = args.data.white.matrix_bsl(:,:,args.stretch);
            features.B = args.data.black.matrix_bsl(:,:,args.stretch);
        case 'ratio'
            features.W = args.data.white.matrix_ratio(:,:,args.stretch);
            features.B = args.data.black.matrix_ratio(:,:,args.stretch);
    end
end

if sum(ismember(args.means,'time'))>0
    features.W = nanmean(features.W,3);
    features.B = nanmean(features.B,3);
end

% --- create labels
labels.W = ones(size(features.W)).*1;
labels.B = ones(size(features.B)).*2;
labels.W(isnan(features.W)) = NaN;
labels.B(isnan(features.B)) = NaN;

% --- keep channels and frequencies as features (rest to samples)
argsX = struct('data',features.W,'featuredimensions',2,'sampledimensions',3,...
    'trialdimensions',1);
[features_final.W, ~] = cschneid_data2feat(argsX);

argsX = struct('data',features.B,'featuredimensions',2,'sampledimensions',3,...
    'trialdimensions',1);
[features_final.B, ~] = cschneid_data2feat(argsX);

% --- do the same with the labels
argsX = struct('data',labels.W,'featuredimensions',2,'sampledimensions',3,...
    'trialdimensions',1);
[labels_final.W] = cschneid_data2feat(argsX);

argsX = struct('data',labels.B,'featuredimensions',2,'sampledimensions',3,...
    'trialdimensions',1);
[labels_final.B] = cschneid_data2feat(argsX);

clsf_features = cat(3,features_final.W,features_final.B);
clsf_labels = cat(3,labels_final.W,labels_final.B);

%% --- plot class distributions
if strcmp(args.plot,'yes')
    colorlegend = {'black','white'};
    colB = [0    0.4470    0.7410];
    colW = [0.9290    0.6940    0.1250];
    plotsamples({colB,colW},features_final.B,features_final.W,colorlegend,args.name)
end

%% --- final classification

DAoptions = {'DiscrimType',args.default.classifier,'Gamma',0.5};
cargs = struct(  'data',clsf_features,...
    'labels',clsf_labels,...
    'numfolds',args.default.et.classification.numfolds,...
    'repetitions',args.default.et.classification.numrep,...
    'type','DA',...
    'options',{DAoptions},...
    'featselection','yes');
[clsf_results.LDA] = cschneid_classification(cargs);

% DAoptions = {'DiscrimType','quadratic','Gamma',1};
% cargs = struct(  'data',clsf_features,...
%     'labels',clsf_labels,...
%     'numfolds',args.default.et.classification.numfolds,...
%     'repetitions',args.default.et.classification.numrep,...
%     'type','DA',...
%     'options',{DAoptions},...
%     'featselection','yes');
% [clsf_results.QDA] = cschneid_classification(cargs);

% % --- Random forest classification (slow)
%  RFoptions = {'OOBPrediction','on','OOBPredictorImportance','on',...
%     'Method','classification','Options',statset('UseParallel',false)};
% cargs = struct(  'data',clsf_features,...
%     'labels',clsf_labels,...
%     'numfolds',args.default.et.classification.numfolds,...
%     'repetitions',args.default.et.classification.numrep,...
%     'type','RF',...
%     'options',{RFoptions},...
%     'featselection','yes',...
%     'numTrees',100);
% [clsf_results.RF] = cschneid_classification(cargs);

% % --- reconstruct original structure
% [features_final.L, fkey_L] = cschneid_data2feat(argsX);
% args = struct('data',features_final.L,'key',fkey_L);
% [checkme] = cschneid_feat2data(args);

end

function [clsf_results] = classifyPupilSize_timefeatures(args)

% --- create features
if ischar(args.signaltype)
    switch args.signaltype
        case 'raw'
            features.W = args.data.white.matrix(:,:,args.stretch);
            features.B = args.data.black.matrix(:,:,args.stretch);
        case 'baselined'
            features.W = args.data.white.matrix_bsl(:,:,args.stretch);
            features.B = args.data.black.matrix_bsl(:,:,args.stretch);
        case 'ratio'
            features.W = args.data.white.matrix_ratio(:,:,args.stretch);
            features.B = args.data.black.matrix_ratio(:,:,args.stretch);
    end
end

if sum(ismember(args.means,'time'))>0
    features.W = nanmean(features.W,3);
    features.B = nanmean(features.B,3);
end

features.W = smoothdata(features.W,3,'movmedian',50);
features.B = smoothdata(features.B,3,'movmedian',50);

for k = 1:size(features.W,1)
    features_ds.W(k,1,:) = downsample(squeeze(features.W(k,:,:)),20);
end
for k = 1:size(features.B,1)
    features_ds.B(k,1,:) = downsample(squeeze(features.B(k,:,:)),20);
end
features = features_ds;

% --- create labels
labels.W = ones(size(features.W)).*1;
labels.B = ones(size(features.B)).*2;
labels.W(isnan(features.W)) = NaN;
labels.B(isnan(features.B)) = NaN;

% --- keep channels and frequencies as features (rest to samples)
argsX = struct('data',features.W,'featuredimensions',3,'sampledimensions',2,...
    'trialdimensions',1);
[features_final.W, ~] = cschneid_data2feat(argsX);

argsX = struct('data',features.B,'featuredimensions',3,'sampledimensions',2,...
    'trialdimensions',1);
[features_final.B, ~] = cschneid_data2feat(argsX);

% --- do the same with the labels
argsX = struct('data',labels.W,'featuredimensions',3,'sampledimensions',2,...
    'trialdimensions',1);
[labels_final.W] = cschneid_data2feat(argsX);

argsX = struct('data',labels.B,'featuredimensions',3,'sampledimensions',2,...
    'trialdimensions',1);
[labels_final.B] = cschneid_data2feat(argsX);

clsf_features = cat(3,features_final.W,features_final.B);
clsf_labels = cat(3,labels_final.W,labels_final.B);

%% --- plot class distributions
if strcmp(args.plot,'yes')
    colorlegend = {'black','white'};
    colB = [0    0.4470    0.7410];
    colW = [0.9290    0.6940    0.1250];
    plotsamples({colB,colW},features_final.B,features_final.W,colorlegend,args.name)
end

%% --- final classification

DAoptions = {'DiscrimType',args.default.classifier,'Gamma',0.5};
cargs = struct(  'data',clsf_features,...
    'labels',clsf_labels,...
    'numfolds',args.default.et.classification.numfolds,...
    'repetitions',args.default.et.classification.numrep,...
    'type','DA',...
    'options',{DAoptions},...
    'featselection','yes');
[clsf_results.LDA] = cschneid_classification(cargs);

end

function [] = plotsamples(col,features1,features2,colorlegend,filename)
%% --- plot class distributions

% --- means over samples per trials
feats1 = permute(nanmean(features1,2),[3,1,2]);
del = isnan(feats1);
feats1(del(:,1),:) = [];

feats2 = permute(nanmean(features2,2),[3,1,2]);
del = isnan(feats2);
feats2(del(:,1),:) = [];

clsf_features = cat(3,features1,features2);
meanfeats = permute(nanmean(clsf_features,2),[3,1,2]);
del = isnan(meanfeats);
meanfeats(del(:,1),:) = [];

if size(meanfeats,2) > 1
    pca_coeff = pca(meanfeats,'Centered',false);
    pca_feats1 = feats1 * pca_coeff;
    feats1 = pca_feats1(:,1);
    pca_feats2 = feats2 * pca_coeff;
    feats2 = pca_feats2(:,1);
end

fig = figure('Position', get(0, 'Screensize'));
subplot(2,1,1); hold on;
H1 = histogram(feats1,'FaceColor',col{1},'Normalization','pdf');
H2 = histogram(feats2,'FaceColor',col{2},'Normalization','pdf');
BinEdges = union(round(H2.BinEdges,4),round(H1.BinEdges,4));
H1.BinEdges = BinEdges;
H2.BinEdges = BinEdges;
x = min(union(H2.BinEdges,H1.BinEdges)):0.01:max(union(H2.BinEdges,H1.BinEdges));
pd1 = makedist('Normal',nanmean(feats1),nanstd(feats1));
pd2 = makedist('Normal',nanmean(feats2),nanstd(feats2));
pdfline1 = pdf(pd1,x);
pdfline2 = pdf(pd2,x);
plot(x,pdfline1,'Color',col{1},'LineWidth',4)
plot(x,pdfline2,'Color',col{2},'LineWidth',4)
title('Histogram of the means of the first PC of each trial')
legend(colorlegend)

% --- concatenate samples and trials
allfeats = reshape(clsf_features,size(clsf_features,1),[])';
del = isnan(allfeats);
allfeats(del(:,1),:) = [];

feats1 = reshape(features1,size(features1,1),[])';
del = isnan(feats1);
feats1(del(:,1),:) = [];

feats2 = reshape(features2,size(features2,1),[])';
del = isnan(feats2);
feats2(del(:,1),:) = [];

if size(allfeats,2) > 1
    pca_coeff = pca(allfeats,'Centered',false);
    pca_feats1 = feats1 * pca_coeff;
    feats1 = pca_feats1(:,1);
    pca_feats2 = feats2 * pca_coeff;
    feats2 = pca_feats2(:,1);
end

subplot(2,1,2); hold on;
H1 = histogram(feats1,'FaceColor',col{1},'Normalization','pdf');
H2 = histogram(feats2,'FaceColor',col{2},'Normalization','pdf');
BinEdges = union(round(H2.BinEdges,4),round(H1.BinEdges,4));
H1.BinEdges = BinEdges;
H2.BinEdges = BinEdges;
x = min(union(H2.BinEdges,H1.BinEdges)):0.01:max(union(H2.BinEdges,H1.BinEdges));
pd1 = makedist('Normal',nanmean(feats1),nanstd(feats1));
pd2 = makedist('Normal',nanmean(feats2),nanstd(feats2));
pdfline1 = pdf(pd1,x);
pdfline2 = pdf(pd2,x);
plot(x,pdfline1,'Color',col{1},'LineWidth',4)
plot(x,pdfline2,'Color',col{2},'LineWidth',4)
title('Histogram of the samples of the first PC of each trial')
legend(colorlegend)

% --- save plot
F = getframe(fig);
imwrite(F.cdata, filename, 'png')
end
