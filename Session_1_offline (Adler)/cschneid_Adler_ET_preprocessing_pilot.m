function [et] = cschneid_Adler_ET_preprocessing_pilot(args)
%| function [et] = cschneid_ET_preprocessing(args)
%|
%| preprocesses the data from the eyelink 1000 eye tracker
%|
%| INPUT
%| args.struct: 'et' struct (output format specified in RAW2CLEAN)
%| args.default: struct with default values provided in the default file
%| args.plot: 'yes' for obtaining plots of raw data and triggers
%|
%| OUTPUT
%| et: 'et' struct with additional fields
%|      et.blinkVector: markes samples in blinks with 1, otherwise 0
%|      et.centeroutVector: markes samples outside of center circle with 1,
%|          otherwise 0
%|      et.badTrials: vector of trial numbers corrupted with blinks or
%|          saccades outside of center

cprintf([0,0,1],'Eye tracker data preprocessing start ...\n');

et = args.struct;
default = args.default;
if ~isfield(args,'plot')
    plotflag = 'no';
else
    plotflag = args.plot;
end

% --- compute some common measures
x = default.et.screensize / norm(default.et.screenratio);
screen_in_cm = default.et.screenratio * x * unitsratio('cm','in');
default.et.pixelspercm = round(sum((et.resolution./screen_in_cm))/2);

% --- find blinks from eyetracker
args = struct('et',et,'padding',default.et.blinkPadding);
et.blinkVector = cschneid_ET_findblinks(args);

% --- find gaze out of center from eyetracker
args = struct('et',et,'distance2screen',default.et.distance2screen,...
    'pxpercm',default.et.pixelspercm,'threshold',default.et.fixationThreshold,...
    'duration',default.et.fixationDuration);
et.centeroutVector = cschneid_ET_findcenterout(args);

% --- check bad trials
starttriggers = et.triggerLegend{3}(~cellfun(@isempty,regexp(et.triggerLegend{1},'trial_start')));
cuetriggers = et.triggerLegend{3}(~cellfun(@isempty,regexp(et.triggerLegend{1},'cue')));
targettriggers = et.triggerLegend{3}(~cellfun(@isempty,regexp(et.triggerLegend{1},'target')));

trialstarttriggers = find(ismember(et.triggers(1,:),starttriggers));
trialcuetriggers = find(ismember(et.triggers(1,:),cuetriggers));
trialendtriggers = find(ismember(et.triggers(1,:),targettriggers));

% --- add missing trial start triggers and background information (BGW=14)
if isempty(starttriggers)
    et.triggerLegend{3}(6) = 33;
    et.triggers(1,trialendtriggers) = 33;
    newtriggers = cat(1,ones(1,length(trialcuetriggers))*14,et.triggers(2,trialcuetriggers)-1);
    et.triggers = cat(2,et.triggers,newtriggers);
    [~,b] = sort(et.triggers(2,:));
    et.triggers = et.triggers(:,b);
    et.triggerLegend{1} = cat(1,et.triggerLegend{1},'trial_start_bgw');
    et.triggerLegend{2} = cat(1,et.triggerLegend{2},'X');
    et.triggerLegend{3} = cat(1,et.triggerLegend{3},14);
end

% --- repeat trigger extraction
starttriggers = et.triggerLegend{3}(~cellfun(@isempty,regexp(et.triggerLegend{1},'trial_start')));
cuetriggers = et.triggerLegend{3}(~cellfun(@isempty,regexp(et.triggerLegend{1},'cue')));
targettriggers = et.triggerLegend{3}(~cellfun(@isempty,regexp(et.triggerLegend{1},'target')));

trialstarttriggers = find(ismember(et.triggers(1,:),starttriggers));
trialcuetriggers = find(ismember(et.triggers(1,:),cuetriggers));
trialendtriggers = find(ismember(et.triggers(1,:),targettriggers));

if length(trialstarttriggers)~=length(trialendtriggers)
    error('[Eye Tracker] Not the same amount of trial start and end triggers!')
end
et.totaltrials = length(trialstarttriggers);

baselinesamples = floor(default.baselinetime * et.samplingRate);
et.trialsWithBlinks = [];
et.trialsWithSaccades = [];
et.trialsCatch = [];

for tr = 1:length(trialstarttriggers)
    trialspan = et.triggers(2,trialstarttriggers(tr))-baselinesamples:...
        et.triggers(2,trialendtriggers(tr));
    if sum(et.blinkVector(trialspan)) > 0
        et.trialsWithBlinks = [et.trialsWithBlinks,tr];
    end
    if sum(et.centeroutVector(trialspan)) > 0
        et.trialsWithSaccades = [et.trialsWithSaccades,tr];
    end
    if mod(et.triggers(1,trialendtriggers(tr)),10) ~= et.triggers(1,trialcuetriggers(tr))
        et.trialsCatch = [et.trialsCatch,tr];
    end
end
et.trialsBad = unique(cat(2,et.trialsWithBlinks,et.trialsWithSaccades));

% --- Extend trigger structure to artifacts and catch trials
et.triggers = cat(1,et.triggers,et.triggers.*0);
trialnum = 0;
for trig = 1:size(et.triggers,2)-1
    if ismember(et.triggers(1,trig),starttriggers)
        trialnum = trialnum + 1;
    end
    if ismember(trialnum,et.trialsBad)
        et.triggers(3,trig) = 1;
    end
    if ismember(trialnum,et.trialsCatch)
        et.triggers(4,trig) = 1;
    end
end

% --- plot data
if strcmp(plotflag,'yes')
    
    % --- plot raw data (takes a long time to generate the plot!)
    x = et.gazePos.y;
    x(isnan(x)) = 0;
    args = struct('data',x,'time',et.time,'triggers',et.triggers,'detrend','no');
    cschneid_plotwaterfall(args);
    
end