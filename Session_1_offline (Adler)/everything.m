close all;
clear;
clc;

% --- run reaction time results computations
run('C:\Users\cschneid\Documents\Git\MATLAB\CVSA2.2 (Biber)\cschneid_Biber_RT.m')
clearvars -except day* Exp* Sham* order VE* exp sham usesubj x* y* chincRT

% --- run LI results computations
x = load('\\sti1files.epfl.ch\cnbi\cschneid\_Experiment_CVSA2\CVSA2.1(Adler)\data_CLEAN\_Group\upstream_Adler.mat');
adler = x.adlerstruct;

x = load('\\sti1files.epfl.ch\cnbi\cschneid\_Experiment_CVSA2\CVSA2.2(Biber)\data_CLEAN\_Group\upstream_Biber.Pre.Real.mat');
biber.real.pre = x.biberstruct;
x = load('\\sti1files.epfl.ch\cnbi\cschneid\_Experiment_CVSA2\CVSA2.2(Biber)\data_CLEAN\_Group\upstream_Biber.Post.Real.mat');
biber.real.post = x.biberstruct;
x = load('\\sti1files.epfl.ch\cnbi\cschneid\_Experiment_CVSA2\CVSA2.2(Biber)\data_CLEAN\_Group\upstream_Biber.Pre.Sham.mat');
biber.sham.pre = x.biberstruct;
x = load('\\sti1files.epfl.ch\cnbi\cschneid\_Experiment_CVSA2\CVSA2.2(Biber)\data_CLEAN\_Group\upstream_Biber.Post.Sham.mat');
biber.sham.post = x.biberstruct;

x = load('\\sti1files.epfl.ch\cnbi\cschneid\_Experiment_CVSA2\CVSA2.3(Chinchilla)\data_CLEAN\_Group\upstream_Chinchilla.Real.mat','chinchillastruct');
chinchilla.real = x.chinchillastruct;
x = load('\\sti1files.epfl.ch\cnbi\cschneid\_Experiment_CVSA2\CVSA2.3(Chinchilla)\data_CLEAN\_Group\upstream_Chinchilla.Sham.mat','chinchillastruct');
chinchilla.sham = x.chinchillastruct;

RT.A = adler.RT;
RT.B.real.pre = biber.real.pre.RT;
RT.B.real.post = biber.real.post.RT;
RT.B.sham.pre = biber.sham.pre.RT;
RT.B.sham.post = biber.sham.post.RT;
RT.C.real = chinchilla.real.RT;
RT.C.sham = chinchilla.sham.RT;
save('\\sti1files.epfl.ch\cnbi\cschneid\_Experiment_CVSA2\Umbrella\reactiontimes.mat','RT')
cd '\\sti1files.epfl.ch\cnbi\cschneid\_Experiment_CVSA2\Umbrella\';

numsubj = length(usesubj);

%% artifact percentages
tottrials = 240 + 80 + 80 + 80 + 80;
artifacts(1,:) = adler.artifact;
artifacts(2,:) = biber.real.pre.artifact;
artifacts(3,:) = biber.real.post.artifact;
artifacts(4,:) = biber.sham.pre.artifact;
artifacts(5,:) = biber.sham.post.artifact;
artifact_mean = mean(sum(artifacts,1) ./ tottrials * 100);
artifact_std = std(sum(artifacts,1) ./ tottrials * 100);

%% plot trial duration online Real

% days1_2 = [16,40,10,56,55,64,63,61,66,54,56,54,58,68];
% days2_3 = [14,19,1,9,28,7,14,16,6,14,9,16,10,15];
%
% disp(['Days between 1. und 2. session: ',num2str(mean(days1_2)),char(177),num2str(std(days1_2))]);
% disp(['Days between 2. und 3. session: ',num2str(mean(days2_3)),char(177),num2str(std(days2_3))]);
% disp(['Mean time between recording sessions: ',num2str(mean([days1_2,days2_3])),...
%     char(177),num2str(std([days1_2,days2_3])),' days']);
%
% times_mean_without = 8.5249;
% times_end_ratio = 0.17844;
% times_mean = 10.5226;
%
% numsubj = length(chinchilla.real.subject);
% for s = 1:numsubj
%     xRT = chinchilla.real.RT{s};
%     xRT(find(xRT.exclude),:) = [];
%     ptdo.ratio(s) = sum(xRT.trialDuration >= 20)/size(xRT,1);
%     ptdo.mean_without(s) = mean(xRT(xRT.trialDuration < 20,:).trialDuration);
%     ptdo.mean(s) = mean(xRT.trialDuration);
% end
%
% [s,p] = ttest(ptdo.mean - times_mean)
%
% fig = figure('Position', get(0, 'Screensize'));
% hold on;
% grid on;
% bar(ptdo.mean - times_mean)
% xlabel('subject');
% set(gca,'XTick',1:length(chinchilla.real.subject));
% set(gca,'XTickLabel',chinchilla.real.subject);
% ylabel('mean trial length difference from the expected value')
% filename = 'ZZZ.timesdiffexp.png';
% F = getframe(fig);
% imwrite(F.cdata, filename);


%% questionnaire

questionnaire = table;
questionnaire.subject = {'ae1','ae2','ae3','ae4','ae5','ae6','ae7','ae8','ae9','ae10','af1','af2','af3','af4',...
    'ae1','ae2','ae3','ae4','ae5','ae6','ae7','ae8','ae9','ae10','af1','af2','af3','af4'}';
questionnaire.sham = logical([1,0,1,1,0,1,0,0,1,0,1,0,1,0,0,1,0,0,1,0,1,1,0,1,0,1,0,1])';
questionnaire.Q1 = [3,4,3,3,2,3,3,2,2,4,3,3,3,3,3,4,4,2,3,3,4,4,2,4,2,4,4,4]';
questionnaire.Q2 = [4,3,5,5,3,3,3,3,2,3,5,5,4,4,4,3,5,3,4,2,3,4,3,5,3,3,5,3]';
questionnaire.Q5 = [4,2,5,3,4,4,5,5,3,4,3,3,1,3,3,3,4,3,4,4,2,5,3,3,3,4,4,4]';
questionnaire.Q6 = [3,5,4,3,2,4,2,3,4,3,2,2,5,1,2,4,4,2,4,2,2,4,2,2,2,3,5,2]';
questionnaire(~ismember(questionnaire.subject,chinchilla.real.subject),:) = [];

% --- Questions about in control and helpfulness
y = [mean(questionnaire.Q1(~questionnaire.sham)),mean(questionnaire.Q1(questionnaire.sham));...
    mean(questionnaire.Q2(~questionnaire.sham)),mean(questionnaire.Q2(questionnaire.sham))];
errY = [std(questionnaire.Q1(~questionnaire.sham)),std(questionnaire.Q1(questionnaire.sham));...
    std(questionnaire.Q2(~questionnaire.sham)),std(questionnaire.Q2(questionnaire.sham))]./sqrt(numsubj);

[~,p1,~,stat] = ttest(questionnaire.Q1(~questionnaire.sham),questionnaire.Q1(questionnaire.sham));
[~,p2,~,stat] = ttest(questionnaire.Q2(~questionnaire.sham),questionnaire.Q1(questionnaire.sham));

fig = figure('Position', get(0, 'Screensize'));
set(gcf,'color','w');
subplot(1,2,1)

barwitherr(errY, y);    % Plot with errorbars
grid on;
legend({'Exp.','Sham'})
ylim([1,5]);
xticklabels({['in control, p = ',num2str(round(p1,2))],['feedback helpful, p = ',num2str(round(p2,2))]})
title('questionnaire resume')
set(gca, 'FontSize', 16)
% --- Questions about tiredness

y = [mean(questionnaire.Q5(~questionnaire.sham)),mean(questionnaire.Q5(questionnaire.sham));...
    mean(questionnaire.Q6(~questionnaire.sham)),mean(questionnaire.Q6(questionnaire.sham))];
errY = [std(questionnaire.Q5(~questionnaire.sham)),std(questionnaire.Q5(questionnaire.sham));...
    std(questionnaire.Q6(~questionnaire.sham)),std(questionnaire.Q6(questionnaire.sham))]./sqrt(numsubj);
[~,p1,~,stat] = ttest(questionnaire.Q5(~questionnaire.sham),questionnaire.Q6(~questionnaire.sham));
[~,p2,~,stat] = ttest(questionnaire.Q5(questionnaire.sham),questionnaire.Q6(questionnaire.sham));

shift = 0.03;
x = 1:2;

subplot(1,2,2)
hold on;
grid on;
errorbar(x-shift,y(:,1),errY(:,1),'LineWidth',1,'Color','k','Marker','o','markers',12,'MarkerFaceColor','w')
errorbar(x+shift,y(:,2),errY(:,2),'LineWidth',1,'Color','k','Marker','o','markers',12,'MarkerFaceColor','k')
ylim([1,5])
xlim([0.5,2.5])
set(gca,'XTick',1:length(x));
set(gca,'XTickLabel',{'beginning of ses.','end of ses.'});
ylabel('alertness level')
legend({['Exp., p = ',num2str(round(p1,2))],['Sham, p = ',num2str(round(p2,2))]})
title('questionnaire: change of alertness')
set(gca, 'FontSize', 16)
filename = 'questionnaire.png';
F = getframe(fig);
imwrite(F.cdata, filename);


%% LI pre and post

span = 9:88;
bslspan = 1:span(1)-1;
whichLI = 'iLI';
% avgtype = 'pseudomedian';
avgtype = 'mean';
baseline = 'no';
type = 'absolute';
action = 'NOnormalize';
usecase = 3;

s = 0;
for sub = usesubj
    s = s + 1;
    
    % --- calculate baseline
%     bsl.real.pre(s,:) = [avg(biber.real.pre.(whichLI).left(sub,bslspan),avgtype),avg(biber.real.pre.(whichLI).right(sub,bslspan),avgtype),...
%         avg(biber.real.pre.(whichLI).right(sub,bslspan) - biber.real.pre.(whichLI).left(sub,bslspan),avgtype),...
%         avg(biber.real.pre.(whichLI).Mahasamples(sub,bslspan),avgtype)];
%     bsl.real.post(s,:) = [avg(biber.real.post.(whichLI).left(sub,bslspan),avgtype),avg(biber.real.post.(whichLI).right(sub,bslspan),avgtype),...
%         avg(biber.real.post.(whichLI).right(sub,bslspan) - biber.real.post.(whichLI).left(sub,bslspan),avgtype),...
%         avg(biber.real.post.(whichLI).Mahasamples(sub,bslspan),avgtype)];
%     bsl.sham.pre(s,:) = [avg(biber.sham.pre.(whichLI).left(sub,bslspan),avgtype),avg(biber.sham.pre.(whichLI).right(sub,bslspan),avgtype),...
%         avg(biber.sham.pre.(whichLI).right(sub,bslspan) - biber.sham.pre.(whichLI).left(sub,bslspan),avgtype),...
%         avg(biber.sham.pre.(whichLI).Mahasamples(sub,bslspan),avgtype)];
%     bsl.sham.post(s,:) = [avg(biber.sham.post.(whichLI).left(sub,bslspan),avgtype),avg(biber.sham.post.(whichLI).right(sub,bslspan),avgtype),...
%         avg(biber.sham.post.(whichLI).right(sub,bslspan) - biber.sham.post.(whichLI).left(sub,bslspan),avgtype),...
%         avg(biber.sham.post.(whichLI).Mahasamples(sub,bslspan),avgtype)];
%     
%     % --- calculate activation
%     LI.real.pre(s,:) = [avg(biber.real.pre.(whichLI).left(sub,span),avgtype),avg(biber.real.pre.(whichLI).right(sub,span),avgtype),...
%         avg(biber.real.pre.(whichLI).right(sub,span) - biber.real.pre.(whichLI).left(sub,span),avgtype),...
%         avg(biber.real.pre.(whichLI).Mahasamples(sub,span),avgtype)];
%     LI.real.post(s,:) = [avg(biber.real.post.(whichLI).left(sub,span),avgtype),avg(biber.real.post.(whichLI).right(sub,span),avgtype),...
%         avg(biber.real.post.(whichLI).right(sub,span) - biber.real.post.(whichLI).left(sub,span),avgtype),...
%         avg(biber.real.post.(whichLI).Mahasamples(sub,span),avgtype)];
%     LI.sham.pre(s,:) = [avg(biber.sham.pre.(whichLI).left(sub,span),avgtype),avg(biber.sham.pre.(whichLI).right(sub,span),avgtype),...
%         avg(biber.sham.pre.(whichLI).right(sub,span) - biber.sham.pre.(whichLI).left(sub,span),avgtype),...
%         avg(biber.sham.pre.(whichLI).Mahasamples(sub,span),avgtype)];
%     LI.sham.post(s,:) = [avg(biber.sham.post.(whichLI).left(sub,span),avgtype),avg(biber.sham.post.(whichLI).right(sub,span),avgtype),...
%         avg(biber.sham.post.(whichLI).right(sub,span) - biber.sham.post.(whichLI).left(sub,span),avgtype),...
%         avg(biber.sham.post.(whichLI).Mahasamples(sub,span),avgtype)];

    LI.day1(s,:) = [avg(adler.(whichLI).left(sub,span),avgtype),avg(adler.(whichLI).right(sub,span),avgtype),...
        avg(adler.(whichLI).right(sub,span) - adler.(whichLI).left(sub,span),avgtype)];
    
        % --- calculate baseline
    bsl.real.pre(s,:) = [avg(biber.real.pre.(whichLI).left(sub,bslspan),avgtype),avg(biber.real.pre.(whichLI).right(sub,bslspan),avgtype),...
        avg(biber.real.pre.(whichLI).right(sub,bslspan) - biber.real.pre.(whichLI).left(sub,bslspan),avgtype)];
    bsl.real.post(s,:) = [avg(biber.real.post.(whichLI).left(sub,bslspan),avgtype),avg(biber.real.post.(whichLI).right(sub,bslspan),avgtype),...
        avg(biber.real.post.(whichLI).right(sub,bslspan) - biber.real.post.(whichLI).left(sub,bslspan),avgtype)];
    bsl.sham.pre(s,:) = [avg(biber.sham.pre.(whichLI).left(sub,bslspan),avgtype),avg(biber.sham.pre.(whichLI).right(sub,bslspan),avgtype),...
        avg(biber.sham.pre.(whichLI).right(sub,bslspan) - biber.sham.pre.(whichLI).left(sub,bslspan),avgtype)];
    bsl.sham.post(s,:) = [avg(biber.sham.post.(whichLI).left(sub,bslspan),avgtype),avg(biber.sham.post.(whichLI).right(sub,bslspan),avgtype),...
        avg(biber.sham.post.(whichLI).right(sub,bslspan) - biber.sham.post.(whichLI).left(sub,bslspan),avgtype)];
    
    bsl.real.pre(s,:) = [avg(biber.real.pre.(whichLI).left(sub,bslspan),avgtype),avg(biber.real.pre.(whichLI).right(sub,bslspan),avgtype),...
        avg(biber.real.pre.(whichLI).right(sub,bslspan) - biber.real.pre.(whichLI).left(sub,bslspan),avgtype)];
    bsl.real.post(s,:) = [avg(biber.real.post.(whichLI).left(sub,bslspan),avgtype),avg(biber.real.post.(whichLI).right(sub,bslspan),avgtype),...
        avg(biber.real.post.(whichLI).right(sub,bslspan) - biber.real.post.(whichLI).left(sub,bslspan),avgtype)];
    bsl.sham.pre(s,:) = [avg(biber.sham.pre.(whichLI).left(sub,bslspan),avgtype),avg(biber.sham.pre.(whichLI).right(sub,bslspan),avgtype),...
        avg(biber.sham.pre.(whichLI).right(sub,bslspan) - biber.sham.pre.(whichLI).left(sub,bslspan),avgtype)];
    bsl.sham.post(s,:) = [avg(biber.sham.post.(whichLI).left(sub,bslspan),avgtype),avg(biber.sham.post.(whichLI).right(sub,bslspan),avgtype),...
        avg(biber.sham.post.(whichLI).right(sub,bslspan) - biber.sham.post.(whichLI).left(sub,bslspan),avgtype)];
    
    % --- calculate activation
    LI.real.pre(s,:) = [avg(biber.real.pre.(whichLI).left(sub,span),avgtype),avg(biber.real.pre.(whichLI).right(sub,span),avgtype),...
        avg(biber.real.pre.(whichLI).right(sub,span) - biber.real.pre.(whichLI).left(sub,span),avgtype)];
    LI.real.post(s,:) = [avg(biber.real.post.(whichLI).left(sub,span),avgtype),avg(biber.real.post.(whichLI).right(sub,span),avgtype),...
        avg(biber.real.post.(whichLI).right(sub,span) - biber.real.post.(whichLI).left(sub,span),avgtype)];
    LI.sham.pre(s,:) = [avg(biber.sham.pre.(whichLI).left(sub,span),avgtype),avg(biber.sham.pre.(whichLI).right(sub,span),avgtype),...
        avg(biber.sham.pre.(whichLI).right(sub,span) - biber.sham.pre.(whichLI).left(sub,span),avgtype)];
    LI.sham.post(s,:) = [avg(biber.sham.post.(whichLI).left(sub,span),avgtype),avg(biber.sham.post.(whichLI).right(sub,span),avgtype),...
        avg(biber.sham.post.(whichLI).right(sub,span) - biber.sham.post.(whichLI).left(sub,span),avgtype)];
    
    if strcmp(baseline,'yes')
        if strcmp(type,'absolute')
            LI.real.pre(s,:) = LI.real.pre(s,:) - bsl.real.pre(s,:);
            LI.real.post(s,:) = LI.real.post(s,:) - bsl.real.post(s,:);
            LI.sham.pre(s,:) = LI.sham.pre(s,:) - bsl.sham.pre(s,:);
            LI.sham.post(s,:) = LI.sham.post(s,:) - bsl.sham.post(s,:);
        elseif strcmp(type,'relative')
            LI.real.pre(s,:) = LI.real.pre(s,:) ./ bsl.real.pre(s,:);
            LI.real.post(s,:) = LI.real.post(s,:) ./ bsl.real.post(s,:);
            LI.sham.pre(s,:) = LI.sham.pre(s,:) ./ bsl.sham.pre(s,:);
            LI.sham.post(s,:) = LI.sham.post(s,:) ./ bsl.sham.post(s,:);
        end
    end
    if strcmp(action,'normalize')
%         LI.real.pre(s,:) = LI.real.pre(s,:) ./ LI.real.pre(s,:);
%         LI.real.post(s,:) = LI.real.post(s,:) ./ LI.real.pre(s,:);
%         LI.sham.pre(s,:) = LI.sham.pre(s,:) ./ LI.sham.pre(s,:);
%         LI.sham.post(s,:) = LI.sham.post(s,:) ./ LI.sham.pre(s,:);
        LI.real.pre(s,:) = LI.real.pre(s,:) ./ adler.palphapower.bsl(s);
        LI.real.post(s,:) = LI.real.post(s,:) ./ adler.palphapower.bsl(s);
        LI.sham.pre(s,:) = LI.sham.pre(s,:) ./ adler.palphapower.bsl(s);
        LI.sham.post(s,:) = LI.sham.post(s,:) ./ adler.palphapower.bsl(s);
    end
    
end

LIdiff.day1 = LI.day1(:,usecase);
LIdiff.real.pre = LI.real.pre(:,usecase);
LIdiff.real.post = LI.real.post(:,usecase);
LIdiff.sham.pre = LI.sham.pre(:,usecase);
LIdiff.sham.post = LI.sham.post(:,usecase);

disp('Alpha power vs LIspan')
GA_bsl = [biber.real.pre.palphapower.bsl',biber.real.post.palphapower.bsl',biber.sham.pre.palphapower.bsl',biber.sham.post.palphapower.bsl'];
GA_trial = [biber.real.pre.palphapower.trial',biber.real.post.palphapower.trial',biber.sham.pre.palphapower.trial',biber.sham.post.palphapower.trial'];
GA_bsl = GA_bsl(usesubj,:);
GA_trial = GA_trial(usesubj,:);
GA_norm = (GA_trial - GA_bsl) ./ GA_bsl;
LIspan = [LIdiff.real.pre,LIdiff.real.post,LIdiff.sham.pre,LIdiff.sham.post];
[rho,p] = corr(GA_bsl(:),LIspan(:))
[rho,p] = corr(GA_trial(:),LIspan(:))

modulationindex = LIspan ./ GA_bsl;

% --- write to file
varnames = {'absl_exp_pre','absl_exp_post','absl_sham_pre','absl_sham_post','order'};
T = table(GA_bsl(:,1),GA_bsl(:,2),GA_bsl(:,3),GA_bsl(:,4),order','VariableNames',varnames);
writetable(T,'alpha_bsl_JASP')

% --- write to file
varnames = {'atrial_exp_pre','atrial_exp_post','atrial_sham_pre','atrial_sham_post','order'};
T = table(GA_trial(:,1),GA_trial(:,2),GA_trial(:,3),GA_trial(:,4),order','VariableNames',varnames);
writetable(T,'alpha_trial_JASP')

% --- write to file
varnames = {'exp_pre','exp_post','sham_pre','sham_post','order'};
T = table(GA_norm(:,1),GA_norm(:,2),GA_norm(:,3),GA_norm(:,4),order','VariableNames',varnames);
writetable(T,'alpha_norm_JASP')

% --- write to file
varnames = {'exp_pre','exp_post','sham_pre','sham_post','order'};
T = table(modulationindex(:,1),modulationindex(:,2),modulationindex(:,3),modulationindex(:,4),order','VariableNames',varnames);
writetable(T,[whichLI,'modulind_JASP'])

disp('Alpha power vs VE')
VE_x = [Exp_pre_diff',Exp_post_diff',Sham_pre_diff',Sham_post_diff'];
[rho,p] = corr(GA_bsl(:),VE_x(:))
[rho,p] = corr(GA_bsl(:),VE_x(:),'type','Spearman')
[rho,p] = corr(GA_trial(:),VE_x(:))
[rho,p] = corr(GA_trial(:),VE_x(:),'type','Spearman')
figure; scatter(GA_bsl(:),VE_x(:))
x = GA_bsl(:);
y = VE_x(:);
[brob,statsrob] = robustfit(x,y)
[brob,statsrob] = robustfit(zscore(x),zscore(y));
disp([num2str(brob(2)),', ',num2str(statsrob.p(2))]);


disp('Alpha power vs RT')
RT_v = [Exp_pre_norm',Exp_post_norm',Sham_pre_norm',Sham_post_norm'];
RT_i = [Exp_pre_catch',Exp_post_catch',Sham_pre_catch',Sham_post_catch'];
[rho,p] = corr(GA_bsl(:),RT_v(:))
[rho,p] = corr(GA_bsl(:),RT_v(:),'type','Spearman')
[rho,p] = corr(GA_trial(:),RT_v(:))
[rho,p] = corr(GA_bsl(:),RT_v(:),'type','Spearman')
figure; scatter(GA_bsl(:),log(RT_v(:)))

disp('Modulationindex vs VE')
x = modulationindex(:);
y = RT_v(:);
[brob,statsrob] = robustfit(x,y)
[brob,statsrob] = robustfit(zscore(x),zscore(y));
disp([num2str(brob(2)),', ',num2str(statsrob.p(2))]);
[rho,p] = corr(x,y)
[rho,p] = corr(x,y,'type','Spearman')

NaNmodulationindex = modulationindex;
NaNmodulationindex(isoutlier(modulationindex)) = NaN;
nanmean(NaNmodulationindex)

%% Regression plots
disp('X vs Y')
fig = figure('Position', get(0, 'Screensize'));
set(gcf,'color','w');

subplot(1,3,1)
% x = LIspan(:);
% y = VE_x(:);
x = mean(LIspan,2);
y = mean(VE_x,2);

counter = 0; clear yhat CI
plotsupport = linspace(min(x),max(x),1000);
for s = plotsupport
    counter = counter+1;
    [yhat(counter),CI(counter,:)] = regress_CI_interval(s,x,y,0.05);
end
scatter(x,y,'ok','filled','MarkerEdgeColor',[0 0 0],'MarkerFaceColor',[0.7 0.7 0.7]); hold on;
[brob,statsrob] = robustfit(x,y)
plot(x,brob(1) + brob(2)*x,'k','LineWidth',2,'LineStyle','-')
plot(plotsupport,CI,'k','LineWidth',0.5,'LineStyle','-')
[brob,statsrob] = robustfit(zscore(x),zscore(y));
title('A  \alpha-LI span vs. VE')
xlabel('\alpha-LI span')
ylabel('validity effect')
set(gca, 'FontSize', 16)
grid on
axis('square')


subplot(1,3,2)
% x = GA_bsl(:);
% y = LIspan(:);
x = mean(GA_bsl,2);
y = mean(LIspan,2);
counter = 0; clear yhat CI
plotsupport = linspace(min(x),max(x),1000);
for s = plotsupport
    counter = counter+1;
    [yhat(counter),CI(counter,:)] = regress_CI_interval(s,x,y,0.05);
end
scatter(x,y,'ok','filled','MarkerEdgeColor',[0 0 0],'MarkerFaceColor',[0.7 0.7 0.7]); hold on;
[brob,statsrob] = robustfit(x,y)
plot(x,brob(1) + brob(2)*x,'k','LineWidth',2,'LineStyle','-')
plot(plotsupport,CI,'k','LineWidth',0.5,'LineStyle','-')
[brob,statsrob] = robustfit(zscore(x),zscore(y));
title('B  baseline \alpha-power vs. \alpha-LI span')
xlabel('baseline \alpha-power')
ylabel('\alpha-LI span')
set(gca, 'FontSize', 16)
grid on
axis('square')

subplot(1,3,3)
% x = GA_bsl(:);
% y = VE_x(:);
x = mean(GA_bsl,2);
y = mean(VE_x,2);
counter = 0; clear yhat CI
plotsupport = linspace(min(x),max(x),1000);
for s = plotsupport
    counter = counter+1;
    [yhat(counter),CI(counter,:)] = regress_CI_interval(s,x,y,0.05);
end
scatter(x,y,'ok','filled','MarkerEdgeColor',[0 0 0],'MarkerFaceColor',[0.7 0.7 0.7]); hold on;
[brob,statsrob] = robustfit(x,y)
plot(x,brob(1) + brob(2)*x,'k','LineWidth',2,'LineStyle','-')
plot(plotsupport,CI,'k','LineWidth',0.5,'LineStyle','-')
[brob,statsrob] = robustfit(zscore(x),zscore(y));
title('C  baseline \alpha-power vs. VE')
xlabel('baseline \alpha-power')
ylabel('validity effect')
set(gca, 'FontSize', 16)
grid on
axis('square')


filename = 'correlations.png';
F = getframe(fig);
imwrite(F.cdata, filename);

% subplot(2,2,3)
% x = GA_bsl(:);
% y = RT_v(:);
% 
% counter = 0; clear yhat CI
% plotsupport = linspace(min(x),max(x),1000);
% for s = plotsupport
%     counter = counter+1;
%     [yhat(counter),CI(counter,:)] = regress_CI_interval(s,x,y,0.05);
% end
% scatter(x,y,'ok','filled','MarkerEdgeColor',[0 0 0],'MarkerFaceColor',[0.7 0.7 0.7]); hold on;
% [brob,statsrob] = robustfit(x,y)
% plot(x,brob(1) + brob(2)*x,'k','LineWidth',2,'LineStyle','-')
% plot(plotsupport,CI,'k','LineWidth',0.5,'LineStyle','-')
% [brob,statsrob] = robustfit(zscore(x),zscore(y));
% title(['Correlation GAbsl-RTv; p=',num2str(statsrob.p(2))])

% 
x = log(GA_bsl(:));
y = LIspan(:);
[brob,statsrob] = robustfit(zscore(x),zscore(y));
disp([num2str(brob(2)),', ',num2str(statsrob.p(2))]);
rho_rob = brob(2);
[rho_pear,p_pear] = corr(x,y,'type','Pearson')
[rho_spear,p_spear] = corr(x,y,'type','Spearman')
for rep = 1:10000
   x_shuf = x;
   y_shuf = y(randperm(length(y)));
   [rho_shuf(rep),p_shuf(rep)] = corr(x_shuf,y_shuf);
end
alpha_ci = 0.05;
rho_ci = quantile(rho_shuf,[alpha_ci/2, 1-alpha_ci/2])
p_spear = sum(abs(rho_spear)<rho_shuf | -abs(rho_spear)>rho_shuf)/length(rho_shuf)
p_pear = sum(abs(rho_pear)<rho_shuf | -abs(rho_pear)>rho_shuf)/length(rho_shuf)
p_rob = sum(abs(rho_rob)<rho_shuf | -abs(rho_rob)>rho_shuf)/length(rho_shuf)

disp('')

 %% Topoplots pre-post
 
% b = 'nobsl';
% 
% data = [biber.real.pre.topo.alpha.data.(b).left,biber.real.post.topo.alpha.data.(b).left,...
%     biber.sham.pre.topo.alpha.data.(b).left,biber.sham.post.topo.alpha.data.(b).left];
% ticks = {'real pre','real post','sham pre','sham post'};
% 
% plotname = 'psd.topo.alpha_timediff.png';
% args = struct('data',data,'channels',{biber.real.pre.topo.alpha.channels},...
%     'layoutfile',biber.real.pre.topo.alpha.layout,'title','Alpha pre post [left]',...
%     'timepointlabels',{ticks},'markers','on','save','no','savename',plotname);
% cschneid_topoplot_eeglab(args)
% 
% % ---
% data = [biber.real.pre.topo.alpha.data.(b).right,biber.real.post.topo.alpha.data.(b).right,...
%     biber.sham.pre.topo.alpha.data.(b).right,biber.sham.post.topo.alpha.data.(b).right];
% ticks = {'real pre','real post','sham pre','sham post'};
% 
% plotname = 'psd.topo.alpha_timediff.png';
% args = struct('data',data,'channels',{biber.real.pre.topo.alpha.channels},...
%     'layoutfile',biber.real.pre.topo.alpha.layout,'title','Alpha pre post [right]',...
%     'timepointlabels',{ticks},'markers','on','save','no','savename',plotname);
% cschneid_topoplot_eeglab(args)
% 
% % ---
% data = [biber.real.pre.topo.alpha.data.(b).diff,biber.real.post.topo.alpha.data.(b).diff,...
%     biber.sham.pre.topo.alpha.data.(b).diff,biber.sham.post.topo.alpha.data.(b).diff];
% ticks = {'real pre','real post','sham pre','sham post'};
% 
% plotname = 'psd.topo.alpha_timediff.png';
% args = struct('data',data,'channels',{biber.real.pre.topo.alpha.channels},...
%     'layoutfile',biber.real.pre.topo.alpha.layout,'title','Alpha pre post [diff]',...
%     'timepointlabels',{ticks},'markers','on','save','no','savename',plotname);
% cschneid_topoplot_eeglab(args)
% 
% % ---
% data = [biber.real.post.topo.alpha.data.(b).left - biber.real.pre.topo.alpha.data.(b).left,...
%         biber.sham.post.topo.alpha.data.(b).left - biber.sham.pre.topo.alpha.data.(b).left,...
%         biber.real.post.topo.alpha.data.(b).right - biber.real.pre.topo.alpha.data.(b).right,...
%         biber.sham.post.topo.alpha.data.(b).right - biber.sham.pre.topo.alpha.data.(b).right,...
%         ];
% ticks = {'post-pre real left','post-pre sham left','post-pre real right','post-pre sham right'};
% 
% plotname = 'psd.topo.alpha_timediff.png';
% args = struct('data',data,'channels',{biber.real.pre.topo.alpha.channels},...
%     'layoutfile',biber.real.pre.topo.alpha.layout,'title','Alpha pre post [left]',...
%     'timepointlabels',{ticks},'markers','on','save','no','savename',plotname);
% cschneid_topoplot_eeglab(args)
% 
% % ---
% data = [biber.real.post.topo.alpha.data.(b).diff - biber.real.pre.topo.alpha.data.(b).diff,...
%         biber.sham.post.topo.alpha.data.(b).diff - biber.sham.pre.topo.alpha.data.(b).diff];
% ticks = {'post-pre real','post-pre sham'};
% 
% plotname = 'psd.topo.alpha_timediff.png';
% args = struct('data',data,'channels',{biber.real.pre.topo.alpha.channels},...
%     'layoutfile',biber.real.pre.topo.alpha.layout,'title','Alpha pre post [diff]',...
%     'timepointlabels',{ticks},'markers','on','save','no','savename',plotname);
% cschneid_topoplot_eeglab(args)
% % 
% % ---
% data = [biber.real.pre.topo.alpha.data.(b).diff - biber.sham.pre.topo.alpha.data.(b).diff,...
%         biber.real.post.topo.alpha.data.(b).diff - biber.sham.post.topo.alpha.data.(b).diff];
% ticks = {'real-sham pre','real-sham post'};
% 
% plotname = 'psd.topo.alpha_timediff.png';
% args = struct('data',data,'channels',{biber.real.pre.topo.alpha.channels},...
%     'layoutfile',biber.real.pre.topo.alpha.layout,'title','Alpha rel sham [diff]',...
%     'timepointlabels',{ticks},'markers','on','save','no','savename',plotname);
% cschneid_topoplot_eeglab(args)

%% Group data in Chinchilla
numparts = 5;
s_s = 9;
part = floor(size(chinchilla.real.(whichLI).left(usesubj,s_s:end),2)./numparts);

for t = 0:numparts
    
    if t > 0
        span = (((t-1)*part+1):t*part)+s_s;
    else
        span = 1:(s_s-1);
    end
    
    LIplot.real.left(:,t+1) = nanmedian(chinchilla.real.(whichLI).left(usesubj,span),2);
    LIplot.real.right(:,t+1) = nanmedian(chinchilla.real.(whichLI).right(usesubj,span),2);
    LIplot.sham.left(:,t+1) = nanmedian(chinchilla.sham.(whichLI).left(usesubj,span),2);
    LIplot.sham.right(:,t+1) = nanmedian(chinchilla.sham.(whichLI).right(usesubj,span),2);
    
%     LIplot.real.left(:,t+1) = nanmedian(chinchilla.real.(whichLI).left(usesubj,span),2)./ adler.palphapower.bsl(usesubj)';
%     LIplot.real.right(:,t+1) = nanmedian(chinchilla.real.(whichLI).right(usesubj,span),2)./ adler.palphapower.bsl(usesubj)';
%     LIplot.sham.left(:,t+1) = nanmedian(chinchilla.sham.(whichLI).left(usesubj,span),2)./ adler.palphapower.bsl(usesubj)';
%     LIplot.sham.right(:,t+1) = nanmedian(chinchilla.sham.(whichLI).right(usesubj,span),2)./ adler.palphapower.bsl(usesubj)';

    [~,pvals.realsham.left(t+1)] = ttest(LIplot.real.left(:,t+1),LIplot.sham.left(:,t+1));
    [~,pvals.realsham.right(t+1)] = ttest(LIplot.real.right(:,t+1),LIplot.sham.right(:,t+1));
    
    [~,pvals.leftright.real(t+1)] = ttest(LIplot.real.right(:,t+1),LIplot.real.left(:,t+1));
    [~,pvals.leftright.sham(t+1)] = ttest(LIplot.sham.right(:,t+1),LIplot.sham.left(:,t+1));
end

fullspan = 9:size(chinchilla.real.(whichLI).left(:,s_s:end),2);
chinchilla.LIspan.real = nanmedian(chinchilla.real.(whichLI).right(usesubj,fullspan) - chinchilla.real.(whichLI).left(usesubj,fullspan),2);
chinchilla.LIspan.sham = nanmedian(chinchilla.sham.(whichLI).right(usesubj,fullspan) - chinchilla.sham.(whichLI).left(usesubj,fullspan),2);
figure; bar([chinchilla.LIspan.real,chinchilla.LIspan.sham])
title('mean LI span during feedback')
legend({'real','sham'})
mean([chinchilla.LIspan.real,chinchilla.LIspan.sham],1)

LIplot.real.right = LIplot.real.right - LIplot.real.right(:,1);
LIplot.real.left = LIplot.real.left - LIplot.real.left(:,1);
LIplot.sham.right = LIplot.sham.right - LIplot.sham.right(:,1);
LIplot.sham.left = LIplot.sham.left - LIplot.sham.left(:,1);

% --- write to file
V1 = LIplot.real.right(:,2:end) - LIplot.real.left(:,2:end);
V2 = LIplot.sham.right(:,2:end) - LIplot.sham.left(:,2:end);
varnames = {'real','sham'};
T = table(V1,V2,'VariableNames',varnames);
writetable(T,[whichLI,'diffLItimeJASP'])

[~,p] = ttest(V1,V2)

x = 1:size(chinchilla.real.(whichLI).left,2);
for s = 1:size(chinchilla.real.(whichLI).left,1)
   [param.real.left(s,:),bstats] = robustfit(x,chinchilla.real.(whichLI).left(s,:),'ols');
   [param.real.right(s,:),bstats] = robustfit(x,chinchilla.real.(whichLI).right(s,:),'ols'); 
   [param.sham.left(s,:),bstats] = robustfit(x,chinchilla.sham.(whichLI).left(s,:),'ols'); 
   [param.sham.right(s,:),bstats] = robustfit(x,chinchilla.sham.(whichLI).right(s,:),'ols'); 
   
   ns.real(s) = nansum(sign(chinchilla.real.(whichLI).right(s,:) - chinchilla.real.(whichLI).left(s,:)));
   ns.sham(s) = nansum(sign(chinchilla.sham.(whichLI).right(s,:) - chinchilla.sham.(whichLI).left(s,:)));
%    mExp(s) = nanmean(chinchilla.real.(whichLI).right(s,:) - chinchilla.real.(whichLI).left(s,:));
%    mSham(s) = nanmean(chinchilla.sham.(whichLI).right(s,:) - chinchilla.sham.(whichLI).left(s,:));
   mExp(s) = nanmean(chinchilla.real.(whichLI).right(s,:)) - nanmean(chinchilla.real.(whichLI).left(s,:));
   mSham(s) = nanmean(chinchilla.sham.(whichLI).right(s,:)) - nanmean(chinchilla.sham.(whichLI).left(s,:));
   trainingEffect(s) = mExp(s) - mSham(s);
end
figure; bar(trainingEffect(usesubj))
figure; boxplot([mExp(usesubj);mSham(usesubj)]'); hold on;
plot([1,2],mean([mExp(usesubj);mSham(usesubj)]'))
figure; plot([mExp(usesubj);mSham(usesubj)])

[pvecL,~,~] = permstattest({nanmean(chinchilla.real.(whichLI).left(usesubj,:),1),nanmean(chinchilla.sham.(whichLI).left(usesubj,:),1)},1000,0.05,0.05,'ttest2',0,1);
[pvecR,~,~] = permstattest({nanmean(chinchilla.real.(whichLI).right(usesubj,:),1),nanmean(chinchilla.sham.(whichLI).right(usesubj,:),1)},1000,0.05,0.05,'ttest2',0,1);
figure; plot(nanmean(chinchilla.real.(whichLI).left,1),'r');
hold on; plot(nanmean(chinchilla.sham.(whichLI).left,1),'r--')
plot(pvecL,'r')
plot(nanmean(chinchilla.real.(whichLI).right(usesubj,:),1),'b');
plot(nanmean(chinchilla.sham.(whichLI).right(usesubj,:),1),'b--')
plot(pvecL,'b')

%% ANOVAs

% --- define data
Y = cat(1,LIdiff.real.pre(:),LIdiff.real.post(:),LIdiff.sham.pre(:),LIdiff.sham.post(:));
SU = cat(1,(1:numsubj)',(1:numsubj)',(1:numsubj)',(1:numsubj)');
F1 = cat(1,-ones(numsubj,1),-ones(numsubj,1),ones(numsubj,1),ones(numsubj,1)); % Feedback
F2 = cat(1,-ones(numsubj,1),ones(numsubj,1),-ones(numsubj,1),ones(numsubj,1)); % Time
F3 = repmat(order,1,4)'; % Order
FACTNAMES = {'feedback','time'};

% --- plot data
figure;
dat = [LIdiff.real.pre(:),LIdiff.real.post(:),LIdiff.sham.pre(:),LIdiff.sham.post(:)];
boxplot(dat);
jitter = repmat(1:size(dat,2),size(dat,1),1) + rand(size(dat))/2 - 0.25;
hold on;
scatter(jitter(:),dat(:));

% Assumption 1: dependent variable continuous <check> Yes

% Assumption 2: within subject factors are matched pairs <check> Yes

% Assumption 3: no significant outliers in any group
% <check> using 3 x standard deviation as cutoff

isoutlier(LIdiff.real.pre,'mean')'
isoutlier(LIdiff.real.post,'mean')'
isoutlier(LIdiff.sham.pre,'mean')'
isoutlier(LIdiff.sham.post,'mean')'

% Assumption 4: Residuals of the GLM are normally distributed
% <check> Not valid!!! using a fitted GLM and Shapiro-Wilk test

tbl = table(Y,F1,F2,F3);
mdl = fitglm(tbl,'ResponseVar','Y','CategoricalVars',[2,3,4]);
figure; subplot(1,2,1); histogram(mdl.Residuals.Raw); subplot(1,2,2); normplot(mdl.Residuals.Raw)
[~, pN, W] = swtest(mdl.Residuals.Raw,0.05);
disp(['Shapiro-Wilk test for GLM residuals: p = ',num2str(pN),', W = ',num2str(round(W,3))]);

% Assumption 5: Sphericity <check> always met when having only 2 levels of
% observations for any given factor

% --- write to file
varnames = {[whichLI,'_exp_pre'],[whichLI,'_exp_post'],[whichLI,'_sham_pre'],[whichLI,'_sham_post'],'order'};
T = table(LIdiff.real.pre(:),LIdiff.real.post(:),LIdiff.sham.pre(:),LIdiff.sham.post(:),order','VariableNames',varnames);
writetable(T,[whichLI,'diff4JASP'])

if pN > 0.05
    % --- perform repeated measures ANOVA
    disp('Difference RT invalid-valid cue');
    stats = rm_anova2(Y,SU,F1,F2,FACTNAMES)
    
    % --- post-hoc tests
    disp('Bonferroni corrected p-value for significance = 0.0125');
    [~,p1,~,stat] = ttest(Exp_pre_diff,Exp_post_diff);
    disp(['t-test Exp pre-post: p = ',num2str(round(p1,3)),', t = ',num2str(round(stat.tstat,3))]);
    [~,p2,~,stat] = ttest(Sham_pre_diff,Sham_post_diff);
    disp(['t-test Sham pre-post: p = ',num2str(round(p2,3)),', t = ',num2str(round(stat.tstat,3))]);
    [~,p3,~,stat] = ttest(Exp_pre_diff,Sham_pre_diff);
    disp(['t-test pre Exp-Sham: p = ',num2str(round(p3,3)),', t = ',num2str(round(stat.tstat,3))]);
    [~,p4,~,stat] = ttest(Exp_post_diff,Sham_post_diff);
    disp(['t-test post Exp-Sham: p = ',num2str(round(p4,3)),', t = ',num2str(round(stat.tstat,3))]);
    
    disp('False discovery rate correction for multiple comparisons')
    [h, crit_p, adj_ci_cvrg, adj_p] = fdr_bh([p1,p2,p3,p4],0.05)
    
    % --- effect size
    [stats,varargout] = mes2way(Y,[F1,F2],'partialeta2','doDataPlot',true,'nBoot',10000,'isDep',[1 1])
    [stats,varargout] = mes1way(Y(1:(2*length(usesubj))),'partialeta2','group',F2(1:(2*length(usesubj))),'nBoot',10000,'isDep',1)
else
    % --- Friedman test (if normality assumption for residual not met)
    % rows = reps and factor time, columns = factor feedback
    v = [Exp_pre_diff(:),Sham_pre_diff(:);Exp_post_diff(:),Sham_post_diff(:)];
    [p,tbl,stats] = friedman(v,numsubj);
end

%% Correlations

PPG.eeg = [LIdiff.real.post(:) - LIdiff.real.pre(:), LIdiff.sham.post(:) - LIdiff.sham.pre(:)];
% PPG.eeg = [LIdiff.real.post(:)./LIdiff.real.pre(:), LIdiff.sham.post(:)./LIdiff.sham.pre(:)];

PPG.ve = [Exp_post_diff(:) - Exp_pre_diff(:), Sham_post_diff(:) - Sham_pre_diff(:)];
% PPG.ve = [Exp_post_catch(:)./Exp_post_norm(:) - Exp_pre_catch(:)./Exp_pre_norm(:), Sham_post_catch(:)./Sham_post_norm(:) - Sham_pre_catch(:)./Sham_pre_norm(:)];
% PPG.ve = [Exp_post_catch(:) - Exp_pre_catch(:), Sham_post_catch(:) - Sham_pre_catch(:)];

PPGD.eeg = PPG.eeg(:,1) - PPG.eeg(:,2);
PPGD.ve = PPG.ve(:,1) - PPG.ve(:,2);
% PPGD.eeg = PPG.eeg(:,1)./PPG.eeg(:,2);
% PPGD.ve = PPG.ve(:,1)./PPG.ve(:,2);

[rho_spear,PVAL] = corr(PPGD.eeg,PPGD.ve,'Type','Spearman');
disp(['Full correlation: rho = ',num2str(rho_spear),'; p = ',num2str(PVAL)])
[rho_pear,PVAL] = corr(PPGD.eeg,PPGD.ve,'Type','Pearson');
disp(['Full correlation: rho = ',num2str(rho_pear),'; p = ',num2str(PVAL)])
% usevec = ~(isoutlier(PPGD.eeg) | isoutlier(PPGD.ve));
% [RHO,PVAL] = corr(PPGD.eeg(usevec),PPGD.ve(usevec),'Type','Pearson');
% disp(['Full correlation: rho = ',num2str(RHO),'; p = ',num2str(PVAL)])

x = PPGD.eeg;
y = PPGD.ve;
for rep = 1:10000
   x_shuf = x;
   y_shuf = y(randperm(length(y)));
   [rho_shuf(rep),p_shuf(rep)] = corr(x_shuf,y_shuf);
end
alpha_ci = 0.05;
p_spear = sum(abs(rho_spear)<rho_shuf | -abs(rho_spear)>rho_shuf)/length(rho_shuf)
p_pear = sum(abs(rho_pear)<rho_shuf | -abs(rho_pear)>rho_shuf)/length(rho_shuf)

[rho,p] = corr(PPG.eeg(:,1),PPG.ve(:,1))
[rho,p] = corr(PPG.eeg(:,2),PPG.ve(:,2))

% Correlations with PPGD

% pre-alpha power
gabslinc = (GA_bsl(:,1)) - (GA_bsl(:,3));
[rho,p] = corr(PPGD.ve,gabslinc)

% day 1 alpha power
[rho,p] = corr(PPGD.ve,adler.palphapower.bsl(usesubj)')

% intertraining alpha power
trainingdiff = chinchilla.LIspan.real - chinchilla.LIspan.sham;
[rho,p] = corr(PPGD.ve,trainingdiff)

% real training alpha power
[rho,p] = corr(VE_x(:,4),chinchilla.LIspan.real)

% reaction times during training
trainingdiffRT = chincRT.real.all' - chincRT.sham.all';
[rho,p] = corr(PPGD.ve,trainingdiffRT)

[rho,p] = corr(Exp_post_norm' - Exp_pre_norm',chincRT.real.all')
[rho,p] = corr(Sham_post_norm' - Sham_pre_norm',chincRT.sham.all')

[rho,p] = corr(chinchilla.LIspan.real,chincRT.real.all')
[rho,p] = corr(chinchilla.LIspan.sham,chincRT.sham.all')

ers = chinchilla.real.palphapower.bsl(usesubj)' - chinchilla.sham.palphapower.bsl(usesubj)'
[rho,p] = corr(PPGD.ve,ers)

[rho,p] = corr(PPG.ve(:,1),chinchilla.real.palphapower.bsl(usesubj)')
[rho,p] = corr(PPG.ve(:,2),chinchilla.sham.palphapower.bsl(usesubj)')



figure;
subplot(1,3,1)
plotsupport = linspace(min([LIdiff.real.post - LIdiff.real.pre;LIdiff.sham.post - LIdiff.sham.pre]),...
                       max([LIdiff.real.post - LIdiff.real.pre;LIdiff.sham.post - LIdiff.sham.pre]),1000);
x = LIdiff.real.post - LIdiff.real.pre;
y = VE_x(:,2) - VE_x(:,1);
counter = 0; clear yhat CI
for s = plotsupport
    counter = counter+1;
    [yhat(counter),CI(counter,:)] = regress_CI_interval(s,x,y,0.05);
end
scatter(x,y,'ok','filled','MarkerEdgeColor',[0 0 0],'MarkerFaceColor','w'); hold on;
scatter(x,y,'ok','filled','MarkerEdgeColor',[0 0 0],'MarkerFaceColor','k'); hold on;
scatter(x,y,'ok','filled','MarkerEdgeColor',[0 0 0],'MarkerFaceColor','w'); hold on;
[brob,~] = robustfit(x,y);
plot(plotsupport,brob(1) + brob(2)*plotsupport,'k','LineWidth',2,'LineStyle','--')
plot(plotsupport,CI,'k','LineWidth',0.5,'LineStyle','--')
title('LIspan vs. babt')
axis('square')
hold on;
x = LIdiff.sham.post - LIdiff.sham.pre;
y = VE_x(:,4) - VE_x(:,3);
counter = 0; clear yhat CI
for s = plotsupport
    counter = counter+1;
    [yhat(counter),CI(counter,:)] = regress_CI_interval(s,x,y,0.05);
end
scatter(x,y,'ok','filled','MarkerEdgeColor',[0 0 0],'MarkerFaceColor','k'); hold on;
[brob,~] = robustfit(x,y);
plot(plotsupport,brob(1) + brob(2)*plotsupport,'k','LineWidth',2,'LineStyle','-')
plot(plotsupport,CI,'k','LineWidth',0.5,'LineStyle','-')
title('pre-post \alpha-LI span vs. pre-post VE')
axis('square')
legend({'experimental','sham'},'Location','southwest')


subplot(1,3,2)
plotsupport = linspace(min([chinchilla.LIspan.real;chinchilla.LIspan.sham]),...
                       max([chinchilla.LIspan.real;chinchilla.LIspan.sham]),1000);
x = chinchilla.LIspan.real;
y = VE_x(:,2) - VE_x(:,1);
counter = 0; clear yhat CI
for s = plotsupport
    counter = counter+1;
    [yhat(counter),CI(counter,:)] = regress_CI_interval(s,x,y,0.05);
end
scatter(x,y,'ok','filled','MarkerEdgeColor',[0 0 0],'MarkerFaceColor','w'); hold on;
scatter(x,y,'ok','filled','MarkerEdgeColor',[0 0 0],'MarkerFaceColor','k'); hold on;
scatter(x,y,'ok','filled','MarkerEdgeColor',[0 0 0],'MarkerFaceColor','w'); hold on;
[brob,~] = robustfit(x,y);
plot(plotsupport,brob(1) + brob(2)*plotsupport,'k','LineWidth',2,'LineStyle','--')
plot(plotsupport,CI,'k','LineWidth',0.5,'LineStyle','--')
title('LIspan vs. babt')
axis('square')
hold on;
x = chinchilla.LIspan.sham;
y = VE_x(:,4) - VE_x(:,3);
counter = 0; clear yhat CI
for s = plotsupport
    counter = counter+1;
    [yhat(counter),CI(counter,:)] = regress_CI_interval(s,x,y,0.05);
end
scatter(x,y,'ok','filled','MarkerEdgeColor',[0 0 0],'MarkerFaceColor','k'); hold on;
[brob,~] = robustfit(x,y);
plot(plotsupport,brob(1) + brob(2)*plotsupport,'k','LineWidth',2,'LineStyle','-')
plot(plotsupport,CI,'k','LineWidth',0.5,'LineStyle','-')
title('\alpha-LI span during training vs. pre-post VE')
axis('square')
legend({'experimental','sham'},'Location','southwest')

subplot(1,3,3)
plotsupport = linspace(min([adler.palphapower.bsl(usesubj)';adler.palphapower.bsl(usesubj)']),...
                       max([adler.palphapower.bsl(usesubj)';adler.palphapower.bsl(usesubj)']),1000);
x = adler.palphapower.bsl(usesubj)';
y = VE_x(:,2) - VE_x(:,1);
counter = 0; clear yhat CI
for s = plotsupport
    counter = counter+1;
    [yhat(counter),CI(counter,:)] = regress_CI_interval(s,x,y,0.05);
end
scatter(x,y,'ok','filled','MarkerEdgeColor',[0 0 0],'MarkerFaceColor','w'); hold on;
scatter(x,y,'ok','filled','MarkerEdgeColor',[0 0 0],'MarkerFaceColor','k'); hold on;
scatter(x,y,'ok','filled','MarkerEdgeColor',[0 0 0],'MarkerFaceColor','w'); hold on;
[brob,~] = robustfit(x,y);
plot(plotsupport,brob(1) + brob(2)*plotsupport,'k','LineWidth',2,'LineStyle','--')
plot(plotsupport,CI,'k','LineWidth',0.5,'LineStyle','--')
title('LIspan vs. babt')
axis('square')
hold on;
x = adler.palphapower.bsl(usesubj)';
y = VE_x(:,4) - VE_x(:,3);
counter = 0; clear yhat CI
for s = plotsupport
    counter = counter+1;
    [yhat(counter),CI(counter,:)] = regress_CI_interval(s,x,y,0.05);
end
scatter(x,y,'ok','filled','MarkerEdgeColor',[0 0 0],'MarkerFaceColor','k'); hold on;
[brob,~] = robustfit(x,y);
plot(plotsupport,brob(1) + brob(2)*plotsupport,'k','LineWidth',2,'LineStyle','-')
plot(plotsupport,CI,'k','LineWidth',0.5,'LineStyle','-')
title('baseline \alpha-power day1 vs. pre-post VE')
axis('square')
legend({'experimental','sham'},'Location','southwest')


%%
fig = figure('Position', get(0, 'Screensize'));
set(gcf,'color','w');

subplot(1,3,1)
hold on;
grid on;
plotsupport = linspace(min(PPGD.eeg),max(PPGD.eeg),1000);
counter = 0; clear yhat CI
for s = plotsupport
    counter = counter+1;
    [yhat(counter),CI(counter,:)] = regress_CI_interval(s,PPGD.eeg,PPGD.ve,0.05);
end
scatter(PPGD.eeg,PPGD.ve,'Marker','o','MarkerFaceColor','k','MarkerEdgeColor','k');
[brob,bstats] = robustfit(PPGD.eeg,PPGD.ve,'ols');
plot(PPGD.eeg,brob(1) + brob(2)*PPGD.eeg,'k','LineWidth',1.5,'LineStyle','-')
plot(plotsupport,CI,'k','LineWidth',0.5,'LineStyle','-')
xlim([-0.6,1.1])
xlabel('interaction effect \alpha-LI')
ylabel('interaction effect differences VE')
title('A changes in \alpha-LI vs. changes in VE')
% legend({'Data','Linear fit'},'Location','southwest');
set(gca, 'FontSize', 16)
axis('square')


% subplot(1,3,1)
% hold on;
% x = (LIdiff.real.post - LIdiff.real.pre) - (LIdiff.sham.post - LIdiff.sham.pre);
% y = PPGD.ve;
% plotsupport = linspace(min(x),max(x),1000);
% counter = 0; clear yhat CI
% for s = plotsupport
%     counter = counter+1;
%     [yhat(counter),CI(counter,:)] = regress_CI_interval(s,x,y,0.05);
% end
% scatter(x,y,'ok','filled','MarkerEdgeColor',[0 0 0],'MarkerFaceColor',[0.7,0.7,0.7]); hold on;
% [brob,~] = robustfit(x,y);
% plot(plotsupport,brob(1) + brob(2)*plotsupport,'k','LineWidth',2,'LineStyle','-')
% plot(plotsupport,CI,'k','LineWidth',0.5,'LineStyle','-')
% title('aPPD_{LI} vs. aPPD_{VE}')
% axis('square')

subplot(1,3,2)
hold on;
grid on;
x = chinchilla.LIspan.real - chinchilla.LIspan.sham;
y = PPGD.ve;
plotsupport = linspace(min(x),max(x),1000);
counter = 0; clear yhat CI
for s = plotsupport
    counter = counter+1;
    [yhat(counter),CI(counter,:)] = regress_CI_interval(s,x,y,0.05);
end
scatter(x,y,'Marker','o','MarkerFaceColor','k','MarkerEdgeColor','k'); hold on;
[brob,bstats] = robustfit(x,y);
plot(plotsupport,brob(1) + brob(2)*plotsupport,'k','LineWidth',2,'LineStyle','-')
plot(plotsupport,CI,'k','LineWidth',0.5,'LineStyle','-')
xlim([-0.8,0.8])
ylim([-0.07,0.13])
title('B differences in training-\alpha-LI vs. changes in VE')
xlabel('\alpha-LI difference during training')
ylabel('interaction effect VE')
set(gca, 'FontSize', 16)
axis('square')

subplot(1,3,3)
hold on;
x = adler.palphapower.bsl(usesubj)';
y = PPGD.ve;
plotsupport = linspace(min(x),max(x),1000);
counter = 0; clear yhat CI
for s = plotsupport
    counter = counter+1;
    [yhat(counter),CI(counter,:)] = regress_CI_interval(s,x,y,0.05);
end
scatter(x,y,'ok','filled','MarkerEdgeColor','k','MarkerFaceColor','k'); hold on;
[brob,~] = robustfit(x,y);
plot(plotsupport,brob(1) + brob(2)*plotsupport,'k','LineWidth',2,'LineStyle','-')
plot(plotsupport,CI,'k','LineWidth',0.5,'LineStyle','-')
title('baseline \alpha-power day1 vs. aPPD_{VE}')
axis('square')

% subplot(2,2,3)
% hold on;
% grid on;
% % x = (log(GA_bsl(:,2))-log(GA_bsl(:,1))) - (log(GA_bsl(:,4))-log(GA_bsl(:,3)));
% % x = (GA_bsl(:,2)-GA_bsl(:,1)) - (GA_bsl(:,4)-GA_bsl(:,3));
% x = adler.palphapower.bsl(usesubj)';
% y = PPGD.eeg;
% % y = mean(LIspan,2);
% plotsupport = linspace(min(x),max(x),1000);
% counter = 0; clear yhat CI
% for s = plotsupport
%     counter = counter+1;
%     [yhat(counter),CI(counter,:)] = regress_CI_interval(s,x,y,0.05);
% end
% scatter(x,y,'Marker','o','MarkerFaceColor','k','MarkerEdgeColor','k'); hold on;
% [brob,~] = robustfit(x,y);
% plot(plotsupport,brob(1) + brob(2)*plotsupport,'k','LineWidth',2,'LineStyle','-')
% plot(plotsupport,CI,'k','LineWidth',0.5,'LineStyle','-')
% % xlim([-0.8,0.8])
% % ylim([-0.07,0.13])
% title('D  bsl \alpha-pow day1 vs. aPPD_{VE}')
% xlabel('baseline \alpha-power day1')
% ylabel('adjusted pre-post differences VE')
% set(gca, 'FontSize', 16)
% axis('square')

% subplot(1,3,3)
figure
hold on;
grid on;
x = adler.palphapower.bsl(usesubj)';
% x = chinchilla.LIspan.real - chinchilla.LIspan.sham;
% x = mean(GA_bsl(:,[1,3]),2);
% x = mean(log(GA_bsl),2);
y = GA_bsl(:,4);
% x = (GA_bsl(:,2)-GA_bsl(:,1)) - (GA_bsl(:,4)-GA_bsl(:,3));
% y = PPGD.eeg;
% y = abs(mean(PPG.eeg,2));
% y = (PPG.eeg(:,1));
% y = chinchilla.LIspan.sham;
% y = [Exp_pre_norm;Exp_post_norm;Sham_pre_norm;Sham_post_norm]'; y = y(:);
% y = mean([Exp_pre_norm;Exp_post_norm;Sham_pre_norm;Sham_post_norm]',2);
% y = mean([Exp_post_norm - Exp_pre_norm;Sham_post_norm - Sham_pre_norm]',2);
% y = mean([Exp_post_norm - Exp_pre_norm]',2);
plotsupport = linspace(min(x),max(x),1000);
counter = 0; clear yhat CI
for s = plotsupport
    counter = counter+1;
    [yhat(counter),CI(counter,:)] = regress_CI_interval(s,x,y,0.05);
end
scatter(x,y,'Marker','o','MarkerFaceColor','k','MarkerEdgeColor','k'); hold on;
[brob,~] = robustfit(x,y);
plot(plotsupport,brob(1) + brob(2)*plotsupport,'k','LineWidth',2,'LineStyle','-')
plot(plotsupport,CI,'k','LineWidth',0.5,'LineStyle','-')
% xlim([-0.8,0.8])
% ylim([-0.07,0.13])
% title('D  bsl \alpha-pow day1 vs. aPPD_{VE}')
xlabel('baseline \alpha-power (log)')
ylabel('norm reaction times')
% ylabel('adjusted pre-post differences VE')
set(gca, 'FontSize', 16)
axis('square')

x = adler.palphapower.bsl(usesubj)';
% y = [(GA_bsl(:,2)-GA_bsl(:,1)),(GA_bsl(:,4)-GA_bsl(:,3))];
% y = y(:,2)
y = PPG.ve(:,2)

nump = 3;
[a,b] = sort(x);
ylen = round(length(x)/nump);
y_split = NaN(ceil(length(x)/nump),nump);

for np = 1:nump
    if np == nump
        len = length(y(b((ylen*(np-1)+1):end)));
        y_split(1:len,np) = y(b((ylen*(np-1)+1):end));
    else
        y_split(1:ylen,np) = y(b((ylen*(np-1)+1):ylen*(np)));
    end
end
figure; boxplot(y_split,'Notch','on')
Ymea = nanmean(y_split);
Ysem = nanstd(y_split)/ylen;
figure(101);
plot(1:nump,Ymea,'k');
errorbar(1:nump,Ymea,Ysem,'k');
xlim([0.8,nump+0.2])
hold on

%% 9vtjsbn0v973905890890n8a67aq92bn plots!!!
plotfontsize = 14;

figure;
% title('A changes in \alpha-LI vs. changes in VE')
set(gcf,'color','w');
axis('square')
hold on;
% grid on;

x = PPGD.eeg;
y = PPGD.ve;
plotsupport = linspace(min(x),max(x),1000);
counter = 0; clear yhat CI
for s = plotsupport
    counter = counter+1;
    [yhat(counter),CI(counter,:)] = regress_CI_interval(s,x,y,0.05);
end
scatter(x,y,100,'Marker','o','MarkerFaceColor','k','MarkerEdgeColor','k');
[brob,bstats] = robustfit(x,y,'ols');
plot(x,brob(1) + brob(2)*x,'k','LineWidth',2.5,'LineStyle','-')
plot(plotsupport,CI,'k','LineWidth',0.5,'LineStyle','-')
% xlim([-0.6,1.1])
xlim([-0.5,0.5])
xlabel('interaction effect $\alpha$-LI $\left[ \frac{\mu V^2}{Hz} \right]$','Interpreter','latex')
ylabel('interaction effect VE [s]')
set(gca, 'FontSize', plotfontsize)
set(gca,'Xtick',-1:0.5:2)

cleanfigure;
strx = strcat('yticklabel style={/pgf/number format/fixed,/pgf/number format/precision=5},scaled y ticks=false', ...
    ',legend image post style={scale=1},legend style = {legend cell align=left, font=\Large}',...
    ',major grid style={gray!20}',...
    ',label style={font=\Large}',...
    ',tick label style={font=\Large}');
matlab2tikz('C:\Users\cschneid\Dropbox\PaperCVSA1\LaTeX\figs\corrLIVE.tex',...
    'extraAxisOptions',strx);

% ---
figure;
% title('B differences in training-\alpha-LI vs. changes in VE')
set(gcf,'color','w');
axis('square')
hold on;
% grid on;

x = chinchilla.LIspan.real - chinchilla.LIspan.sham;
y = PPGD.ve;
plotsupport = linspace(min(x),max(x),1000);
counter = 0; clear yhat CI
for s = plotsupport
    counter = counter+1;
    [yhat(counter),CI(counter,:)] = regress_CI_interval(s,x,y,0.05);
end
scatter(x,y,100,'Marker','o','MarkerFaceColor','k','MarkerEdgeColor','k');
[brob,bstats] = robustfit(x,y,'ols');
plot(x,brob(1) + brob(2)*x,'k','LineWidth',2.5,'LineStyle','-')
plot(plotsupport,CI,'k','LineWidth',0.5,'LineStyle','-')
% xlim([-0.8,0.7])
% ylim([-0.1,0.15])
xlim([-0.1,0.1])
ylim([-0.1,0.15])
xlabel('$\alpha$-LI difference during training $\left[ \frac{\mu V^2}{Hz} \right]$','Interpreter','latex')
ylabel('interaction effect VE [s]')
set(gca, 'FontSize', plotfontsize)
set(gca,'Xtick',-1:0.5:2)

cleanfigure;
strx = strcat('yticklabel style={/pgf/number format/fixed,/pgf/number format/precision=5},scaled y ticks=false', ...
    ',legend image post style={scale=1},legend style = {legend cell align=left, font=\Large}',...
    ',major grid style={gray!20}',...
    ',label style={font=\Large}',...
    ',tick label style={font=\Large}');
matlab2tikz('C:\Users\cschneid\Dropbox\PaperCVSA1\LaTeX\figs\corrLItrainVE.tex',...
    'extraAxisOptions',strx);

% ---
figure;
% title('C Baseline \alpha-power day1 vs. IE_{VE}')
set(gcf,'color','w');
axis('square')
hold on;
% grid on;

x = adler.palphapower.bsl(usesubj)';
y = PPGD.ve;
plotsupport = linspace(min(x),max(x),1000);
counter = 0; clear yhat CI
for s = plotsupport
    counter = counter+1;
    [yhat(counter),CI(counter,:)] = regress_CI_interval(s,x,y,0.05);
end
scatter(x,y,100,'Marker','o','MarkerFaceColor','k','MarkerEdgeColor','k');
[brob,bstats] = robustfit(x,y,'ols');
plot(x,brob(1) + brob(2)*x,'k','LineWidth',2.5,'LineStyle','-')
plot(plotsupport,CI,'k','LineWidth',0.5,'LineStyle','-')
xlabel('baseline $\alpha$-power day 1 $\left[ \frac{\mu V^2}{Hz} \right]$','Interpreter','latex')
ylabel('interaction effect VE [s]')
set(gca, 'FontSize', plotfontsize)
set(gca,'Xtick',0:1:4)

cleanfigure;
strx = strcat('yticklabel style={/pgf/number format/fixed,/pgf/number format/precision=5},scaled y ticks=false', ...
    ',legend image post style={scale=1},legend style = {legend cell align=left, font=\Large}',...
    ',major grid style={gray!20}',...
    ',label style={font=\Large}',...
    ',tick label style={font=\Large}');
matlab2tikz('C:\Users\cschneid\Dropbox\PaperCVSA1\LaTeX\figs\corrLIbslVE.tex',...
    'extraAxisOptions',strx);

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear rho p
disp('Correlation reaction time valid vs LI; left trials')
[rho(1),p(1)] = corr(exp.pre.norm.left',LI.real.pre(:,1));
[rho(2),p(2)] = corr(exp.post.norm.left',LI.real.post(:,1));
[rho(3),p(3)] = corr(sham.pre.norm.left',LI.sham.pre(:,1));
[rho(4),p(4)] = corr(sham.post.norm.left',LI.sham.post(:,1));
rho
p

clear rho p
disp('Correlation reaction time valid vs LI; right trials')
[rho(1),p(1)] = corr(exp.pre.norm.right',LI.real.pre(:,2));
[rho(2),p(2)] = corr(exp.post.norm.right',LI.real.post(:,2));
[rho(3),p(3)] = corr(sham.pre.norm.right',LI.sham.pre(:,2));
[rho(4),p(4)] = corr(sham.post.norm.right',LI.sham.post(:,2));
rho
p

clear rho p
disp('Correlation reaction time valid vs LI; sides combined (mirrored LI)')
[rho(1),p(1)] = corr([exp.pre.norm.left';exp.pre.norm.right'],[-LI.real.pre(:,1);LI.real.pre(:,2)]);
[rho(2),p(2)] = corr([exp.post.norm.left';exp.post.norm.right'],[-LI.real.post(:,1);LI.real.post(:,2)]);
[rho(3),p(3)] = corr([sham.pre.norm.left';sham.pre.norm.right'],[-LI.sham.pre(:,1);LI.sham.pre(:,2)]);
[rho(4),p(4)] = corr([sham.post.norm.left';sham.post.norm.right'],[-LI.sham.post(:,1);LI.sham.post(:,2)]);
rho
p
% figure; scatter([exp.post.norm.left';exp.post.norm.right'],[-LI.real.post(:,1);LI.real.post(:,2)])
% figure; scatter([exp.post.norm.left';exp.post.norm.right'],log([-LI.real.post(:,1);LI.real.post(:,2)]+2))
% [rho,p] = corr([exp.post.norm.left';exp.post.norm.right'],log([-LI.real.post(:,1);LI.real.post(:,2)]+2))

clear rho p
disp('Correlation [slope post-pre] reaction time valid vs LI; sides combined (mirrored LI)')
[rho(1),p(1)] = corr([exp.post.norm.left';exp.post.norm.right']-[exp.pre.norm.left';exp.pre.norm.right'],...
    [-LI.real.post(:,1);LI.real.post(:,2)]-[-LI.real.pre(:,1);LI.real.pre(:,2)]);
[rho(2),p(2)] = corr([sham.post.norm.left';sham.post.norm.right']-[sham.pre.norm.left';sham.pre.norm.right'],...
    [-LI.sham.post(:,1);LI.sham.post(:,2)]-[-LI.sham.pre(:,1);LI.sham.pre(:,2)]);
rho
p

%%%%%
clear rho p
disp('Correlation reaction time invalid vs LI; left trials')
[rho(1),p(1)] = corr(exp.pre.catch.left',LI.real.pre(:,1));
[rho(2),p(2)] = corr(exp.post.catch.left',LI.real.post(:,1));
[rho(3),p(3)] = corr(sham.pre.catch.left',LI.sham.pre(:,1));
[rho(4),p(4)] = corr(sham.post.catch.left',LI.sham.post(:,1));
rho
p

clear rho p
disp('Correlation reaction time invalid vs LI; right trials')
[rho(1),p(1)] = corr(exp.pre.catch.right',LI.real.pre(:,2));
[rho(2),p(2)] = corr(exp.post.catch.right',LI.real.post(:,2));
[rho(3),p(3)] = corr(sham.pre.catch.right',LI.sham.pre(:,2));
[rho(4),p(4)] = corr(sham.post.catch.right',LI.sham.post(:,2));
rho
p

clear rho p
disp('Correlation reaction time invalid vs LI; sides combined (mirrored LI)')
[rho(1),p(1)] = corr([exp.pre.catch.left';exp.pre.catch.right'],[-LI.real.pre(:,1);LI.real.pre(:,2)]);
[rho(2),p(2)] = corr([exp.post.catch.left';exp.post.catch.right'],[-LI.real.post(:,1);LI.real.post(:,2)]);
[rho(3),p(3)] = corr([sham.pre.catch.left';sham.pre.catch.right'],[-LI.sham.pre(:,1);LI.sham.pre(:,2)]);
[rho(4),p(4)] = corr([sham.post.catch.left';sham.post.catch.right'],[-LI.sham.post(:,1);LI.sham.post(:,2)]);
rho
p

clear rho p
disp('Correlation [slope post-pre] reaction time invalid vs LI; sides combined (mirrored LI)')
[rho(1),p(1)] = corr([exp.post.catch.left';exp.post.catch.right']-[exp.pre.catch.left';exp.pre.catch.right'],...
    [-LI.real.post(:,1);LI.real.post(:,2)]-[-LI.real.pre(:,1);LI.real.pre(:,2)]);
[rho(2),p(2)] = corr([sham.post.catch.left';sham.post.catch.right']-[sham.pre.catch.left';sham.pre.catch.right'],...
    [-LI.sham.post(:,1);LI.sham.post(:,2)]-[-LI.sham.pre(:,1);LI.sham.pre(:,2)]);
rho
p

% minmax = [min(PPGD.eeg),max(PPGD.eeg)];
% [yhat,CI] = regress_CI_interval(x,PPGD.eeg,PPGD.rt,0.05)
% s = linspace(min(PPGD.eeg),max(PPGD.eeg),1000);
counter = 0;
plotsupport = linspace(min(PPGD.eeg),max(PPGD.eeg),1000);
for s = plotsupport
    counter = counter+1;
    [yhat(counter),CI(counter,:)] = regress_CI_interval(s,PPGD.eeg,PPGD.ve,0.05);
end

%
a = Exp_post_norm - Exp_pre_norm;
b = Sham_post_norm - Sham_pre_norm;

c = LIdiff.real.post(:) - LIdiff.real.pre(:);
d = LIdiff.sham.post(:) - LIdiff.sham.pre(:);

%% Plots

fig = figure('Position', get(0, 'Screensize'));
set(gcf,'color','w');

% ---
subplot(1,2,1);

x = 1:numparts+1;
shift = 0.05;
hold on;
grid on;

errorbar(x+shift,nanmean(LIplot.sham.left,1),nanstd(LIplot.sham.left,[],1)./sqrt(numsubj),...
    'LineWidth',1,'Color','k','Marker','diamond','markers',12,'MarkerFaceColor','k','LineStyle','--');
errorbar(x-shift,nanmean(LIplot.real.left,1),nanstd(LIplot.real.left,[],1)./sqrt(numsubj),...
    'LineWidth',1,'Color','k','Marker','diamond','markers',12,'MarkerFaceColor','w');

errorbar(x+shift,nanmean(LIplot.sham.right,1),nanstd(LIplot.sham.right,[],1)./sqrt(numsubj),...
    'LineWidth',1,'Color','k','Marker','square','markers',12,'MarkerFaceColor','k','LineStyle','--');
errorbar(x-shift,nanmean(LIplot.real.right,1),nanstd(LIplot.real.right,[],1)./sqrt(numsubj),...
    'LineWidth',1,'Color','k','Marker','square','markers',12,'MarkerFaceColor','w');

set(gca,'XTick',1:length(x));
ticklabel = round(round(20/numparts,2) .* x(1:end-1) - round(20/numparts/2,2),1);
set(gca,'XTickLabel',{'bsl',ticklabel});
% ylim([-0.7,0.7]);
ylim([-0.25,0.25]);
xlabel('trial duration [s]')
ylabel('$\alpha$-lateralization $\left[ \frac{\mu V^2}{Hz} \right]$','Interpreter','latex')
legend({'Sham left','Exp. left','Sham right','Exp. right'},'Location','northwest');
title('A  \alpha-LI during feedback')
set(gca, 'FontSize', 16)
set(gca, 'FontName', 'Calibri')
axis('square')

[~,ps_real] = ttest(LIplot.real.left(:,2:end),LIplot.real.right(:,2:end))
[~,ps_sham] = ttest(LIplot.sham.left(:,2:end),LIplot.sham.right(:,2:end))
disp('False discovery rate correction for multiple comparisons')
[h, crit_p, adj_ci_cvrg, adj_p] = fdr_bh(ps_real,0.05)
[h, crit_p, adj_ci_cvrg, adj_p] = fdr_bh(ps_sham,0.05)


% % ---
% subplot(1,3,2);
%
% hold on;
% grid on;
%
% errorbar(x-shift,nanmean(LIplot.real.right - LIplot.real.left,1),...
%     nanstd(LIplot.real.right - LIplot.real.left,[],1)./sqrt(numsubj),...
%     'LineWidth',1,'Color','k','Marker','o','markers',12,'MarkerFaceColor','w');
% errorbar(x+shift,nanmean(LIplot.sham.right - LIplot.sham.left,1),...
%     nanstd(LIplot.sham.right - LIplot.sham.left,[],1)./sqrt(numsubj),...
%     'LineWidth',1,'Color','k','Marker','o','markers',12,'MarkerFaceColor','k');
%
% set(gca,'XTick',1:length(x));
% ticklabel = round(round(20/numparts,2) .* x - round(20/numparts/2,2),1);
% set(gca,'XTickLabel',ticklabel);
% xlabel('seconds [s]')
% ylabel('baselined Lateralization Index')
% legend({'Experimental','Sham'},'Location','northwest');
% title('Lateralization index')
% set(gca, 'FontSize', 16)


subplot(1,2,2);

y = [mean(LIdiff.real.pre),mean(LIdiff.real.post);
    mean(LIdiff.sham.pre),mean(LIdiff.sham.post)];
errY = [std(LIdiff.real.pre),std(LIdiff.real.post);
    std(LIdiff.sham.pre),std(LIdiff.sham.post)]./sqrt(numsubj);

shift = 0.03;
x = 1:2;

hold on;
grid on;
errorbar(x-shift,y(1,:),errY(1,:),'LineWidth',1,'Color','k','Marker','o','markers',12,'MarkerFaceColor','w');
errorbar(x+shift,y(2,:),errY(2,:),'LineWidth',1,'Color','k','Marker','o','markers',12,'MarkerFaceColor','k');
ylim([0.05,0.6])
xlim([0.7,2.3])
set(gca,'XTick',1:length(x));
set(gca,'XTickLabel',{'pre','post'});
ylabel('$\alpha$-lateralization span $\left[ \frac{\mu V^2}{Hz} \right]$','Interpreter','latex')
legend({'Experimental','Sham'},'Location','southwest');
title('B  \alpha-LI pre and post intervention')
set(gca, 'FontSize', 16)
set(gca, 'FontName', 'Calibri')
axis('square')

% subplot(1,3,3)
% hold on;
% grid on;
% scatter(PPGD.eeg,PPGD.ve,'Marker','o','MarkerFaceColor','k','MarkerEdgeColor','k');
% [brob,bstats] = robustfit(PPGD.eeg,PPGD.ve,'ols');
% plot(PPGD.eeg,brob(1) + brob(2)*PPGD.eeg,'k','LineWidth',1.5,'LineStyle','-')
% plot(plotsupport,CI,'k','LineWidth',0.5,'LineStyle','-')
% xlim([-0.6,1.1])
% xlabel('adjusted pre-post differences \alpha-LI')
% ylabel('adjusted pre-post differences VE')
% title('C  Correlation aPPD_{\alpha-LI} and aPPD_{VE}')
% % legend({'Data','Linear fit'},'Location','southwest');
% set(gca, 'FontSize', 16)
% axis('square')

filename = 'LI.png';
F = getframe(fig);
imwrite(F.cdata, filename);

%% -------------------------------------------------------------------------
figure;
% title('A  \alpha-LI during feedback')
set(gcf,'color','w');
axis('square')
hold on;
% grid on;
set(gca, 'FontSize', plotfontsize)

x = 1:numparts+1;
shift = 0.05;

% dummy data for legend
scatter(x-shift,nanmean(LIplot.real.left,1),70,'diamond','filled','MarkerEdgeColor','k','MarkerFaceColor','w');
scatter(x+shift,nanmean(LIplot.sham.left,1),70,'diamond','filled','MarkerEdgeColor','k','MarkerFaceColor','k');
scatter(x-shift,nanmean(LIplot.real.right,1),70,'square','filled','MarkerEdgeColor','k','MarkerFaceColor','w');
scatter(x+shift,nanmean(LIplot.sham.right,1),70,'square','filled','MarkerEdgeColor','k','MarkerFaceColor','k');

errorbar(x+shift,nanmean(LIplot.sham.left,1),nanstd(LIplot.sham.left,[],1)./sqrt(numsubj),...
    'LineWidth',1,'Color','k','Marker','diamond','markers',5,'MarkerFaceColor','k','LineStyle','--');
errorbar(x-shift,nanmean(LIplot.real.left,1),nanstd(LIplot.real.left,[],1)./sqrt(numsubj),...
    'LineWidth',1,'Color','k','Marker','diamond','markers',5,'MarkerFaceColor','w');

errorbar(x+shift,nanmean(LIplot.sham.right,1),nanstd(LIplot.sham.right,[],1)./sqrt(numsubj),...
    'LineWidth',1,'Color','k','Marker','square','markers',5,'MarkerFaceColor','k','LineStyle','--');
errorbar(x-shift,nanmean(LIplot.real.right,1),nanstd(LIplot.real.right,[],1)./sqrt(numsubj),...
    'LineWidth',1,'Color','k','Marker','square','markers',5,'MarkerFaceColor','w');

% plot markers again for tikz
scatter(x-shift,nanmean(LIplot.real.left,1),200,'diamond','filled','MarkerEdgeColor','k','MarkerFaceColor','w','LineWidth',1);
scatter(x+shift,nanmean(LIplot.sham.left,1),200,'diamond','filled','MarkerEdgeColor','k','MarkerFaceColor','k','LineWidth',1);
scatter(x-shift,nanmean(LIplot.real.right,1),500,'square','filled','MarkerEdgeColor','k','MarkerFaceColor','w','LineWidth',1);
scatter(x+shift,nanmean(LIplot.sham.right,1),500,'square','filled','MarkerEdgeColor','k','MarkerFaceColor','k','LineWidth',1);

set(gca,'XTick',1:length(x));
ticklabel = round(round(20/numparts,2) .* x(1:end-1) - round(20/numparts/2,2),1);
set(gca,'XTickLabel',{'bsl',ticklabel});
% ylim([-0.72,0.72]);
ylim([-0.3,0.3]);
xlim([0.6,length(x)+0.4])
xlabel('time in trial [s]')
ylabel('$\alpha$-lateralization $\left[ \frac{\mu V^2}{Hz} \right]$','Interpreter','latex')
leg = legend({'Exp left','Sham left','Exp right','Sham right'},'Location','northwest');
set(leg,'position',[0.2461 0.725 0.2375 0.2131]);
legend boxoff
axis('square')

cleanfigure;
strx = strcat('yticklabel style={/pgf/number format/fixed,/pgf/number format/precision=5},scaled y ticks=false', ...
    ',legend image post style={scale=1},legend style = {legend cell align=left, font=\Large}',...
    ',major grid style={gray!20}',...
    ',label style={font=\Large}',...
    ',tick label style={font=\Large}');
matlab2tikz('C:\Users\cschneid\Dropbox\PaperCVSA1\LaTeX\figs\LItrain.tex',...
    'extraAxisOptions',strx);

% -------------------------------------------------------------------------
figure;
% title('B  \alpha-LI pre and post intervention')
set(gcf,'color','w');
axis('square')
hold on;
% grid on;
set(gca, 'FontSize', plotfontsize)

y = [mean(LIdiff.real.pre),mean(LIdiff.real.post);
    mean(LIdiff.sham.pre),mean(LIdiff.sham.post)];
errY = [std(LIdiff.real.pre),std(LIdiff.real.post);
    std(LIdiff.sham.pre),std(LIdiff.sham.post)]./sqrt(numsubj);

shift = 0.03;
x = 1:2;

% dummy data for legend
scatter(x-shift,y(1,:),70,'o','filled','MarkerEdgeColor','k','MarkerFaceColor','w');
scatter(x+shift,y(2,:),70,'o','filled','MarkerEdgeColor','k','MarkerFaceColor','k');

% plot data
errorbar(x-shift,y(1,:),errY(1,:),'LineWidth',1,'Color','k','Marker','o','markers',12,'MarkerFaceColor','w');
errorbar(x+shift,y(2,:),errY(2,:),'LineWidth',1,'Color','k','Marker','o','markers',12,'MarkerFaceColor','k');

% plot markers for tikz
scatter(x-shift,y(1,:),500,'o','filled','MarkerFaceColor','w','MarkerEdgeColor','k')
scatter(x+shift,y(2,:),500,'o','filled','MarkerFaceColor','k','MarkerEdgeColor','k')

% ylim([0.05,0.6])
ylim([0.0,0.25])
xlim([0.6,2.4])
set(gca,'XTick',1:length(x));
set(gca,'XTickLabel',{'pre','post'});
ylabel('$\alpha$-lateralization span $\left[ \frac{\mu V^2}{Hz} \right]$','Interpreter','latex')
xlabel('~','Interpreter','latex');
legend({'Experimental','Sham'},'Location','northwest');
legend boxoff

cleanfigure;
strx = strcat('yticklabel style={/pgf/number format/fixed,/pgf/number format/precision=5},scaled y ticks=false', ...
    ',legend image post style={scale=1},legend style = {legend cell align=left, font=\Large}',...
    ',major grid style={gray!20}',...
    ',label style={font=\Large}',...
    ',tick label style={font=\Large}');
matlab2tikz('C:\Users\cschneid\Dropbox\PaperCVSA1\LaTeX\figs\interactionLI.tex',...
    'extraAxisOptions',strx);

%% plot alpha power w.r.t. baseline

avgtype = 'mean';

figure;
subplot(2,2,1);
y = [avg(biber.real.pre.alpha_bsl.rightcue.left,avgtype),avg(biber.real.post.alpha_bsl.rightcue.left,avgtype);
    avg(biber.sham.pre.alpha_bsl.rightcue.left,avgtype),avg(biber.sham.post.alpha_bsl.rightcue.left,avgtype)];
errY = [std(biber.real.pre.alpha_bsl.rightcue.left),std(biber.real.post.alpha_bsl.rightcue.left);
    std(biber.sham.pre.alpha_bsl.rightcue.left),std(biber.sham.post.alpha_bsl.rightcue.left)]./sqrt(numsubj);
shift = 0.03;
x = 1:2;
hold on;
grid on;
ylim([-1,1])
errorbar(x-shift,y(1,:),errY(1,:),'LineWidth',1,'Color','k','Marker','o','markers',12,'MarkerFaceColor','w');
errorbar(x+shift,y(2,:),errY(2,:),'LineWidth',1,'Color','k','Marker','o','markers',12,'MarkerFaceColor','k');
set(gca,'XTick',1:length(x));
set(gca,'XTickLabel',{'pre','post'});
ylabel('bLI span (diff right - left)')
legend({'Experimental','Sham'},'Location','southwest');
title('Right cue, PO7')
set(gca, 'FontSize', 16)

subplot(2,2,2);
y = [avg(biber.real.pre.alpha_bsl.rightcue.right,avgtype),avg(biber.real.post.alpha_bsl.rightcue.right,avgtype);
    avg(biber.sham.pre.alpha_bsl.rightcue.right,avgtype),avg(biber.sham.post.alpha_bsl.rightcue.right,avgtype)];
errY = [std(biber.real.pre.alpha_bsl.rightcue.right),std(biber.real.post.alpha_bsl.rightcue.right);
    std(biber.sham.pre.alpha_bsl.rightcue.right),std(biber.sham.post.alpha_bsl.rightcue.right)]./sqrt(numsubj);
shift = 0.03;
x = 1:2;
hold on;
grid on;
ylim([-1,1])
errorbar(x-shift,y(1,:),errY(1,:),'LineWidth',1,'Color','k','Marker','o','markers',12,'MarkerFaceColor','w');
errorbar(x+shift,y(2,:),errY(2,:),'LineWidth',1,'Color','k','Marker','o','markers',12,'MarkerFaceColor','k');
set(gca,'XTick',1:length(x));
set(gca,'XTickLabel',{'pre','post'});
ylabel('bLI span (diff right - left)')
legend({'Experimental','Sham'},'Location','southwest');
title('Right cue, PO8')
set(gca, 'FontSize', 16)

subplot(2,2,3);
y = [avg(biber.real.pre.alpha_bsl.leftcue.left,avgtype),avg(biber.real.post.alpha_bsl.leftcue.left,avgtype);
    avg(biber.sham.pre.alpha_bsl.leftcue.left,avgtype),avg(biber.sham.post.alpha_bsl.leftcue.left,avgtype)];
errY = [std(biber.real.pre.alpha_bsl.leftcue.left),std(biber.real.post.alpha_bsl.leftcue.left);
    std(biber.sham.pre.alpha_bsl.leftcue.left),std(biber.sham.post.alpha_bsl.leftcue.left)]./sqrt(numsubj);
shift = 0.03;
x = 1:2;
hold on;
grid on;
ylim([-1,1])
errorbar(x-shift,y(1,:),errY(1,:),'LineWidth',1,'Color','k','Marker','o','markers',12,'MarkerFaceColor','w');
errorbar(x+shift,y(2,:),errY(2,:),'LineWidth',1,'Color','k','Marker','o','markers',12,'MarkerFaceColor','k');
set(gca,'XTick',1:length(x));
set(gca,'XTickLabel',{'pre','post'});
ylabel('bLI span (diff right - left)')
legend({'Experimental','Sham'},'Location','southwest');
title('Left cue, PO7')
set(gca, 'FontSize', 16)

subplot(2,2,4);
y = [avg(biber.real.pre.alpha_bsl.leftcue.right,avgtype),avg(biber.real.post.alpha_bsl.leftcue.right,avgtype);
    avg(biber.sham.pre.alpha_bsl.leftcue.right,avgtype),avg(biber.sham.post.alpha_bsl.leftcue.right,avgtype)];
errY = [std(biber.real.pre.alpha_bsl.leftcue.right),std(biber.real.post.alpha_bsl.leftcue.right);
    std(biber.sham.pre.alpha_bsl.leftcue.right),std(biber.sham.post.alpha_bsl.leftcue.right)]./sqrt(numsubj);
shift = 0.03;
x = 1:2;
hold on;
grid on;
ylim([-1,1])
errorbar(x-shift,y(1,:),errY(1,:),'LineWidth',1,'Color','k','Marker','o','markers',12,'MarkerFaceColor','w');
errorbar(x+shift,y(2,:),errY(2,:),'LineWidth',1,'Color','k','Marker','o','markers',12,'MarkerFaceColor','k');
set(gca,'XTick',1:length(x));
set(gca,'XTickLabel',{'pre','post'});
ylabel('bLI span (diff right - left)')
legend({'Experimental','Sham'},'Location','southwest');
title('Left cue, PO8')
set(gca, 'FontSize', 16)

% -----------------------------------------------------

figure;
subplot(2,2,1);
y = [avg(biber.real.pre.alpha_bsl.leftcue.right - biber.real.pre.alpha_bsl.leftcue.left,avgtype),...
    avg(biber.real.post.alpha_bsl.leftcue.right - biber.real.post.alpha_bsl.leftcue.left,avgtype);...
    avg(biber.sham.pre.alpha_bsl.leftcue.right - biber.sham.pre.alpha_bsl.leftcue.left,avgtype),...
    avg(biber.sham.post.alpha_bsl.leftcue.right - biber.sham.post.alpha_bsl.leftcue.left,avgtype)];
errY = [std(biber.real.pre.alpha_bsl.leftcue.right - biber.real.pre.alpha_bsl.leftcue.left),...
    std(biber.real.post.alpha_bsl.leftcue.right - biber.real.post.alpha_bsl.leftcue.left);...
    std(biber.sham.pre.alpha_bsl.leftcue.right - biber.sham.pre.alpha_bsl.leftcue.left),...
    std(biber.sham.post.alpha_bsl.leftcue.right - biber.sham.post.alpha_bsl.leftcue.left)]./sqrt(numsubj);
shift = 0.03;
x = 1:2;
hold on;
grid on;
ylim([-1,1])
errorbar(x-shift,y(1,:),errY(1,:),'LineWidth',1,'Color','k','Marker','o','markers',12,'MarkerFaceColor','w');
errorbar(x+shift,y(2,:),errY(2,:),'LineWidth',1,'Color','k','Marker','o','markers',12,'MarkerFaceColor','k');
set(gca,'XTick',1:length(x));
set(gca,'XTickLabel',{'pre','post'});
ylabel('bLI span (diff right - left)')
legend({'Experimental','Sham'},'Location','southwest');
title('Left cue, LI')
set(gca, 'FontSize', 16)

subplot(2,2,2);
y = [avg(biber.real.pre.alpha_bsl.rightcue.right - biber.real.pre.alpha_bsl.rightcue.left,avgtype),...
    avg(biber.real.post.alpha_bsl.rightcue.right - biber.real.post.alpha_bsl.rightcue.left,avgtype);...
    avg(biber.sham.pre.alpha_bsl.rightcue.right - biber.sham.pre.alpha_bsl.rightcue.left,avgtype),...
    avg(biber.sham.post.alpha_bsl.rightcue.right - biber.sham.post.alpha_bsl.rightcue.left,avgtype)];
errY = [std(biber.real.pre.alpha_bsl.rightcue.right - biber.real.pre.alpha_bsl.rightcue.left),...
    std(biber.real.post.alpha_bsl.rightcue.right - biber.real.post.alpha_bsl.rightcue.left);...
    std(biber.sham.pre.alpha_bsl.rightcue.right - biber.sham.pre.alpha_bsl.rightcue.left),...
    std(biber.sham.post.alpha_bsl.rightcue.right - biber.sham.post.alpha_bsl.rightcue.left)]./sqrt(numsubj);
shift = 0.03;
x = 1:2;
hold on;
grid on;
ylim([-1,1])
errorbar(x-shift,y(1,:),errY(1,:),'LineWidth',1,'Color','k','Marker','o','markers',12,'MarkerFaceColor','w');
errorbar(x+shift,y(2,:),errY(2,:),'LineWidth',1,'Color','k','Marker','o','markers',12,'MarkerFaceColor','k');
set(gca,'XTick',1:length(x));
set(gca,'XTickLabel',{'pre','post'});
ylabel('bLI span (diff right - left)')
legend({'Experimental','Sham'},'Location','southwest');
title('Right cue, LI')
set(gca, 'FontSize', 16)

subplot(2,2,3);
y = [avg(biber.real.pre.alpha_bsl.rightcue.left - biber.real.pre.alpha_bsl.leftcue.left,avgtype),...
    avg(biber.real.post.alpha_bsl.rightcue.left - biber.real.post.alpha_bsl.leftcue.left,avgtype);...
    avg(biber.sham.pre.alpha_bsl.rightcue.left - biber.sham.pre.alpha_bsl.leftcue.left,avgtype),...
    avg(biber.sham.post.alpha_bsl.rightcue.left - biber.sham.post.alpha_bsl.leftcue.left,avgtype)];
errY = [std(biber.real.pre.alpha_bsl.rightcue.left - biber.real.pre.alpha_bsl.leftcue.left),...
    std(biber.real.post.alpha_bsl.rightcue.left - biber.real.post.alpha_bsl.leftcue.left);...
    std(biber.sham.pre.alpha_bsl.rightcue.left - biber.sham.pre.alpha_bsl.leftcue.left),...
    std(biber.sham.post.alpha_bsl.rightcue.left - biber.sham.post.alpha_bsl.leftcue.left)]./sqrt(numsubj);
shift = 0.03;
x = 1:2;
hold on;
grid on;
ylim([-1,1])
errorbar(x-shift,y(1,:),errY(1,:),'LineWidth',1,'Color','k','Marker','o','markers',12,'MarkerFaceColor','w');
errorbar(x+shift,y(2,:),errY(2,:),'LineWidth',1,'Color','k','Marker','o','markers',12,'MarkerFaceColor','k');
set(gca,'XTick',1:length(x));
set(gca,'XTickLabel',{'pre','post'});
ylabel('bLI span (diff right - left)')
legend({'Experimental','Sham'},'Location','southwest');
title('Right cue - left cue, PO7')
set(gca, 'FontSize', 16)

subplot(2,2,4);
y = [avg(biber.real.pre.alpha_bsl.rightcue.right - biber.real.pre.alpha_bsl.leftcue.right,avgtype),...
    avg(biber.real.post.alpha_bsl.rightcue.right - biber.real.post.alpha_bsl.leftcue.right,avgtype);...
    avg(biber.sham.pre.alpha_bsl.rightcue.right - biber.sham.pre.alpha_bsl.leftcue.right,avgtype),...
    avg(biber.sham.post.alpha_bsl.rightcue.right - biber.sham.post.alpha_bsl.leftcue.right,avgtype)];
errY = [std(biber.real.pre.alpha_bsl.rightcue.right - biber.real.pre.alpha_bsl.leftcue.right),...
    std(biber.real.post.alpha_bsl.rightcue.right - biber.real.post.alpha_bsl.leftcue.right);...
    std(biber.sham.pre.alpha_bsl.rightcue.right - biber.sham.pre.alpha_bsl.leftcue.right),...
    std(biber.sham.post.alpha_bsl.rightcue.right - biber.sham.post.alpha_bsl.leftcue.right)]./sqrt(numsubj);
shift = 0.03;
x = 1:2;
hold on;
grid on;
ylim([-1,1])
errorbar(x-shift,y(1,:),errY(1,:),'LineWidth',1,'Color','k','Marker','o','markers',12,'MarkerFaceColor','w');
errorbar(x+shift,y(2,:),errY(2,:),'LineWidth',1,'Color','k','Marker','o','markers',12,'MarkerFaceColor','k');
set(gca,'XTick',1:length(x));
set(gca,'XTickLabel',{'pre','post'});
ylabel('bLI span (diff right - left)')
legend({'Experimental','Sham'},'Location','southwest');
title('Right cue - left cue, PO8')
set(gca, 'FontSize', 16)


%% plot LI changes

medfiltn = 3;
figure;
hold on;
PP_Exp_left = medfilt1(nanmean(biber.real.post.(whichLI).left(usesubj,:),1) - nanmean(biber.real.pre.(whichLI).left(usesubj,:),1),medfiltn);
PP_Exp_right = medfilt1(nanmean(biber.real.post.(whichLI).right(usesubj,:),1) - nanmean(biber.real.pre.(whichLI).right(usesubj,:),1),medfiltn);
PP_Sham_left = medfilt1(nanmean(biber.sham.post.(whichLI).left(usesubj,:),1) - nanmean(biber.sham.pre.(whichLI).left(usesubj,:),1),medfiltn);
PP_Sham_right = medfilt1(nanmean(biber.sham.post.(whichLI).right(usesubj,:),1) - nanmean(biber.sham.pre.(whichLI).right(usesubj,:),1),medfiltn);
plot((1:length(PP_Exp_left))./16 - 0.5,PP_Exp_left,'r');
plot((1:length(PP_Exp_right))./16 - 0.5,PP_Exp_right,'b');
plot((1:length(PP_Sham_left))./16 - 0.5,PP_Sham_left,'r--');
plot((1:length(PP_Sham_right))./16 - 0.5,PP_Sham_right,'b--');
cschneid_vline(0,'k','-','cue');

figure;
bar([mean(PP_Exp_left),mean(PP_Sham_left),0,mean(PP_Exp_right),mean(PP_Sham_right)])

avgtype = 'median' %'pseudomedian';
L.E.pre = avg(LI.real.pre(:,1),avgtype);
L.E.post = avg(LI.real.post(:,1),avgtype);
R.E.pre = avg(LI.real.pre(:,2),avgtype);
R.E.post = avg(LI.real.post(:,2),avgtype);

L.S.pre = avg(LI.sham.pre(:,1),avgtype);
L.S.post = avg(LI.sham.post(:,1),avgtype);
R.S.pre = avg(LI.sham.pre(:,2),avgtype);
R.S.post = avg(LI.sham.post(:,2),avgtype);

u = [(LI.real.post(:,3) - LI.real.pre(:,3))./abs(LI.real.pre(:,3)),(LI.sham.post(:,3) - LI.sham.pre(:,3))./abs(LI.sham.pre(:,3))]
% plot baseline LI
bslLI = [bsl.real.pre(:,usecase),bsl.real.post(:,usecase),bsl.sham.pre(:,usecase),bsl.sham.post(:,usecase)];
% bslLI_changes = [(bslLI(:,2) - bslLI(:,1)) ./ bslLI(:,1), (bslLI(:,4) - bslLI(:,3)) ./ bslLI(:,3)];
bslLI_changes = [(bslLI(:,2) - bslLI(:,1)), (bslLI(:,4) - bslLI(:,3))];

LI_changes = [(LI.real.post(:,1) - LI.real.pre(:,1))./abs(LI.real.pre(:,1)) ,(LI.sham.post(:,1) - LI.sham.pre(:,1))./abs(LI.sham.pre(:,1)),...
    (LI.real.post(:,2) - LI.real.pre(:,2))./abs(LI.real.pre(:,2)),(LI.sham.post(:,2) - LI.sham.pre(:,2))./abs(LI.sham.pre(:,2))];

figure; boxplot(bslLI);
figure; boxplot(bslLI_changes);

% plot global alpha
figure;
boxplot(GA_bsl);

GA_bsl_changes = [(GA_bsl(:,2) - GA_bsl(:,1)) ./ GA_bsl(:,1), (GA_bsl(:,4) - GA_bsl(:,3)) ./ GA_bsl(:,3)];
a = [(GA_bsl(:,2) - GA_bsl(:,1)), (GA_bsl(:,4) - GA_bsl(:,3))];
b = [(GA_bsl(:,2) ./ GA_bsl(:,1)), (GA_bsl(:,4) ./ GA_bsl(:,3))];
figure;
boxplot(GA_bsl_changes);

avgtype = 'median' %'pseudomedian';
E.pre = avg(GA_bsl(:,1),avgtype);
E.post = avg(GA_bsl(:,2),avgtype);
S.pre = avg(GA_bsl(:,3),avgtype);
S.post = avg(GA_bsl(:,4),avgtype);

data1 = [E.pre,S.pre;E.post,S.post]';
err1(:,:,1) = [0, 0; 0, 0];
err1(:,:,2) = [std(GA_bsl(:,1)),std(GA_bsl(:,2));std(GA_bsl(:,3)),std(GA_bsl(:,4))]'./sqrt(length(E.pre));
err1(:,:,2) = [mad(GA_bsl(:,1)),mad(GA_bsl(:,2));mad(GA_bsl(:,3)),mad(GA_bsl(:,4))]'./sqrt(length(E.pre));
h = barwitherr(err1,data1);
set(h(1),'FaceColor','k');
set(h(2),'FaceColor','w');
legend({'pre','post'},'Location','northwest')
xticklabels({'exp','sham'})
title('Global baseline alpha changes')
ylabel('baseline alpha power')

%% LI changes and GA changes
fig = figure('Position', get(0, 'Screensize'));
set(gcf,'color','w');
subplot(19,2,1:2:18)
data2 = [R.E.pre,R.S.pre;R.E.post,R.S.post]';
err2(:,:,2) = [std(LI.real.pre(:,2)),std(LI.sham.pre(:,2));std(LI.real.post(:,2)),std(LI.sham.post(:,2))]'./sqrt(size(biber.real.pre.(whichLI).right,1));
err2(:,:,1) = [0, 0; 0, 0];
h = barwitherr(err2,data2);
set(h(1),'FaceColor','k');
set(h(2),'FaceColor','w');

ylim([0,0.45])
legend({'pre','post'},'Location','northwest')
xticklabels({'exp','sham'})
title('Lateralization index changes')
ylabel('right side LI')
grid on;
set(gca, 'FontSize', 16)

subplot(19,2,21:2:37)
data1 = [L.E.pre,L.S.pre;L.E.post,L.S.post]';
err1(:,:,1) = [std(LI.real.pre(:,1)),std(LI.sham.pre(:,1));std(LI.real.post(:,1)),std(LI.sham.post(:,1))]'./sqrt(size(biber.real.pre.(whichLI).right,1));
err1(:,:,2) = [0, 0; 0, 0];
h = barwitherr(err1,data1);
set(h(1),'FaceColor','k');
set(h(2),'FaceColor','w');
ylabel('left side LI')
ylim([-0.45,0])
grid on;
set(gca, 'FontSize', 16)

subplot(19,2,2:2:38)
clear err*
data1 = mean(GA_bsl_changes) * 100;
err1(:,:,1) = [std(GA_bsl_changes(:,1) * 100),std(GA_bsl_changes(:,2) * 100)]./sqrt(size(biber.real.pre.(whichLI).right,1));
err1(:,:,2) = [std(GA_bsl_changes(:,1) * 100),std(GA_bsl_changes(:,2) * 100)]./sqrt(size(biber.real.pre.(whichLI).right,1));
h = barwitherr(err1,data1);
set(h(1),'FaceColor','w');
ylabel('changes in baseline \alpha-power [%]')
xticks([1,2])
xticklabels({'experimental','sham'})
grid on;
set(gca, 'FontSize', 16)

%%


% figure;
% avgtype = 'mean' %'pseudomedian';
% L.E.pre = avg(VE.exp.pre.left,avgtype);
% L.E.post = avg(VE.exp.post.left,avgtype);
% R.E.pre = avg(VE.exp.pre.right,avgtype);
% R.E.post = avg(VE.exp.post.left,avgtype);
% 
% L.S.pre = avg(VE.sham.pre.left,avgtype);
% L.S.post = avg(VE.sham.post.left,avgtype);
% R.S.pre = avg(VE.sham.pre.right,avgtype);
% R.S.post = avg(VE.sham.post.left,avgtype);
% 
% data1 = -[L.E.pre,L.S.pre;L.E.post,L.S.post]';
% err1(:,:,1) = [std(VE.exp.pre.left),std(VE.sham.pre.left);std(VE.exp.post.left),std(VE.sham.post.left)]'./sqrt(length(VE.exp.pre.left));
% err1(:,:,2) = [0, 0; 0, 0];
% h = barwitherr(err1,data1);
% set(h(1),'FaceColor','k');
% set(h(2),'FaceColor','w');
% 
% hold on;
% grid on;
% 
% data2 = [R.E.pre,R.S.pre;R.E.post,R.S.post]';
% err2(:,:,2) = [std(VE.exp.pre.right),std(VE.sham.pre.right);std(VE.exp.post.right),std(VE.sham.post.right)]'./sqrt(length(VE.exp.pre.right));
% err2(:,:,1) = [0, 0; 0, 0];
% h = barwitherr(err2,data2);
% set(h(1),'FaceColor','k');
% set(h(2),'FaceColor','w');
% 
% ylim([-0.45,0.45])
% legend({'pre','post'},'Location','northwest')
% xticklabels({'exp','sham'})
% title('Validity Effect changes')
% ylabel('left side --- VE --- right side')

% ---------------------
gray = [0.4,0.4,0.4];
figure;
subplot(1,2,1)
hold on;
grid on;
x = 1:2;
y1 = [nanmean(L.E.pre),nanmean(L.E.post)];
y2 = [nanmean(L.S.pre),nanmean(L.S.post)];
s1 = [nanstd(LI.real.pre(:,1)),nanstd(LI.real.post(:,1))]/sqrt(length(day1.left));
s2 = [nanstd(LI.sham.pre(:,1)),nanstd(LI.sham.post(:,1))]/sqrt(length(day1.left));
errorbar(x-shift,y1,s1,'LineWidth',1,'Color',gray,'Marker','<','markers',12,'MarkerFaceColor',gray)
errorbar(x+shift,y2,s2,'LineWidth',1,'Color',gray,'Marker','<','markers',12,'MarkerFaceColor',gray,'LineStyle','--')
% ylim([0.1,0.21]);
xticks([1,2])
ylabel('Lateralization index [log(V^2)]')
xticklabels({'pre','post'})
legend({'Exp left side','Sham left side'},'Location','southeast')
title('A  LI left side')
set(gca, 'FontSize', 16)

subplot(1,2,2)
hold on;
grid on;
x = 1:2;
y1 = [nanmean(R.E.pre),nanmean(R.E.post)];
y2 = [nanmean(R.S.pre),nanmean(R.S.post)];
s1 = [nanstd(LI.real.pre(:,2)),nanstd(LI.real.post(:,2))]/sqrt(length(day1.left));
s2 = [nanstd(LI.sham.pre(:,2)),nanstd(LI.sham.post(:,2))]/sqrt(length(day1.left));
errorbar(x-shift,y1,s1,'LineWidth',1,'Color','k','Marker','>','markers',12,'MarkerFaceColor','k')
errorbar(x+shift,y2,s2,'LineWidth',1,'Color','k','Marker','>','markers',12,'MarkerFaceColor','k','LineStyle','--')
% ylim([0.1,0.21]);
xticks([1,2])
ylabel('Validity effect [s]')
xticklabels({'pre','post'})
legend({'Exp right side','Sham right side'},'Location','southeast')
title('B LI right side')
set(gca, 'FontSize', 16)
%%%

% % VE - LI
% A = [VE.exp.post.left' - VE.exp.pre.left',VE.sham.post.left' - VE.sham.pre.left',VE.exp.post.right' - VE.exp.pre.right', VE.sham.post.right' - VE.sham.pre.right'];
% B = [-(LI.real.post(:,1) - LI.real.pre(:,1)),-(LI.sham.post(:,1) - LI.sham.pre(:,1)), LI.real.post(:,2) - LI.real.pre(:,2),LI.sham.post(:,2) - LI.sham.pre(:,2)];
% 
% % Left-right
% A = [VE.exp.post.left' - VE.exp.pre.left',VE.sham.post.left' - VE.sham.pre.left',-(LI.real.post(:,1) - LI.real.pre(:,1)),-(LI.sham.post(:,1) - LI.sham.pre(:,1))];
% B = [VE.exp.post.right' - VE.exp.pre.right',VE.sham.post.right' - VE.sham.pre.right',LI.real.post(:,2) - LI.real.pre(:,2),LI.sham.post(:,2) - LI.sham.pre(:,2)];

% Exp - Sham
% A = [VE.exp.post.left' - VE.exp.pre.left',VE.exp.post.right' - VE.exp.pre.right',(LI.real.post(:,1) - LI.real.pre(:,1)),LI.real.post(:,2) - LI.real.pre(:,2)];
% B = [VE.sham.post.left' - VE.sham.pre.left',VE.sham.post.right' - VE.sham.pre.right',(LI.sham.post(:,1) - LI.sham.pre(:,1)),LI.sham.post(:,2) - LI.sham.pre(:,2)];
% 
% C = [mean(A(:,1:2),2),A(:,4) - A(:,3)];
% D = [mean(B(:,1:2),2),B(:,4) - B(:,3)];

A = [exp.post.norm.left' - exp.pre.norm.left', sham.post.norm.left' - sham.pre.norm.left', exp.post.norm.right' - exp.pre.norm.right', sham.post.norm.right' - sham.pre.norm.right'];
B = [(LI.real.post(:,1) - LI.real.pre(:,1)),(LI.sham.post(:,1) - LI.sham.pre(:,1)), LI.real.post(:,2) - LI.real.pre(:,2),LI.sham.post(:,2) - LI.sham.pre(:,2)];

E = [exp.post.norm.left',exp.pre.norm.left',sham.post.norm.left',sham.pre.norm.left',exp.post.norm.right',exp.pre.norm.right',sham.post.norm.right',sham.pre.norm.right'];
F = [-LI.real.post(:,1),-LI.real.pre(:,1),-LI.sham.post(:,1),-LI.sham.pre(:,1),LI.real.post(:,2),LI.real.pre(:,2),LI.sham.post(:,2),LI.sham.pre(:,2)];


figure;
% bar([mean(A)./norm(mean(A));mean(B)./norm(mean(B))]')
bar([mean(A);mean(B)]')

[rho,p] = corr(A(:),B(:))
[rho,p] = corr(A(:),B(:),'type','Spearman')

[rho,p] = corr(E(:),F(:))
[rho,p] = corr(E(:),F(:),'type','Spearman')

% [rho,p] = corr(A(:,1),B(:,1))
% [rho,p] = corr(A(:,2),B(:,2))
% [rho,p] = corr(A(:,3),B(:,3))
% [rho,p] = corr(A(:,4),B(:,4))


[~,p] = ttest2(mean(biber.real.pre.(whichLI).left(usesubj,:),2),mean(biber.real.post.(whichLI).left(usesubj,:),2))
[~,p] = ttest2(mean(biber.real.pre.(whichLI).right(usesubj,:),2),mean(biber.real.post.(whichLI).right(usesubj,:),2))
[~,p] = ttest2(mean(biber.sham.pre.(whichLI).left(usesubj,:),2),mean(biber.sham.post.(whichLI).left(usesubj,:),2))
[~,p] = ttest2(mean(biber.sham.pre.(whichLI).right(usesubj,:),2),mean(biber.sham.post.(whichLI).right(usesubj,:),2))

% difference gradient
medfiltn = 1;
figure;
hold on;
t = (1:96)./16 - 0.5;
PP_Exp_pre = medfilt1(nanmean(biber.real.pre.(whichLI).right(usesubj,:),1) - nanmean(biber.real.pre.(whichLI).left(usesubj,:),1),medfiltn);
PP_Exp_post = medfilt1(nanmean(biber.real.post.(whichLI).right(usesubj,:),1) - nanmean(biber.real.post.(whichLI).left(usesubj,:),1),medfiltn);
PP_Sham_pre = medfilt1(nanmean(biber.sham.pre.(whichLI).right(usesubj,:),1) - nanmean(biber.sham.pre.(whichLI).left(usesubj,:),1),medfiltn);
PP_Sham_post = medfilt1(nanmean(biber.sham.post.(whichLI).right(usesubj,:),1) - nanmean(biber.sham.post.(whichLI).left(usesubj,:),1),medfiltn);
plot((1:length(PP_Exp_pre))./16 - 0.5,PP_Exp_pre,'b');
plot((1:length(PP_Exp_post))./16 - 0.5,PP_Exp_post,'r');
plot((1:length(PP_Sham_pre))./16 - 0.5,PP_Sham_pre,'b--');
plot((1:length(PP_Sham_post))./16 - 0.5,PP_Sham_post,'r--');
cschneid_vline(0,'k','-','cue');

figure;
hold on;
grid on;
plot((1:length(PP_Exp_pre))./16 - 0.5,PP_Exp_post - PP_Exp_pre,'b');
plot((1:length(PP_Sham_pre))./16 - 0.5,PP_Sham_post - PP_Sham_pre,'r');
% plot((1:length(PP_Sham_pre))./16 - 0.5,mean([PP_Exp_post - PP_Exp_pre;PP_Sham_post - PP_Sham_pre],1),'m');
hline(mean(PP_Exp_post - PP_Exp_pre),'b:','mean Exp')
hline(mean(PP_Sham_post - PP_Sham_pre),'r:','mean Sham')

figure;
hold on;
grid on;
plot((1:length(PP_Exp_pre))./16 - 0.5,PP_Exp_pre - PP_Sham_pre,'c');
plot((1:length(PP_Sham_pre))./16 - 0.5,PP_Exp_post - PP_Sham_post,'g');
hline(mean(PP_Exp_pre - PP_Sham_pre),'c:','mean pre')
hline(mean(PP_Exp_post - PP_Sham_post),'g:','mean post')

%% plot changes per group LI span

LIday1.all = nanmean(adler.(whichLI).right(usesubj,:) - adler.(whichLI).left(usesubj,:),2);
LIday1.G1 = nanmean(adler.(whichLI).right(usesubj(order==2),:) - adler.(whichLI).left(usesubj(order==2),:),2);
LIday1.G2 = nanmean(adler.(whichLI).right(usesubj(order==3),:) - adler.(whichLI).left(usesubj(order==3),:),2);
LIday2_pre.all = nanmean([biber.real.pre.(whichLI).right(usesubj(order==2),:)-biber.real.pre.(whichLI).left(usesubj(order==2),:);...
    biber.sham.pre.(whichLI).right(usesubj(order==3),:)-biber.sham.pre.(whichLI).left(usesubj(order==3),:)],2);
LIday2_pre.G1 = nanmean(biber.real.pre.(whichLI).right(usesubj(order==2),:)-biber.real.pre.(whichLI).left(usesubj(order==2),:),2);
LIday2_pre.G2 = nanmean(biber.sham.pre.(whichLI).right(usesubj(order==3),:)-biber.sham.pre.(whichLI).left(usesubj(order==3),:),2);
LIday2_post.G1 = nanmean(biber.real.post.(whichLI).right(usesubj(order==2),:)-biber.real.post.(whichLI).left(usesubj(order==2),:),2);
LIday2_post.G2 = nanmean(biber.sham.post.(whichLI).right(usesubj(order==3),:)-biber.sham.post.(whichLI).left(usesubj(order==3),:),2);

LIday3_pre.G1 = nanmean(biber.sham.pre.(whichLI).right(usesubj(order==2),:)-biber.sham.pre.(whichLI).left(usesubj(order==2),:),2);
LIday3_pre.G2 = nanmean(biber.real.pre.(whichLI).right(usesubj(order==3),:)-biber.real.pre.(whichLI).left(usesubj(order==3),:),2);
LIday3_post.G1 = nanmean(biber.sham.post.(whichLI).right(usesubj(order==2),:)-biber.sham.post.(whichLI).left(usesubj(order==2),:),2);
LIday3_post.G2 = nanmean(biber.real.post.(whichLI).right(usesubj(order==3),:)-biber.real.post.(whichLI).left(usesubj(order==3),:),2);


figure;
subplot(1,2,2)
hold on;
grid on;
Y_1 = [nanmean(LIday1.G1),nanmean(LIday2_pre.G1),nanmean(LIday2_post.G1),nanmean(LIday3_pre.G1),nanmean(LIday3_post.G1)];
SEM_1 = [nanstd(LIday1.G1)./sqrt(length(LIday1.G1)),...
        nanstd(LIday2_pre.G1)./sqrt(length(LIday2_pre.G1)),...
        nanstd(LIday2_post.G1)./sqrt(length(LIday2_post.G1)),...
        nanstd(LIday3_pre.G1)./sqrt(length(LIday3_pre.G1)),....
        nanstd(LIday3_post.G1)./sqrt(length(LIday3_post.G1))];
Y_2 = [nanmean(LIday1.G2),nanmean(LIday2_pre.G2),nanmean(LIday2_post.G2),nanmean(LIday3_pre.G2),nanmean(LIday3_post.G2)];
SEM_2 = [nanstd(LIday1.G2)./sqrt(length(LIday1.G2)),...
        nanstd(LIday2_pre.G2)./sqrt(length(LIday2_pre.G2)),...
        nanstd(LIday2_post.G2)./sqrt(length(LIday2_post.G2)),...
        nanstd(LIday3_pre.G2)./sqrt(length(LIday3_pre.G2)),....
        nanstd(LIday3_post.G2)./sqrt(length(LIday3_post.G2))];
    
% Y_1 = [nanmean(LIday1.G1 - LIday1.G1),nanmean(LIday2_pre.G1 - LIday1.G1),nanmean(LIday2_post.G1 - LIday1.G1),nanmean(LIday3_pre.G1 - LIday1.G1),nanmean(LIday3_post.G1 - LIday1.G1)];
% SEM_1 = [nanstd(LIday1.G1 - LIday1.G1)./sqrt(length(LIday1.G1)),...
%         nanstd(LIday2_pre.G1 - LIday1.G1)./sqrt(length(LIday2_pre.G1)),...
%         nanstd(LIday2_post.G1 - LIday1.G1)./sqrt(length(LIday2_post.G1)),...
%         nanstd(LIday3_pre.G1 - LIday1.G1)./sqrt(length(LIday3_pre.G1)),....
%         nanstd(LIday3_post.G1 - LIday1.G1)./sqrt(length(LIday3_post.G1))];
% Y_2 = [nanmean(LIday1.G2 - LIday1.G2),nanmean(LIday2_pre.G2 - LIday1.G2),nanmean(LIday2_post.G2 - LIday1.G2),nanmean(LIday3_pre.G2 - LIday1.G2),nanmean(LIday3_post.G2 - LIday1.G2)];
% SEM_2 = [nanstd(LIday1.G2 - LIday1.G2)./sqrt(length(LIday1.G2)),...
%         nanstd(LIday2_pre.G2 - LIday1.G2)./sqrt(length(LIday2_pre.G2)),...
%         nanstd(LIday2_post.G2 - LIday1.G2)./sqrt(length(LIday2_post.G2)),...
%         nanstd(LIday3_pre.G2 - LIday1.G2)./sqrt(length(LIday3_pre.G2)),....
%         nanstd(LIday3_post.G2 - LIday1.G2)./sqrt(length(LIday3_post.G2))];
x = 1:5;
errorbar(x+shift,Y_2,SEM_2,'LineWidth',1,'Color','k','Marker','square','markers',12,'MarkerFaceColor','k')
errorbar(x-shift,Y_1,SEM_1,'LineWidth',1,'Color','k','Marker','diamond','markers',12,'MarkerFaceColor','w')
xlim([0.8,5.2])
xticks([1,2,3,4,5])
ylabel('Lateralization index span [mean(right - left)]')
xticklabels({'day 1','day2 pre','day2 post','day3 pre','day3 post'})
legend({'sham day 2','exp. day 2'})
title('Evolution of the lateralization index span (separate groups)')
set(gca, 'FontSize', 14)

% -----------------------------------------------------------
subplot(1,2,1)
hold on;
grid on;

Y_0 = [nanmean(LIday1.all),nanmean(LIday2_pre.all)];
SEM_0 = [nanstd(LIday1.all)./sqrt(length(LIday1.all)),nanstd(LIday2_pre.all)./sqrt(length(LIday2_pre.all))];      
Y_1 = [nanmean(LIday2_post.G1),nanmean(LIday3_pre.G1),nanmean(LIday3_post.G1)];
SEM_1 = [nanstd(LIday2_post.G1)./sqrt(length(LIday2_post.G1)),...
        nanstd(LIday3_pre.G1)./sqrt(length(LIday3_pre.G1)),....
        nanstd(LIday3_post.G1)./sqrt(length(LIday3_post.G1))];
Y_2 = [nanmean(LIday2_post.G2),nanmean(LIday3_pre.G2),nanmean(LIday3_post.G2)];
SEM_2 = [nanstd(LIday2_post.G2)./sqrt(length(LIday2_post.G2)),...
        nanstd(LIday3_pre.G2)./sqrt(length(LIday3_pre.G2)),....
        nanstd(LIday3_post.G2)./sqrt(length(LIday3_post.G2))];
    
% Y_0 = [nanmean(LIday1.all - LIday1.all),nanmean(LIday2_pre.all - LIday1.all)];
% SEM_0 = [nanstd(LIday1.all - LIday1.all)./sqrt(length(LIday1.all)),nanstd(LIday2_pre.all - LIday1.all)./sqrt(length(LIday2_pre.all))];      
% Y_1 = [nanmean(LIday2_post.G1 - LIday1.G1),nanmean(LIday3_pre.G1 - LIday1.G1),nanmean(LIday3_post.G1 - LIday1.G1)];
% SEM_1 = [nanstd(LIday2_post.G1 - LIday1.G1)./sqrt(length(LIday2_post.G1)),...
%         nanstd(LIday3_pre.G1 - LIday1.G1)./sqrt(length(LIday3_pre.G1)),....
%         nanstd(LIday3_post.G1 - LIday1.G1)./sqrt(length(LIday3_post.G1))];
% Y_2 = [nanmean(LIday2_post.G2 - LIday1.G2),nanmean(LIday3_pre.G2 - LIday1.G2),nanmean(LIday3_post.G2 - LIday1.G2)];
% SEM_2 = [nanstd(LIday2_post.G2 - LIday1.G2)./sqrt(length(LIday2_post.G2)),...
%         nanstd(LIday3_pre.G2 - LIday1.G2)./sqrt(length(LIday3_pre.G2)),....
%         nanstd(LIday3_post.G2 - LIday1.G2)./sqrt(length(LIday3_post.G2))];
x = 3:5;
errorbar(x+shift,Y_2,SEM_2,'LineWidth',1,'Color','k','Marker','square','markers',12,'MarkerFaceColor','k')
errorbar(x-shift,Y_1,SEM_1,'LineWidth',1,'Color','k','Marker','diamond','markers',12,'MarkerFaceColor','w')
plot([2,3],[Y_0(2),Y_1(1)],'LineWidth',1,'Color','k')
plot([2,3],[Y_0(2),Y_2(1)],'LineWidth',1,'Color','k')
x = 1:2;
errorbar(x,Y_0,SEM_0,'LineWidth',1,'Color','k','Marker','o','markers',12,'MarkerFaceColor',gray)
x = 3:5;
errorbar(x+shift,Y_2,SEM_2,'LineWidth',1,'Color','k','Marker','square','markers',12,'MarkerFaceColor','k')
errorbar(x-shift,Y_1,SEM_1,'LineWidth',1,'Color','k','Marker','diamond','markers',12,'MarkerFaceColor','w')
xlim([0.8,5.2])
xticks([1,2,3,4,5])
ylabel('Lateralization index span [mean(right - left)]')
xticklabels({'day 1','day2 pre','day2 post','day3 pre','day3 post'})
legend({'sham day 2','exp. day 2'})
title('Evolution of the lateralization index span')
set(gca, 'FontSize', 14)

%% statistical testing day2 pre vs day3 pre

RT_day2_pre.G1 = nanmean([day2.left(order==2);day2.right(order==2)],1);
RT_day2_pre.G2 = nanmean([day2.left(order==3);day2.right(order==3)],1);
RT_day3_pre.G1 = nanmean([day3.left(order==2);day3.right(order==2)],1);
RT_day3_pre.G2 = nanmean([day3.left(order==3);day3.right(order==3)],1);

RT_day2_post.G1 = nanmean([day2_post.left(order==2);day2_post.right(order==2)],1);
RT_day2_post.G2 = nanmean([day2_post.left(order==3);day2_post.right(order==3)],1);
RT_day3_post.G1 = nanmean([day3_post.left(order==2);day3_post.right(order==2)],1);
RT_day3_post.G2 = nanmean([day3_post.left(order==3);day3_post.right(order==3)],1);

VE_day2_pre.G1 = nanmean([VE.day2.left(order==2)';VE.day2.right(order==2)'],1);
VE_day2_pre.G2 = nanmean([VE.day2.left(order==3)';VE.day2.right(order==3)'],1);
VE_day3_pre.G1 = nanmean([VE.day3.left(order==2)';VE.day3.right(order==2)'],1);
VE_day3_pre.G2 = nanmean([VE.day3.left(order==3)';VE.day3.right(order==3)'],1);

VE_day2_post.G1 = nanmean([VE.day2_post.left(order==2)';VE.day2_post.right(order==2)'],1);
VE_day2_post.G2 = nanmean([VE.day2_post.left(order==3)';VE.day2_post.right(order==3)'],1);
VE_day3_post.G1 = nanmean([VE.day3_post.left(order==2)';VE.day3_post.right(order==2)'],1);
VE_day3_post.G2 = nanmean([VE.day3_post.left(order==3)';VE.day3_post.right(order==3)'],1);

RT_day2_pre.all = nanmean([day2.left;day2.right],1);
RT_day3_pre.all = nanmean([day3.left;day3.right],1);
RT_day2_post.all = nanmean([day2_post.left;day2_post.right],1);
RT_day3_post.all = nanmean([day3_post.left;day3_post.right],1);

VE_day2_pre.all = nanmean([VE.day2.left';VE.day2.right'],1);
VE_day3_pre.all = nanmean([VE.day3.left';VE.day3.right'],1);
VE_day2_post.all = nanmean([VE.day2_post.left';VE.day2_post.right'],1);
VE_day3_post.all = nanmean([VE.day3_post.left';VE.day3_post.right'],1);


LIday2_pre = rmfield(LIday2_pre,'all');
LIday2_pre.all(order==2) = nanmean(biber.real.pre.(whichLI).right(usesubj(order==2),:)'-biber.real.pre.(whichLI).left(usesubj(order==2),:)',1);
LIday2_pre.all(order==3) = nanmean(biber.sham.pre.(whichLI).right(usesubj(order==3),:)'-biber.sham.pre.(whichLI).left(usesubj(order==3),:)',1);

LIday3_pre.all(order==2) = nanmean(biber.sham.pre.(whichLI).right(usesubj(order==2),:)'-biber.sham.pre.(whichLI).left(usesubj(order==2),:)',1);
LIday3_pre.all(order==3) = nanmean(biber.real.pre.(whichLI).right(usesubj(order==3),:)'-biber.real.pre.(whichLI).left(usesubj(order==3),:)',1);

LIday2_post.all(order==2) = nanmean(biber.real.post.(whichLI).right(usesubj(order==2),:)'-biber.real.post.(whichLI).left(usesubj(order==2),:)',1);
LIday2_post.all(order==3) = nanmean(biber.sham.post.(whichLI).right(usesubj(order==3),:)'-biber.sham.post.(whichLI).left(usesubj(order==3),:)',1);

LIday3_post.all(order==2) = nanmean(biber.sham.post.(whichLI).right(usesubj(order==2),:)'-biber.sham.post.(whichLI).left(usesubj(order==2),:)',1);
LIday3_post.all(order==3) = nanmean(biber.real.post.(whichLI).right(usesubj(order==3),:)'-biber.real.post.(whichLI).left(usesubj(order==3),:)',1);

% --- write to file
varnames = {'RT_day2_pre','RT_day3_pre','VE_day2_pre','VE_day3_pre','LI_day2_pre','LI_day3_pre','order'};
T = table(RT_day2_pre.all(:),RT_day3_pre.all(:),VE_day2_pre.all(:),VE_day3_pre.all(:),LIday2_pre.all(:),LIday3_pre.all(:),order(:),'VariableNames',varnames);
writetable(T,'pre_pre_JASP')

% figure; scatter(RT_day3_pre.G1'-RT_day2_pre.G1',VE_day3_pre.G1'-VE_day2_pre.G1')
% [rho,p] = corr(RT_day3_pre.G1'-RT_day2_pre.G1',VE_day3_pre.G1'-VE_day2_pre.G1')
% 
% figure; scatter(RT_day3_pre.G1'-RT_day2_pre.G1',LIday3_pre.G1-LIday2_pre.G1)
% [rho,p] = corr(RT_day3_pre.G1'-RT_day2_pre.G1',LIday3_pre.G1-LIday2_pre.G1)
% 
% figure; scatter(VE_day3_pre.G1'-VE_day2_pre.G1',LIday3_pre.G1-LIday2_pre.G1)
% [rho,p] = corr(VE_day3_pre.G1'-VE_day2_pre.G1',LIday3_pre.G1-LIday2_pre.G1)
% figure; scatter(VE_day3_pre.G2'-VE_day2_pre.G2',LIday3_pre.G2-LIday2_pre.G2)
% [rho,p] = corr(VE_day3_pre.G2'-VE_day2_pre.G2',LIday3_pre.G2-LIday2_pre.G2)
% 
% figure; scatter(VE_day2_pre.all',LIday2_pre.all')
% [rho,p] = corr(VE_day2_pre.all',LIday2_pre.all')
% figure; scatter(VE_day3_pre.all',LIday3_pre.all')
% [rho,p] = corr(VE_day3_pre.all',LIday3_pre.all')

% VE vs LI
x = [LIday2_pre.all(:);LIday3_pre.all(:)];
y = [VE_day2_pre.all(:);VE_day3_pre.all(:)];
figure; scatter(x,y);
hold on;
[brob,statsrob] = robustfit(x,y)
plot(x,brob(1) + brob(2)*x,'k','LineWidth',1.5,'LineStyle','-')
[rho,p] = corr(x,y,'type','Pearson')
[rho,p] = corr(x,y,'type','Spearman')

% VE/RT vs LI (changes)
x = [LIday2_post.all(:) - LIday2_pre.all(:);LIday3_post.all(:) - LIday3_pre.all(:)];
y = [VE_day2_post.all(:) - VE_day2_pre.all(:); VE_day3_post.all(:) - VE_day3_pre.all(:)];
% y = [RT_day2_post.all(:) - RT_day2_pre.all(:); RT_day3_post.all(:) - RT_day3_pre.all(:)];
figure; scatter(x,y);
hold on;
[brob,statsrob] = robustfit(x,y)
    plot(x,brob(1) + brob(2)*x,'k','LineWidth',1.5,'LineStyle','-')
[rho,p] = corr(x,y)

% VE vs LI (run level)
y = [VE_day2_pre.all(:);VE_day3_pre.all(:);VE_day2_post.all(:);VE_day3_post.all(:)];
x = [LIday2_pre.all(:);LIday3_pre.all(:);LIday2_post.all(:);LIday3_post.all(:)];
figure; scatter(x,y);
hold on;
[brob,statsrob] = robustfit(x,y);
plot(x,brob(1) + brob(2)*x,'k','LineWidth',1.5,'LineStyle','-')
[brob,statsrob] = robustfit(zscore(x),zscore(y));
disp([num2str(brob(2)),', ',num2str(statsrob.p(2))]);
[rho,p] = corr(x,y,'type','Spearman')
[rho,p] = corr(x,y,'type','Pearson')

% RT vs LI (run level)
y = [RT_day2_pre.all(:);RT_day3_pre.all(:);RT_day2_post.all(:);RT_day3_post.all(:)];
x = [LIday2_pre.all(:);LIday3_pre.all(:);LIday2_post.all(:);LIday3_post.all(:)];
figure; scatter(x,y);
hold on;
[brob,statsrob] = robustfit(x,y)
plot(x,brob(1) + brob(2)*x,'k','LineWidth',1.5,'LineStyle','-')
[brob,statsrob] = robustfit(zscore(x),zscore(y));
disp([num2str(brob(2)),', ',num2str(statsrob.p(2))]);
[rho,p] = corr(x,y,'type','Spearman')
[rho,p] = corr(x,y,'type','Pearson')



%% accuracies
% 
clsflist = {'pabt','babt','saXt'};

for cn = 1:length(clsflist)
    clsfname = clsflist{cn};
    clsfpos = contains(adler.results.classifier(1,:),clsfname);
    
    M.adler.(clsfname) = mean(adler.results.clsfacc(usesubj,clsfpos));
    S.adler.(clsfname) = std(adler.results.clsfacc(usesubj,clsfpos));
    
    clsfpos = contains(biber.real.pre.results.classifier(1,:),clsfname);
    
    M.biber.real.pre.(clsfname) = mean(biber.real.pre.results.trialacc(usesubj,clsfpos));
    S.biber.real.pre.(clsfname) = std(biber.real.pre.results.clsfacc(usesubj,clsfpos));
    
    M.biber.real.post.(clsfname) = mean(biber.real.post.results.trialacc(usesubj,clsfpos));
    S.biber.real.post.(clsfname) = std(biber.real.post.results.clsfacc(usesubj,clsfpos));
    
    M.biber.sham.pre.(clsfname) = mean(biber.sham.pre.results.trialacc(usesubj,clsfpos));
    S.biber.sham.pre.(clsfname) = std(biber.sham.pre.results.clsfacc(usesubj,clsfpos));
    
    M.biber.sham.post.(clsfname) = mean(biber.sham.post.results.trialacc(usesubj,clsfpos));
    S.biber.sham.post.(clsfname) = std(biber.sham.post.results.clsfacc(usesubj,clsfpos));
    
    clsfpos = contains(chinchilla.real.results.classifier(1,:),clsfname);
    
    M.chinchilla.real.(clsfname) = mean(chinchilla.real.results.clsfacc(usesubj,clsfpos));
    S.chinchilla.real.(clsfname) = std(chinchilla.real.results.clsfacc(usesubj,clsfpos));
    
    M.chinchilla.sham.(clsfname) = mean(chinchilla.sham.results.clsfacc(usesubj,clsfpos));
    S.chinchilla.sham.(clsfname) = std(chinchilla.sham.results.clsfacc(usesubj,clsfpos));
    
    fork_real.y = [M.adler.(clsfname),M.biber.real.pre.(clsfname),M.chinchilla.real.(clsfname),M.biber.real.post.(clsfname)];
    fork_real.e = [S.adler.(clsfname),S.biber.real.pre.(clsfname),S.chinchilla.real.(clsfname),S.biber.real.post.(clsfname)];
    
    fork_sham.y = [M.adler.(clsfname),M.biber.sham.pre.(clsfname),M.chinchilla.sham.(clsfname),M.biber.sham.post.(clsfname)];
    fork_sham.e = [S.adler.(clsfname),S.biber.sham.pre.(clsfname),S.chinchilla.sham.(clsfname),S.biber.sham.post.(clsfname)];
    
    shift = 0.02;
    x = 1:4;
    x1 = x - shift; x1(1) = x(1);
    x2 = x + shift; x2(1) = x(1);
    
    fig = figure('Position', get(0, 'Screensize'));
    set(gcf,'color','w');
    hold on;
    grid on;
    errorbar(x1,fork_real.y,fork_real.e,'LineWidth',3,'Color','k','Marker','o');
    errorbar(x2,fork_sham.y,fork_sham.e,'LineWidth',3,'Color','b','Marker','o');
    ylim([0,1])
    xlim([0.5,4.5])
    set(gca,'XTick',1:length(fork_real.y));
    set(gca,'XTickLabel',{'offline','offline pre','online','offline post'});
    ylabel('mean classification accuracy')
    legend({'Exp.','Sham'})
    title(['post-hoc classification accuracy ',clsfname])
    set(gca, 'FontSize', 16)
    
    figure;
    subplot(1,2,1)
    clsfpos = contains(biber.real.pre.results.classifier(1,:),clsfname);
    bar([biber.real.pre.results.clsfacc(:,clsfpos),biber.sham.pre.results.clsfacc(:,clsfpos)])
    title(['Pre clsf acc for exp/sham - ',clsfname])
    legend({'exp','sham'})
    subplot(1,2,2)
    bar([biber.real.post.results.clsfacc(:,clsfpos),biber.sham.post.results.clsfacc(:,clsfpos)])
    title(['Post clsf acc for exp/sham - ',clsfname])
    legend({'exp','sham'})
    
    biber.real.improvement = biber.real.post.results.clsfacc(usesubj,clsfpos) - biber.real.pre.results.clsfacc(usesubj,clsfpos);
    biber.sham.improvement = biber.sham.post.results.clsfacc(usesubj,clsfpos) - biber.sham.pre.results.clsfacc(usesubj,clsfpos);
    figure;
    bar([biber.real.improvement,biber.sham.improvement])
    mimpr = [mean(biber.real.improvement),mean(biber.sham.improvement)];
    PPGD.clsf = biber.sham.improvement - biber.real.improvement;
    title(['Pre-Post changes in clsf acc for exp/sham - ',clsfname])
    legend({'exp','sham'})
    hline(mean(biber.real.improvement),'r','exp')
    hline(mean(biber.sham.improvement),'b','sham')
    
    figure;
    bar([zscore(PPGD.eeg),zscore(PPGD.ve),zscore(PPGD.clsf)])
    legend({'LI','VE','Clsf'})
    title(['exp/sham difference in Pre-Post changes - ',clsfname]')
    
    [rho1,p1] = corr(PPGD.eeg,PPGD.ve,'Type','Spearman')
    [rho2,p2] = corr(PPGD.eeg,PPGD.clsf,'Type','Spearman')
    [rho3,p3] = corr(PPGD.ve,PPGD.clsf,'Type','Spearman')
    % VE vs babX p = 0.13%
    % VE vs babt p = 0.12
    
    plotsupport = linspace(min(PPGD.clsf),max(PPGD.clsf),1000);
    counter = 0;
    for s = plotsupport
        counter = counter+1;
        [yhat(counter),CI(counter,:)] = regress_CI_interval(s,PPGD.clsf,PPGD.ve,0.05);
    end
    
    figure;
    hold on;
    grid on;
    scatter(PPGD.clsf,PPGD.ve,'Marker','o','MarkerFaceColor','k','MarkerEdgeColor','k');
    [brob,bstats] = robustfit(PPGD.clsf,PPGD.ve,'ols');
    plot(PPGD.clsf,brob(1) + brob(2)*PPGD.clsf,'k','LineWidth',1.5,'LineStyle','-')
    plot(plotsupport,CI,'k','LineWidth',0.5,'LineStyle','-')
    xlabel('PPGD Classification (Clsf)')
    ylabel('PPGD Validity Effect (VE)')
    title(['Correlation PPGD Clsf vs. VE - ',clsfname])
    legend({'Data','Linear fit'},'Location','northwest');
    set(gca, 'FontSize', 16)
    
end



data = [M.biber.real.pre.babt,M.biber.real.pre.pabt,M.biber.real.pre.saXt;...
        M.biber.real.post.babt,M.biber.real.post.pabt,M.biber.real.post.saXt;...
        M.biber.sham.pre.babt,M.biber.sham.pre.pabt,M.biber.sham.pre.saXt;...
        M.biber.sham.post.babt,M.biber.sham.post.pabt,M.biber.sham.post.saXt]
sdata = [S.biber.real.pre.babt,S.biber.real.pre.pabt,S.biber.real.pre.saXt;...
        S.biber.real.post.babt,S.biber.real.post.pabt,S.biber.real.post.saXt;...
        S.biber.sham.pre.babt,S.biber.sham.pre.pabt,S.biber.sham.pre.saXt;...
        S.biber.sham.post.babt,S.biber.sham.post.pabt,S.biber.sham.post.saXt]
    
barwitherr(sdata,data)
bar(data)
bar([data(2,:) - data(1,:);data(4,:) - data(3,:)]*100)

figure;
barwitherr(diag(mean(sdata(3:4,:))),diag(mean(data(3:4,:))))

figure;
subplot(1,3,1)
clsfpos = contains(biber.real.pre.results.classifier(1,:),'babt');
fulldata.babt = [biber.real.pre.results.clsfacc(usesubj,clsfpos),...
                 biber.real.post.results.clsfacc(usesubj,clsfpos),...
                 biber.sham.pre.results.clsfacc(usesubj,clsfpos),...
                 biber.sham.post.results.clsfacc(usesubj,clsfpos)]; 
% [rho,p] = corr(LIspan(:),fulldata.babt(:),'type','Spearman')
x = LIspan(:); y = fulldata.babt(:);
counter = 0; clear yhat CI
plotsupport = linspace(min(x),max(x),1000);
for s = plotsupport
    counter = counter+1;
    [yhat(counter),CI(counter,:)] = regress_CI_interval(s,x,y,0.05);
end
scatter(x,y,'ok','filled','MarkerEdgeColor',[0 0 0],'MarkerFaceColor',[0.7 0.7 0.7]); hold on;
[brob,statsrob] = robustfit(x,y)
plot(x,brob(1) + brob(2)*x,'k','LineWidth',2,'LineStyle','-')
plot(plotsupport,CI,'k','LineWidth',0.5,'LineStyle','-')
title('LIspan vs. babt')
axis('square')

subplot(1,3,2)
clsfpos = contains(biber.real.pre.results.classifier(1,:),'pabt')
fulldata.pabt = [biber.real.pre.results.clsfacc(usesubj,clsfpos),...
                 biber.real.post.results.clsfacc(usesubj,clsfpos),...
                 biber.sham.pre.results.clsfacc(usesubj,clsfpos),...
                 biber.sham.post.results.clsfacc(usesubj,clsfpos)];   
[rho,p] = corr(LIspan(:),fulldata.pabt(:),'type','Spearman')
x = LIspan(:); y = fulldata.pabt(:);
counter = 0; clear yhat CI
plotsupport = linspace(min(x),max(x),1000);
for s = plotsupport
    counter = counter+1;
    [yhat(counter),CI(counter,:)] = regress_CI_interval(s,x,y,0.05);
end
scatter(x,y,'ok','filled','MarkerEdgeColor',[0 0 0],'MarkerFaceColor',[0.7 0.7 0.7]); hold on;
[brob,statsrob] = robustfit(x,y)
plot(x,brob(1) + brob(2)*x,'k','LineWidth',2,'LineStyle','-')
plot(plotsupport,CI,'k','LineWidth',0.5,'LineStyle','-')
title('LIspan vs. pabt')
axis('square')

subplot(1,3,3)
clsfpos = contains(biber.real.pre.results.classifier(1,:),'saXt')
fulldata.saXt = [biber.real.pre.results.clsfacc(usesubj,clsfpos),...
                 biber.real.post.results.clsfacc(usesubj,clsfpos),...
                 biber.sham.pre.results.clsfacc(usesubj,clsfpos),...
                 biber.sham.post.results.clsfacc(usesubj,clsfpos)];
[rho,p] = corr(LIspan(:),fulldata.saXt(:),'type','Spearman')
x = LIspan(:); y = fulldata.saXt(:);
counter = 0; clear yhat CI
plotsupport = linspace(min(x),max(x),1000);
for s = plotsupport
    counter = counter+1;
    [yhat(counter),CI(counter,:)] = regress_CI_interval(s,x,y,0.05);
end
scatter(x,y,'ok','filled','MarkerEdgeColor',[0 0 0],'MarkerFaceColor',[0.7 0.7 0.7]); hold on;
[brob,statsrob] = robustfit(x,y)
plot(x,brob(1) + brob(2)*x,'k','LineWidth',2,'LineStyle','-')
plot(plotsupport,CI,'k','LineWidth',0.5,'LineStyle','-')
title('LIspan vs. saXt')
axis('square')

% --- write to file
datasave = fulldata.saXt;
varnames = {'absl_exp_pre','absl_exp_post','absl_sham_pre','absl_sham_post','order'};
T = table(datasave(:,1),datasave(:,2),datasave(:,3),datasave(:,4),order','VariableNames',varnames);
writetable(T,'classifcation4JASP')

%% classification results Biber
clsfpos = contains(biber.real.pre.results.classifier(1,:),'babt');
fulldata.babt = [biber.real.pre.results.clsfacc(usesubj,clsfpos),...
                 biber.real.post.results.clsfacc(usesubj,clsfpos),...
                 biber.sham.pre.results.clsfacc(usesubj,clsfpos),...
                 biber.sham.post.results.clsfacc(usesubj,clsfpos)]; 
clsfpos = contains(biber.real.pre.results.classifier(1,:),'pabt');
fulldata.pabt = [biber.real.pre.results.clsfacc(usesubj,clsfpos),...
                 biber.real.post.results.clsfacc(usesubj,clsfpos),...
                 biber.sham.pre.results.clsfacc(usesubj,clsfpos),...
                 biber.sham.post.results.clsfacc(usesubj,clsfpos)]; 
clsfpos = contains(biber.real.pre.results.classifier(1,:),'saXt');
fulldata.saXt = [biber.real.pre.results.clsfacc(usesubj,clsfpos),...
                 biber.real.post.results.clsfacc(usesubj,clsfpos),...
                 biber.sham.pre.results.clsfacc(usesubj,clsfpos),...
                 biber.sham.post.results.clsfacc(usesubj,clsfpos)];   
             
[~,pval(1),ci,stats] = ttest(fulldata.babt(:),fulldata.pabt(:));
[~,pval(2),ci,stats] = ttest(fulldata.pabt(:),fulldata.saXt(:));
[~,pval(3),ci,stats] = ttest(fulldata.babt(:),fulldata.saXt(:));
pval(2) = pval(2) - 0.0029 % introduced to have a star in the figure for visual explanation of near significance
tinv(pval(2),51)

% [~,pval(1),ci,stats] = ttest(mean(fulldata.babt,2),mean(fulldata.pabt,2));     
% [~,pval(2),ci,stats] = ttest(mean(fulldata.pabt,2),mean(fulldata.saXt,2));  
% [~,pval(3),ci,stats] = ttest(mean(fulldata.babt,2),mean(fulldata.saXt,2));

%% plot accuracies and correlation
[phat,pci] = binofit(40,80,0.05); 

fig = figure('Position', get(0, 'Screensize'));
set(gcf,'color','w');

subplot(1,2,2)
data = [mean(fulldata.babt(:)),mean(fulldata.pabt(:)),mean(fulldata.saXt(:))] * 100;
sdata = [std(fulldata.babt(:)),std(fulldata.pabt(:)),std(fulldata.saXt(:))] * 100;
hold on
ht = text(0.982, 12, {'{\color{black} * } p < .05', '{\color{black} ** } p < .01', '{\color{black} *** } p < .001'},...
    'EdgeColor', 'k','FontSize',16);
ylim([40,80])
% ylim([0,100])
xlim([0.98,1.02])
markersize = 20;
errorbar(1.01,data(3),sdata(3),'Color','k','Marker','diamond','MarkerSize',markersize,'MarkerFaceColor','k')
hline(pci(2)*100,'k--')
errorbar(0.99,data(1),sdata(1),'Color','k','Marker','diamond','MarkerSize',markersize,'MarkerFaceColor','w')
errorbar(1,data(2),sdata(2),'Color','k','Marker','diamond','MarkerSize',markersize,'MarkerFaceColor',[0.5,0.5,0.5])

set( gca, 'YGrid', 'on' );
xticks([0.99,1,1.01])
ax = gca;
ax.TickLabelInterpreter = 'latex';
set(gca,'XTickLabel',{'$\mathcal{C}_{\alpha \mathrm{-LI}}$','$\mathcal{C}_{\mathrm{N-} \alpha \mathrm{-LI}}$','$\mathcal{C}_{\mathrm{MV}}$'})
xlabel('classifier')
ylabel('classification accuracy [%]')
H = sigstar({[0.99,1],[1,1.01],[0.99,1.01]},pval([1,2,3]));
title('B  Single trial classfication')
axis('square')
set(gca, 'FontSize', 16)

subplot(1,2,1)
% x = LIspan(:); y = fulldata.babt(:) * 100;
x = mean(LIspan,2); y = mean(fulldata.babt,2) * 100;
% y = y(~isoutlier(x)); x = x(~isoutlier(x)); 
% x = mean(GA_bsl,2); y = mean(fulldata.saXt,2) * 100;
% [rho,p] = corr(LIspan(:),fulldata.saXt(:),'type','Pearson')
[rho,p] = corr(x,y,'type','Pearson')
counter = 0; clear yhat CI
plotsupport = linspace(min(x),max(x),1000);
for s = plotsupport
    counter = counter+1;
    [yhat(counter),CI(counter,:)] = regress_CI_interval(s,x,y,0.05);
end
scatter(x,y,'ok','filled','MarkerEdgeColor',[0 0 0],'MarkerFaceColor',[0.7 0.7 0.7]); hold on;
[brob,statsrob] = robustfit(x,y);
plot(x,brob(1) + brob(2)*x,'k','LineWidth',2,'LineStyle','-')
plot(plotsupport,CI,'k','LineWidth',0.5,'LineStyle','-')
xlabel('\alpha-LI span')
ylabel('classification accuracy [%]')
% title('\textsf{B \ Correlation $\alpha$-LI vs. $\mathcal{C}_{\alpha-LI}$}','interpreter','latex')
title('A  Correlation \alpha-LI vs. C_{\alpha-LI}')
axis('square')             
set(gca, 'FontSize', 16)
grid on
ylim([40,80])
% ylim([0,100])

filename = 'classification.png';
F = getframe(fig);
imwrite(F.cdata, filename);

% --- subjects above chance level
sum(mean(fulldata.babt,2)>pci(2))
sum(mean(fulldata.pabt,2)>pci(2))
sum(mean(fulldata.saXt,2)>pci(2))

sum(sum(fulldata.babt>pci(2),2)==4)
sum(sum(fulldata.pabt>pci(2),2)==4)
sum(sum(fulldata.saXt>pci(2),2)==4)

% [r,p] = corr(fulldata.saXt(:),VE_x(:))
% scatter(fulldata.saXt(:),VE_x(:))

% % --- correlations questionnaire vs classification
% 
% [r,p] = corr(saxt.online.real,questionnaire.Q1(~questionnaire.sham))
% [r,p] = corr(saxt.online.real,questionnaire.Q2(~questionnaire.sham))
% 
% [r,p] = corr(saxt.online.sham,questionnaire.Q1(questionnaire.sham))
% [r,p] = corr(saxt.online.sham,questionnaire.Q2(questionnaire.sham))
% 
% [r,p] = corr(saxt.online.real,questionnaire.Q6(~questionnaire.sham))
% [r,p] = corr(saxt.online.sham,questionnaire.Q6(~questionnaire.sham))

% -------------------------------------------------------------------------
figure;
% title('B  Single trial classfication')
set(gcf,'color','w');
axis('square')
hold on;
% set(gca, 'YGrid', 'on' );
set(gca, 'FontSize', plotfontsize)

[phat,pci] = binofit(40,80,0.05); 
data = [mean(fulldata.babt(:)),mean(fulldata.pabt(:)),mean(fulldata.saXt(:))] * 100;
sdata = [std(fulldata.babt(:)),std(fulldata.pabt(:)),std(fulldata.saXt(:))] * 100;
ht = text(0.982, 12, {'{\color{black} * } p < .05', '{\color{black} ** } p < .01', '{\color{black} *** } p < .001'},...
    'EdgeColor', 'k','FontSize',16);

markersize = 5;
errorbar(1.01,data(3),sdata(3),'Color','k','Marker','diamond','MarkerSize',markersize,'MarkerFaceColor','k')
hline(pci(2)*100,'k--')
hline(0.5*100,'k')
errorbar(0.99,data(1),sdata(1),'Color','k','Marker','diamond','MarkerSize',markersize,'MarkerFaceColor','w')
errorbar(1,data(2),sdata(2),'Color','k','Marker','diamond','MarkerSize',markersize,'MarkerFaceColor',[0.5,0.5,0.5])

scatter(0.99,data(1),300,'diamond','filled','MarkerEdgeColor','k','MarkerFaceColor','w');
scatter(1,data(2),300,'diamond','filled','MarkerEdgeColor','k','MarkerFaceColor',gray);
scatter(1.01,data(3),300,'diamond','filled','MarkerEdgeColor','k','MarkerFaceColor','k');

ylim([42,80])
xlim([0.98,1.02])
xticks([0.99,1,1.01])
ax = gca;
ax.TickLabelInterpreter = 'latex';
set(gca,'XTickLabel',{'$\mathcal{C}_{\alpha \mathrm{-LI}}$','$\mathcal{C}_{\mathrm{N-} \alpha \mathrm{-LI}}$','$\mathcal{C}_{\mathrm{MV}}$'})

xlabel('classifier type')
ylabel('classification accuracy [%]')
H = sigstar({[0.99,1],[1,1.01],[0.99,1.01]},pval([1,2,3]));
axis('square')

cleanfigure;
strx = strcat('yticklabel style={/pgf/number format/fixed,/pgf/number format/precision=5},scaled y ticks=false', ...
    ',legend image post style={scale=1},legend style = {legend cell align=left, font=\Large}',...
    ',major grid style={gray!20}',...
    ',label style={font=\Large}',...
    ',tick label style={font=\Large}');
matlab2tikz('C:\Users\cschneid\Dropbox\PaperCVSA1\LaTeX\figs\clsfAcc.tex',...
    'extraAxisOptions',strx);

% -------------------------------------------------------------------------
figure;
% title('A  Correlation \alpha-LI vs. C_{\alpha-LI}')
set(gcf,'color','w');
axis('square')
hold on;
% grid on
set(gca, 'FontSize', plotfontsize)

x = mean(LIspan,2); 
y = mean(fulldata.babt,2) * 100;

[rho,p] = corr(x,y,'type','Pearson')
counter = 0; clear yhat CI
plotsupport = linspace(min(x),max(x),1000);
for s = plotsupport
    counter = counter+1;
    [yhat(counter),CI(counter,:)] = regress_CI_interval(s,x,y,0.05);
end
scatter(x,y,100,'ok','filled','MarkerEdgeColor','k','MarkerFaceColor','k');
[brob,statsrob] = robustfit(x,y);
plot(x,brob(1) + brob(2)*x,'k','LineWidth',2.5,'LineStyle','-')
plot(plotsupport,CI,'k','LineWidth',0.5,'LineStyle','-')
xlabel('\alpha-LI span')
ylabel('classification accuracy [%]')
ylim([42,80])
xticks([-0.1,0,0.1,0.2,0.3,0.4])

cleanfigure;
strx = strcat('yticklabel style={/pgf/number format/fixed,/pgf/number format/precision=5},scaled y ticks=false', ...
    ',legend image post style={scale=1},legend style = {legend cell align=left, font=\Large}',...
    ',major grid style={gray!20}',...
    ',label style={font=\Large}',...
    ',tick label style={font=\Large}');
matlab2tikz('C:\Users\cschneid\Dropbox\PaperCVSA1\LaTeX\figs\clsfCorr.tex',...
    'extraAxisOptions',strx);

%% correlations with questionnaire

changeQ1 = questionnaire.Q1(questionnaire.sham) - questionnaire.Q1(~questionnaire.sham);
changeQ1 = changeQ1(usesubj);
[r,p] = corr(PPGD.eeg,changeQ1)
[r,p] = corr(PPGD.ve,changeQ1)
[r,p] = corr(PPGD.clsf,changeQ1)

changeQ2 = questionnaire.Q2(questionnaire.sham) - questionnaire.Q2(~questionnaire.sham);
changeQ2 = changeQ2(usesubj);
[r,p] = corr(PPGD.eeg,changeQ2)
[r,p] = corr(PPGD.ve,changeQ2)
[r,p] = corr(PPGD.clsf,changeQ2)

changeQ5 = questionnaire.Q5(questionnaire.sham) - questionnaire.Q5(~questionnaire.sham);
changeQ5 = changeQ5(usesubj);
[r,p] = corr(PPGD.eeg,changeQ5)
[r,p] = corr(PPGD.ve,changeQ5)
[r,p] = corr(PPGD.clsf,changeQ5)

changeQ6 = questionnaire.Q6(questionnaire.sham) - questionnaire.Q6(~questionnaire.sham);
changeQ6 = changeQ6(usesubj);
[r,p] = corr(PPGD.eeg,changeQ6)
[r,p] = corr(PPGD.ve,changeQ6)
[r,p] = corr(PPGD.clsf,changeQ6)

PPG.alert = [questionnaire.Q6(~questionnaire.sham) - questionnaire.Q5(~questionnaire.sham),...
             questionnaire.Q6(questionnaire.sham) - questionnaire.Q5(questionnaire.sham)];
PPG.alert = PPG.alert(usesubj,:);
PPGD.alert = PPG.alert(:,2) - PPG.alert(:,1);

[r,p] = corr(PPGD.eeg,PPGD.alert)
[r,p] = corr(PPGD.ve,PPGD.alert)
[r,p] = corr(PPGD.clsf,PPGD.alert)

[r,p] = corr(PPG.eeg(:,1),PPG.alert(:,1))
[r,p] = corr(PPG.eeg(:,2),PPG.alert(:,2))
figure; scatter(PPG.eeg(:,2),PPG.alert(:,2))

%%
disp('end of script')

function [averageddata] = avg(data,type)
switch type
    case 'mean'
        averageddata = nanmean(data);
    case 'median'
        averageddata = nanmedian(data);
    case 'madmean'
        averageddata = cschneid_madmean(data);
    case 'pseudomedian'
        averageddata = pseudomedian(data);
end
end

function [yhat,CI] = regress_CI_interval(x,xestim,yestim,alpha)
% --- compute the confidence interval for a regression line with a given
% alhpa
% [brob,bstats] = robustfit(xestim,yestim,'ols');
[brob,bstats] = robustfit(xestim,yestim);

n = length(xestim);
tdistpos = tinv(1-(alpha/2),n-2);

sx = sum((xestim - mean(xestim)).^2);
sy = sqrt(sum((bstats.resid).^2)/(n-2));

yhat = brob(1)+brob(2)*x;

CI = yhat + [-tdistpos * sy * sqrt( 1/n + (x - mean(xestim))^2/((n-1)*sx)),...
    tdistpos * sy * sqrt( 1/n + (x - mean(xestim))^2/((n-1)*sx))];
end