plotfontsize = 16;
gray = [0.5, 0.5, 0.5];


acc_EEG = [215.5, 214.5, 241.5, 235, 260.5, 192, 299, 167, 220, 259, 215.5, 293.5, 265.5]/379.5;
acc_ET = [205, 209.5, 229.5, 217.5, 179.5, 217, 204.5, 224, 210, 309.5, 245, 226, 246.5]/379.5;
acc_hyb = [212, 211.5, 239, 241, 253, 202.5, 293, 194, 224, 324, 250, 277.5, 273]/379.5;

figure;
% title('B  Single trial classfication')
set(gcf,'color','w');
axis('square')
hold on;
% set(gca, 'YGrid', 'on' );
set(gca, 'FontSize', plotfontsize)

[phat,pci] = binofit(40,80,0.05); 
data = [mean(acc_EEG),mean(acc_ET),mean(acc_hyb)] * 100;
sdata = [std(acc_EEG),std(acc_ET),std(acc_hyb)] * 100;
ht = text(0.982, 12, {'{\color{black} * } p < .05', '{\color{black} ** } p < .01', '{\color{black} *** } p < .001'},...
    'EdgeColor', 'k','FontSize',16);

markersize = 5;
errorbar(1.01,data(3),sdata(3),'Color','k','Marker','diamond','MarkerSize',markersize,'MarkerFaceColor','k')
hline(pci(2)*100,'k--')
hline(0.5*100,'k')
errorbar(0.99,data(1),sdata(1),'Color','k','Marker','diamond','MarkerSize',markersize,'MarkerFaceColor','w')
errorbar(1,data(2),sdata(2),'Color','k','Marker','diamond','MarkerSize',markersize,'MarkerFaceColor',[0.5,0.5,0.5])

scatter(0.99,data(1),300,'diamond','filled','MarkerEdgeColor','k','MarkerFaceColor','w');
scatter(1,data(2),300,'diamond','filled','MarkerEdgeColor','k','MarkerFaceColor',gray);
scatter(1.01,data(3),300,'diamond','filled','MarkerEdgeColor','k','MarkerFaceColor','k');

ylim([42,80])
xlim([0.98,1.02])
xticks([0.99,1,1.01])
ax = gca;
% ax.TickLabelInterpreter = 'latex';
% set(gca,'XTickLabel',{'$\mathcal{C}_{\alpha \mathrm{-LI}}$','$\mathcal{C}_{\mathrm{N-} \alpha \mathrm{-LI}}$','$\mathcal{C}_{\mathrm{MV}}$'})
set(gca,'XTickLabel',{'EEG', 'ET', 'Hybrid'});
% xlabel('classifier type')
ylabel('classification accuracy [%]')
pval = [0.3787, 0.042, 0.1658];
H = sigstar({[1,1.01]},0.042);
axis('square')

% ----------------------------------

figure;
data = [acc_EEG; acc_ET; acc_hyb];
bar(data')


figure;
subplot(1,2,1);
plot(data([1,3],:)./data(1,:),'k')
ylim([0.8, 1.5])
xticks([1,2])
xticklabels({'EEG','Hybrid'})
ylabel('accuracy changes')
set(gca, 'FontSize', plotfontsize)
subplot(1,2,2);
plot(data([2,3],:)./data(2,:),'k')
ylim([0.8, 1.5])
xticks([1,2])
xticklabels({'ET','Hybrid'})
set(gca, 'FontSize', plotfontsize)

figure;
subplot(1,2,1);
plot(data([1,3],:)-data(1,:),'k')
ylim([-0.2, 0.2])
xticks([1,2])
xticklabels({'EEG','Hybrid'})
ylabel('accuracy changes')
set(gca, 'FontSize', plotfontsize)
subplot(1,2,2);
plot(data([2,3],:)-data(2,:),'k')
ylim([-0.2, 0.2])
xticks([1,2])
xticklabels({'ET','Hybrid'})
set(gca, 'FontSize', plotfontsize)


figure;
subplot(1,2,1);
hold on;
plot(data([1,3],:),'Color',gray)
plot(mean(data([1,3],:),2),'k','LineWidth',2)
ylim([0.4, 1])
xticks([1,2])
xticklabels({'EEG','Hybrid'})
ylabel('accuracy [%]')
set(gca, 'FontSize', plotfontsize)
subplot(1,2,2);
hold on;
plot(data([2,3],:),'Color',gray)
plot(mean(data([2,3],:),2),'k','LineWidth',2)
ylim([0.4, 1])
xticks([1,2])
xticklabels({'ET','Hybrid'})
set(gca, 'FontSize', plotfontsize)



