% author: Christoph Schneider, CNBI, 2017

% Offline analysis of CVSA data for experiment CVSA 2.1 Adler
% Start this file to start processing - all the steps are implemented in a
% modular way and can therefore be reordered and/or commented out

% Can be called as function with the argument fileargs
% fileargs is a struct with the fields
% fileargs.pathname: path to folder containing the files
% fileargs.filename: cell array of file names

% Outputs a struct with all the concatenated runs of each modality

function  [] = cschneid_Adler_Main(fileargs)

close all force;
clearvars -except fileargs;
disp('-------------------------------------------------------------------')

% Load defaults
% -------------------------------------------------------------------------
cschneid_Adler_Default()

% Initialize global structs
% -------------------------------------------------------------------------
allruns = struct;

% Get file parameters
% -------------------------------------------------------------------------
if ~exist('fileargs','var')
    cd(default.datapath);
    [filename,pathname,~] = uigetfile( ...
        { '*.mat','Clean data files (*.mat)';}, ...
        'MultiSelect','on');
    if iscell(filename)==0  % if only one file
        filename = {filename};
    end
else
    pathname = fileargs.pathname;
    filename = fileargs.filenames;
end

disp(filename{1})

%% Process files
% -------------------------------------------------------------------------
for f = 1:length(filename)
    
    clearvars -except filename pathname allruns f default
    filecontent = load(fullfile(pathname,filename{f}));
    
    %% Eye tracker preprocessing
    
    if isfield(filecontent,'et')
        filecontent.et.run = str2double(filename{f}(strfind(filename{f},'run')+3));
        args = struct('struct',filecontent.et,'default',default,'plot','no');
        run.et = cschneid_ET_preprocessing(args);
        %         run.et = cschneid_Adler_ET_preprocessing_pilot(args);
    end
    
    %% EEG preprocessing
    
    if isfield(filecontent,'eeg')
        filecontent.eeg.run = str2double(filename{f}(strfind(filename{f},'run')+3));
        args = struct('struct',filecontent.eeg,'default',default,'filename',...
            fullfile(pathname,filename{f}),'plot','no');
        run.eeg = cschneid_EEG_preprocessing(args);
    end
    
    %% synchronize corrupted trials (blinks/saccades) between ET and EEG
    
    args = struct('run',run,'useclass',default.artifact.consider4rejection);
    [run] = cschneid_ETEEG_marryartifactstructures(args);
    
    %% Compute PSDs
    
    if isfield(filecontent,'eeg')
        [pn,fn1,fn2] = fileparts(fullfile(pathname,filename{f}));
        args = struct('struct',run.eeg,'default',default,'type','eeg','path',pn,...
            'filename',[fn1,fn2],'save','yes');
        [run.PSD] = cschneid_computePSD(args);
        
        % --- plot for identifying bad channels/trials
        args = struct('struct',run.PSD,'default',default,'plot','no');
        cschneid_PSD_rawplots(args);
    end
    
    
    %% Merge runs
    
    if isstruct(run)
        args = struct('allruns',allruns,'newrun',run);
        allruns = cschneid_mergeruns(args);
    end
    
end

clearvars -except default allruns;
newsavestruct = struct;
cprintf([0,0.5,0.2],'Preprocessing finished!\n');

%% ADD GENERAL FIELDS

if isfield(allruns,'et')
    newsavestruct.general.subject = allruns.et.subject;
    newsavestruct.general.date = allruns.et.date;
    newsavestruct.general.sessionID = allruns.et.sessionID;
elseif isfield(allruns,'eeg')
    newsavestruct.general.subject = allruns.eeg.subject;
    newsavestruct.general.date = allruns.eeg.date;
    newsavestruct.general.sessionID = allruns.eeg.sessionID;
end

%% HYBRID ANALYSIS NEEDS TO COME FIRST !!

% if isfield(allruns,'et') && isfield(allruns,'eeg')
%     args = struct('ETstruct',allruns.et,'EEGstruct',allruns.PSD,...
%         'default',default,'savestruct',newsavestruct,'plot','no');
%     newsavestruct = cschneid_Adler_ETEEG_analysis(args);
% end

%% PSD ANALYSIS

if isfield(allruns,'PSD')
    args = struct('struct',allruns.PSD,'default',default,'savestruct',newsavestruct,'plot','yes');
    newsavestruct = cschneid_PSD_analysis_CVSA2(args);
end

%% EYE TRACKER ANALYSIS

% if isfield(allruns,'et')
%     args = struct('struct',allruns.et,'default',default,'savestruct',newsavestruct,'plot','yes');
%     newsavestruct = cschneid_Adler_ET_analysis(args);
% end

%% Save in file

filename = fullfile(default.grouppath,[newsavestruct.general.sessionID,'.',default.GAsuffix,'.mat']);
if exist(filename,'file')
    load(filename,'savestruct')
    
    f = fieldnames(newsavestruct);
    for i = 1:length(f)
        savestruct.(f{i}) = newsavestruct.(f{i}); %#ok<STRNU>
    end
else
    savestruct = newsavestruct; %#ok<NASGU>
end

save(filename, 'savestruct');

end
