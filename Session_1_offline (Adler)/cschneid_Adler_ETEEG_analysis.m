function [savestruct] = cschneid_Adler_ETEEG_analysis(args)
%| function [savestruct] = cschneid_ETEEG_analysis(args)
%|
%| processes eye tracker data from the input structs 'et' and 'psd' in a
%| joint manner (hybrid BCI)
%|
%| INPUT
%| args.ETstruct: PREPROCESSED eye tracker structure (cschneid_ET_preprocessing)
%| args.EEGstruct: PREPROCESSED eeg-psd structure (cschneid_PSD_computation)
%| args.default: struct with default values provided in the default file
%| args.savestruct: structure that holds computed results - if defined this
%|                  function will add further fields (savestruct.et.___)
%| args.plot[optional]: 'yes' for obtaining plots
%|
%| OUTPUT
%| savestruct: structure that holds all computed results for saving

cprintf([0,0,1],'Hybrid eye tracker - EEG/PSD analysis ...\n');

% --- free the args structure
if ~isfield(args,'savestruct')
    savestruct = struct;
else
    savestruct = args.savestruct;
end
if ~isfield(args,'plot')
    plotflag = 'no';
else
    plotflag = args.plot;
end
default = args.default;
et = args.ETstruct;
PSD = args.EEGstruct;
clear args;

%% EQUAL BAD TRIAL ENTRIES
trigA = et.triggers(4,:);
trigB = PSD.triggers(4,:);
trigU = trigA | trigB;
et.triggers(4,:) = trigU;
PSD.triggers(4,:) = trigU;

%% EYE TRACKER ANALYSIS: Pupil Size

if length(et.trialsBad)/et.totaltrials < default.toocorrupted
    
    % --- save general information about the eye tracker data
    savestruct.et = et;
    
    % --- save percentage of trials corrupted with saccades
    trialsWithSaccades = setdiff(et.trialsWithSaccades,et.trialsWithBlinks);
    savestruct.et.ratio_TrialsWithSaccades = round(length(trialsWithSaccades)/et.totaltrials,2);
    
    % --- smooth pupil area data
    win = round(et.samplingRate * default.et.pupilsmoothwindow);
    if  win > 0
        et.pupilArea_smoothed =  medfilt1(et.pupilArea,win);
    end
    
    % --- replace left/right triggers with black/white triggers
    % 4 = cue to white side | 5 = cue to black side
    [et] = et_triggers_LR2BW(et);
    
    % --- extract epochs
    [savestruct.et.pupil.epochs] = et_extractEpochs(et.pupilArea,et,default.et.baselinetime);
    [savestruct.et.pupil.epochs_smoothed] = et_extractEpochs(et.pupilArea_smoothed,et,default.et.baselinetime);
    [savestruct.et.pupil.epochs_detrended] = et_extractEpochs(et.pupilArea_detrended,et,default.et.baselinetime);
    
    % --- plot trial means and single trials
    if strcmp(plotflag,'yes')
        plotname = fullfile(pwd,default.plotpath,[savestruct.et.sessionID,'.et.epochsaverages.png']);
        et_plotepochs_nbsl_bsl(savestruct.et.pupil.epochs_smoothed,et.samplingRate,default.et.baselinetime,plotname)
    end
    
    %% EEG Analysis PSDs
    
    % --- save general information about PSD struct
    savestruct.PSD = PSD;
    savestruct.PSD = rmfield(savestruct.PSD,'data');
    
    % --- compute lateralization index (LI) and add to PSD struct
    [savestruct.LI_PSD] = psd_calculateLI(PSD,default);
    
    % --- extract epochs
    [savestruct.PSD.epochs] = psd_extractEpochs(PSD,default.PSD.baselinetime);
    [savestruct.LI_PSD.epochs] = psd_extractEpochs(savestruct.LI_PSD,default.PSD.baselinetime);
    
    % Plotting
    
    % --- plot trial means and single trials of LI
    if strcmp(plotflag,'yes')
        psd_plotepochs_LI(savestruct.LI_PSD,default.PSD.baselinetime,...
            fullfile(pwd,default.plotpath,[savestruct.PSD.sessionID,'.psd.LImean.png']));
    end
    
    % --- spectrogram plots of the LI clusters
    if strcmp(plotflag,'yes')
        [savestruct.PSD] = psd_plotepochs_LIspectrogram(savestruct.PSD,default);
    end
    
    % --- topoplots of different bands (theta, alpha, beta)
    if strcmp(plotflag,'yes')
        [savestruct.PSD] = psd_plotepochs_topo(savestruct.PSD,default);
    end
    
    %% Classification
    
    % Abbreviations for savestruct.et.pupil.classification.abcd
    % a = X... raw; s... smoothed; d... detrended
    % b = X... raw; b... baselined; r... ratio
    % c = X... all samples; t... mean over time per trial
    % d = X... all time; w... smaller time window
    
    % EEG electrode configuration used for classification
    % 'all'...  all electrodes
    % 'aLI'...  Lateralization index as in Thut G, Nietzel A, Brandt SA, Pascual-Leone A. ?-Band electroencephalographic activity over occipital cortex indexes visuospatial attention bias and predicts visual target detection. Journal of Neuroscience. 2006 Sep 13;26(37):9494-502.
    % 'pLI'...  Lateralization index between PO7 and PO8
    % [numbers] ... vector with electrodes to use
    
    % Abbreviations for savestruct.psd.classification.abcd
    % a <wich electrodes>: X... all; s... selection; a... alpha
    %   lateralization index; p... parietal lateralization index
    % b <band>: t... theta; a... alpha; b... beta
    % c <band averaging>: X... no averaging; b... band average
    % d <time averaging>: X... no averaging; t... time average
    
    %% build final model to save

    et_window = default.et.classification.window;
    et_stretch = round(savestruct.et.samplingRate*(default.et.baselinetime + et_window(1))):...
        round(savestruct.et.samplingRate*(default.et.baselinetime + et_window(2)));
    
    plotname = fullfile(pwd,default.plotpath,[savestruct.et.sessionID,'.et.clsf_srtw.png']);
    argsET = struct('data',savestruct.et.pupil.epochs,'stretch',et_stretch,...
        'numfolds',1,'numreps',1,'signaltype','ratio','means',{{'time'}},'default',default,...
        'plot','yes','name',plotname);
    
    plotname = fullfile(pwd,default.plotpath,[savestruct.et.sessionID,'.psd.clsf_saXt.png']);
    argsPSD = struct('data',savestruct.PSD,'channels',default.PSD.classification.channels,...
        'numfolds',1,'numreps',1,'band',default.alphaband,'means',{{'time'}},'signaltype','raw',...
        'default',default,'plot','yes','name',plotname,'maxfeats',default.PSD.classification.maxfeatures);
    
    [savestruct.hybrid.classifier.ETsrtw_EEGsaXt] = classifyHybrid(argsET,argsPSD);
 
    %% Estimate model performance
    
    plotname = fullfile(pwd,default.plotpath,[savestruct.et.sessionID,'.et.clsf_srtw.png']);
    argsET = struct('data',savestruct.et.pupil.epochs_smoothed,'stretch',et_stretch,...
        'numfolds',default.et.classification.numfolds,'numreps',default.et.classification.numrep,...
        'signaltype','ratio','means',{{'time'}},'default',default,'plot','yes','name',plotname);
    
    plotname = fullfile(pwd,default.plotpath,[savestruct.et.sessionID,'.psd.clsf_saXt.png']);
    argsPSD = struct('data',savestruct.PSD,'channels',default.PSD.classification.channels,...
        'numfolds',default.PSD.classification.numfolds,'numreps',default.PSD.classification.numrep,...
        'band',default.alphaband,'means',{{'time'}},'signaltype','raw',...
        'default',default,'plot','yes','name',plotname,'maxfeats',default.PSD.classification.maxfeatures);
    
    [savestruct.hybrid.classification.ETsrtw_EEGsaXt] = classifyHybrid(argsET,argsPSD);   
  
    %% finalize what is saved and what not
    
    % --- takes up too much space and won't really be used
    savestruct.PSD = rmfield(savestruct.PSD,'epochs');
    
else
    disp('Too many trials corrupted!')
end
end


%% COMMON AUXILIARY FUNCTIONS
%%

function [clsf_results] = classifyHybrid(argsET,argsPSD)

%% ### Extract ET features ###

if isfield(argsET,'stretch')
    stretch = argsET.stretch;
else
    stretch = round(argsET.data.samplingRate*argsET.default.et.baselinetime):...
        argsET.data.epochs.maxlength;
end
if isfield(argsET,'numfolds')
    et_numfolds = argsET.numfolds;
else
    et_numfolds = argsET.default.et.classification.numfolds;
end
if isfield(argsET,'numreps')
    et_numrep = argsET.numreps;
else
    et_numrep = argsET.default.et.classification.numrep;
end

% --- create features
if ischar(argsET.signaltype)
    switch argsET.signaltype
        case 'raw'
            features.W = argsET.data.white.matrix(:,:,stretch);
            features.B = argsET.data.black.matrix(:,:,stretch);
        case 'baselined'
            features.W = argsET.data.white.matrix_bsl(:,:,stretch);
            features.B = argsET.data.black.matrix_bsl(:,:,stretch);
        case 'ratio'
            features.W = argsET.data.white.matrix_ratio(:,:,stretch);
            features.B = argsET.data.black.matrix_ratio(:,:,stretch);
    end
end

if sum(ismember(argsET.means,'time'))>0
    features.W = nanmean(features.W,3);
    features.B = nanmean(features.B,3);
end

% --- create labels
labels.W = repmat("W",[size(features.W),1]);
labels.B = repmat("B",[size(features.B),1]);
labels.W(isnan(features.W)) = string(NaN);
labels.B(isnan(features.B)) = string(NaN);
trialnumbers.W = argsET.data.white.trialnumbers;
trialnumbers.B = argsET.data.black.trialnumbers;

% --- keep channels and frequencies as features (rest to samples)
argsX = struct('data',features.W,'featuredimensions',2,'sampledimensions',3,...
    'trialdimensions',1);
[features_final.W, ~] = cschneid_data2feat(argsX);

argsX = struct('data',features.B,'featuredimensions',2,'sampledimensions',3,...
    'trialdimensions',1);
[features_final.B, ~] = cschneid_data2feat(argsX);

% --- do the same with the labels
argsX = struct('data',{labels.W},'featuredimensions',2,'sampledimensions',3,...
    'trialdimensions',1);
[labels_final.W] = cschneid_data2feat(argsX);

argsX = struct('data',{labels.B},'featuredimensions',2,'sampledimensions',3,...
    'trialdimensions',1);
[labels_final.B] = cschneid_data2feat(argsX);

et_features = cat(3,features_final.W,features_final.B);
et_labels = cat(3,labels_final.W,labels_final.B);
et_trialnumbers = cat(1,trialnumbers.W,trialnumbers.B);

%% --- plot class distributions
if strcmp(argsET.plot,'yes')
    colorlegend = {'black','white'};
    colB = [0    0.4470    0.7410];
    colW = [0.9290    0.6940    0.1250];
    plotsamples({colB,colW},features_final.B,features_final.W,colorlegend,[argsET.name,'.et.clsf_srtw.png'])
end

clearvars -except et_* argsET argsPSD

%% ### Extract PSD features ###
if isfield(argsPSD,'stretch')
    stretch = argsPSD.stretch;
else
    stretch = round(argsPSD.data.samplingRate*argsPSD.default.PSD.baselinetime)+1:...
        argsPSD.data.epochs.maxlength;
end
if isfield(argsPSD,'numfolds')
    psd_numfolds = argsPSD.numfolds;
else
    psd_numfolds = argsPSD.default.PSD.classification.numfolds;
end
if isfield(argsPSD,'numreps')
    psd_numrep = argsPSD.numreps;
else
    psd_numrep = argsPSD.default.PSD.classification.numrep;
end
if isfield(argsPSD,'maxfeats')
    psd_maxfeats = argsPSD.maxfeats;
else
    psd_maxfeats = inf;
end

freqbins = argsPSD.data.frequency>=argsPSD.band(1) & argsPSD.data.frequency<=argsPSD.band(end);
ROIchanL = find(ismember(argsPSD.data.channels,argsPSD.default.LI.roiL));
ROIchanR = find(ismember(argsPSD.data.channels,argsPSD.default.LI.roiR));

% --- create features
if ischar(argsPSD.signaltype)
    switch argsPSD.signaltype
        case 'raw'
            features.L = argsPSD.data.epochs.left.matrix(:,:,stretch,freqbins);
            features.R = argsPSD.data.epochs.right.matrix(:,:,stretch,freqbins);
        case 'baselined'
            features.L = argsPSD.data.epochs.left.matrix_bsl(:,:,stretch,freqbins);
            features.R = argsPSD.data.epochs.right.matrix_bsl(:,:,stretch,freqbins);
        case 'ratio'
            features.L = argsPSD.data.epochs.left.matrix_ratio(:,:,stretch,freqbins);
            features.R = argsPSD.data.epochs.right.matrix_ratio(:,:,stretch,freqbins);
    end
end

if ischar(argsPSD.channels)
    switch argsPSD.channels
        case 'all'
            
        case 'aLI'
            lchans = nanmean(features.L(:,ROIchanL,:,:),2);
            rchans = nanmean(features.L(:,ROIchanR,:,:),2);
            features.L = rchans-lchans;
            
            lchans = nanmean(features.R(:,ROIchanL,:,:),2);
            rchans = nanmean(features.R(:,ROIchanR,:,:),2);
            features.R = rchans-lchans;
        case 'pLI'
            lchans = nanmean(features.L(:,ROIchanL(end),:,:),2);
            rchans = nanmean(features.L(:,ROIchanR(end),:,:),2);
            features.L = rchans-lchans;
            
            lchans = nanmean(features.R(:,ROIchanL(end),:,:),2);
            rchans = nanmean(features.R(:,ROIchanR(end),:,:),2);
            features.R = rchans-lchans;
    end
else
    features.L = features.L(:,argsPSD.channels,:,:);
    features.R = features.R(:,argsPSD.channels,:,:);
end

if sum(ismember(argsPSD.means,'band'))>0
    features.L = nanmean(features.L,4);
    features.R = nanmean(features.R,4);
end
if sum(ismember(argsPSD.means,'time'))>0
    features.L = nanmean(features.L,3);
    features.R = nanmean(features.R,3);
end

% --- create labels
labels.L = repmat("L",[size(features.L),1]);
labels.R = repmat("R",[size(features.R),1]);
labels.L(isnan(features.L)) = string(NaN);
labels.R(isnan(features.R)) = string(NaN);
% labels.L = repmat({'L'},[size(features.L),1]);
% labels.R = repmat({'R'},[size(features.R),1]);
% labels.L(isnan(features.L)) = {''};
% labels.R(isnan(features.R)) = {''};
trialnumbers.L = argsPSD.data.epochs.left.trialnumbers;
trialnumbers.R = argsPSD.data.epochs.right.trialnumbers;

% --- keep channels and frequencies as features (rest to samples)
argsX = struct('data',features.L,'featuredimensions',[2,4],'sampledimensions',3,...
    'trialdimensions',1);
[features_final.L, ~] = cschneid_data2feat(argsX);

argsX = struct('data',features.R,'featuredimensions',[2,4],'sampledimensions',3,...
    'trialdimensions',1);
[features_final.R, ~] = cschneid_data2feat(argsX);

% --- do the same with the labels
argsX = struct('data',{labels.L},'featuredimensions',[2,4],'sampledimensions',3,...
    'trialdimensions',1);
[labels_final.L] = cschneid_data2feat(argsX);

argsX = struct('data',{labels.R},'featuredimensions',[2,4],'sampledimensions',3,...
    'trialdimensions',1);
[labels_final.R] = cschneid_data2feat(argsX);

psd_features = cat(3,features_final.L,features_final.R);
psd_labels = cat(3,labels_final.L,labels_final.R);
psd_trialnumbers = cat(1,trialnumbers.L,trialnumbers.R);

%% --- plot class distributions
if strcmp(argsPSD.plot,'yes')
    colorlegend = {'left','right'};
    colL = [0.8500    0.3250    0.0980];
    colR = [0    0.4470    0.7410];
    plotsamples({colL,colR},features_final.L,features_final.R,colorlegend,argsPSD.name)
end

clearvars -except et_* psd_* argsET argsPSD
%% ### reorder features ###
[et_trialnumbers,et_tnidx] = sort(et_trialnumbers);
et_features = et_features(:,:,et_tnidx);
et_labels = et_labels(:,:,et_tnidx);

[psd_trialnumbers,psd_tnidx] = sort(psd_trialnumbers);
psd_features = psd_features(:,:,psd_tnidx);
psd_labels = psd_labels(:,:,psd_tnidx);

% LABELS are not the same, because L/R and B/W are randomized!!!!

%% ### last consitency checks ###

if et_trialnumbers ~= psd_trialnumbers
    error('Trial numbers between eye tracker and EEG are not matching!')
end

if et_numfolds ~= psd_numfolds
    error('Fold numbers between eye tracker and EEG are not matching!');
else
    numfolds = et_numfolds;
end

if et_numrep ~= psd_numrep
    error('Repetition numbers between eye tracker and EEG are not matching!');
else
    numrep = et_numrep;
end
    
%% ### Final classification ###

DAoptions_et = {'DiscrimType',argsET.default.classifier,'Gamma',0.5,...
    'ClassNames',{'W','B'},'Prior',[0.5,0.5]};
DAoptions_psd = {'DiscrimType',argsPSD.default.classifier,'Gamma',0.5,...
    'ClassNames',{'L','R'},'Prior',[0.5,0.5]};
DAoptions_fusion = {'DiscrimType',argsPSD.default.classifier,'Gamma',0.5,...
    'ClassNames',{'L','R'},'Prior',[0.5,0.5]};

first.et = struct('data',et_features,...
    'labels',{et_labels},...
    'type','DA',...
    'options',{DAoptions_et},...
    'featselection','no');
first.psd = struct('data',psd_features,...
    'labels',{psd_labels},...
    'type','DA',...
    'options',{DAoptions_psd},...
    'featselection','yes',...
    'maxfeats',psd_maxfeats);
second = struct('type','DA',...
    'options',{DAoptions_fusion},...
    'featselection','no');

cargs = struct('numfolds',numfolds,...
    'repetitions',numrep,...
    'first',first,...
    'second',second);
    
[clsf_results.LDA] = cschneid_hybrid_classification(cargs);

%% plot data if 1D or 2D

if cargs.repetitions == 1
    if strcmp(argsET.plot,'yes') || strcmp(argsPSD.plot,'yes')
        
        a = strfind(argsPSD.name,'\');
        b = strfind(argsPSD.name((a(end)+1):end),'.');
        stamp = argsPSD.name(1:a(end)+b(2));
        firstLevelClsfrs = fieldnames(first)';
        pos = find(strcmp(first.(firstLevelClsfrs{1}).options,'ClassNames')) + 1;
        counter = 0;
        for nfl = firstLevelClsfrs
            counter = counter + 1;
            plotdata(counter,:,:) = clsf_results.LDA.first.(nfl{1}).newfeature'; %#ok<AGROW>
            plotlabels(counter,:,:) = clsf_results.LDA.first.(nfl{1}).newlabels'; %#ok<AGROW>
        end
        plotdata_reshaped = reshape(plotdata,size(plotdata,1),[]);
        plotlabels_reshaped = reshape(plotlabels,size(plotlabels,1),[]);
        class1 = strcmp(plotlabels_reshaped(1,:),second.options{pos}(1));
        class2 = strcmp(plotlabels_reshaped(1,:),second.options{pos}(2));
        if size(plotdata_reshaped,1) == 1
            fig = figure;
            histogram(plotdata_reshaped(class1));
            hold on;
            histogram(plotdata_reshaped(class2));
            legend(second.options{pos});
            xlabel(firstLevelClsfrs{1});
            ylabel('count');
            title('Histogram of first level classifier output')
            xlim([0,1]);
            % --- save plot
            F = getframe(fig);
            imwrite(F.cdata, [stamp,'hybrid.1D.png'], 'png')
        elseif size(plotdata_reshaped,1) == 2
            fig = figure;
            scatter(plotdata_reshaped(1,class1),plotdata_reshaped(2,class1),'filled');
            hold on;
            scatter(plotdata_reshaped(1,class2),plotdata_reshaped(2,class2),'filled');
            legend(second.options{pos});
            xlabel(firstLevelClsfrs{1});
            ylabel(firstLevelClsfrs{2});
            title('Scatter plot of first level classifier outputs')
            xlim([0,1]);
            ylim([0,1]);
            % --- save plot
            F = getframe(fig);
            imwrite(F.cdata, [stamp,'hybrid.2D.png'], 'png')
        end
    end
end

end

%% AUXILIARY FUNCTIONS EYE TRACKER
%%

function [et] = et_triggers_LR2BW(et)
% replaces left/right [1/2] triggers with white/black [4/5] triggers
% adds field triggersBW to et struct

BGW = [11,13]; % start trial triggers for black-gray-white background
WGB = [12,14]; % start trial triggers for white-gray-black background

et.triggersBW = et.triggers;

starttriggers = et.triggerLegend{3}(~cellfun(@isempty,regexp(et.triggerLegend{1},'trial_start')));
cuetriggers = et.triggerLegend{3}(~cellfun(@isempty,regexp(et.triggerLegend{1},'cue')));
targettriggers = et.triggerLegend{3}(~cellfun(@isempty,regexp(et.triggerLegend{1},'target')));

trialstarttriggers = find(ismember(et.triggers(1,:),starttriggers));
trialcuetriggers = find(ismember(et.triggers(1,:),cuetriggers));
trialendtriggers = find(ismember(et.triggers(1,:),targettriggers));

for tr = 1:length(trialstarttriggers)
    if ismember(et.triggersBW(1,trialstarttriggers(tr)),BGW)
        switch et.triggersBW(1,trialcuetriggers(tr))
            case 1
                et.triggersBW(1,trialcuetriggers(tr)) = 5;
                et.triggersBW(1,trialendtriggers(tr)) = 35;
            case 2
                et.triggersBW(1,trialcuetriggers(tr)) = 4;
                et.triggersBW(1,trialendtriggers(tr)) = 34;
        end
    elseif ismember(et.triggersBW(1,trialstarttriggers(tr)),WGB)
        switch et.triggersBW(1,trialcuetriggers(tr))
            case 1
                et.triggersBW(1,trialcuetriggers(tr)) = 4;
                et.triggersBW(1,trialendtriggers(tr)) = 34;
            case 2
                et.triggersBW(1,trialcuetriggers(tr)) = 5;
                et.triggersBW(1,trialendtriggers(tr)) = 35;
        end
    end
end
end

function [epochs] = et_extractEpochs(data,datastruct,baselinetime)

% --- extract black and white and center (=gray) trials
args = struct('data',data,'time',datastruct.time,'triggers',datastruct.triggersBW,...
    'epoch',{{[4,34],[-baselinetime,0]}},'samplingRate',datastruct.samplingRate,'excludeartifacts','yes');
[white.cell,~,white.trialnumbers] = cschneid_epoching(args);

args = struct('data',data,'time',datastruct.time,'triggers',datastruct.triggersBW,...
    'epoch',{{[5,35],[-baselinetime,0]}},'samplingRate',datastruct.samplingRate,'excludeartifacts','yes');
[black.cell,~,black.trialnumbers] = cschneid_epoching(args);

args = struct('data',data,'time',datastruct.time,'triggers',datastruct.triggersBW,...
    'epoch',{{[3,33],[-baselinetime,0]}},'samplingRate',datastruct.samplingRate,'excludeartifacts','yes');
[center.cell,~,center.trialnumbers] = cschneid_epoching(args);

% --- put into common-length matrices
sW = max(cellfun(@(C) size(C,2), white.cell));
sB = max(cellfun(@(C) size(C,2), black.cell));
sC = max(cellfun(@(C) size(C,2), center.cell));
epochs.maxlength = max([sW,sB,sC]);

white.matrix = NaN(size(white.cell,2),size(white.cell{1},1),epochs.maxlength,size(white.cell{1},3));
black.matrix = NaN(size(black.cell,2),size(black.cell{1},1),epochs.maxlength,size(black.cell{1},3));
center.matrix = NaN(size(center.cell,2),size(center.cell{1},1),epochs.maxlength,size(center.cell{1},3));

for ep = 1:size(white.cell,2)
    white.matrix(ep,:,1:size(white.cell{ep},2),:) = white.cell{ep};
end
for ep = 1:size(black.cell,2)
    black.matrix(ep,:,1:size(black.cell{ep},2),:) = black.cell{ep};
end
for ep = 1:size(center.cell,2)
    center.matrix(ep,:,1:size(center.cell{ep},2),:) = center.cell{ep};
end

% --- create baselined data
white.bsl = nanmean(white.matrix(:,:,1:round((datastruct.samplingRate)*baselinetime),:),3);
black.bsl = nanmean(black.matrix(:,:,1:round((datastruct.samplingRate)*baselinetime),:),3);
center.bsl = nanmean(center.matrix(:,:,1:round((datastruct.samplingRate)*baselinetime),:),3);

white.matrix_bsl = white.matrix - repmat(white.bsl,1,1,epochs.maxlength,1);
black.matrix_bsl = black.matrix - repmat(black.bsl,1,1,epochs.maxlength,1);
center.matrix_bsl = center.matrix - repmat(center.bsl,1,1,epochs.maxlength,1);

white.matrix_ratio = white.matrix ./ repmat(white.bsl,1,1,epochs.maxlength);
black.matrix_ratio = black.matrix ./ repmat(black.bsl,1,1,epochs.maxlength);
center.matrix_ratio = center.matrix ./ repmat(center.bsl,1,1,epochs.maxlength);

epochs.white = white;
epochs.black = black;
epochs.center = center;

end

function [] = et_plotepochs_nbsl_bsl(epochs,samplingRate,baselinetime,name)

fig = figure('Position', get(0, 'Screensize'));
subplot(2,2,1);
means = squeeze([nanmean(epochs.white.matrix,1);nanmean(epochs.black.matrix,1);nanmean(epochs.center.matrix,1)]);
bounds(:,1,:) = squeeze([nanstd(epochs.white.matrix,1);nanstd(epochs.black.matrix,1);nanstd(epochs.center.matrix,1)])';
timeline = (1:epochs.maxlength)./samplingRate - baselinetime;
boundedline(timeline,means,bounds,'alpha')
cschneid_vline(0, 'k', '-', 'cue')
co = get(gca,'colororder');
legend({'white','black','center'})
title('Non-baselined pupil area: means+std over time')

subplot(2,2,2)
hold on;
hi = plot(timeline,squeeze(epochs.center.matrix),'Color',co(3,:));
h(3) = hi(1);
hi = plot(timeline,squeeze(epochs.white.matrix),'Color',co(1,:));
h(1) = hi(1);
hi = plot(timeline,squeeze(epochs.black.matrix),'Color',co(2,:));
h(2) = hi(1);
cschneid_vline(0, 'k', '-', 'cue')
legend(h,{'white','black','center'})
title('Non-baselined pupil area: single trials over time')

subplot(2,2,3);
means = squeeze([nanmean(epochs.white.matrix_ratio,1);nanmean(epochs.black.matrix_ratio,1);nanmean(epochs.center.matrix_ratio,1)]);
bounds(:,1,:) = squeeze([nanstd(epochs.white.matrix_ratio,1);nanstd(epochs.black.matrix_ratio,1);nanstd(epochs.center.matrix_ratio,1)])';
boundedline(timeline,means,bounds,'alpha')
cschneid_vline(0, 'k', '-', 'cue')
co = get(gca,'colororder');
legend({'white','black','center'})
title('Baselined pupil area: means+std over time')

subplot(2,2,4)
hold on;
hi = plot(timeline,squeeze(epochs.center.matrix_ratio),'Color',co(3,:));
h(3) = hi(1);
hi = plot(timeline,squeeze(epochs.white.matrix_ratio),'Color',co(1,:));
h(1) = hi(1);
hi = plot(timeline,squeeze(epochs.black.matrix_ratio),'Color',co(2,:));
h(2) = hi(1);
cschneid_vline(0, 'k', '-', 'cue')
legend(h,{'white','black','center'})
title('Non-baselined pupil area: single trials over time')

% --- save plot
F = getframe(fig);
imwrite(F.cdata, name, 'png')

end

%% AUXILIARY FUNCTIONS EEG/PSD
%%
function [datastruct] = psd_calculateLI(datastruct,default)
% --- calculate the lateralization index (LI, Thut et al. 2006) and exports
% it as a new datastruct

% first mean over alpha band, then mean over ROIs
ROIchanR = ismember(datastruct.channels,default.LI.roiR);
ROIchanL = ismember(datastruct.channels,default.LI.roiL);
freqbins = datastruct.frequency>=default.alphaband(1) & datastruct.frequency<=default.alphaband(end);
data_alphamean = nanmean(datastruct.data(:,:,freqbins),3);
data_alphameanR = nanmean(data_alphamean(ROIchanR,:),1);
data_alphameanL = nanmean(data_alphamean(ROIchanL,:),1);
data_alphameanmean = mean([data_alphameanR(:),data_alphameanL(:)],2)';

datastruct.data = (data_alphameanR - data_alphameanL)./data_alphameanmean;
datastruct.channelLegend = {'LI'};

datastruct = rmfield(datastruct,'montage');
datastruct = rmfield(datastruct,'layout2D');
datastruct = rmfield(datastruct,'layout3D');
end

function [epochs] = psd_extractEpochs(datastruct,baselinetime)
% --- extract left, right, and center trials
args = struct('data',datastruct.data,'time',datastruct.time,'triggers',datastruct.triggers,...
    'epoch',{{{1,[31,32]},[-baselinetime,0]}},'samplingRate',datastruct.samplingRate,'excludeartifacts','yes');
[left.cell,~,left.trialnumbers] = cschneid_epoching(args);

args = struct('data',datastruct.data,'time',datastruct.time,'triggers',datastruct.triggers,...
    'epoch',{{{2,[31,32]},[-baselinetime,0]}},'samplingRate',datastruct.samplingRate,'excludeartifacts','yes');
[right.cell,~,right.trialnumbers] = cschneid_epoching(args);

args = struct('data',datastruct.data,'time',datastruct.time,'triggers',datastruct.triggers,...
    'epoch',{{[3,33],[-baselinetime,0]}},'samplingRate',datastruct.samplingRate,'excludeartifacts','yes');
[center.cell,~,center.trialnumbers] = cschneid_epoching(args);

% --- put into common-length matrices
sL = max(cellfun(@(C) size(C,2), left.cell));
sR = max(cellfun(@(C) size(C,2), right.cell));
sC = max(cellfun(@(C) size(C,2), center.cell));
epochs.maxlength = max([sL,sR,sC]);

left.matrix = NaN(size(left.cell,2),size(left.cell{1},1),epochs.maxlength,size(left.cell{1},3));
right.matrix = NaN(size(right.cell,2),size(right.cell{1},1),epochs.maxlength,size(right.cell{1},3));
center.matrix = NaN(size(center.cell,2),size(center.cell{1},1),epochs.maxlength,size(center.cell{1},3));

for ep = 1:size(left.cell,2)
    left.matrix(ep,:,1:size(left.cell{ep},2),:) = left.cell{ep};
end
for ep = 1:size(right.cell,2)
    right.matrix(ep,:,1:size(right.cell{ep},2),:) = right.cell{ep};
end
for ep = 1:size(center.cell,2)
    center.matrix(ep,:,1:size(center.cell{ep},2),:) = center.cell{ep};
end

% --- create baselined data
left.bsl = nanmean(left.matrix(:,:,1:round((datastruct.samplingRate)*baselinetime),:),3);
right.bsl = nanmean(right.matrix(:,:,1:round((datastruct.samplingRate)*baselinetime),:),3);
center.bsl = nanmean(center.matrix(:,:,1:round((datastruct.samplingRate)*baselinetime),:),3);

left.matrix_bsl = left.matrix - repmat(left.bsl,1,1,epochs.maxlength,1);
right.matrix_bsl = right.matrix - repmat(right.bsl,1,1,epochs.maxlength,1);
center.matrix_bsl = center.matrix - repmat(center.bsl,1,1,epochs.maxlength,1);

left.matrix_ratio = left.matrix ./ repmat(left.bsl,1,1,epochs.maxlength,1);
right.matrix_ratio = right.matrix ./ repmat(right.bsl,1,1,epochs.maxlength,1);
center.matrix_ratio = center.matrix ./ repmat(center.bsl,1,1,epochs.maxlength,1);

epochs.left = left;
epochs.right = right;
epochs.center = center;

end

function [] = psd_plotepochs_LI(LIstruct,baselinetime,filename)
% --- plot side differences in lateralization index
fig = figure('Position', get(0, 'Screensize'));
if ~isempty(LIstruct.epochs.center)
    means = [nanmean(squeeze(LIstruct.epochs.right.matrix),1);...
        nanmean(squeeze(LIstruct.epochs.left.matrix),1);nanmean(squeeze(LIstruct.epochs.center.matrix),1)];
    bounds(:,1,:) = [nanstd(squeeze(LIstruct.epochs.right.matrix),1);...
        nanstd(squeeze(LIstruct.epochs.left.matrix),1);nanstd(squeeze(LIstruct.epochs.center.matrix),1)]';
else
    means = [nanmean(squeeze(LIstruct.epochs.right.matrix),1);...
        nanmean(squeeze(LIstruct.epochs.left.matrix),1)];
    bounds(:,1,:) = [nanstd(squeeze(LIstruct.epochs.right.matrix),1);...
        nanstd(squeeze(LIstruct.epochs.left.matrix),1)]';
end

timeline = (1:LIstruct.epochs.maxlength)./LIstruct.samplingRate - baselinetime;
boundedline(timeline,means,bounds,'alpha')
cschneid_vline(0, 'k', '-', 'cue')
if ~isempty(LIstruct.epochs.center)
    legend({'right','left','center'})
else
    legend({'right','left'})
end
title('Lateralization index over time')
grid on;

F = getframe(fig);
imwrite(F.cdata, filename, 'png')
end

function [datastruct] = psd_plotepochs_LIspectrogram(datastruct,default)
% plots the spectrogram of the LI clusters

plotmaxfreq = 45;
usefreq = datastruct.frequency <= plotmaxfreq;
ROIchanR = ismember(datastruct.channels,default.LI.roiR);
ROIchanL = ismember(datastruct.channels,default.LI.roiL);
t_start = round(default.PSD.baselinetime * datastruct.samplingRate);
t_max = datastruct.epochs.maxlength/datastruct.samplingRate;

left.rightcluster = squeeze(nanmean(datastruct.epochs.left.matrix(:,ROIchanR,:,usefreq),2));
left.leftcluster = squeeze(nanmean(datastruct.epochs.left.matrix(:,ROIchanL,:,usefreq),2));
right.rightcluster = squeeze(nanmean(datastruct.epochs.right.matrix(:,ROIchanR,:,usefreq),2));
right.leftcluster = squeeze(nanmean(datastruct.epochs.right.matrix(:,ROIchanL,:,usefreq),2));

left.mean.rightcluster = squeeze(nanmean(left.rightcluster,1));
left.mean.leftcluster = squeeze(nanmean(left.leftcluster,1));
right.mean.rightcluster = squeeze(nanmean(right.rightcluster,1));
right.mean.leftcluster = squeeze(nanmean(right.leftcluster,1));

left.bsl.rightcluster = mean(left.mean.rightcluster(1:t_start,:));
left.bsl.leftcluster = mean(left.mean.leftcluster(1:t_start,:));
right.bsl.rightcluster = mean(right.mean.rightcluster(1:t_start,:));
right.bsl.leftcluster = mean(right.mean.leftcluster(1:t_start,:));

left.plot.rightcluster = 10*log10(left.mean.rightcluster./...
    repmat(left.bsl.rightcluster,size(left.mean.rightcluster,1),1));
left.plot.leftcluster = 10*log10(left.mean.leftcluster./...
    repmat(left.bsl.leftcluster,size(left.mean.leftcluster,1),1));
right.plot.rightcluster = 10*log10(right.mean.rightcluster./...
    repmat(right.bsl.rightcluster,size(right.mean.rightcluster,1),1));
right.plot.leftcluster = 10*log10(right.mean.leftcluster./...
    repmat(right.bsl.leftcluster,size(right.mean.leftcluster,1),1));

left.LI = left.plot.rightcluster - left.plot.leftcluster;
right.LI = right.plot.rightcluster - right.plot.leftcluster;

zmax = max([
    max(max(abs(left.plot.rightcluster))),...
    max(max(abs(left.plot.leftcluster))),...
    max(max(abs(right.plot.rightcluster))),...
    max(max(abs(right.plot.leftcluster))),...
    max(max(abs(left.LI))),...
    max(max(abs(right.LI)))]);

% compute x-axis values
xt = linspace(0,t_max,datastruct.epochs.maxlength)-default.PSD.baselinetime;
ticks = unique(round(xt));
tickind = ticks.*0;
for t = 1:length(ticks)
    [~,tickind(t)] = min(abs(xt-ticks(t)));
end

fig = figure('Position', get(0, 'Screensize'));
suptitle('Time-frequency plots for left and right trials')

subplot(2,3,1)
pcolor(left.plot.leftcluster')
shading interp;
caxis([-zmax,zmax]);
ax1 = gca;
set(ax1, 'XTick', tickind, 'XTickLabel', ticks);
xlabel('seconds'); ylabel('frequency [Hz]');
cschneid_vline(tickind(ticks==0),'k','-','cue');
title('Left cue: left cluster (P7, P5, PO7)');
colorbar;

subplot(2,3,2)
pcolor(left.plot.rightcluster')
shading interp;
caxis([-zmax,zmax]);
ax1 = gca;
set(ax1, 'XTick', tickind, 'XTickLabel', ticks);
xlabel('seconds'); ylabel('frequency [Hz]');
cschneid_vline(tickind(ticks==0),'k','-','cue');
title('Left cue: right cluster (P8, P6, PO8)');
colorbar;

subplot(2,3,3)
pcolor(left.LI')
shading interp;
caxis([-zmax,zmax]);
ax1 = gca;
set(ax1, 'XTick', tickind, 'XTickLabel', ticks);
xlabel('seconds'); ylabel('frequency [Hz]');
cschneid_vline(tickind(ticks==0),'k','-','cue');
title('Left cue: lateralization (right-left)');
colorbar;

subplot(2,3,4)
pcolor(right.plot.leftcluster')
shading interp;
caxis([-zmax,zmax]);
ax1 = gca;
set(ax1, 'XTick', tickind, 'XTickLabel', ticks);
xlabel('seconds'); ylabel('frequency [Hz]');
cschneid_vline(tickind(ticks==0),'k','-','cue');
title('Right cue: left cluster (P7, P5, PO7)');
colorbar;

subplot(2,3,5)
pcolor(right.plot.rightcluster')
shading interp;
caxis([-zmax,zmax]);
ax1 = gca;
set(ax1, 'XTick', tickind, 'XTickLabel', ticks);
xlabel('seconds'); ylabel('frequency [Hz]');
cschneid_vline(tickind(ticks==0),'k','-','cue');
title('Right cue: right cluster (P8, P6, PO8)');
colorbar;

subplot(2,3,6)
pcolor(right.LI')
shading interp;
caxis([-zmax,zmax]);
ax1 = gca;
set(ax1, 'XTick', tickind, 'XTickLabel', ticks);
xlabel('seconds'); ylabel('frequency [Hz]');
cschneid_vline(tickind(ticks==0),'k','-','cue');
title('Right cue: lateralization (right-left)');
colorbar;

filename = fullfile(pwd,default.plotpath,[datastruct.sessionID,'.psd.LIspectrum.png']);
F = getframe(fig);
imwrite(F.cdata, filename, 'png')

datastruct.spectrumLI.leftcue.leftcluster = left.plot.leftcluster;
datastruct.spectrumLI.leftcue.rightcluster = left.plot.rightcluster;
datastruct.spectrumLI.leftcue.LI = left.LI;

datastruct.spectrumLI.rightcue.leftcluster = right.plot.leftcluster;
datastruct.spectrumLI.rightcue.rightcluster = right.plot.rightcluster;
datastruct.spectrumLI.rightcue.LI = right.LI;

end

function [datastruct] = psd_plotepochs_topo(datastruct,default)
% plots spatial distribution in different frequency bands

trialstart = round(default.PSD.baselinetime*datastruct.samplingRate)+1;

% --- extract all plotting bands
bins.theta = datastruct.frequency>=default.thetaband(1) & datastruct.frequency<=default.thetaband(end);
bins.alpha = datastruct.frequency>=default.alphaband(1) & datastruct.frequency<=default.alphaband(end);
bins.beta = datastruct.frequency>=default.betaband(1) & datastruct.frequency<=default.betaband(end);

left.epochs = datastruct.epochs.left.matrix;
left.trialavg_epochs = squeeze(nanmean(left.epochs,1));
left.timeavg_trialavg_epochs = squeeze(nanmean(left.trialavg_epochs(:,trialstart:end,:),2));
left.theta_timeavg_trialavg_epochs = squeeze(nanmean(left.timeavg_trialavg_epochs(:,bins.theta),2));
left.alpha_timeavg_trialavg_epochs = squeeze(nanmean(left.timeavg_trialavg_epochs(:,bins.alpha),2));
left.beta_timeavg_trialavg_epochs = squeeze(nanmean(left.timeavg_trialavg_epochs(:,bins.beta),2));
left.theta_trialavg_epochs = squeeze(nanmean(left.trialavg_epochs(:,:,bins.theta),3));
left.alpha_trialavg_epochs = squeeze(nanmean(left.trialavg_epochs(:,:,bins.alpha),3));
left.beta_trialavg_epochs = squeeze(nanmean(left.trialavg_epochs(:,:,bins.beta),3));

right.epochs = datastruct.epochs.right.matrix;
right.trialavg_epochs = squeeze(nanmean(right.epochs,1));
right.timeavg_trialavg_epochs = squeeze(nanmean(right.trialavg_epochs(:,trialstart:end,:),2));
right.theta_timeavg_trialavg_epochs = squeeze(nanmean(right.timeavg_trialavg_epochs(:,bins.theta),2));
right.alpha_timeavg_trialavg_epochs = squeeze(nanmean(right.timeavg_trialavg_epochs(:,bins.alpha),2));
right.beta_timeavg_trialavg_epochs = squeeze(nanmean(right.timeavg_trialavg_epochs(:,bins.beta),2));
right.theta_trialavg_epochs = squeeze(nanmean(right.trialavg_epochs(:,:,bins.theta),3));
right.alpha_trialavg_epochs = squeeze(nanmean(right.trialavg_epochs(:,:,bins.alpha),3));
right.beta_trialavg_epochs = squeeze(nanmean(right.trialavg_epochs(:,:,bins.beta),3));

if isfield(datastruct.epochs,'center')
    center.epochs = datastruct.epochs.center.matrix;
    center.trialavg_epochs = squeeze(nanmean(center.epochs,1));
    center.timeavg_trialavg_epochs = squeeze(nanmean(center.trialavg_epochs(:,trialstart:end,:),2));
    center.theta_timeavg_trialavg_epochs = squeeze(nanmean(center.timeavg_trialavg_epochs(:,bins.theta),2));
    center.alpha_timeavg_trialavg_epochs = squeeze(nanmean(center.timeavg_trialavg_epochs(:,bins.alpha),2));
    center.beta_timeavg_trialavg_epochs = squeeze(nanmean(center.timeavg_trialavg_epochs(:,bins.beta),2));
    center.theta_trialavg_epochs = squeeze(nanmean(center.trialavg_epochs(:,:,bins.theta),3));
    center.alpha_trialavg_epochs = squeeze(nanmean(center.trialavg_epochs(:,:,bins.alpha),3));
    center.beta_trialavg_epochs = squeeze(nanmean(center.trialavg_epochs(:,:,bins.beta),3));
end

%% --- plot theta band

% --- global
if isfield(datastruct.epochs,'center')
    data = [left.theta_timeavg_trialavg_epochs(:),center.theta_timeavg_trialavg_epochs(:),...
        right.theta_timeavg_trialavg_epochs(:)];
    plotname = fullfile(pwd,default.plotpath,[datastruct.sessionID,'.psd.topo.theta_lcr.png']);
    args = struct('data',data,'channels',{datastruct.channels},...
        'layoutfile',default.locsfile,'title','Theta band power',...
        'timepointlabels',{{'Left trials', 'Center trials', 'Right trials'}},'markers','on',...
        'save','yes','savename',plotname);
    cschneid_topoplot_eeglab(args)
else
    data = [left.theta_timeavg_trialavg_epochs(:),right.theta_timeavg_trialavg_epochs(:)];
    plotname = fullfile(pwd,default.plotpath,[datastruct.sessionID,'.psd.topo.theta_lr.png']);
    args = struct('data',data,'channels',{datastruct.channels},...
        'layoutfile',default.locsfile,'title','Theta band power',...
        'timepointlabels',{{'Left trials', 'Right trials'}},'markers','on',...
        'save','yes','savename',plotname);
    cschneid_topoplot_eeglab(args)
end

% --- class contrast global
data = right.theta_timeavg_trialavg_epochs(:)-left.theta_timeavg_trialavg_epochs(:);
plotname = fullfile(pwd,default.plotpath,[datastruct.sessionID,'.psd.topo.theta_lrdiff.png']);
args = struct('data',data,'channels',{datastruct.channels},...
    'layoutfile',default.locsfile,'title','Theta band power difference',...
    'timepointlabels',{{'Right minus left trials'}},'markers','on','save','yes',...
    'savename',plotname);
cschneid_topoplot_eeglab(args)

% --- time steps
clear data ticks
timeline = linspace(0, datastruct.epochs.maxlength / datastruct.samplingRate, datastruct.epochs.maxlength)-default.PSD.baselinetime;
data(:,1) = (mean(right.theta_trialavg_epochs(:,timeline<0),2) + mean(left.theta_trialavg_epochs(:,timeline<0),2))/2;
ticks = {'baseline'};
for win = 1:length(default.topotimes.theta)-1
    t = timeline>default.topotimes.theta(win) & timeline<default.topotimes.theta(win+1);
    addit =  (mean(right.theta_trialavg_epochs(:,t),2) + mean(left.theta_trialavg_epochs(:,t),2))/2;
    data = cat(2,data,addit);
    ticks = cat(2,ticks,[num2str(default.topotimes.theta(win)),' - ',num2str(default.topotimes.theta(win+1))]);
end

plotname = fullfile(pwd,default.plotpath,[datastruct.sessionID,'.psd.topo.theta_time.png']);
args = struct('data',data,'channels',{datastruct.channels},...
    'layoutfile',default.locsfile,'title','Global theta over time (average between left and right)',...
    'timepointlabels',{ticks},'markers','on','save','yes','savename',plotname);
cschneid_topoplot_eeglab(args)

%% --- plot alpha band

% --- global
if isfield(datastruct.epochs,'center')
    plotname = fullfile(pwd,default.plotpath,[datastruct.sessionID,'.psd.topo.alpha_lcr.png']);
    data = [left.alpha_timeavg_trialavg_epochs(:),center.alpha_timeavg_trialavg_epochs(:),...
        right.alpha_timeavg_trialavg_epochs(:)];
    args = struct('data',data,'channels',{datastruct.channels},...
        'layoutfile',default.locsfile,'title','Alpha band power',...
        'timepointlabels',{{'Left trials', 'Center trials', 'Right trials'}},'markers','on','save','yes','savename',plotname);
    cschneid_topoplot_eeglab(args)
else
    plotname = fullfile(pwd,default.plotpath,[datastruct.sessionID,'.psd.topo.alpha_lr.png']);
    data = [left.alpha_timeavg_trialavg_epochs(:),right.alpha_timeavg_trialavg_epochs(:)];
    args = struct('data',data,'channels',{datastruct.channels},...
        'layoutfile',default.locsfile,'title','Alpha band power',...
        'timepointlabels',{{'Left trials', 'Right trials'}},'markers','on','save','yes','savename',plotname);
    cschneid_topoplot_eeglab(args)
end

% --- class contrast global
data = right.alpha_timeavg_trialavg_epochs(:)-left.alpha_timeavg_trialavg_epochs(:);
plotname = fullfile(pwd,default.plotpath,[datastruct.sessionID,'.psd.topo.alpha_lrdiff.png']);
args = struct('data',data,'channels',{datastruct.channels},...
    'layoutfile',default.locsfile,'title','Alpha band power difference',...
    'timepointlabels',{{'Right minus left trials'}},'markers','names','save','yes','savename',plotname);
cschneid_topoplot_eeglab(args)

% --- class contrast time steps
clear data ticks
timeline = linspace(0, datastruct.epochs.maxlength / datastruct.samplingRate, datastruct.epochs.maxlength)-default.PSD.baselinetime;
data(:,1) = mean(right.alpha_trialavg_epochs(:,timeline<0),2) - mean(left.alpha_trialavg_epochs(:,timeline<0),2);
ticks = {'baseline'};
for win = 1:length(default.topotimes.alpha)-1
    t = timeline>default.topotimes.alpha(win) & timeline<default.topotimes.alpha(win+1);
    addit =  mean(right.alpha_trialavg_epochs(:,t),2) - mean(left.alpha_trialavg_epochs(:,t),2);
    data = cat(2,data,addit);
    ticks = cat(2,ticks,[num2str(default.topotimes.alpha(win)),' - ',num2str(default.topotimes.alpha(win+1))]);
end

plotname = fullfile(pwd,default.plotpath,[datastruct.sessionID,'.psd.topo.alpha_timediff.png']);
args = struct('data',data,'channels',{datastruct.channels},...
    'layoutfile',default.locsfile,'title','Alpha lateralization over time',...
    'timepointlabels',{ticks},'markers','on','save','yes','savename',plotname);
cschneid_topoplot_eeglab(args)

% --- time steps
clear data ticks
timeline = linspace(0, datastruct.epochs.maxlength / datastruct.samplingRate, datastruct.epochs.maxlength)-default.PSD.baselinetime;
data(:,1) = (mean(right.alpha_trialavg_epochs(:,timeline<0),2) + mean(left.alpha_trialavg_epochs(:,timeline<0),2))/2;
ticks = {'baseline'};
for win = 1:length(default.topotimes.alpha)-1
    t = timeline>default.topotimes.alpha(win) & timeline<default.topotimes.alpha(win+1);
    addit =  (mean(right.alpha_trialavg_epochs(:,t),2) + mean(left.alpha_trialavg_epochs(:,t),2))/2;
    data = cat(2,data,addit);
    ticks = cat(2,ticks,[num2str(default.topotimes.alpha(win)),' - ',num2str(default.topotimes.alpha(win+1))]);
end

plotname = fullfile(pwd,default.plotpath,[datastruct.sessionID,'.psd.topo.alpha_time.png']);
args = struct('data',data,'channels',{datastruct.channels},...
    'layoutfile',default.locsfile,'title','Global alpha power over time (average between left and right)',...
    'timepointlabels',{ticks},'markers','on','save','yes','savename',plotname);
cschneid_topoplot_eeglab(args)

%% --- plot beta band

% --- global
if isfield(datastruct.epochs,'center')
    data = [left.beta_timeavg_trialavg_epochs(:),center.beta_timeavg_trialavg_epochs(:),...
        right.beta_timeavg_trialavg_epochs(:)];
    plotname = fullfile(pwd,default.plotpath,[datastruct.sessionID,'.psd.topo.beta_lcr.png']);
    args = struct('data',data,'channels',{datastruct.channels},...
        'layoutfile',default.locsfile,'title','Beta band power',...
        'timepointlabels',{{'Left trials', 'Center trials', 'Right trials'}},'markers','on','save','yes','savename',plotname);
    cschneid_topoplot_eeglab(args)
else
    data = [left.beta_timeavg_trialavg_epochs(:),right.beta_timeavg_trialavg_epochs(:)];
    plotname = fullfile(pwd,default.plotpath,[datastruct.sessionID,'.psd.topo.beta_lr.png']);
    args = struct('data',data,'channels',{datastruct.channels},...
        'layoutfile',default.locsfile,'title','Beta band power',...
        'timepointlabels',{{'Left trials', 'Right trials'}},'markers','on','save','yes','savename',plotname);
    cschneid_topoplot_eeglab(args)
end

% --- global contrast
data = right.beta_timeavg_trialavg_epochs(:)-left.beta_timeavg_trialavg_epochs(:);
plotname = fullfile(pwd,default.plotpath,[datastruct.sessionID,'.psd.topo.beta_lrdiff.png']);
args = struct('data',data,'channels',{datastruct.channels},...
    'layoutfile',default.locsfile,'title','Beta band power difference',...
    'timepointlabels',{{'Right minus left trials'}},'markers','names','save','yes','savename',plotname);
cschneid_topoplot_eeglab(args)

% --- class contrast time steps
clear data ticks
timeline = linspace(0, datastruct.epochs.maxlength / datastruct.samplingRate, datastruct.epochs.maxlength)-default.PSD.baselinetime;
data(:,1) = mean(right.beta_trialavg_epochs(:,timeline<0),2) - mean(left.beta_trialavg_epochs(:,timeline<0),2);
ticks = {'baseline'};
for win = 1:length(default.topotimes.beta)-1
    t = timeline>default.topotimes.beta(win) & timeline<default.topotimes.beta(win+1);
    addit =  mean(right.beta_trialavg_epochs(:,t),2) - mean(left.beta_trialavg_epochs(:,t),2);
    data = cat(2,data,addit);
    ticks = cat(2,ticks,[num2str(default.topotimes.beta(win)),' - ',num2str(default.topotimes.beta(win+1))]);
end

plotname = fullfile(pwd,default.plotpath,[datastruct.sessionID,'.psd.topo.beta_timediff.png']);
args = struct('data',data,'channels',{datastruct.channels},...
    'layoutfile',default.locsfile,'title','Beta lateralization over time',...
    'timepointlabels',{ticks},'markers','on','save','yes','savename',plotname);
cschneid_topoplot_eeglab(args)

%% Save means in datastruct

% --- left
datastruct.band.left.alpha.average = left.alpha_timeavg_trialavg_epochs;
datastruct.band.left.beta.average = left.beta_timeavg_trialavg_epochs;
datastruct.band.left.theta.average = left.theta_timeavg_trialavg_epochs;
datastruct.band.left.alpha.power = left.alpha_trialavg_epochs;
datastruct.band.left.beta.power = left.beta_trialavg_epochs;
datastruct.band.left.theta.power = left.theta_trialavg_epochs;

% --- right
datastruct.band.right.alpha.average = right.alpha_timeavg_trialavg_epochs;
datastruct.band.right.beta.average = right.beta_timeavg_trialavg_epochs;
datastruct.band.right.theta.average = right.theta_timeavg_trialavg_epochs;
datastruct.band.right.alpha.power = right.alpha_trialavg_epochs;
datastruct.band.right.beta.power = right.beta_trialavg_epochs;
datastruct.band.right.theta.power = right.theta_trialavg_epochs;


if isfield(datastruct.epochs,'center')
    datastruct.band.center.alpha.average = center.alpha_timeavg_trialavg_epochs;
    datastruct.band.center.beta.average = center.beta_timeavg_trialavg_epochs;
    datastruct.band.center.theta.average = center.theta_timeavg_trialavg_epochs;
    datastruct.band.center.alpha.power = center.alpha_trialavg_epochs;
    datastruct.band.center.beta.power = center.beta_trialavg_epochs;
    datastruct.band.center.theta.power = center.theta_trialavg_epochs;
end
end

function [] = plotsamples(col,features1,features2,colorlegend,filename)
%% --- plot class distributions

% --- means over samples per trials
feats1 = permute(nanmean(features1,2),[3,1,2]);
del = isnan(feats1);
feats1(del(:,1),:) = [];

feats2 = permute(nanmean(features2,2),[3,1,2]);
del = isnan(feats2);
feats2(del(:,1),:) = [];

clsf_features = cat(3,features1,features2);
meanfeats = permute(nanmean(clsf_features,2),[3,1,2]);
del = isnan(meanfeats);
meanfeats(del(:,1),:) = [];

if size(meanfeats,2) > 1
    pca_coeff = pca(meanfeats,'Centered',false);
    pca_feats1 = feats1 * pca_coeff;
    feats1 = pca_feats1(:,1);
    pca_feats2 = feats2 * pca_coeff;
    feats2 = pca_feats2(:,1);
end

fig = figure('Position', get(0, 'Screensize'));
subplot(2,1,1); hold on;
H1 = histogram(feats1,'FaceColor',col{1},'Normalization','pdf');
H2 = histogram(feats2,'FaceColor',col{2},'Normalization','pdf');
BinEdges = union(round(H2.BinEdges,4),round(H1.BinEdges,4));
H1.BinEdges = BinEdges;
H2.BinEdges = BinEdges;
x = min(union(H2.BinEdges,H1.BinEdges)):0.01:max(union(H2.BinEdges,H1.BinEdges));
pd1 = makedist('Normal',nanmean(feats1),nanstd(feats1));
pd2 = makedist('Normal',nanmean(feats2),nanstd(feats2));
pdfline1 = pdf(pd1,x);
pdfline2 = pdf(pd2,x);
plot(x,pdfline1,'Color',col{1},'LineWidth',4)
plot(x,pdfline2,'Color',col{2},'LineWidth',4)
title('Histogram of the means of the first PC of each trial')
legend(colorlegend)

% --- concatenate samples and trials
allfeats = reshape(clsf_features,size(clsf_features,1),[])';
del = isnan(allfeats);
allfeats(del(:,1),:) = [];

feats1 = reshape(features1,size(features1,1),[])';
del = isnan(feats1);
feats1(del(:,1),:) = [];

feats2 = reshape(features2,size(features2,1),[])';
del = isnan(feats2);
feats2(del(:,1),:) = [];

if size(allfeats,2) > 1
    pca_coeff = pca(allfeats,'Centered',false);
    pca_feats1 = feats1 * pca_coeff;
    feats1 = pca_feats1(:,1);
    pca_feats2 = feats2 * pca_coeff;
    feats2 = pca_feats2(:,1);
end

subplot(2,1,2); hold on;
H1 = histogram(feats1,'FaceColor',col{1},'Normalization','pdf');
H2 = histogram(feats2,'FaceColor',col{2},'Normalization','pdf');
BinEdges = union(round(H2.BinEdges,4),round(H1.BinEdges,4));
H1.BinEdges = BinEdges;
H2.BinEdges = BinEdges;
x = min(union(H2.BinEdges,H1.BinEdges)):0.01:max(union(H2.BinEdges,H1.BinEdges));
pd1 = makedist('Normal',nanmean(feats1),nanstd(feats1));
pd2 = makedist('Normal',nanmean(feats2),nanstd(feats2));
pdfline1 = pdf(pd1,x);
pdfline2 = pdf(pd2,x);
plot(x,pdfline1,'Color',col{1},'LineWidth',4)
plot(x,pdfline2,'Color',col{2},'LineWidth',4)
title('Histogram of the samples of the first PC of each trial')
legend(colorlegend)

% --- save plot
F = getframe(fig);
imwrite(F.cdata, filename, 'png')
end
