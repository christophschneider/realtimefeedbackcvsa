% author: Christoph Schneider, CNBI, 2017

% Offline analysis of CVSA data for experiment CVSA 2.1 Adler
% Start this file to start processing - all the steps are implemented in a
% modular way and can therefore be reordered and/or commented out

% Can be called as function with the argument fileargs
% fileargs is a struct with the fields
% fileargs.pathname: path to folder containing the files
% fileargs.filename: cell array of file names

function Adler_tmp(fileargs)

clearvars -except fileargs;

% Load defaults
% -------------------------------------------------------------------------
cschneid_Adler_Default()

% Get file parameters
% -------------------------------------------------------------------------
if ~exist('fileargs','var')
    cd(default.datapath);
    [filename,pathname,~] = uigetfile( ...
        { '*.mat','Clean data files (*.mat)';}, ...
        'MultiSelect','on');
    if iscell(filename)==0  % if only one file
        filename = {filename};
    end
else
    pathname = fileargs.pathname;
    filename = fileargs.filenames;
end

% Extract Subject
% -------------------------------------------------------------------------
foldername = pathname(length(default.datapath)+2:end-1);
subject = foldername(1:strfind(foldername,'.')-1);

%% Process files
% -------------------------------------------------------------------------
for f = 1:length(filename)
    
    load(fullfile(pathname,filename{f}))
    
    % --- load montage file
    load(default.montage);
    
%     % --- treat eye tracker data
%     args = struct('struct',et,'default',default);
%     cschneid_ET_analysis(args);
    
    %--- load artifact- and bad channel markers
    args = struct('filename', fullfile(pathname,filename{f}(1:end-4)),'channelnames',{eeg.channelLegend});
    [bad_channels, bad_trials] = cschneid_readbadfile(args);
    
    %--- check for NaNs in the EEG data
    warning backtrace off
    for k = 1:size(eeg.data,1)
        if sum(isnan(eeg.data(k,:))) > 1
            eeg.data(k,isnan(eeg.data(k,:))) = max(max(eeg.data));
            msg = ['channel ',num2str(k),' contains NaNs!'];
            warning(msg)
        end
    end
    warning backtrace on
    
    % --- detect early reaction
    starttriggers = eeg.triggerLegend{3}(~cellfun(@isempty,regexp(eeg.triggerLegend{1},'trial_start')));
    cuetriggers = eeg.triggerLegend{3}(~cellfun(@isempty,regexp(eeg.triggerLegend{1},'cue')));
    targettriggers = eeg.triggerLegend{3}(~cellfun(@isempty,regexp(eeg.triggerLegend{1},'target')));
    
    wrong_reaction = [];
    catch_trials = [];
    x = 0;
    for i = 1:size(eeg.triggers,2)-1
        if ismember(eeg.triggers(1,i),cuetriggers)
            x = x + 1;
            if eeg.triggers(1,i+1) == eeg.triggerLegend{3}(~cellfun(@isempty,regexp(eeg.triggerLegend{1},'reaction_wrong')))
                wrong_reaction = [wrong_reaction,x];
            end
            if ismember(eeg.triggers(1,i+1),targettriggers) && (mod(eeg.triggers(1,i+1),10) ~= eeg.triggers(1,i))
                catch_trials = [catch_trials,x];
            end
        end
        if ismember(eeg.triggers(1,i),targettriggers)
            if eeg.triggers(1,i+1) ~= eeg.triggerLegend{3}(~cellfun(@isempty,regexp(eeg.triggerLegend{1},'reaction_correct')))
                wrong_reaction = [wrong_reaction,x];
            end
        end
    end
    bad_trials = union(bad_trials,unique(wrong_reaction)); % consider such trials as bad
    
    % --- Build artifact structure
    trialnr = eeg.triggers(1,:).*0;
    x = 0;
    for i = 1:size(eeg.triggers,2)-1
        if ismember(eeg.triggers(1,i),starttriggers)
            x = x + 1;
        end
        trialnr(i) = x;
        if ismember(trialnr(i),bad_trials)
            eeg.triggers(3,i) = 1;
        else
            eeg.triggers(3,i) = 0;
        end
        if ismember(trialnr(i),catch_trials)
            eeg.triggers(4,i) = 1;
        else
            eeg.triggers(4,i) = 0;
        end
    end
    
    % --- filter EEG data temporally
    args = struct('struct',eeg,'type','butterworth 4','causal','no','band',[1,40]);
    [eeg_tfiltered] = cschneid_tempfilter(args);
    
    % --- plot channel correlations (to find bad channels)
    tr = eeg.triggers; % pool all trials
    tr(1,tr(1,:)<10) = 61; tr(1,tr(1,:)<40 & tr(1,:)>30) = 62;
    
    args = struct('data',eeg_tfiltered.data,'time',eeg_tfiltered.time,'triggers',tr,...
        'epoch',{{[61,62],[-default.eeg.baselinetime,0]}},'samplingRate',...
        eeg_tfiltered.samplingRate,'excludeartifacts','yes');
    [~, span] = cschneid_epoching(args);
    
    corrchan = corr(span');
    figure;
    subplot(1,9, 1:8)
    clims = [0,1];
    imagesc(corrchan,clims);
    daspect([1,1,1])
    title(['Channel cross correlation - run ',num2str(f)])
    colorbar;
    
    subplot(1,9,9)
    corrchanmedian = nanmedian(corrchan)';
    imagesc(corrchanmedian,clims);
    yticks(1:length(corrchanmedian));
    yticklabels(eeg.channelLegend(1:length(corrchanmedian)));
    title('Median')
    
    % --- plot raw data
    x = eeg_tfiltered.data;
    x(isnan(x)) = 0;
    args = struct('data',x,'time',eeg.time,'detrend','yes','labels',{eeg.channelLegend});
    cschneid_plotwaterfall(args);
    
    % --- plot epochs (artifact check)
    %     args = struct('data',eeg_tfiltered.data,'time',eeg.time,'triggers',eeg.triggers,...
    %         'detrend','yes','starttriggers', [11,12], 'endtriggers', 21, ...
    %         'buffer', [-0.1,0.1],'badchannels',bad_channels,'badtrials',bad_trials,...
    %         'figurenumber',102,'samplingRate',eeg.samplingRate,'labels',{eeg.channelLegend});
    %     bad_trials = cschneid_plotepochs(args);
    
    % --- save bad trials in artifacts file
    if ~strcmp(default.interpolate,'yes')
        args = struct('mode','write','filename', [pathname,filename{f}(1:end-4)],...
            'badchannels',bad_channels,'badtrials',bad_trials,'channelnames',{eeg.channelLegend});
        cschneid_accessbadfile(args);
    end
    
    %--- interpolate bad channels by default
    args = struct('struct',eeg,'badchannels',bad_channels,...
        'maxdistance',default.interpolationdistance,'layoutfile',fullfile(pathname,eeg.layout3D));
    eeg = cschneid_interpolate(args);
    
    %% frequency-domain data
    
    % --- compute PSDs
    args = struct('struct',eeg,'default',default,'type','eeg','path',pathname,'filename',filename{f});
    [PSD] = cschneid_computePSD(args);
    PSD.triggers(3,:) = eeg.triggers(3,:);
    
    % --- plot mean PSD curves (to find bad channels)
    tr = PSD.triggers; % pool all trials
    tr(1,tr(1,:)<10) = 61; tr(1,tr(1,:)<40 & tr(1,:)>30) = 62;
    
    args = struct('data',PSD.data,'time',PSD.time,'triggers',tr,...
        'epoch',{{[61,62],[-default.PSD.baselinetime,0]}},'samplingRate',...
        PSD.samplingRate,'excludeartifacts','yes');
    [~, span] = cschneid_epoching(args);
    
    plotx = squeeze(mean(span,2));
    figure; semilogy(plotx');
    legend(PSD.channels);
    
    %         % --- plot PSD data
    %         for k = 1:size(PSD.data,1)
    %             args = struct('data',PSD.data,'struct',PSD,'type','channel','range',4:55,...
    %                 'channel',k,'normalize','no','figurenumber',10,'beauty','no');
    %             cschneid_plotPSD(args);
    %                     waitforbuttonpress
    %         end
    
    %     args = struct('data',PSD.data,'struct',PSD,'type','band','range',8:14,'normalize','yes');
    %     cschneid_plotPSD(args);
    
    %% merge runs
    
    if exist('eeg','var')
        if f == 1
            concat_eeg = eeg;
        else
            newstart = concat_eeg.samples(end);
            concat_eeg.samples = cat(2,concat_eeg.samples,eeg.samples + newstart);
            concat_eeg.time = cat(2,concat_eeg.time,eeg.time + concat_eeg.time(end));
            concat_eeg.data = cat(2,concat_eeg.data,eeg.data);
            t_shift = eeg.triggers;
            t_shift(2,:) = t_shift(2,:) + newstart;
            concat_eeg.triggers = cat(2,concat_eeg.triggers,t_shift);
        end
    end
    
    if exist('HT','var')
        if f == 1
            concat_HT = HT;
        else
            newstart = length(concat_HT.time);
            concat_HT.time = cat(2,concat_HT.time,HT.time + concat_HT.time(end));
            concat_HT.data = cat(2,concat_HT.data,HT.data);
            t_shift = HT.triggers;
            t_shift(2,:) = t_shift(2,:) + newstart;
            concat_HT.triggers = cat(2,concat_HT.triggers,t_shift);
        end
    end
    
    if exist('PSD','var')
        if f == 1
            concat_PSD = PSD;
        else
            newstart = length(concat_PSD.time);
            concat_PSD.time = cat(2,concat_PSD.time,PSD.time + concat_PSD.time(end));
            concat_PSD.data = cat(2,concat_PSD.data,PSD.data);
            t_shift = PSD.triggers;
            t_shift(2,:) = t_shift(2,:) + newstart;
            concat_PSD.triggers = cat(2,concat_PSD.triggers,t_shift);
        end
    end
    
end

%% EXTRACT ALPHA PEAK
electrodes = 35:60;

% find peak frequency
mx = log(squeeze(mean(concat_PSD.data(electrodes,:,2:end),2)));
[pks,locs] = findpeaks(nanmean(mx,1));
maxalphafreq = 0;
limit = 0;
for k = 1:length(locs)
    if locs(k) > 8 && locs(k) < 14
        if pks(k) > limit
            maxalphafreq = locs(k);
            limit = pks(k);
        end
    end
end

% find maximally varying alpha frequency
mxvar = nanvar(squeeze(mean(concat_PSD.data(electrodes,:,2:end),2)));
[pks,locs] = findpeaks(mxvar);
maxvaralphafreq = 0;
limit = 0;
for k = 1:length(locs)
    if locs(k) > 8 && locs(k) < 14
        if pks(k) > limit
            maxvaralphafreq = locs(k);
            limit = pks(k);
        end
    end
end

% find maximally different alpha for L/R
args = struct('data',concat_PSD.data,'time',concat_PSD.time,'triggers',concat_PSD.triggers,...
    'epoch',{{[1,31],[0,0]}},'samplingRate',concat_PSD.samplingRate,'excludeartifacts','yes');
[epochsL, concatL] = cschneid_epoching(args);

args = struct('data',concat_PSD.data,'time',concat_PSD.time,'triggers',concat_PSD.triggers,...
    'epoch',{{[2,32],[0,0]}},'samplingRate',concat_PSD.samplingRate,'excludeartifacts','yes');
[epochsR, concatR] = cschneid_epoching(args);

for ch = 1:size(concatL,1)
    for f = 1:size(concatL,3)
        [~, p] = ttest2(concatL(ch,:,f),concatR(ch,:,f));
        pval(ch,f) = p;
    end
end

px = -nanmean(pval(electrodes,2:end),1);
[pks,locs] = findpeaks(px);
maxtalphafreq = 0;
limit = 1;
for k = 1:length(locs)
    if locs(k) > 8 && locs(k) < 14
        if pks(k) < limit
            maxtalphafreq = locs(k);
            limit = pks(k);
        end
    end
end

disp([maxalphafreq,maxvaralphafreq,maxtalphafreq])

% find peak frequency
mx = log(squeeze(mean(cat(2,concatL(electrodes,:,2:end),concatR(electrodes,:,2:end)),2)));
[pks,locs] = findpeaks(nanmean(mx,1));
maxalphafreq = 0;
limit = 0;
for k = 1:length(locs)
    if locs(k) > 8 && locs(k) < 14
        if pks(k) > limit
            maxalphafreq = locs(k);
            limit = pks(k);
        end
    end
end
maxalphafreq

% find maximally varying alpha frequency
mxvar = nanvar(squeeze(mean(cat(2,concatL(electrodes,:,2:end),concatR(electrodes,:,2:end)),2)));
[pks,locs] = findpeaks(mxvar);
maxvaralphafreq = 0;
limit = 0;
for k = 1:length(locs)
    if locs(k) > 8 && locs(k) < 14
        if pks(k) > limit
            maxvaralphafreq = locs(k);
            limit = pks(k);
        end
    end
end
maxvaralphafreq

%% ROIs
roiL = {'P7','P5','PO7'};
roiR = {'P8','P6','PO8'};

% roiL = {'P7','P5','PO7','P3','PO3','O1'};
% roiR = {'P8','P6','PO8','P4','PO4','O2'};
chRoiL = find(ismember(concat_PSD.channels,roiL));
chRoiR = find(ismember(concat_PSD.channels,roiR));
%
% chRoiL = setdiff(chRoiL, unique(overallbadchannels));
% chRoiR = setdiff(chRoiR, unique(overallbadchannels));

% Alpha lateralization index (ALI) = (alpha_roiR - alpha_roiL) /
% avg(alpha_roiR + alpha_roiL)

ALI = [];
for f = default.alphaband
    AroiR(f,:) = squeeze(nanmean(concat_PSD.data(chRoiR,:,f+1),1));
    AroiL(f,:) = squeeze(nanmean(concat_PSD.data(chRoiL,:,f+1),1));
    AroiLR = (AroiR + AroiL)./2;
    ALI(f,:) = (AroiR(f,:) - AroiL(f,:)); % ./ AroiLR; CAVE: if division, baselining is more complicated
end
ALIx = mean(ALI(default.alphaband,:),1);
%
% % find maximally different alpha for L/R
% args = struct('data',ALI,'time',concat_PSD.time,'triggers',concat_PSD.triggers,...
%     'epoch',{{[1,31],[0,0]}},'samplingRate',concat_PSD.samplingRate,'excludeartifacts','yes');
% [epochsALIL, concatALIL] = cschneid_epoching(args);
%
% args = struct('data',ALI,'time',concat_PSD.time,'triggers',concat_PSD.triggers,...
%     'epoch',{{[2,32],[0,0]}},'samplingRate',concat_PSD.samplingRate,'excludeartifacts','yes');
% [epochsALIR, concatALIR] = cschneid_epoching(args);
%
% [nanmean(concatALIL,2),nanmean(concatALIR,2),nanmean(concatALIR,2) - nanmean(concatALIL,2)]
% boxplot([concatALIL(12,:)';concatALIR(12,:)'],[concatALIL(12,:)'.*0;concatALIR(12,:)'.*0+1],'Notch','on')

% % ---
% % find maximally different alpha for L/R
% args = struct('data',ALIx,'time',concat_PSD.time,'triggers',concat_PSD.triggers,...
%     'epoch',{{[1,31],[0,0]}},'samplingRate',concat_PSD.samplingRate,'excludeartifacts','yes');
% [epochsALIxL, concatALIxL] = cschneid_epoching(args);
%
% args = struct('data',ALIx,'time',concat_PSD.time,'triggers',concat_PSD.triggers,...
%     'epoch',{{[2,32],[0,0]}},'samplingRate',concat_PSD.samplingRate,'excludeartifacts','yes');
% [epochsALIxR, concatALIxR] = cschneid_epoching(args);
%
% [nanmean(concatALIxL,2),nanmean(concatALIxR,2),nanmean(concatALIxR,2) - nanmean(concatALIxL,2)]
% boxplot([concatALIxL';concatALIxR'],[concatALIxL'.*0;concatALIxR'.*0+1],'Notch','on')

% ---
bslspan = 1;
args = struct('data',ALIx,'time',concat_PSD.time,'triggers',concat_PSD.triggers,...
    'epoch',{{[1,31],[-bslspan,0]}},'samplingRate',concat_PSD.samplingRate,'excludeartifacts','yes');
[epochsALIxL, ~] = cschneid_epoching(args);

args = struct('data',ALIx,'time',concat_PSD.time,'triggers',concat_PSD.triggers,...
    'epoch',{{[2,32],[-bslspan,0]}},'samplingRate',concat_PSD.samplingRate,'excludeartifacts','yes');
[epochsALIxR, ~] = cschneid_epoching(args);

clear l concatALIxbslR concatALIxbslL epochsALIxbslR epochsALIxbslL
maxlength = round(concat_PSD.samplingRate * 2.1);
concatALIxbslR = [];
concatALIxbslR_short = [];
for k = 1:size(epochsALIxR,2)
    l(k) = round((size(epochsALIxR{k},2)-round((concat_PSD.samplingRate)*bslspan))/concat_PSD.samplingRate);
    bsl = mean(epochsALIxR{k}(1:round((concat_PSD.samplingRate)*bslspan)));
    if isnan(bsl)
        bsl = 0;
    end
    epochsALIxbslR{k} = epochsALIxR{k}-bsl;
    concatALIxbslR = cat(2,concatALIxbslR,epochsALIxbslR{k});
    if length(epochsALIxbslR{k})>=maxlength
        concatALIxbslR_short = cat(2,concatALIxbslR_short,epochsALIxbslR{k}(1:maxlength));
    else
        concatALIxbslR_short = cat(2,concatALIxbslR_short,epochsALIxbslR{k});
    end
end
[Cx,IAx,ICx] = unique(l);

concatALIxbslL = [];
concatALIxbslL_short = [];
clear l A B
for k = 1:size(epochsALIxL,2)
    l(k) = round((size(epochsALIxL{k},2)-round((concat_PSD.samplingRate)*bslspan))/concat_PSD.samplingRate);
    bsl = mean(epochsALIxL{k}(1:round((concat_PSD.samplingRate)*bslspan)));
    if isnan(bsl)
        bsl = 0;
    end
    epochsALIxbslL{k} = epochsALIxL{k}-bsl;
    concatALIxbslL = cat(2,concatALIxbslL,epochsALIxbslL{k});
    if length(epochsALIxbslL{k})>=maxlength
        concatALIxbslL_short = cat(2,concatALIxbslL_short,epochsALIxbslL{k}(1:maxlength));
    else
        concatALIxbslL_short = cat(2,concatALIxbslL_short,epochsALIxbslL{k});
    end
end
[Cy,IAy,ICy] = unique(l);

if Cx ~= Cy
    error('trials for left and right have different length!')
end
C = Cx

clear A B Ao Bo ALIxRmeans ALIxLmeans
m = (epochsALIxbslL(ICy == C(end)));
% s = size(m{1});
s = [1, min(cellfun(@(C) size(C,2), m))];
plotnobsl = 0; %round((concat_PSD.samplingRate)*bslspan)
for k = 1:length(C)
    A = epochsALIxbslL(ICy == k);
    B = epochsALIxbslR(ICx == k);
    Ao = [];
    Bo = [];
    mini = inf;
    for j = 1:size(A,2)
        if (size(A{j},2) < mini)
            mini = size(A{j},2);
        end
    end
    for j = 1:size(B,2)
        if (size(B{j},2) < mini)
            mini = size(B{j},2);
        end
    end
    for j = 1:size(A,2)
        Ao(j,:) = NaN(s);
        Ao(j,1:mini) = A{j}(1:mini);
    end
    for j = 1:size(B,2)
        Bo(j,:) = NaN(s);
        Bo(j,1:mini) = B{j}(1:mini);
    end
    ALIxLmeans(k,:) = nanmean(Ao,1);
    ALIxRmeans(k,:) = nanmean(Bo,1);
end

figure(333); clf;
for k = 1:size(ALIxRmeans,1)
    t = linspace(-bslspan,size(ALIxRmeans,1),length(ALIxRmeans(k,:)));
    plot(t,ALIxRmeans(k,plotnobsl+1:end),'b');
    hold on;
    plot(t,ALIxLmeans(k,plotnobsl+1:end),'r');
    grid on;
    %     waitforbuttonpress
end
legend({'right trials','left trials'})
plot(t,nanmean(ALIxRmeans,1),'b','LineWidth',3)
plot(t,nanmean(ALIxLmeans,1),'r','LineWidth',3)
figurename = ['ALI_bsl_',subject,'.png'];
set(gcf,'PaperUnits','centimeters','PaperPosition',[0 0 30 20])
print(gcf,'-dpng',figurename,'-r300');


[nanmean(concatALIxbslL,2),nanmean(concatALIxbslR,2),nanmean(concatALIxbslR,2) - nanmean(concatALIxbslL,2)]
figure; boxplot([concatALIxbslL';concatALIxbslR'],[concatALIxbslL'.*0;concatALIxbslR'.*0+1],'Notch','on')

alpha_smooth = 0.1;
smoothedALIx = smoothts(ALIx,'e',alpha_smooth);

smALIx(1) = ALIx(1);
for t = 2:length(ALIx)
    smALIx(t) =  alpha_smooth * ALIx(t) + (1 - alpha_smooth) * smALIx(t-1);
end

% smbsldat = smoothts([concatALIxbslL,concatALIxbslR],'e',alpha_smooth);
% limits = [prctile(smbsldat,5), prctile(smbsldat,95)];
% 
% map_mean = mean(limits);
% map_halfspan = (limits(2)-limits(1))/2;

smbsldat = smoothts([concatALIxbslL_short,concatALIxbslR_short],'e',alpha_smooth);
limits = [prctile(smbsldat,5), prctile(smbsldat,95)];

map_mean = mean(limits);
map_halfspan = (limits(2)-limits(1))/2;

smALIx_mapped = (smbsldat - map_mean)/map_halfspan;
smALIx_mapped(smALIx_mapped>1) = 1;
smALIx_mapped(smALIx_mapped<-1) = -1;

zci = @(v) find(v(:).*circshift(v(:), [-1 0]) <= 0);
zerocrossings = length(zci(smoothedALIx));
disp(['Zero crossing of feedback signal every ', ...
    num2str((size(smoothedALIx,2)/zerocrossings) / concat_PSD.samplingRate),...
    ' second']);

%% REACTION TIMES
tID = 0;
run = 0;
RT = [];
Subj = {};
for k = 1:size(concat_eeg.triggers,2)
    
    if concat_eeg.triggers(1,k) == concat_eeg.triggerLegend{3}...
            (ismember(concat_eeg.triggerLegend{1},'run_start'))
        run = run + 1;
    end
    if (concat_eeg.triggers(1,k) > 10 && concat_eeg.triggers(1,k) < 20) && concat_eeg.triggers(3,k) == 0
        tID = tID + 1;
        RT(tID,1) = run;
        RT(tID,2) = tID;
        RT(tID,3) = concat_eeg.triggers(1,k);
        Subj{tID,1} = concat_eeg.subject;
    end
    if concat_eeg.triggers(1,k) < 4 && concat_eeg.triggers(3,k) == 0
        RT(tID,4) = concat_eeg.triggers(1,k);
    end
    if (concat_eeg.triggers(1,k) > 30 && concat_eeg.triggers(1,k) < 40) && concat_eeg.triggers(3,k) == 0
        RT(tID,5) = concat_eeg.triggers(1,k);
        RT(tID,6) = NaN;
        RT(tID,7) = round((concat_eeg.triggers(2,k) - concat_eeg.triggers(2,k-1)) / concat_eeg.samplingRate);
    end
    if (concat_eeg.triggers(1,k) == concat_eeg.triggerLegend{3}...
            (ismember(concat_eeg.triggerLegend{1},'reaction_correct')))...
            && (concat_eeg.triggers(1,k-1) > 30 && concat_eeg.triggers(1,k-1) < 40)...
            && concat_eeg.triggers(3,k) == 0
        reactiontime = (concat_eeg.triggers(2,k) - concat_eeg.triggers(2,k-1)) / concat_eeg.samplingRate;
        RT(tID,6) = reactiontime;
    end
    
end

Creactiontimes = RT(RT(:,4)==3,:);
LRreactiontimes = RT(RT(:,4)~=3,:); % reaction time mean over left/right trials
xy = LRreactiontimes(:,6:7);
figure; boxplot(xy(:,1),xy(:,2))
zz = Creactiontimes(:,6:7);
figure; boxplot(zz(:,1),zz(:,2))
% for k = 1:length(unique(zz(:,2)))
%     cmean(k) = median(zz(zz(:,2)==k),1);
% end
% for k = 1:size(LRreactiontimes(:,6:7),1)
%     subst = cmean(xy(k,2));
%     xyz(k,:) = [xy(k,1)-subst,xy(k,2)];
% end
% figure; boxplot(xyz(:,1),xyz(:,2))
%
% savestruct = struct('RT',RT,'Subj',Subj);
% savename = [concat_eeg.subject,'.RTstats.mat'];
% save(savename,'savestruct');

% ANOVA:
anov_rt = RT(:,6); % reaction time, dependent variable
anov_f1 = RT(:,4); % side | left/right/center
anov_f2 = RT(:,3) < 12.5; % color | yes/no
anov_f3 = (RT(:,3) == 11) | (RT(:,3) == 13) ; % background | bgw/wgb
anov_f4 = Subj; % subject ID
varnames = {'Side';'Color';'Background';'Subject'};
anovan(anov_rt,{anov_f1, anov_f2, anov_f3, anov_f4},3,3,varnames);


%%
% % --- plot time average (ERPs)
% for c = 1:size(concat_eeg.data,1)
%     args = struct('data',concat_eeg.data,'channelLabels',{concat_eeg.channelLegend},'triggers',concat_eeg.triggers,...
%         'zero',[1,2,3],'limits',[-1, 1],'type','mean+std','samplingrate',concat_eeg.samplingRate,...
%         'channels',c,'figurenumber',101, 'baseline',[0,0],'excludeartifacts','yes');
%     cschneid_average(args);
% %     waitforbuttonpress
% end
%
% % --- plot frequency average
% for c = 1:size(concat_eeg.data,1)
%     args = struct('data',squeeze(abs(mean(concat_PSD.data(:,:,8:12),3))),...
%         'channelLabels',{concat_PSD.channels},'triggers',concat_PSD.triggers,'zero',[1],...
%         'limits',[-2, 5],'type','mean+std','samplingrate',concat_PSD.samplingRate,'channels',c,...
%         'figurenumber',101, 'baseline',[-1,0],'excludeartifacts','yes');
%     cschneid_average(args);
% %     waitforbuttonpress
% end

% % --- plot Hilbert transformed average
% for i = skipChanIdx
%     args = struct('data',concat_HT.data,...
%         'channelLabels',{concat_HT.channels},'triggers',concat_HT.triggers,'zero',11,...
%         'limits',[-2, 10],'type','mean+std','samplingrate',concat_HT.samplingRate,'channels',i,...
%         'figurenumber',101,'excludeartifacts','yes'); %'baseline',[-2,0] [-2, 6]
%     cschneid_average(args);
%     waitforbuttonpress
% end

%% Extract and baseline epochs

% --- epoch data
bslspan = 1;  % seconds of baseline (time before cue)
args = struct('data',concat_PSD.data,'time',concat_PSD.time,'triggers',concat_PSD.triggers,...
    'epoch',{{[2,32],[-bslspan,0]}},'samplingRate',concat_PSD.samplingRate,'excludeartifacts','yes');
[epochsPSDR, ~] = cschneid_epoching(args);
args = struct('data',concat_PSD.data,'time',concat_PSD.time,'triggers',concat_PSD.triggers,...
    'epoch',{{[1,31],[-bslspan,0]}},'samplingRate',concat_PSD.samplingRate,'excludeartifacts','yes');
[epochsPSDL, ~] = cschneid_epoching(args);

sR = max(cellfun(@(C) size(C,2), epochsPSDR));
sL = max(cellfun(@(C) size(C,2), epochsPSDL));
maxlength = max(sR,sL);

% --- baseline and average
for k = 1:size(epochsPSDR,2)
    bsl = nanmean(epochsPSDR{k}(:,1:round((concat_PSD.samplingRate)*bslspan),:),2);
    bsl(isnan(bsl)) = 0; % should only affect deleted channels
    l = size(epochsPSDR{k},2);
    epochsPSDR_bsl{k} = epochsPSDR{k} - repmat(bsl,[1,l,1]);
    %     epochsPSDR_bslavg(k,:,:) = nanmean(epochsPSDR_bsl{k},2);
    mat = NaN(size(epochsPSDR{k},1),maxlength,size(epochsPSDR{k},3));
    mat(:,1:size(epochsPSDR{k},2),:) = epochsPSDR_bsl{k};
    epochsPSDR_padded(k,:,:,:) = mat;
end
for k = 1:size(epochsPSDL,2)
    bsl = nanmean(epochsPSDL{k}(:,1:round((concat_PSD.samplingRate)*bslspan),:),2);
    bsl(isnan(bsl)) = 0; % should only affect deleted channels
    l = size(epochsPSDL{k},2);
    epochsPSDL_bsl{k} = epochsPSDL{k} - repmat(bsl,[1,l,1]);
    %     epochsPSDL_bslavg(k,:,:) = nanmean(epochsPSDL_bsl{k},2);
    mat = NaN(size(epochsPSDL{k},1),maxlength,size(epochsPSDL{k},3));
    mat(:,1:size(epochsPSDL{k},2),:) = epochsPSDL_bsl{k};
    epochsPSDL_padded(k,:,:,:) = mat;
end


% --- mean over trials
PSDR_collapsed_trials = squeeze(nanmean(epochsPSDR_padded,1));
PSDL_collapsed_trials = squeeze(nanmean(epochsPSDL_padded,1));

% --- mean over time (exclude baseline)
PSDR_collapsed_time = squeeze(nanmean(epochsPSDR_padded(:,:,round((concat_PSD.samplingRate)*bslspan):end,:),3));
PSDL_collapsed_time = squeeze(nanmean(epochsPSDL_padded(:,:,round((concat_PSD.samplingRate)*bslspan):end,:),3));

% --- mean over alpha band
PSDR_collapsed_band = squeeze(nanmean(epochsPSDR_padded(:,:,:,default.alphaband+1),4));
PSDL_collapsed_band = squeeze(nanmean(epochsPSDL_padded(:,:,:,default.alphaband+1),4));

% --- mean over alpha band + trials
PSDR_collapsed_bandtrials = squeeze(nanmean(PSDR_collapsed_band,1));
PSDL_collapsed_bandtrials = squeeze(nanmean(PSDL_collapsed_band,1));

% --- mean over time + alpha band
PSDR_collapsed_timeband = squeeze(nanmean(PSDR_collapsed_time(:,:,default.alphaband+1),3));
PSDL_collapsed_timeband = squeeze(nanmean(PSDL_collapsed_time(:,:,default.alphaband+1),3));

% --- mean over time + alpha band + trials
PSDR_collapsed_timebandtrials = squeeze(nanmean(PSDR_collapsed_timeband,1));
PSDL_collapsed_timebandtrials = squeeze(nanmean(PSDL_collapsed_timeband,1));

% % --- mean over alpha band + time + trials
% PSDR_collapsed_bandtimetrials = squeeze(nanmean(PSDR_collapsed_bandtime,1));
% PSDL_collapsed_bandtimetrials = squeeze(nanmean(PSDL_collapsed_bandtime,1));

% --- mean over individual seconds
sec2sampl = round(concat_PSD.samplingRate);
c = 0;
while ((c+1) * sec2sampl) < size(PSDR_collapsed_bandtrials,2)
    offset = c * sec2sampl;
    vec = 1:sec2sampl;
    period = offset + vec;
    c = c + 1;
    PSDR_collapsed_bandtrialssecond(:,c) = squeeze(nanmean(PSDR_collapsed_bandtrials(:,period,:),2));
    PSDL_collapsed_bandtrialssecond(:,c) = squeeze(nanmean(PSDL_collapsed_bandtrials(:,period,:),2));
end

% --- create left and right trial means
diff_alphaband_full = (PSDR_collapsed_timebandtrials - PSDL_collapsed_timebandtrials)';
diff_alphaband_sec = PSDR_collapsed_bandtrialssecond - PSDL_collapsed_bandtrialssecond;

%% TOPOPLOTS

if strcmp(default.interpolate,'no')
    
    args = struct('data',diff_alphaband_full,'channels',{concat_PSD.channels},...
        'layoutfile',fullfile(pathname,'1005_2D.lay'),'title','Alpha band difference right-left',...
        'timepointlabels',{{'0 - 5 seconds'}},'markers','names');
    cschneid_topoplot(args)
    
    args = struct('data',diff_alphaband_sec,'channels',{concat_PSD.channels},...
        'layoutfile',fullfile(pathname,'1005_2D.lay'),'title','Alpha band difference right-left',...
        'timepointlabels',{{'-1 - 0','0 - 1','1 - 2','2 - 3','3 - 4','4 - 5'}});
    cschneid_topoplot(args)
    
end

%% Automatic feature selection
% featsR = PSDR_collapsed_time(:,:,1:20);
% featsL = PSDL_collapsed_time(:,:,1:20);
%
% % featsIDX = reshape(1:(size(featsR,1)*size(featsR,2)*size(featsR,3)), [size(featsR,1), size(featsR,2), size(featsR,3)]);
%
% featsR_concat = reshape(featsR,size(featsR,1),[]);
% featsL_concat = reshape(featsL,size(featsL,1),[]);
% featsIDX_concat = repmat(1:size(featsR_concat,2),size(featsR_concat,1),1);
%
% X = [featsR_concat;featsL_concat]';
% group = [featsR_concat(:,1)*0;featsL_concat(:,1)*0+1]';
% [IDX, Z] = rankfeatures(X,group,'Criterion','bhattacharyya');
%
% featscrit_concat = repmat(Z,size(featsR_concat,1),1);
%
% featsR2 = reshape(featsR_concat,[size(featsR,1), size(featsR,2), size(featsR,3)]);
% featsIDX = reshape(featsIDX_concat,[size(featsR,1), size(featsR,2), size(featsR,3)]);
% featscrit = reshape(featscrit_concat,[size(featsR,1), size(featsR,2), size(featsR,3)]);
%
% F = squeeze(featscrit(1,:,:));
% figure; surf(F); axis([1 20 0 size(featsR,2) 0 max(Z)])

%% Classification
% 
% if strcmp(default.interpolate,'yes')
%     
%     % --- epoch PSDs for each class
%     args = struct('data',concat_PSD.data,'time',concat_PSD.time,'triggers',concat_PSD.triggers,...
%         'epoch',{{[1,31],[-default.PSD.baselinetime,0]}},'samplingRate',concat_PSD.samplingRate,'excludeartifacts','yes');
%     [epochsL, ~] = cschneid_epoching(args);
%     
%     args = struct('data',concat_PSD.data,'time',concat_PSD.time,'triggers',concat_PSD.triggers,...
%         'epoch',{{[2,32],[-default.PSD.baselinetime,0]}},'samplingRate',concat_PSD.samplingRate,'excludeartifacts','yes');
%     [epochsR, ~] = cschneid_epoching(args);
%     
%     args = struct('data',concat_PSD.data,'time',concat_PSD.time,'triggers',concat_PSD.triggers,...
%         'epoch',{{[3,33],[-default.PSD.baselinetime,0]}},'samplingRate',concat_PSD.samplingRate,'excludeartifacts','yes');
%     [epochsC, ~] = cschneid_epoching(args);
%     
%     % --- baseline (or not) features
%     bsl_length = round(default.PSD.baselinetime * concat_PSD.samplingRate);
%     
%     if strcmp(default.baselinefeatures,'yes')
%         for ep = 1:length(epochsL)
%             bsl = nanmean(epochsL{ep}(:,1:bsl_length,:),2);
%             bslMat = repmat(bsl,[1,size(epochsL{ep},2)-bsl_length,1]);
%             true_epoch = epochsL{ep}(:,bsl_length+1:end,:);
%             epochsL{ep} = true_epoch - bslMat;
%         end
%         for ep = 1:length(epochsR)
%             bsl = nanmean(epochsR{ep}(:,1:bsl_length,:),2);
%             bslMat = repmat(bsl,[1,size(epochsR{ep},2)-bsl_length,1]);
%             true_epoch = epochsR{ep}(:,bsl_length+1:end,:);
%             epochsR{ep} = true_epoch - bslMat;
%         end
%         for ep = 1:length(epochsC)
%             bsl = nanmean(epochsC{ep}(:,1:bsl_length,:),2);
%             bslMat = repmat(bsl,[1,size(epochsC{ep},2)-bsl_length,1]);
%             true_epoch = epochsC{ep}(:,bsl_length+1:end,:);
%             epochsC{ep} = true_epoch - bslMat;
%         end
%     else
%         for ep = 1:length(epochsL)
%             epochsL{ep} = epochsL{ep}(:,bsl_length+1:end,:);
%         end
%         for ep = 1:length(epochsR)
%             epochsR{ep} = epochsR{ep}(:,bsl_length+1:end,:);
%         end
%         for ep = 1:length(epochsC)
%             epochsC{ep} = epochsC{ep}(:,bsl_length+1:end,:);
%         end
%     end
%     
%     % --- extract features (channel x frequencies)
%     clsf_featuresL = {}; clsf_featuresR = {}; clsf_featuresC = {};
%     for ep = 1:length(epochsL)
%         clear a b
%         a = epochsL{ep}(default.electrodes,:,default.alphaband+1);
%         for sample = 1:size(a,2)
%             featuremat = squeeze(a(:,sample,:));
%             featurevec = reshape(featuremat',1,[]);
%             b(sample,:) = featurevec;
%         end
%         clsf_featuresL{ep} = b;
%     end
%     for ep = 1:length(epochsR)
%         clear a b
%         a = epochsR{ep}(default.electrodes,:,default.alphaband+1);
%         for sample = 1:size(a,2)
%             featuremat = squeeze(a(:,sample,:));
%             featurevec = reshape(featuremat',1,[]);
%             b(sample,:) = featurevec;
%         end
%         clsf_featuresR{ep} = b;
%     end
%     for ep = 1:length(epochsC)
%         clear a b
%         a = epochsC{ep}(default.electrodes,:,default.alphaband+1);
%         for sample = 1:size(a,2)
%             featuremat = squeeze(a(:,sample,:));
%             featurevec = reshape(featuremat',1,[]);
%             b(sample,:) = featurevec;
%         end
%         clsf_featuresC{ep} = b;
%     end
%     
%     % --- combine in one feature set 
%     clsf_features = [clsf_featuresL,clsf_featuresR,clsf_featuresC];
%     clsf_labels = [ones(1,length(clsf_featuresL)).*1,...
%         ones(1,length(clsf_featuresR)).*2,ones(1,length(clsf_featuresC)).*3];
%     
%     % --- start 10-fold X-val right/left
%     clsf_features = clsf_features(ismember(clsf_labels,[1,2]));
%     clsf_labels = clsf_labels(ismember(clsf_labels,[1,2]));
%     
%     cvp = cvpartition(clsf_labels,'Kfold',10);
%     numfeatures = 10;
%     features_restored.scores = cell(size(clsf_features));
%     features_restored.binary = cell(size(clsf_features));
%     
%     for k = 1:cvp.NumTestSets
%         
%         features_train = []; labels_train = [];
%         features_cell_train = clsf_features(training(cvp,k));
%         labels_cell_train = clsf_labels(training(cvp,k));
%         for ep = 1:length(features_cell_train)
%             features_train = cat(1,features_train,features_cell_train{ep});
%             labels_train = cat(1,labels_train,ones(size(features_cell_train{ep},1),1).*labels_cell_train(ep));
%         end
%         
%         features_test = []; labels_test = [];
%         features_cell_test = clsf_features(test(cvp,k));
%         labels_cell_test = clsf_labels(test(cvp,k));
%         for ep = 1:length(features_cell_test)
%             features_test = cat(1,features_test,features_cell_test{ep});
%             labels_test = cat(1,labels_test,ones(size(features_cell_test{ep},1),1).*labels_cell_test(ep));
%         end
%         
%         % --- feature selection
%         [idx, ~] = rankfeatures(features_train', labels_train, 'Criterion', 'wilcoxon');
%         useidx = idx(1:numfeatures);
%         
%         % --- classify
%         Mdl = fitcdiscr(features_train(:,useidx),labels_train,'Gamma',0.5);
%         [pred_label, scores] = predict(Mdl,features_test(:,useidx));
%         clsf_error.sample(k) = mean(pred_label ~= labels_test);
%         [~,~,~,clsf_error.AUC(k)] = perfcurve(labels_test,scores(:,1),1);
%         
%         % --- restore
%         delscores = scores;
%         delpred = pred_label;
%         feature_scores = []; feature_pred = [];
%         for ep = 1:length(features_cell_test)
%             feature_scores{ep} = delscores(1:size(features_cell_test{ep},1),1);
%             feature_pred{ep} = delpred(1:size(features_cell_test{ep},1),1);
%             delscores(1:size(features_cell_test{ep},1),:) = [];
%             delpred(1:size(features_cell_test{ep},1),:) = [];
%         end
%         if ~isempty(delscores) || ~isempty(delpred)
%             error('Something went wrong when restoring the feature scores')
%         end
%         features_restored.scores(test(cvp,k)) = feature_scores;
%         features_restored.binary(test(cvp,k)) = feature_pred;
%         
%     end
%     disp(['Mean accuracy [single sample] = ',num2str(1-mean(clsf_error.sample))]);
%     disp(['Mean AUC [single sample] = ',num2str(mean(clsf_error.AUC))]);
%     
%     % --- add integration framework
%     % scores > 0.5 are class 1, < 0.5 class 2
%     for ep = 1:length(clsf_features)
%     % a) mean value of scores/binary
%     trial.meanscores(ep) = mean(features_restored.scores{ep});   
%     trial.meanbinary(ep) = mean(features_restored.binary{ep});   
%     % b) abs max value of scores
%     mx = max(features_restored.scores{ep}); mn = min(features_restored.scores{ep});
%     if abs(0.5 - mx) >= abs(0.5 - mn)
%        m = mx;
%     else
%         m = mn;
%     end
%     trial.minmax(ep) = m;
%     % c) exponential smoothing for scores/binary
%     clsf_smoothed = smoothts(features_restored.scores{ep},'e',alpha_smooth);
%     mx = max(clsf_smoothed); mn = min(clsf_smoothed);
%     if abs(0.5 - mx) >= abs(0.5 - mn)
%         m_smoothed = mx;
%     else
%         m_smoothed = mn;
%     end
%     trial.smoothscoreend(ep) = clsf_smoothed(end);
%     trial.smoothscoremax(ep) = m_smoothed(end);
%     end
%     trial.truelabels = clsf_labels;
%     trial.matchlabels = trial.truelabels;
%     trial.matchlabels(clsf_labels==2) = 0;
%     
%     % --- classify trials
%     clsf_acc.meanscores = sum(round(trial.meanscores)==trial.matchlabels)/length(trial.matchlabels);
%     clsf_acc.meanbinary = sum(round(trial.meanbinary)==trial.truelabels)/length(trial.matchlabels);
%     clsf_acc.smoothscoreend = sum(round(trial.smoothscoreend)==trial.matchlabels)/length(trial.matchlabels);
%     clsf_acc.smoothscoremax = sum(round(trial.smoothscoremax)==trial.matchlabels)/length(trial.matchlabels);
%     clsf_acc.minmax = sum(round(trial.minmax)==trial.matchlabels)/length(trial.matchlabels);
%     clsf_acc.singlesample = 1-mean(clsf_error.sample);
%     clsf_acc.AUC = mean(clsf_error.AUC);
%     
%     
%     % 9898989898989898998999898^^^^^^^^
%     freqz = default.alphaband;
%     classifiertype = 'linear';
%     
%     featuresR.all = [];
%     featuresL.all = [];
%     
%     for k = 1:size(epochsALIxbslR,2)
%         x = epochsALIxbslR{k}(round(concat_PSD.samplingRate*bslspan):end);
%         featuresR.mean(k) = mean(x);
%         featuresR.all = cat(2,featuresR.all,x);
%         featuresR.labels(k) = 1;
%     end
%     for k = 1:size(epochsALIxbslL,2)
%         x = epochsALIxbslL{k}(round(concat_PSD.samplingRate*bslspan):end);
%         featuresL.mean(k) = mean(x);
%         featuresL.all = cat(2,featuresL.all,x);
%         featuresL.labels(k) = 2;
%     end
%     
%     figure;
%     HR = histogram(featuresR.mean,'Normalization','pdf');
%     hold on;
%     HL = histogram(featuresL.mean,'Normalization','pdf');
%     x = min(union(HR.BinEdges,HL.BinEdges)):0.01:max(union(HR.BinEdges,HL.BinEdges));
%     L = [mean(featuresL.mean),std(featuresL.mean)];
%     pdR = makedist('Normal',mean(featuresR.mean),std(featuresR.mean));
%     pdflineR = pdf(pdR,x);
%     plot(x,pdflineR,'b','LineWidth',4)
%     pdL = makedist('Normal',mean(featuresL.mean),std(featuresL.mean));
%     pdflineL = pdf(pdL,x);
%     plot(x,pdflineL,'r','LineWidth',4)
%     title('Histogram of the means of the LI of each trial')
%     legend({'right','left'})
%     
%     figure;
%     HR = histogram(featuresR.all,'Normalization','pdf');
%     hold on;
%     HL = histogram(featuresL.all,'Normalization','pdf');
%     x = min(union(HR.BinEdges,HL.BinEdges)):0.01:max(union(HR.BinEdges,HL.BinEdges));
%     L = [mean(featuresL.mean),std(featuresL.mean)];
%     pdR = makedist('Normal',mean(featuresR.all),std(featuresR.all));
%     pdflineR = pdf(pdR,x);
%     plot(x,pdflineR,'b','LineWidth',4)
%     pdL = makedist('Normal',mean(featuresL.all),std(featuresL.all));
%     pdflineL = pdf(pdL,x);
%     plot(x,pdflineL,'r','LineWidth',4)
%     title('Histogram of the samples of the LI of each trial')
%     legend({'right','left'})
%     
%     % -------------------------------------------- mean over trials
%     features = cat(2,featuresR.mean,featuresL.mean);
%     labels = cat(2,featuresR.labels,featuresL.labels);
%     cvp = cvpartition(labels,'Kfold',10);
%     
%     for k = 1:cvp.NumTestSets
%         
%         Mdl = fitcdiscr(features(training(cvp,k))',labels(training(cvp,k)));
%         pred_label = predict(Mdl,features(test(cvp,k))');
%         clsf_error.mean(k) = mean(pred_label ~= labels(test(cvp,k))');
%         
%     end
%     
%     disp(['Mean accuracy [mean over trials] = ',num2str(1-mean(clsf_error.mean))]);
%     
%     % -------------------------------------------- single samples
%     features = cat(2,featuresR.all,featuresL.all);
%     labels = cat(2,featuresR.all*0+1,featuresL.all*0+2);
%     cvp = cvpartition(labels,'Kfold',10);
%     
%     for k = 1:cvp.NumTestSets
%         
%         Mdl = fitcdiscr(features(training(cvp,k))',labels(training(cvp,k)));
%         pred_label = predict(Mdl,features(test(cvp,k))');
%         clsf_error.sample(k) = mean(pred_label ~= labels(test(cvp,k))');
%         
%     end
%     
%     disp(['Mean accuracy [single sample] = ',num2str(1-mean(clsf_error.sample))]);
%     
%     clsf_acc.singlesampleLI = 1-mean(clsf_error.sample);
%     
%     % % --------------------------------------------
%     %
%     % epochs = cat(2,epochsL,epochsR);
%     % % epochs = cat(2,epochsALIL,epochsALIR);
%     % labels = cat(2,zeros(1,size(epochsL,2)),ones(1,size(epochsR,2)));
%     % cvp = cvpartition(labels,'Kfold',10);
%     %
%     % for k = 1:cvp.NumTestSets
%     %     % --- training
%     %     tr_epochs = epochs(training(cvp,k));
%     %     tr_labels = labels(training(cvp,k));
%     %     tr_samples = [];
%     %     tr_samplelabels = [];
%     %     for c = 1:size(tr_epochs,2)
%     %         tr_samples = cat(2,tr_samples,tr_epochs{c});
%     %         tr_samplelabels = cat(2,tr_samplelabels,...
%     %             ones(1,size(tr_epochs{c},2)).*tr_labels(c));
%     %     end
%     %     tr_samples = tr_samples(:,:,ismember(concat_PSD.frequency,freqz));
%     %     tr_features = permute(tr_samples,[1,3,2]);
%     %     tr_features = reshape(tr_features,[],size(tr_features,3));
%     %     % format: ch1-f1, ch2-f1, ch3-f1 ...
%     %     fun = @(xT,yT,xt,yt) length(yt)*(cschneid_classerror(yt,...
%     %         predict(fitcdiscr(xT,yT,'discrimtype',classifiertype,'Prior','uniform'),xt)));
%     %     opt = statset('Display','iter','MaxIter',10,'TolFun',1e-03,'UseParallel',true);
%     %     [sel,hst] = sequentialfs(fun,tr_features',tr_samplelabels','cv',10,'options',opt);
%     %     Mdl = fitcdiscr(tr_features(sel,:)',tr_samplelabels,'Prior','uniform',...
%     %         'DiscrimType','linear','Gamma',0);
%     %
%     %     % --- testing
%     %     te_epochs = epochs(test(cvp,k));
%     %     te_labels = labels(test(cvp,k));
%     %     te_samples = [];
%     %     te_samplelabels = [];
%     %     for c = 1:size(te_epochs,2)
%     %         te_samples = cat(2,te_samples,te_epochs{c});
%     %         te_samplelabels = cat(2,te_samplelabels,...
%     %             ones(1,size(te_epochs{c},2)).*te_labels(c));
%     %     end
%     %     te_samples = te_samples(:,:,ismember(concat_PSD.frequency,freqz));
%     %     te_features = permute(te_samples,[1,3,2]);
%     %     te_features = reshape(te_features,[],size(te_features,3));
%     %
%     %     te_labels_pred = predict(Mdl, te_features(sel,:)')';
%     %     acc(k) = 1 - cschneid_classerror(te_samplelabels,te_labels_pred)
%     % end
%     
% end

%% Save to mat file for group analysis

% filename = fullfile(default.grouppath,[eeg.sessionID,'.',default.GAsuffix,'.mat']);
% if exist(filename,'file')
%     load(filename)
% else
%     savestruct = [];
% end
% 
% if strcmp(default.interpolate,'yes')
%     savestruct.clsf_acc = clsf_acc;
% else
%     savestruct.subjectID = concat_eeg.subject;
%     savestruct.RT = RT;
    
%     savestruct.subjectID = concat_eeg.subject;
%     savestruct.sessionID = concat_eeg.sessionID;
%     savestruct.layoutfile = fullfile(pathname,eeg.layout2D);
%     savestruct.electrodes = concat_PSD.channels;
%     
%     savestruct.LI.time = LItime;
%     savestruct.LI.meanR = ALIxRmeans;
%     savestruct.LI.meanL = ALIxLmeans;
%     
%     savestruct.topo.span = diff_alphaband_concat;
%     savestruct.topo.span_bsl = diff_alphaband_full;
%     savestruct.topo.windows = diff_alphaband_win;
%     savestruct.topo.t_windows = windowlabels;
%     
%     savestruct.RT = RT;
%     savestruct.group = concat_eeg.group;
%     savestruct.RTGB_slope = brob(2);
%     savestruct.RTLI_slope = brobLI(2);
% end

% save(filename, 'savestruct');

% %% Save to mat file for ndf online
% 
% --- for online
LI = ALIx;
LI_time = concat_PSD.time;
meanRT = mean(LRreactiontimes(:,6));
map_mean = mean(limits);
map_halfspan = (limits(2)-limits(1))/2;
from_offline = struct('LI',LI,'LI_time',LI_time,'meanreactiontime',...
    meanRT,'map_mean',map_mean,'map_halfspan',map_halfspan);

% % --- lateralization index
% from_offline.ALI.left = nanmean(ALIxLmeans,1);
% from_offline.ALI.right = nanmean(ALIxRmeans,1);
% from_offline.ALI.time = linspace(-bslspan,size(ALIxRmeans,1),length(ALIxRmeans(C(end),:)));
% 
% 
% % --- for topoplots
% if strcmp(default.interpolate,'no')
%     from_offline.TOPO.full = diff_alphaband_full;
%     from_offline.TOPO.sec = diff_alphaband_sec;
%     from_offline.TOPO.chan = concat_PSD.channels;
% end
% 
% % --- classification
% if strcmp(default.interpolate,'yes')
%     from_offline.featuresR = featuresR;
%     from_offline.featuresL = featuresL;
%     from_offline.clsf_error.mean = clsf_error.mean;
%     from_offline.clsf_error.sample = clsf_error.sample;
% end
% 
% 
savename = ['2s.',eeg.subject,'.2online.mat'];
save(savename,'from_offline');
disp('file for online experiment saved');

end
