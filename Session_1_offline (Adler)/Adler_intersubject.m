% Data analysis CVSA 2.1 Adler

clear;
close all;
clc;

% [filename,pathname,~] = uigetfile( ...
%     { '*.mat','Clean data files (*.mat)';}, ...
%     'MultiSelect','on');
% if iscell(filename)==0          % if only one file
%     filename = {filename};
% end
cd('\\sti1files.epfl.ch\cnbi\cschneid\_Experiment_CVSA2\CVSA2.1(Adler)\data_CLEAN');

filename = {'ae1.2online.mat','ae2.2online.mat','ae3.2online.mat','ae4.2online.mat','ae5.2online.mat',...
    'ae6.2online.mat','ae7.2online.mat','ae8.2online.mat','ae9.2online.mat','ae10.2online.mat','af1.2online.mat',...
    'af2.2online.mat','af3.2online.mat','af4.2online.mat'};

for f = 1:size(filename,2)
    
    load(filename{f})
   
    MLI.left(f,:) = from_offline.ALI.left;
    MLI.right(f,:) = from_offline.ALI.right;
    MLI.time = from_offline.ALI.time;
    
    topo.full(f,:) = from_offline.TOPO.full;
    topo.sec(f,:,:) = from_offline.TOPO.sec;
    topo.chan = from_offline.TOPO.chan;
    
    clsf_error(f,:) = from_offline.clsf_error.mean;
    
end


% --- Lateralization index plot
mlir = mean(MLI.right,1);
slir = std(MLI.right,1);
mlil = mean(MLI.left,1);
slil = std(MLI.left,1);

boundedline(MLI.time, mlir, slir, '-b', MLI.time, mlil, slil, '-r', 'alpha');
cschneid_vline(0, 'k', '-', 'cue')
grid on;
title('Lateralization: Grand averages and standard error of the means');
legend({'right trials', 'left trials'})

% --- Topoplots
topoFull = mean(topo.full,1)';
topoSec = squeeze(mean(topo.sec,1));

args = struct('data',topoFull,'channels',{topo.chan},...
    'layoutfile','1005_2D.lay','title','Grand average: alpha band difference right-left',...
    'timepointlabels',{{'0 - 5 seconds'}});
cschneid_topoplot(args)

args = struct('data',topoSec,'channels',{topo.chan},...
    'layoutfile','1005_2D.lay','title','Grand average: alpha band difference right-left',...
    'timepointlabels',{{'-1 - 0','0 - 1','1 - 2','2 - 3','3 - 4','4 - 5'}});
cschneid_topoplot(args)

% --- classification results
figure;
line
boxplot(1-clsf_error','PlotStyle','compact','LabelOrientation','horizontal')
ylim([0 1])
hline(0.5)
title('Classification accuracy: 10-fold x-val, feature = mean LI over trials')